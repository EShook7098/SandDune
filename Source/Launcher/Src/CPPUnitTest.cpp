/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CPPUnitTest.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CPPUnitTest.h"

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	void CPPUnitTestLauncher::LaunchCPPUnitTests ()
	{
		TestComparisons();
		TestSizes();
		TestOperations();
		TestCasting();
	}

	void CPPUnitTestLauncher::TestComparisons ()
	{
		CHECK_TRUE(true)
		CHECK_FALSE(false)

		CHECK_TRUE(4 == 4)
		CHECK_TRUE(2.5f != 2.f)

		CHECK_TRUE(true && (true || false))
		CHECK_FALSE(true && false && true)
		CHECK_TRUE(true || false || true)
	}

	void CPPUnitTestLauncher::TestSizes ()
	{
		CHECK_TRUE(sizeof(int) == 4)
		CHECK_TRUE(sizeof(float) == 4)

#ifdef PLATFORM_32BIT
		CHECK_TRUE(sizeof(CPPUnitTest_BaseClass) == 4)
		CHECK_TRUE(sizeof(CPPUnitTest_SubClass) == 16)
#else
		CHECK_TRUE(sizeof(CPPUnitTest_BaseClass) == 8)
		CHECK_TRUE(sizeof(CPPUnitTest_SubClass) == 32)
#endif
	}

	void CPPUnitTestLauncher::TestOperations ()
	{
		int a = 4;
		int b = 5;
		CHECK_TRUE((a+b) == 9)
		CHECK_TRUE((b-a) == 1)
		CHECK_TRUE((a-b) == -1)
		CHECK_TRUE((a*b) == 20)
		CHECK_TRUE((a&b) == 4)
		CHECK_TRUE((a|b) == 5)
		CHECK_TRUE((a^b) == 1)

		b = 6;
		CHECK_TRUE(b == 6)

		float c = 2.5f;
		float d = 2.f;
		CHECK_TRUE((c+d) == 4.5f)
		CHECK_TRUE((c-d) == 0.5f)
		CHECK_TRUE((c*d) == 5.f)
		CHECK_TRUE((c/d) == 1.25f)
	}

	void CPPUnitTestLauncher::TestInheritance ()
	{
		CPPUnitTest_SubClass testObj;

		CHECK_TRUE(testObj.GetNum() == 15)
	}

	void CPPUnitTestLauncher::TestCasting ()
	{
		int a = 5;
		float b = 6.f;

		CHECK_TRUE((static_cast<int>(b) + a) == 11)
		CHECK_TRUE((static_cast<float>(a) - b) == -1.f)

		CPPUnitTest_BaseClass* testObj = new CPPUnitTest_SubClass();
		CHECK_TRUE(dynamic_cast<CPPUnitTest_SubClass*>(testObj) != nullptr)
		CHECK_TRUE(dynamic_cast<CPPUnitTest_ProtectedInterface*>(testObj) == nullptr)
		CHECK_TRUE(dynamic_cast<CPPUnitTest_PublicInterface*>(testObj) != nullptr)

		delete testObj;
	}


	int CPPUnitTest_BaseClass::GetNum () const
	{
		return 1;
	}

	int CPPUnitTest_SubClass::GetNum () const
	{
		int result = 0;

		result += CPPUnitTest_BaseClass::GetNum();
		result += GetProtectedInterfaceNum();
		result += GetPublicInterfaceNum();
		result += 8;

		return result;
	}

	int CPPUnitTest_SubClass::GetPublicInterfaceNum () const
	{
		return 2;
	}

	int CPPUnitTest_SubClass::GetProtectedInterfaceNum () const
	{
		return 4;
	}
}

#endif