/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SandDune.cpp

  Defines the application's entry point, and launches the engine.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifdef _WIN32
#include "stdafx.h"
#endif

#include "Configuration.h"
#include "CoreClasses.h"

#ifdef DEBUG_MODE
#include "CPPUnitTest.h"
#endif

#ifdef SAND_DUNE_STANDALONE

using namespace std;

//Forward function declarations
int BeginSandDune (int argc, TCHAR* argv[]);
int RunMainLoop ();

/**
  Entry points based on operating system.
 */
#ifdef PLATFORM_WINDOWS
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int numArgs;
	//Only using this to get numArgs because of reasons
	LPWSTR *notUsed = CommandLineToArgvW(GetCommandLineW(), &numArgs);
	LocalFree(notUsed);

	TCHAR* args = GetCommandLine();
	return BeginSandDune(numArgs, &args);
}
#else //Not PLATFORM_WINDOWS
int main (int numArgs, char* args[])
{
	return BeginSandDune(numArgs, args);
}
#endif

/**
  Entry point of the engine
 */
int BeginSandDune (int numArg, TCHAR* args[])
{
#ifdef DEBUG_MODE
	//Ensure the current platform can perform primitive operations
	SD::CPPUnitTestLauncher::LaunchCPPUnitTests();
#endif

	SD::Engine::SetEngineParameters(numArg, args);
	SD::Engine* engine = SD::Engine::GetEngine();
	if (engine == nullptr)
	{
		cout << "Failed to instantiate Sand Dune Engine!\n";
		throw std::exception("Failed to instantiate Sand Dune Engine!");
		return -1;
	}

	engine->InitializeEngine(); //Initialize engine components and static objects

#ifdef DEBUG_MODE
	SD::Engine::GetEngine()->RecordLog(SD::DString::Format(TXT("Running %s in Debug mode."), {PROJECT_NAME}), LOG_DEBUG);

	#if CONDUCT_UNIT_TESTS
		SD::UnitTester::EUnitTestFlags testFlags;
		testFlags = SD::UnitTester::EUnitTestFlags::UTF_SmokeTest |
						SD::UnitTester::EUnitTestFlags::UTF_FeatureTest |
						SD::UnitTester::EUnitTestFlags::UTF_StressTest |
						SD::UnitTester::EUnitTestFlags::UTF_Automatic |
						SD::UnitTester::EUnitTestFlags::UTF_Manual |
						SD::UnitTester::EUnitTestFlags::UTF_CanDetectErrors |
						SD::UnitTester::EUnitTestFlags::UTF_NeverFails |
						SD::UnitTester::EUnitTestFlags::UTF_Synchronous |
						SD::UnitTester::EUnitTestFlags::UTF_Asynchronous |
						SD::UnitTester::EUnitTestFlags::UTF_Verbose |
						SD::UnitTester::EUnitTestFlags::UTF_CrashOnError;

		if (!SD::UnitTestLauncher::RunAllTests(testFlags))
		{
			SD::Engine::GetEngine()->FatalError(TXT("One of the unit tests failed.  See the logs for details."));
		}
	#endif
#endif

	return RunMainLoop();
}

int RunMainLoop ()
{
	SD::Engine* sandDuneEngine = SD::Engine::GetEngine();
	while (sandDuneEngine != nullptr)
	{
		sandDuneEngine->Tick();

		if (sandDuneEngine->bShuttingDown)
		{
			//At the end of Tick, the Engine should have cleaned up its resources.  It's safe to delete Engine here.
			delete sandDuneEngine;
			sandDuneEngine = nullptr;
			break;
		}
	}

	return 0;
}

#endif //SAND_DUNE_STANDALONE