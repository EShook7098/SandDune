/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Configuration.h
  This file determines which modules should be included for the next build.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "Definitions.h"

//General settings

/* If true, then the DString will validate its parameters.  For example, character indices will clamp between 0
and Length() - 1.  Setting this to 0 will slightly improve performance and will crash for invalid input. */
#define SAFE_STRINGS 1

#ifdef _DEBUG
/* If true, then the SandDune.cpp will launch the UnitTestLauncher (testing various aspects of the engine). */
#define CONDUCT_UNIT_TESTS 1
#endif


//Character encodings (Only one of these are used).  If all are false, then local encoding is used.
#define USE_WIDE_STRINGS 0
#define USE_UTF32 0
#define USE_UTF16 0
#define USE_UTF8 1

/* If true, then the rotators will be using degrees instead of radians. */
#define ROTATOR_UNIT_DEGREES 1


//Module includes
#define INCLUDE_CORE 1
#define INCLUDE_SD_TIME 1
#define INCLUDE_FILE 1
#define INCLUDE_SFMLGRAPHICS 1
#define INCLUDE_GRAPHICS 1
#define INCLUDE_BASEEDITOR 1
#define INCLUDE_MULTITHREAD 1
#define INCLUDE_LOGGER 1
#define INCLUDE_CONSOLE 0
#define INCLUDE_LOCALIZATION 1
#define INCLUDE_INPUT 1
#define INCLUDE_GUI 1
#define INCLUDE_GUIEDITOR 1
#define INCLUDE_GEOMETRY2D 1
#define INCLUDE_RANDOM 1

#endif