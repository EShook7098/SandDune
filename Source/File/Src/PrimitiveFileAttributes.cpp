/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PrimitiveFileAttributes.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "FileClasses.h"

#if INCLUDE_FILE

using namespace std;

namespace SD
{
	PrimitiveFileAttributes::PrimitiveFileAttributes ()
	{
		FileName = TXT("");
		FileExtension = TXT("");
		Path = TXT("");
		bFileExists = false;
	}

	PrimitiveFileAttributes::PrimitiveFileAttributes (const FilePtr& fileHandle)
	{
		bFileExists = true;
		DString fullFileName;
		OS_GetFileName(fileHandle, Path, fullFileName);
		FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
	}

	PrimitiveFileAttributes::PrimitiveFileAttributes (const DString& relativeDirectory, const DString& fullFileName)
	{
		FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
		Path = relativeDirectory;

		bFileExists = OS_CheckIfFileExists(FileUtils::ToAbsolutePath(Path), fullFileName);
		if (!bFileExists)
		{
			LOG(LOG_FILE, FileName + TXT(" does not exist."));
		}
	}

	PrimitiveFileAttributes::PrimitiveFileAttributes (const PrimitiveFileAttributes& attributesCopy)
	{
		FileName = attributesCopy.FileName;
		FileExtension = attributesCopy.FileExtension;
		Path = attributesCopy.Path;
		bFileExists = attributesCopy.bFileExists;
	}

	PrimitiveFileAttributes::PrimitiveFileAttributes (const FileAttributes& attributesCopy)
	{
		FileName = attributesCopy.FileName;
		FileExtension = attributesCopy.FileExtension;
		Path = attributesCopy.Path;
		bFileExists = attributesCopy.bFileExists;
	}

	PrimitiveFileAttributes::~PrimitiveFileAttributes ()
	{
	}

	DString PrimitiveFileAttributes::ToString () const
	{
		return GetFullFileName();
	}

	DString PrimitiveFileAttributes::GetFullFileName () const
	{
		return (FileExtension.IsEmpty()) ? FileName : FileName + TXT(".") + FileExtension;
	}

	void PrimitiveFileAttributes::ClearValues ()
	{
		FileName = TXT("");
		FileExtension = TXT("");
		Path = TXT("");
		bFileExists = false;
	}

	bool PrimitiveFileAttributes::SetFileName (const DString& path, const DString& fullFileName)
	{
		FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
		Path = path;
		PopulateFileInfo();

		return bFileExists;
	}

	DString PrimitiveFileAttributes::GetFileName () const
	{
		return FileName;
	}

	DString PrimitiveFileAttributes::GetFileExtension () const
	{
		return FileExtension;
	}

	DString PrimitiveFileAttributes::GetPath () const
	{
		return Path;
	}

	bool PrimitiveFileAttributes::GetFileExists () const
	{
		return bFileExists;
	}

	void PrimitiveFileAttributes::PopulateFileInfo ()
	{
		bFileExists = OS_CheckIfFileExists(FileUtils::ToAbsolutePath(Path), FileName + "." + FileExtension);

		if (!bFileExists)
		{
			HandleMissingFile();
		}
	}

	void PrimitiveFileAttributes::PopulateFileInfoFromHandle (FilePtr fileHandle)
	{
		DString fullFileName;
		OS_GetFileName(fileHandle, Path, fullFileName);
		FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
	}

	void PrimitiveFileAttributes::HandleMissingFile ()
	{
		LOG(LOG_FILE, FileName + TXT(" does not exist."));
	}
}

#endif