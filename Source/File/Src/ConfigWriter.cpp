/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ConfigWriter.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "FileClasses.h"

#if INCLUDE_FILE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(ConfigWriter, TextFileWriter)

	void ConfigWriter::InitProps ()
	{
		Super::InitProps();

		bNeedsSave = false;
		BufferLimit = 8192; //Large buffer since all content will be written on SaveConfig
	}

	bool ConfigWriter::OpenFile (const DString& fileName, bool bReplaceFile)
	{
		if (Super::OpenFile(fileName, bReplaceFile))
		{

			HeaderData.clear();
			Sections.clear();

			//abort early since there's nothing in the new file
			if (bReplaceFile)
			{
#ifdef DEBUG_MODE
				//Typically bReplaceFile is undesirable for config writers.  Display a log message just in case.
				LOG1(LOG_DEFAULT, TXT("Opening a config file (%s) with bReplaceFile to true.  You'll not be able to recall saved properties since you're clearing the content before reading it."), fileName);
#endif
				return true;
			}

			PopulateHeaderData();
			PopulateSectionData();

			return true;
		}

		return false;
	}

	void ConfigWriter::Destroy ()
	{
		if (File.is_open() && bNeedsSave)
		{
			SaveConfig();
		}

		Super::Destroy();
	}

	DString ConfigWriter::TextToWriteAfterFlush () const
	{
		//The last line should not be followed by a new line character (to prevent accumulating new line characters when replacing property values).
		return TXT("");
	}

	void ConfigWriter::SaveConfig ()
	{
		if (!bNeedsSave || Sections.size() <= 0 || bReadOnly)
		{
			return;
		}

		if (!File.is_open())
		{
			LOG1(LOG_WARNING, TXT("Attempting to write to the configuration file %s when the file is not opened."), CurrentFile);
			return;
		}

		EmptyFile();

		//Write the header data
		for (unsigned int i = 0; i < HeaderData.size(); i++)
		{
			AddTextEntry(HeaderData.at(i));
		}

		//Write the properties to config
		for (unsigned int i = 0; i < Sections.size(); i++)
		{
			AddTextEntry(TXT("[") + Sections.at(i).SectionName + TXT("]"));

			for (unsigned int j = 0; j < Sections.at(i).SectionData.size(); j++)
			{
				AddTextEntry(Sections.at(i).SectionData.at(j));
			}

			//Ensure there's a blank line between sections for readability
			if (i != Sections.size() - 1 && !Sections.at(i).SectionData.at(Sections.at(i).SectionData.size() - 1).IsEmpty())
			{
				AddTextEntry(TXT(""));
			}
		}

		WriteToFile(true);
		bNeedsSave = false;
	}

	void ConfigWriter::SavePropertyText (const DString& sectionName, const DString& propertyName, const DString& propertyValue)
	{
		unsigned int i = GetSectionIndex(sectionName);

		if (i == UINT_INDEX_NONE)
		{
			//Create a new section and add the data there
			SDataSection newSection;
			newSection.SectionName = sectionName;
			newSection.SectionData.push_back(propertyName + TXT("=") + propertyValue);
			Sections.push_back(newSection);
		}
		else
		{
			//Search where the property resides in the section
			unsigned int propIndex;
			FindPropertyWithinSection(i, propertyName, propIndex);

			if (propIndex != UINT_INDEX_NONE)
			{
				Sections.at(i).SectionData.at(propIndex) = propertyName + TXT("=") + propertyValue;
			}
			else
			{
				//Add property at the end of the section
				DString newData = propertyName + TXT("=") + propertyValue;
				Sections.at(i).SectionData.push_back(newData);
			}
		}

		bNeedsSave = true;
	}

	DString ConfigWriter::GetPropertyText (const DString& sectionName, const DString& propertyName) const
	{
		unsigned int sectionIndex = UINT_INDEX_NONE;

		if (!sectionName.IsEmpty())
		{
			sectionIndex = GetSectionIndex(sectionName);
			if (sectionIndex < Sections.size())
			{
				return FindPropertyWithinSection(sectionIndex, propertyName);
			}
			
			LOG2(LOG_FILE, TXT("Section name %s is not found within file %s"), sectionName, CurrentFile);
			return TXT("");
		}

		//Section name is not specified, search through header data
		for (unsigned int i = 0; i < HeaderData.size(); i++)
		{
			DString result;
			if (ReadPropertyValue(HeaderData.at(i), propertyName, result))
			{
				return result;
			}
		}

		return TXT("");
	}

	void ConfigWriter::PopulateHeaderData ()
	{
		ResetSeekPosition();

		DString currentLine;

		//HeaderData
		while(getline(File, currentLine.String))
		{
			//This is the beginning of actual data.  Stop writing to the header.
			if (IsSectionHeaderFormat(currentLine))
			{
				break;
			}

			HeaderData.push_back(currentLine);
		}
	}

	void ConfigWriter::PopulateSectionData ()
	{
		ResetSeekPosition();

		DString currentLine;
		SDataSection currentSection;

		while(true)
		{
			getline(File, currentLine.String);

			if (!IsSectionHeaderFormat(currentLine))
			{
				currentSection.SectionData.push_back(currentLine);
			}

			//Either at end of the file or when a new section is found, insert the section data to the Sections vector and reset currentSection
			if (File.eof() || IsSectionHeaderFormat(currentLine))
			{
				//Add currentSection to the Sections vector
				if (!currentSection.SectionName.IsEmpty())
				{
					if (currentSection.SectionData.size() > 0 && currentSection.SectionData.at(currentSection.SectionData.size() - 1).Length() < 2)
					{
						//Ignore the blank line between sections since that's automatically added in SaveConfig
						currentSection.SectionData.pop_back();
					}

					Sections.push_back(currentSection);
				}

				if (File.eof())
				{
					break;
				}

				currentSection.SectionName = currentLine.SubStringCount(1, currentLine.Length() - 2); //Include the text between the square brackets
				currentSection.SectionData.clear(); //Reset recycled variable for the next section
			}
		}
	}

	bool ConfigWriter::IsSectionHeaderFormat (const DString& text) const
	{
		//Must contain square brackets at the beginning and end.  Must contain text within square brackets.
		return (text.Length() > 2 && text.At(0) == TEXT('[') && text.At(text.Length() - 1) == TEXT(']'));
	}

	bool ConfigWriter::IsCommentFormat (const DString& line) const
	{
		return (line.IsEmpty() || line.At(0) == TEXT(';'));
	}

	unsigned int ConfigWriter::GetSectionIndex (const DString& sectionName) const
	{
		for (unsigned int i = 0; i < Sections.size(); i++)
		{
			if (sectionName.Compare(Sections.at(i).SectionName, false) == 0)
			{
				return i;
			}
		}

		return UINT_INDEX_NONE;
	}

	DString ConfigWriter::FindPropertyWithinSection (unsigned int sectionIndex, const DString& propertyName) const
	{
		unsigned int unusedIndex;
		return (FindPropertyWithinSection(sectionIndex, propertyName, unusedIndex));
	}

	DString ConfigWriter::FindPropertyWithinSection (unsigned int sectionIndex, const DString& propertyName, unsigned int& outPropertyIndex) const
	{
		outPropertyIndex = UINT_INDEX_NONE;

		for (unsigned int i = 0; i < Sections.at(sectionIndex).SectionData.size(); i++)
		{
			DString result;
			if (ReadPropertyValue(Sections.at(sectionIndex).SectionData.at(i), propertyName, result))
			{
				outPropertyIndex = i;
				return result;
			}
		}

		return TXT("");
	}

	bool ConfigWriter::ReadPropertyValue (const DString& fullLine, const DString& expectedPropertyName, DString& outPropertyValue) const
	{
		INT equalPosIdx;

		if (ContainsPropertyValue(fullLine, expectedPropertyName, equalPosIdx))
		{
			//Return the content after the equals
			outPropertyValue = fullLine.SubString(equalPosIdx + 1); //Plus 1 to start from the character after equal.
			return true;
		}

		return false;
	}

	bool ConfigWriter::ContainsPropertyValue (const DString& fullLine, const DString& targetPropertyName, INT& outEqualPosIdx) const
	{
		outEqualPosIdx = INT_INDEX_NONE;

		if (IsCommentFormat(fullLine))
		{
			return false;
		}

		outEqualPosIdx = fullLine.FindSubText(TXT("="));
		if (outEqualPosIdx < 0)
		{
			return false;
		}

		return (fullLine.SubString(0, outEqualPosIdx - 1).Compare(targetPropertyName, false) == 0);
	}
}

#endif