/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileAttributes.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "FileClasses.h"

#if INCLUDE_FILE

using namespace std;

namespace SD
{
	FileAttributes::FileAttributes () : PrimitiveFileAttributes()
	{
		bReadOnly = false;
		FileSize = 0;
	}

	FileAttributes::FileAttributes (const FilePtr& fileHandle) : PrimitiveFileAttributes(fileHandle)
	{
		DString absPath = FileUtils::ToAbsolutePath(Path);
		FileSize = OS_GetFileSize(absPath, FileName + TXT(".") + FileExtension);
		bReadOnly = OS_IsFileReadOnly(absPath, FileName + TXT(".") + FileExtension);
		OS_GetFileTimestamps(absPath, FileName + TXT(".") + FileExtension, CreatedDate, LastModifiedDate);
	}

	FileAttributes::FileAttributes (const DString& relativeDirectory, const DString& fullFileName) : PrimitiveFileAttributes(relativeDirectory, fullFileName)
	{
		if (bFileExists)
		{
			DString absPath = FileUtils::ToAbsolutePath(Path);
			FileSize = OS_GetFileSize(absPath, FileName + TXT(".") + FileExtension);
			bReadOnly = OS_IsFileReadOnly(absPath, FileName + TXT(".") + FileExtension);
			OS_GetFileTimestamps(absPath, FileName + TXT(".") + FileExtension, CreatedDate, LastModifiedDate);
		}
	}

	FileAttributes::FileAttributes (const PrimitiveFileAttributes& attributesCopy) : PrimitiveFileAttributes(attributesCopy)
	{
		bReadOnly = false;
		FileSize = 0;
		PopulateFileInfo();
	}

	FileAttributes::FileAttributes (const FileAttributes& attributesCopy) : PrimitiveFileAttributes(attributesCopy)
	{
		bReadOnly = attributesCopy.bReadOnly;
		FileSize = attributesCopy.FileSize;
		LastModifiedDate = attributesCopy.LastModifiedDate;
		CreatedDate = attributesCopy.CreatedDate;
	}

	FileAttributes::~FileAttributes ()
	{

	}

	void FileAttributes::ClearValues ()
	{
		PrimitiveFileAttributes::ClearValues();

		bReadOnly = false;
		FileSize = 0;
	}

	void FileAttributes::PopulateFileInfo ()
	{
		PrimitiveFileAttributes::PopulateFileInfo();

		if (bFileExists)
		{
			DString absPath = FileUtils::ToAbsolutePath(Path);
			FileSize = OS_GetFileSize(absPath, FileName + TXT(".") + FileExtension);
			bReadOnly = OS_IsFileReadOnly(absPath, FileName + TXT(".") + FileExtension);
			OS_GetFileTimestamps(absPath, FileName + TXT(".") + FileExtension, CreatedDate, LastModifiedDate);
		}
	}

	void FileAttributes::PopulateFileInfoFromHandle (FilePtr handle)
	{
		PrimitiveFileAttributes::PopulateFileInfoFromHandle(handle);

		DString absPath = FileUtils::ToAbsolutePath(Path);
		FileSize = OS_GetFileSize(absPath, FileName + TXT(".") + FileExtension);
		bReadOnly = OS_IsFileReadOnly(absPath, FileName + TXT(".") + FileExtension);
		OS_GetFileTimestamps(absPath, FileName + TXT(".") + FileExtension, CreatedDate, LastModifiedDate);
	}

	bool FileAttributes::GetReadOnly () const
	{
		return bReadOnly;
	}

	INT FileAttributes::GetFileSize () const
	{
		return INT(static_cast<int>(FileSize));
	}

	unsigned long long FileAttributes::GetFileSizeLarge () const
	{
		return FileSize;
	}

	DateTime FileAttributes::GetLastModifiedDate () const
	{
		return LastModifiedDate;
	}

	DateTime FileAttributes::GetCreatedDate () const
	{
		return CreatedDate;
	}
}

#endif