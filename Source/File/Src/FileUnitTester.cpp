/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileUnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "FileClasses.h"

#if INCLUDE_FILE

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	DString FileUnitTester::UnitTestLocation = TXT("Source") + DString(DIR_SEPARATOR) + TXT("File") + DString(DIR_SEPARATOR) + TXT("UnitTest") + DString(DIR_SEPARATOR);

	IMPLEMENT_CLASS(FileUnitTester, UnitTester)

	bool FileUnitTester::RunTests (EUnitTestFlags testFlags) const
	{
		if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
		{
			return (TestOSCalls(testFlags) && TestFileAttributes(testFlags) && TestFileIterator(testFlags) && TestFileWriter(testFlags) && TestConfig(testFlags));
		}

		return true;
	}

	bool FileUnitTester::MetRequirements (const vector<const UnitTester*>& completedTests) const
	{
		if (!Super::MetRequirements(completedTests))
		{
			return false;
		}

		//The FileUnitTester depends on the TimeUnitTester
		for (unsigned int i = 0; i < completedTests.size(); i++)
		{
			if (completedTests.at(i) == TimeUnitTester::SStaticClass()->GetDefaultObject())
			{
				return true;
			}
		}

		//TimeUnitTester is not completed yet.  Try again later.
		return false;
	}

	bool FileUnitTester::TestOSCalls (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("File OS Calls"));

		UNITTESTER_LOG(testFlags, TXT("The engine's base directory is:  ") + BASE_DIR + TXT("."));
		UNITTESTER_LOG(testFlags, TXT("The current working directory is:  ") + FileUtils::ToAbsolutePath(TXT("")) + TXT("."));

		SetTestCategory(testFlags, TXT("Relative to Absolute Path"));
		UNITTESTER_LOG(testFlags, TXT("Testing conversion from relative path to absolute path."));
		if (FileUtils::ToAbsolutePath(TXT("")) != BASE_DIR)
		{
			UnitTestError(testFlags, TXT("Conversion relative to absolute path test failed.  Expected absolute path for blank input is  ") + BASE_DIR + TXT(".  Instead it returned ") + FileUtils::ToAbsolutePath(TXT("")) + TXT(".  Make sure the current working directory is set to the base directory."));
			return false;
		}

		DString relativePath = UnitTestLocation + DIR_SEPARATOR + TXT("SubDirectoryTest") + DIR_SEPARATOR + TXT("..");
		if (FileUtils::ToAbsolutePath(relativePath) != BASE_DIR + UnitTestLocation)
		{
			UnitTestError(testFlags, TXT("Conversion relative to absolute path test failed.  Expected value for ") + relativePath + TXT(" is ") + BASE_DIR + UnitTestLocation + TXT(".  Instead it returned ") + FileUtils::ToAbsolutePath(relativePath) + TXT("."));
			return false;
		}
		UNITTESTER_LOG(testFlags, TXT("absolute path conversion test passed!"));

		UNITTESTER_LOG(testFlags, TXT("Testing if system can check if a file exists or not."));
		if (!OS_CheckIfFileExists(FileUtils::ToAbsolutePath(UnitTestLocation), TXT("FileA")))
		{
			UnitTestError(testFlags, TXT("Check if file test failed.  Could not find FileA within ") + FileUtils::ToAbsolutePath(UnitTestLocation) + TXT(".  FileA should exist within that location."));
			return false;
		}
		UNITTESTER_LOG(testFlags, TXT("Unit test was able find the file."));

		DString nonExistingFile(TXT("FileUnitTesterExpectsThisFileNotToExist"));
		UNITTESTER_LOG2(testFlags, TXT("Testing if system does not get a handle to a nonexisting file:  %s%s"), UnitTestLocation, nonExistingFile);
		if (OS_CheckIfFileExists(FileUtils::ToAbsolutePath(UnitTestLocation), nonExistingFile))
		{
			UnitTestError(testFlags, TXT("Check if file does not exist test failed.  Unit test obtained a handle to a file that should not exist:  %s%s"), {FileUtils::ToAbsolutePath(UnitTestLocation).ToString(), nonExistingFile.ToString()});
			return false;
		}
		UNITTESTER_LOG(testFlags, TXT("Unit test could not find a non existing file [this is good!]."));

		UNITTESTER_LOG(testFlags, TXT("Testing if system could get a file handle."));
		FilePtr fileHandle = OS_GetFileHandle(FileUtils::ToAbsolutePath(UnitTestLocation), TXT("FileA"));
		if (fileHandle == nullptr)
		{
			UnitTestError(testFlags, TXT("File handle test failed.  Could not obtain a handle to ") + FileUtils::ToAbsolutePath(UnitTestLocation) + TXT("FileA."));
			return false;
		}

		DString handlePath;
		DString handleName;
		OS_GetFileName(fileHandle, handlePath, handleName);
		if (handlePath.IsEmpty())
		{
			OS_CloseFile(fileHandle);
			UnitTestError(testFlags, TXT("File handle test failed.   Handle path to the file 'FileA' is empty."));
			return false;
		}

		if (handleName != TXT("FileA"))
		{
			OS_CloseFile(fileHandle);
			UnitTestError(testFlags, TXT("File handle test failed.  Unable to get file name from handle.  Found name is:  ") + handleName + TXT(".  The expected value is:  FileA."));
			return false;
		}

		OS_CloseFile(fileHandle);
		if (fileHandle != nullptr)
		{
			UnitTestError(testFlags, TXT("File handle test failed.  Was not able to close the file handle to FileA"));
			return false;
		}
		UNITTESTER_LOG(testFlags, TXT("System was able to obtain File handles!"));

		UNITTESTER_LOG(testFlags, TXT("Testing file size."));
		INT fileSize = static_cast<int>(OS_GetFileSize(FileUtils::ToAbsolutePath(UnitTestLocation), TXT("FileA")));
		if (fileSize != 75)
		{
			UnitTestError(testFlags, TXT("File size test failed.  File size is:  ") + fileSize.ToString() + TXT(" bytes.  The expected value is:  75 bytes."));
			return false;
		}
		UNITTESTER_LOG(testFlags, TXT("The file size of FileA is ") + fileSize.ToString() + TXT(" bytes."));

		UNITTESTER_LOG(testFlags, TXT("Testing obtaining Read Only attribute."));
		if (!OS_IsFileReadOnly(FileUtils::ToAbsolutePath(UnitTestLocation + TXT("SubDirectoryTest")), TXT("SubFileB")))
		{
			UnitTestError(testFlags, TXT("Read only test failed.  SubFileB within ") + UnitTestLocation + TXT("SubDirectoryTest") + DIR_SEPARATOR + TXT("SubFileB should be read only."));
			return false;
		}
		UNITTESTER_LOG(testFlags, TXT("System was able to check the Read Only attribute."));

		UNITTESTER_LOG(testFlags, TXT("Testing file timestamps."));
		DateTime createdDate;
		DateTime modifiedDate;
		DateTime minDate;
		minDate.Year = 2015;
		OS_GetFileTimestamps(FileUtils::ToAbsolutePath(UnitTestLocation), TXT("FileA"), createdDate, modifiedDate);
		UNITTESTER_LOG(testFlags, TXT("Created date for FileA is:  ") + createdDate.ToString() + TXT("."));
		UNITTESTER_LOG(testFlags, TXT("Last modified date for FileA is:  ") + createdDate.ToString() + TXT("."));
		if (createdDate == DateTime() || modifiedDate == DateTime())
		{
			UnitTestError(testFlags, TXT("Timestamp test failed.  Either created date [") + createdDate.ToString() + TXT("] or last modified date [") + modifiedDate.ToString() + TXT("] is empty."));
			return false;
		}

		if (createdDate.Year < minDate.Year || modifiedDate.Year < minDate.Year)
		{
			UnitTestError(testFlags, TXT("Timestamp test failed.  The created date [") + createdDate.ToString() + TXT("] and the last modified date [") + modifiedDate.ToString() + TXT("] must be later than ") + minDate.ToString() + TXT("."));
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Testing file creation."));
		DString tempFileName = TXT("TemporaryFileTest");
		FilePtr createdFile = OS_CreateFile(FileUtils::ToAbsolutePath(UnitTestLocation), tempFileName);
		if (createdFile == nullptr)
		{
			UnitTestError(testFlags, TXT("Failed to create file named:  ") + UnitTestLocation + tempFileName + TXT(".  Make sure the file does not already exist."));
			return false;
		}
		UNITTESTER_LOG(testFlags, TXT("System was able to create files."));

		UNITTESTER_LOG(testFlags, TXT("Testing file writing."));
		TCHAR* dataBuffer = TXT("The FileUnitTester wrote this data to this file.\n");
		int bufferSize = static_cast<int>(strlen(dataBuffer));
		if (!OS_WriteToFile(createdFile, dataBuffer, bufferSize))
		{
			OS_CloseFile(createdFile);
			UnitTestError(testFlags, TXT("Failed to write ") + DString(dataBuffer) + TXT(" to ") + tempFileName + TXT(".  Make sure application has write access to this file."));
			UnitTestError(testFlags, TXT("You'll need to manually delete ") + FileUtils::ToAbsolutePath(UnitTestLocation) + tempFileName + TXT(" since the unit test will be attempting to create a new file with the same name."));
			return false;
		}
		UNITTESTER_LOG(testFlags, TXT("Write to file test passed!"));
		OS_CloseFile(createdFile);

		UNITTESTER_LOG(testFlags, TXT("Testing file copy."));
		DString fileCopy = tempFileName + TXT("_Copy");
		if (!OS_CopyFile(FileUtils::ToAbsolutePath(UnitTestLocation) + tempFileName, FileUtils::ToAbsolutePath(UnitTestLocation) + fileCopy, true))
		{
			UnitTestError(testFlags, TXT("File copy test failed.  Failed to copy ") + tempFileName + TXT(" to ") + fileCopy + TXT("."));
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Testing file deletion."));
		if (!OS_DeleteFile(FileUtils::ToAbsolutePath(UnitTestLocation), tempFileName))
		{
			UnitTestError(testFlags, TXT("Failed to delete ") + FileUtils::ToAbsolutePath(UnitTestLocation) + tempFileName + TXT(".  Make sure application has write access to this file."));
			UNITTESTER_LOG(testFlags, TXT("You'll need to manually delete this file since the unt test will be attempting to create a new file with the same name."));
			return false;
		}

		if (!OS_DeleteFile(FileUtils::ToAbsolutePath(UnitTestLocation), fileCopy))
		{
			UnitTestError(testFlags, TXT("Failed to delete ") + FileUtils::ToAbsolutePath(UnitTestLocation) + fileCopy + TXT(".  Make sure application has write access to this file."));
			//No need to notify user to manually delete fileCopy since this file will be replaced next time this unit tester runs.
			return false;
		}
		UNITTESTER_LOG(testFlags, TXT("System was able to delete files."));

		ExecuteSuccessSequence(testFlags, TXT("File OS Calls"));
		return true;
	}

	bool FileUnitTester::TestFileAttributes (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("File Attributes"));

		UNITTESTER_LOG(testFlags, TXT("Constructing File Attributes from handle."));
		FilePtr fileHandle = OS_GetFileHandle(FileUtils::ToAbsolutePath(UnitTestLocation), TXT("FileA"));
		if (fileHandle == nullptr)
		{
			UnitTestError(testFlags, TXT("File handle constructor test failed.  Unable to get a handle to FileA"));
			return false;
		}

		FileAttributes attributes(fileHandle);
		OS_CloseFile(fileHandle);
		if (attributes.GetFileName() != TXT("FileA"))
		{
			UnitTestError(testFlags, TXT("File handle constructor test failed.  The attributes file name (") + attributes.GetFileName() + TXT(") is not equal to FileA."));
			return false;
		}

		if (attributes.GetFileSize() != 75)
		{
			UnitTestError(testFlags, TXT("File handle constructor test failed.  The attributes describing FileA is ") + attributes.GetFileSize().ToString() + TXT(" bytes.  The expected size is 75 bytes."));
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Constructing File Attributes from string."));
		FileAttributes stringAttributes(UnitTestLocation, TXT("FileA"));
		if (!stringAttributes.GetFileExists())
		{
			UnitTestError(testFlags, TXT("Constructing file attributes from string test failed.  Unable to locate FileA from ") + FileUtils::ToAbsolutePath(UnitTestLocation) + TXT("."));
			return false;
		}

		UNITTESTER_LOG2(testFlags, TXT("The created date for FileA is %s.  The last modified date is %s."), stringAttributes.GetCreatedDate(), stringAttributes.GetLastModifiedDate());
		if (stringAttributes.GetCreatedDate().Year < 2015 || stringAttributes.GetLastModifiedDate().Year < 2015)
		{
			UnitTestError(testFlags, TXT("Unable to obtain correct created date for FileA.  The year should at least be 2015.  The created date is [%s]."), {stringAttributes.GetCreatedDate().ToString()});
			UNITTESTER_LOG1(testFlags, TXT("    The last modified date for FileA reported [%s]."), stringAttributes.GetLastModifiedDate());
			return false;
		}

		if (stringAttributes.GetFileSize() != 75)
		{
			UnitTestError(testFlags, TXT("Constructing file attributes from string test failed.  The attributes describing FileA suggests it's ") + stringAttributes.GetFileSize().ToString() + TXT(" bytes.  The expected file size is 75 bytes."));
			return false;
		}

		ExecuteSuccessSequence(testFlags, TXT("File Attributes"));
		return true;
	}

	bool FileUnitTester::TestFileIterator (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("File Iterator"));

		//List all files within the unit test directory that are expected to be captured from the file iterator.
		vector<DString> expectedFileNames;
		expectedFileNames.push_back(TXT("FileA"));
		expectedFileNames.push_back(TXT("ReadMe.txt"));
		expectedFileNames.push_back(TXT("SubFileA"));
		expectedFileNames.push_back(TXT("SubFileB"));
		INT fileCounter = 0;

		for (FileIterator fileIter(UnitTestLocation, FileIterator::INCLUDE_EVERYTHING); !fileIter.FinishedIterating(); fileIter++)
		{
			if (fileIter.IsDirectorySelected())
			{
				UNITTESTER_LOG1(testFlags, TXT("File iterator found a directory named:  %s"), fileIter.GetSelectedDirectory());
				continue;
			}

			DString curFileName = fileIter.GetSelectedAttributes().ToString();
			UNITTESTER_LOG1(testFlags, TXT("File iterator found an file named:  %s"), curFileName);
			fileCounter++;

			for (unsigned int i = 0; i < expectedFileNames.size(); i++)
			{
				if (expectedFileNames.at(i) == curFileName)
				{
					expectedFileNames.erase(expectedFileNames.begin() + i);
					break;
				}
			}
		}

		if (expectedFileNames.size() > 0)
		{
			UnitTestError(testFlags, TXT("File iterator test failed.  Not all expected file names were found.  See the logs for details."));

			UNITTESTER_LOG(testFlags, TXT("The file iterator did not find the following files within the ") + UnitTestLocation + TXT(" directory. . ."));
			for (unsigned int i = 0; i < expectedFileNames.size(); i++)
			{
				UNITTESTER_LOG(testFlags, TXT("    ") + expectedFileNames.at(i));
			}

			return false;
		}
		
		UNITTESTER_LOG1(testFlags, TXT("The file iterator found a total of %s files."), fileCounter);

		ExecuteSuccessSequence(testFlags, TXT("File Iterator"));
		return true;
	}

	bool FileUnitTester::TestFileWriter (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("File Writer"));

		DString fileName = UnitTestLocation + TXT("FileWriterTest.txt");
		TextFileWriter* testWriter = TextFileWriter::CreateObject();
		if (!VALID_OBJECT(testWriter))
		{
			UnitTestError(testFlags, TXT("File writer test failed.  Unable to instantiate a TextFileWriter."));
			return false;
		}

		if (!testWriter->OpenFile(fileName, true))
		{
			UnitTestError(testFlags, TXT("File writer test failed.  Failed to open file named:  %s"), {fileName});
			return false;
		}
		
		vector<DString> contentToWrite;
		contentToWrite.push_back(TXT("First Line"));
		contentToWrite.push_back(TXT("Second Line"));
		contentToWrite.push_back(TXT("Third Line"));
		contentToWrite.push_back(TXT(""));
		contentToWrite.push_back(TXT("Last Line"));

		for (unsigned int i = 0; i < contentToWrite.size(); i++)
		{
			UNITTESTER_LOG1(testFlags, TXT("Writing %s to the file."), contentToWrite.at(i));
			testWriter->AddTextEntry(contentToWrite.at(i));
		}

		if (!testWriter->WriteToFile())
		{
			UnitTestError(testFlags, TXT("File writer test failed.  Failed to flush buffer to the file."));
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Finished writing to file.  Now reading all text from %s"), fileName);
		DString currentLine;
		testWriter->ResetSeekPosition();
		while(testWriter->ReadLine(currentLine))
		{
			UNITTESTER_LOG1(testFlags, TXT("Recalled \"%s\" from text file."), currentLine);

			for (unsigned int i = 0; i < contentToWrite.size(); i++)
			{
				if (contentToWrite.at(i) == currentLine)
				{
					contentToWrite.erase(contentToWrite.begin() + i);
					break;
				}
			}
		}

		UNITTESTER_LOG(testFlags, TXT("Finished reading from file."));

		if (contentToWrite.size() > 0)
		{
			UnitTestError(testFlags, TXT("File writer test failed.  Some of the written text was not recalled when reading from file.  See the logs for details."));
			UNITTESTER_LOG(testFlags, TXT("File writer test failed.  The following written text was not recalled when reading file."));
			for (unsigned int i = 0; i < contentToWrite.size(); i++)
			{
				UNITTESTER_LOG1(testFlags, TXT("    %s"), contentToWrite.at(i));
			}

			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Deleting file writer test file."));
		testWriter->Destroy(); //closes file

		DString path;
		DString name;
		FileUtils::ExtractFileName(fileName, path, name);
		if (!OS_DeleteFile(path, name))
		{
			UnitTestError(testFlags, TXT("File writer test failed.  Unable to clean up resources.  Attempted to delete file using %s%s"), {path, name});
			return false;
		}

		ExecuteSuccessSequence(testFlags, TXT("File Writer"));
		return true;
	}

	bool FileUnitTester::TestConfig (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Config"));

		DString fileName = UnitTestLocation + TXT("ConfigTest.ini");
		ConfigWriter* testConfig = ConfigWriter::CreateObject();
		if (!VALID_OBJECT(testConfig))
		{
			UnitTestError(testFlags, TXT("Config test failed.  Unable to instantiate a ConfigWriter."));
			return false;
		}

		if (!OS_CheckIfFileExists(UnitTestLocation, TXT("ConfigTest.ini")))
		{
			//File doesn't exist, restore file using backup
			UNITTESTER_LOG(testFlags, TXT("Unable to find ConfigTest.ini.  Attempting to restore unit test config file by copying its backup file."));
			OS_CopyFile(FileUtils::ToAbsolutePath(UnitTestLocation) + TEXT("ConfigTestBackup.ini"), FileUtils::ToAbsolutePath(UnitTestLocation) + TEXT("ConfigTest.ini"));

			if (!OS_CheckIfFileExists(UnitTestLocation, TXT("ConfigTest.ini")))
			{
				UnitTestError(testFlags, TXT("Config test failed.  Failed to restore missing file named %s."), {fileName});
				testConfig->Destroy();
				return false;
			}
		}

		if (!testConfig->OpenFile(fileName, false))
		{
			UnitTestError(testFlags, TXT("Config test failed.  Failed to open file named:  %s"), {fileName});
			testConfig->Destroy();
			return false;
		}
		
		UNITTESTER_LOG(testFlags, TXT("Begin reading from config test."));

		DString unitTestSection = TXT("UnitTest");

		DString oldTimeStampStr = testConfig->GetProperty<DString>(unitTestSection, TXT("FullTimeStamp"));
		if (oldTimeStampStr.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Config test failed.  Unable to get FullTimeStamp property from %s section within %s file."), {unitTestSection, fileName});
			testConfig->Destroy();
			return false;
		}

		//Retrieve the same variable without specifying a section
		DString oldTimeStampStrNoSection = testConfig->GetProperty<DString>(TXT(""), TXT("FullTimeStamp"));
		if (oldTimeStampStrNoSection == oldTimeStampStr)
		{
			UnitTestError(testFlags, TXT("Config test failed.  Retrieving the FullTimeStamp property without specifying a section within %s file should have failed to retrieve that property."), {fileName});
			testConfig->Destroy();
			return false;
		}

		//Retrieving a property value that's not within a section
		DString sectionlessValue = testConfig->GetProperty<DString>(TXT(""), TXT("PropertyWithoutSection"));
		if (sectionlessValue != TXT("Some Value"))
		{
			UnitTestError(testFlags, TXT("Config test failed.  The expected value of PropertyWithoutSection is \"Some Value\" from config file %s.  Instead it returned \"%s\"."), {fileName, sectionlessValue});
			testConfig->Destroy();
			return false;
		}
		UNITTESTER_LOG2(testFlags, TXT("Successfully extracted FullTimeStamp from %s section.  The value is:  %s"), unitTestSection, oldTimeStampStr);

		DateTime oldTimeStamp;
		oldTimeStamp.Year = testConfig->GetProperty<INT>(unitTestSection, TXT("Year"));
		oldTimeStamp.Month = testConfig->GetProperty<INT>(unitTestSection, TXT("Month"));
		oldTimeStamp.Day = testConfig->GetProperty<INT>(unitTestSection, TXT("Day"));
		oldTimeStamp.Hour = testConfig->GetProperty<INT>(unitTestSection, TXT("Hour"));
		oldTimeStamp.Minute = testConfig->GetProperty<INT>(unitTestSection, TXT("Minute"));
		oldTimeStamp.Second = testConfig->GetProperty<INT>(unitTestSection, TXT("Second"));
		UNITTESTER_LOG6(testFlags, TXT("Extracted the following INTs from Config:  Year=%s, Month=%s, Day=%s, Hour=%s, Minute=%s, Second=%s."), oldTimeStamp.Year, oldTimeStamp.Month, oldTimeStamp.Day, oldTimeStamp.Hour, oldTimeStamp.Minute, oldTimeStamp.Second);
		if (oldTimeStamp.CalculateTotalSeconds() <= 0.f)
		{
			UnitTestError(testFlags, TXT("Config test failed.  The time stamp from %s is not positive (%s seconds).  The unit test should have been able to extract the previous unit time's time stamp."), {oldTimeStamp.ToString(), oldTimeStamp.CalculateTotalSeconds().ToString()});
			testConfig->Destroy();
			return false;
		}

		if (oldTimeStamp.ToString() != oldTimeStampStr)
		{
			UnitTestError(testFlags, TXT("Config test failed.  The components of the time stamp in config should have been equal to the string representation.  Instead the components string representation is %s.  The string representation in config is %s."), {oldTimeStamp.ToString(), oldTimeStampStr.ToString()});
			testConfig->Destroy();
			return false;
		}
		UNITTESTER_LOG1(testFlags, TXT("Reading variables from config successful.  The components of the config's time stamp's string representation is equal to the string representation in config.  %s"), oldTimeStamp);

		DateTime current;
		current.SetToCurrentTime();
		UNITTESTER_LOG1(testFlags, TXT("Testing saving data to config.  Updating time stamp variables to current time:  %s"), current);

		testConfig->SaveProperty<DString>(unitTestSection, TXT("FullTimeStamp"), current.ToString());
		testConfig->SaveProperty<INT>(unitTestSection, TXT("Year"), current.Year);
		testConfig->SaveProperty<INT>(unitTestSection, TXT("Month"), current.Month);
		testConfig->SaveProperty<INT>(unitTestSection, TXT("Day"), current.Day);
		testConfig->SaveProperty<INT>(unitTestSection, TXT("Hour"), current.Hour);
		testConfig->SaveProperty<INT>(unitTestSection, TXT("Minute"), current.Minute);
		testConfig->SaveProperty<INT>(unitTestSection, TXT("Second"), current.Second);
		testConfig->SaveConfig();

#if 0
		//This was removed since this will most likely cause a sharing violation.
		//Check if data was actually saved to the file (not just in buffer)
		if (OS_GetFileSize(UnitTestLocation, TXT("ConfigTest.ini")) <= 0)
		{
			LOG1(testFlags, TXT("Save config test failed.  %s file size is zero after calling SaveConfig()."), fileName);
			testConfig->Destroy();
			return false;
		}
#endif

		UNITTESTER_LOG(testFlags, TXT("Retrieving newly saved data from config."));

		DString currentReaderStr = testConfig->GetProperty<DString>(unitTestSection, TXT("FullTimeStamp"));
		if (currentReaderStr != current.ToString())
		{
			UnitTestError(testFlags, TXT("Config test failed.  The retrieved value of FullTimeStamp should be equal to %s.  Instead it returned %s."), {current.ToString(), currentReaderStr});
			return false;
		}

		DateTime currentReader;
		currentReader.Year = testConfig->GetProperty<INT>(unitTestSection, TXT("Year"));
		currentReader.Month = testConfig->GetProperty<INT>(unitTestSection, TXT("Month"));
		currentReader.Day = testConfig->GetProperty<INT>(unitTestSection, TXT("Day"));
		currentReader.Hour = testConfig->GetProperty<INT>(unitTestSection, TXT("Hour"));
		currentReader.Minute = testConfig->GetProperty<INT>(unitTestSection, TXT("Minute"));
		currentReader.Second = testConfig->GetProperty<INT>(unitTestSection, TXT("Second"));

		if (currentReader != current)
		{
			UnitTestError(testFlags, TXT("Config test failed.  The retrieved DateTime should be equal to %s.  Instead it retrieved %s."), {current.ToString(), currentReader.ToString()});
			testConfig->Destroy();
			return false;
		}
		UNITTESTER_LOG1(testFlags, TXT("Saving data to config test passed!  Was able to retrieve the current time from config:  %s"), currentReader);

		SetTestCategory(testFlags, TXT("Config Array"));
		UNITTESTER_LOG(testFlags, TXT("Reading the counter from the config array."));
		vector<INT> counterKey;
		for (unsigned int i = 1; i < 11; i++)
		{
			counterKey.push_back(INT(i));
		}

		vector<INT> counter;
		testConfig->GetArrayValues<INT>(TXT("ArrayTest"), TXT("CounterArray"), counter);

		if (counter.size() != counterKey.size())
		{
			UnitTestError(testFlags, TXT("Config array tests failed.  There's a size mismatch from the expected counter array (size: %s) and the counter array read from config file (size: %s)."), {INT(counterKey.size()).ToString(), INT(counter.size()).ToString()});
			testConfig->Destroy();
			return false;
		}

		for (unsigned int i = 0; i < counter.size(); i++)
		{
			UNITTESTER_LOG2(testFlags, TXT("    Counter[%s]=%s"), INT(i), counter.at(i));
			if (counter.at(i) != counterKey.at(i))
			{
				UnitTestError(testFlags, TXT("Config array tests failed.  Reading the array from config does not match the expected array.  At index %s, the expected value is %s.  Instead it found %s."), {INT(i).ToString(), counterKey.at(i).ToString(), counter.at(i).ToString()});
				testConfig->Destroy();
				return false;
			}
		}

		//Test saving array
		counter.clear();
		testConfig->SaveArray<INT>(TXT("ArrayTest"), TXT("CounterArray"), counter);

		//Retrieve that array to see if the result is empty
		testConfig->GetArrayValues<INT>(TXT("ArrayTest"), TXT("CounterArray"), counter);
		if (!counter.empty())
		{
			UnitTestError(testFlags, TXT("Config array tests failed.  After clearing the counter array, and updating the config file array with an empty one, retrieving that same array should have also returned an empty array.  Instead the size of the array is %s"), {INT(counter.size()).ToString()});
			testConfig->Destroy();
			return false;
		}

		//Repopulate array
		testConfig->SaveArray<INT>(TXT("ArrayTest"), TXT("CounterArray"), counterKey);
		testConfig->GetArrayValues<INT>(TXT("ArrayTest"), TXT("CounterArray"), counter);

		if (counter.size() != counterKey.size())
		{
			UnitTestError(testFlags, TXT("Config array tests failed.  After updating the array in config file, there's a size mismatch from the expected counter array (size: %s) and the counter array read from config file (size: %s)."), {INT(counterKey.size()).ToString(), INT(counter.size()).ToString()});
			testConfig->Destroy();
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Rereading the counter array after updating the config file."));
		for (unsigned int i = 0; i < counter.size(); i++)
		{
			UNITTESTER_LOG2(testFlags, TXT("    Counter[%s]=%s"), INT(i), counter.at(i));
			if (counter.at(i) != counterKey.at(i))
			{
				UnitTestError(testFlags, TXT("Config array tests failed.  After updating the array in config file, reading the array from config does not match the expected array.  At index %s, the expected value is %s.  Instead it found %s."), {INT(i).ToString(), counterKey.at(i).ToString(), counter.at(i).ToString()});
				testConfig->Destroy();
				return false;
			}
		}
		testConfig->Destroy();
		CompleteTestCategory(testFlags);

		ExecuteSuccessSequence(testFlags, TXT("Config"));
		return true;
	}
}

#endif
#endif