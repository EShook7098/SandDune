/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFileWriter.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "FileClasses.h"

#if INCLUDE_FILE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TextFileWriter, Object)

	void TextFileWriter::InitProps ()
	{
		Super::InitProps();

		CurrentFile = TXT("");
		BufferLimit = 12;
		bReadOnly = false;
	}

	void TextFileWriter::Destroy ()
	{
		if (!bReadOnly)
		{
			WriteToFile(true);
		}

		if (File.is_open())
		{
			File.close();
		}

		Super::Destroy();
	}

	DString TextFileWriter::GetFriendlyName () const
	{
		if (!CurrentFile.IsEmpty())
		{
			return GetName() + TXT(":  ") + CurrentFile;
		}

		return Super::GetFriendlyName();
	}

	bool TextFileWriter::OpenFile (const DString& fileName, bool bReplaceFile)
	{
		if (File.is_open())
		{
			WriteToFile(true);
			File.close();
		}

		if (bReplaceFile && !bReadOnly)
		{
			remove(fileName.ToCString());
		}

		File.open(fileName.String, fstream::out | fstream::in | fstream::app);

		CurrentFile = fileName;

		return File.is_open();
	}

	void TextFileWriter::AddTextEntry (DString newEntry)
	{
		TextBuffer.push_back(newEntry);

		if (TextBuffer.size() >= BufferLimit)
		{
			WriteToFile(true);
		}
	}

	bool TextFileWriter::WriteToFile (bool bClearBufferIfFailed)
	{
		if (bReadOnly)
		{
			LOG1(LOG_FILE, TXT("Cannot write to %s since it's read only."), CurrentFile);
			return false;
		}

		bool bResult = false;

		if (TextBuffer.size() <= 0)
		{
			return true; //Nothing to copy
		}

		if (File.is_open())
		{
			for (unsigned int i = 0; i < TextBuffer.size() - 1; i++)
			{
				File << TextBuffer.at(i).String + "\n";
			}

			File << TextBuffer.at(TextBuffer.size() - 1).String << TextToWriteAfterFlush().String;

			File.flush();
			bResult = (!File.bad());
		}

		if (bResult || bClearBufferIfFailed)
		{
			TextBuffer.clear();
		}

		return bResult;
	}

	bool TextFileWriter::EmptyFile ()
	{
		if (bReadOnly)
		{
			LOG1(LOG_FILE, TXT("Cannot empty %s since it's read only."), CurrentFile);
			return false;
		}

		bool bOpenFile = false;

		if (File.is_open())
		{
			bOpenFile = true;
			File.close();
		}

		remove(CurrentFile.ToCString());

		//Reopen files
		if (bOpenFile)
		{
			File.open(CurrentFile.ToCString(), fstream::in | fstream::out | ofstream::app);
		}

		return true;
	}

	bool TextFileWriter::ReadLine (DString& outCurrentLine)
	{
		if (File.eof())
		{
			return false;
		}

		getline(File, outCurrentLine.String);
		return true;
	}

	bool TextFileWriter::IsFileOpened () const
	{
		return File.is_open();
	}

	void TextFileWriter::SetBufferLimit (unsigned int newBufferLimit)
	{
		BufferLimit = newBufferLimit;

		if (TextBuffer.size() >= BufferLimit)
		{
			WriteToFile(true);
		}
	}

	void TextFileWriter::SetReadOnly (bool bNewReadOnly)
	{
		if (bNewReadOnly)
		{
			WriteToFile(true);
		}

		bReadOnly = bNewReadOnly;
	}

	unsigned int TextFileWriter::GetBufferLimit () const
	{
		return BufferLimit;
	}

	DString TextFileWriter::GetCurrentFileName () const
	{
		return CurrentFile;
	}

	bool TextFileWriter::GetReadOnly () const
	{
		return bReadOnly;
	}

	DString TextFileWriter::TextToWriteAfterFlush () const
	{
		return TXT("\n");
	}

	void TextFileWriter::ResetSeekPosition ()
	{
		//Clear any flags that may prevent seeking (ie:  EOF flag)
		File.clear();

		//Set file position back to the beginning
		File.seekg(0);
	}
}

#endif