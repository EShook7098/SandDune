/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileIterator.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "FileClasses.h"

#if INCLUDE_FILE

using namespace std;

namespace SD
{
	const INT FileIterator::INCLUDE_FILES = 1;
	const INT FileIterator::INCLUDE_DIRECTORIES = 2;
	const INT FileIterator::ITERATE_SUBDIRECTORIES = 4;
	const INT FileIterator::INCLUDE_REFLECTIVE_DIRECTORIES = 8;
	const INT FileIterator::INCLUDE_EVERYTHING = INCLUDE_FILES | INCLUDE_DIRECTORIES | ITERATE_SUBDIRECTORIES | INCLUDE_REFLECTIVE_DIRECTORIES;

	FileIterator::FileIterator ()
	{
		PreviousExpandedDirectory = TXT("");
		Directory = TXT("");
		SearchFlags = INCLUDE_FILES | INCLUDE_DIRECTORIES | ITERATE_SUBDIRECTORIES;
		SelectFirstFile();
	}

	FileIterator::FileIterator (const DString& newDirectory)
	{
		PreviousExpandedDirectory = TXT("");
		Directory = newDirectory;
		SearchFlags = INCLUDE_FILES | INCLUDE_DIRECTORIES | ITERATE_SUBDIRECTORIES;
		SelectFirstFile();
	}

	FileIterator::FileIterator (const DString& newDirectory, INT searchFlags)
	{
		PreviousExpandedDirectory = TXT("");
		Directory = newDirectory;
		SearchFlags = searchFlags;
		SelectFirstFile();
	}

	FileIterator::FileIterator (const FileIterator& otherFileIterator)
	{
		PreviousExpandedDirectory = TXT("");
		Directory = otherFileIterator.Directory;
		SearchFlags = otherFileIterator.SearchFlags;
		SelectFirstFile();
	}

	FileIterator::~FileIterator ()
	{
		if (SubIterator != nullptr)
		{
			delete SubIterator;
			SubIterator = nullptr;
		}

#ifdef PLATFORM_WINDOWS
		if (InitialFile != nullptr)
		{
			FindClose(InitialFile);
			InitialFile = nullptr;
		}
#else
		OS_CloseFile(InitialFile);
#endif
	}

	void FileIterator::operator++ ()
	{
		SelectNextFile();
	}

	void FileIterator::operator++ (int)
	{
		SelectNextFile();
	}

	FileAttributes FileIterator::RetrieveFileAttributes () const
	{
		return FileAttributes(GetSelectedAttributes());
	}

	bool FileIterator::IsDirectorySelected () const
	{
		return (SubIterator != nullptr) ? SubIterator->IsDirectorySelected() : !SelectedDirectory.IsEmpty();
	}

	bool FileIterator::FinishedIterating () const
	{
		return (SubIterator == nullptr && SelectedDirectory.IsEmpty() && !SelectedAttributes.GetFileExists());
	}

	PrimitiveFileAttributes FileIterator::GetSelectedAttributes () const
	{
		return (SubIterator != nullptr) ? SubIterator->GetSelectedAttributes() : SelectedAttributes;
	}

	DString FileIterator::GetSelectedDirectory () const
	{
		return (SubIterator != nullptr) ? SelectedDirectory + DIR_SEPARATOR + SubIterator->GetSelectedDirectory() : SelectedDirectory;
	}

	void FileIterator::SelectFirstFile ()
	{
		Directory = FileUtils::ToAbsolutePath(Directory);

		SelectedDirectory = TXT("");
		SelectedAttributes = PrimitiveFileAttributes();
		SubIterator = nullptr;

#ifdef PLATFORM_WINDOWS
		/**
		Since Windows has a terrible implementation for file iterators (ie:  Inconsistent params and return
		values for FindFirstFile and FindNextFile, and being overly dependant on MS-specific structs and
		flags); it's cleaner to have the iterator handle the file selection than extracting functionality
		to platform independent utility functions, which was implemented earlier (but was messy).
	    */
		DString searchString = Directory + TXT("\\*");
		InitialFile = FindFirstFile(searchString.ToCString(), &WindowsFindData);
		if (InitialFile == INVALID_HANDLE_VALUE)
		{
			return; //Nothing was found
		}

		if (WindowsFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if ((SearchFlags & INCLUDE_DIRECTORIES) == 0)
			{
				SelectNextFile(); //Skip directories
				return;
			}

			if ((SearchFlags & INCLUDE_REFLECTIVE_DIRECTORIES) == 0 && OS_IsDirectoryReflection(DString(WindowsFindData.cFileName)))
			{
				SelectNextFile(); //Skip reflective directories
				return;
			}

			SelectedDirectory = WindowsFindData.cFileName;
			return;
		}
		else
		{
			if ((SearchFlags & INCLUDE_FILES) == 0)
			{
				SelectNextFile(); //Skip files
				return;
			}

			SelectedAttributes = PrimitiveFileAttributes(InitialFile);
		}
#else
	#error Please implement SelectFirstFile for your platform.
#endif
	}

	void FileIterator::SelectNextFile ()
	{ 
		if (SubIterator != nullptr)
		{
			SubIterator->SelectNextFile();
			if (!SubIterator->FinishedIterating())
			{
				return;
			}

			//finished iterating through sub directory.  Carry on with this iterator
			delete SubIterator;
			SubIterator = nullptr;
		}

		//Check if it needs to search sub directory.  This is implemented outside of loop to have the parent directory listed before subdirectories.
		if ((SearchFlags & ITERATE_SUBDIRECTORIES) > 0 && !SelectedDirectory.IsEmpty() && SelectedDirectory != PreviousExpandedDirectory && !OS_IsDirectoryReflection(SelectedDirectory))
		{
			//File iterator found a directory that wasn't investigated yet.
			SubIterator = new FileIterator(Directory + SelectedDirectory, SearchFlags);
			PreviousExpandedDirectory = SelectedDirectory;
			return; //Using sub iterator until it finishes
		}

#ifdef PLATFORM_WINDOWS
		if (FindNextFile(InitialFile, &WindowsFindData))
		{
			if (WindowsFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if ((SearchFlags & INCLUDE_DIRECTORIES) == 0)
				{
					if ((SearchFlags & ITERATE_SUBDIRECTORIES) > 0 && !OS_IsDirectoryReflection(DString(WindowsFindData.cFileName)))
					{
						SubIterator = new FileIterator(Directory + WindowsFindData.cFileName, SearchFlags);
						return;
					}

					SelectNextFile(); //Skip directories
					return;
				}

				if ((SearchFlags & INCLUDE_REFLECTIVE_DIRECTORIES) == 0 && OS_IsDirectoryReflection(DString(WindowsFindData.cFileName)))
				{
					SelectNextFile(); //Skip reflective directories
					return;
				}

				SelectedAttributes = PrimitiveFileAttributes();
				SelectedDirectory = WindowsFindData.cFileName;
			}
			else
			{
				if ((SearchFlags & INCLUDE_FILES) == 0)
				{
					SelectNextFile(); //Skip files
					return;
				}

				SelectedAttributes = PrimitiveFileAttributes(Directory, WindowsFindData.cFileName);
				SelectedDirectory.Clear();
			}
		}
		else //Nothing is found
		{
			SelectedDirectory.Clear();
			SelectedAttributes = PrimitiveFileAttributes();
		}
#else
		#error Please implement SelectNextFile for your platform.
#endif
	}
}

#endif