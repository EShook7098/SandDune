/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PrimitiveFileAttributes.h
  Cross-platform file object that essentially contains basic
  attributes about the OS's file.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef PRIMITIVEFILEATTRIBUTES_H
#define PRIMITIVEFILEATTRIBUTES_H

#include "File.h"

#if INCLUDE_FILE

namespace SD
{
	class FileAttributes;

	class PrimitiveFileAttributes : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Name of the file without the extension. */
		DString FileName;

		/* Location this file resides. Path could either be
		relative to the engine's location or absolute. */
		DString Path;

		/* File name extension without the '.' character. */
		DString FileExtension;

		/* Becomes true if these set of attributes are describing a file that exists. */
		bool bFileExists;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		PrimitiveFileAttributes ();
		PrimitiveFileAttributes (const FilePtr& fileHandle);
		PrimitiveFileAttributes (const DString& relativeDirectory, const DString& fullFileName);
		PrimitiveFileAttributes (const PrimitiveFileAttributes& attributesCopy);
		PrimitiveFileAttributes (const FileAttributes& attributesCopy);

		virtual ~PrimitiveFileAttributes ();


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual DString ToString () const override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Displays the file name with extension.  This excludes the path.
		 */
		virtual DString GetFullFileName () const;

		/**
		  Clears all property values for these attributes.
		 */
		virtual void ClearValues ();

		/**
		  Updates attributes to describe the file that matches the newly specified address.
		  Returns true if the file exists.
		 */
		virtual bool SetFileName (const DString& path, const DString& fullFileName);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual DString GetFileName () const;
		virtual DString GetFileExtension () const;
		virtual DString GetPath () const;
		virtual bool GetFileExists () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Checks the target directory to see if the file exists.  If the file exists, then the properties
		  of this object will be filled accordingly.  This does not create a file.
		  Returns true if the file exists.
		 */
		virtual void PopulateFileInfo (); 

		/**
		  Populates property values from the given file pointer.
		 */
		virtual void PopulateFileInfoFromHandle (FilePtr handle);

		/**
		  Handle that's invoked when it failed to find a file.
		 */
		virtual void HandleMissingFile ();
	};
}

#endif
#endif