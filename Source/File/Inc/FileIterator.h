/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileIterator.h
  The FileIterator is an utility class that makes it easy to iterate
  through files and directories within a directory.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef FILEITERATOR_H
#define FILEITERATOR_H

#include "File.h"

#if INCLUDE_FILE

namespace SD
{
	class FileIterator
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Flag to determine if the FileIterator should select files. */
		static const INT INCLUDE_FILES;

		/* Flag to determine if the FileIterator should select directories. */
		static const INT INCLUDE_DIRECTORIES;

		/* Flag to determine if the FileIterator should select OS objects within sub directories. */
		static const INT ITERATE_SUBDIRECTORIES;

		/* Flag to determine if FileIterator should include reflective directories such as current and parent directory. */
		static const INT INCLUDE_REFLECTIVE_DIRECTORIES;

		/* If set, then the FileIterator will find everything. */
		static const INT INCLUDE_EVERYTHING;

		/**
		  Directory the iterator is searching through.  Relative Directories
		  are automatically converted to absolute paths.
		 */
		DString Directory;

		/* Various bit-wise flags this iterator uses to determine if an OS object should be selected or not. */
		INT SearchFlags;

	protected:
		/**
		  Attributes describing the current OS object (could either be a directory or a file.
		 */
		PrimitiveFileAttributes SelectedAttributes;

		/**
		  If the iterator currently selected a directory, then this field is set to the selected directory.
		 */
		DString SelectedDirectory;

		/**
		  Some OS like Windows does not seem to support recursive searches.  So the iterator creates iterators
		  when iterating through files within subdirectories.  This value is not used if SearchFlags does not include subdirectories.
		 */
		FileIterator* SubIterator;

#ifdef PLATFORM_WINDOWS
		/* Windows-specific helper to iterate through files. */
		WIN32_FIND_DATA WindowsFindData;

		/**
		  Windows needs a handle to the initial file for some undocumented reason.
		  My guess is that Windows cache information to the handle for the FindNextFile to reference later.
		  For example the wild card character "\\*" must have been cached somewhere.  Was it cached in the handle, itself?  ::rolls eyes::
		 */
		FilePtr InitialFile;
#endif

	private:
		/* Name of the most recent directory that expanded into another FileIterator.
		Used to determine if this iterator needs to create a subiterator for a directory. */
		DString PreviousExpandedDirectory;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		FileIterator ();
		FileIterator (const DString& newDirectory);
		FileIterator (const DString& newDirectory, const INT searchFlags);
		FileIterator (const FileIterator& otherFileIterator);
		virtual ~FileIterator ();


		/*
		=====================
		  Operators
		=====================
		*/

	public:
		virtual void operator++ (); //++iter
		virtual void operator++ (int); //iter++


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Retrieves additional information about the selected file.
		 */
		virtual FileAttributes RetrieveFileAttributes () const;

		/**
		  Returns true if the iterator (or its subiterator) selected a directory.
		 */
		virtual bool IsDirectorySelected () const;

		/**
		  Returns true, if the iterator can no longer find another file or a directory.
		 */
		virtual bool FinishedIterating () const;


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		/**
		  Returns the current selected file.  This may return the SubIterator's SelectedFile
		  if the FileIterator is currently searching through a subdirectory.
		 */
		virtual PrimitiveFileAttributes GetSelectedAttributes () const;

		/**
		  Returns the current selected directory.  This may return the SubIterator's
		  SelectedDirectory if the FileIterator is currently searching through a subdirectory.
		 */
		virtual DString GetSelectedDirectory () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Selects the first file within the directory.  The path is relative to the FileIterator's Directory property.
		 */
		virtual void SelectFirstFile ();

		/**
		  Selects the next available file.  If there aren't any more files, then it'll return the next
		  file within another directory if sub directories are relevant.
		 */
		virtual void SelectNextFile ();
	};
}

#endif
#endif