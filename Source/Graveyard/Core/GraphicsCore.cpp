/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsCore.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	bool GraphicsCore::bInitialized = false;
	sf::RenderWindow* GraphicsCore::WindowHandle = nullptr;
	const float GraphicsCore::BackBufferResetColor[4] = {0.f, 0.f, 0.f, 0.f};
	INT GraphicsCore::WindowHeight = INT(1);
	INT GraphicsCore::WindowWidth = INT(1);
	GraphicsCore* GraphicsCore::GraphicsInstance = new GraphicsCore();

	GraphicsCore::GraphicsCore ()
	{
	}

	void GraphicsCore::InitializeGraphicsEngine ()
	{
#ifdef DEBUG_MODE
		LOG(LOG_DEBUG, TXT("Initializing Graphics Engine. . ."));
#endif

		if (bInitialized)
		{
			LOG(LOG_WARNING, TXT("Attempting to initialize the graphics engine more than once!  Denying call to initialize graphics engine."));
			return;
		}

		//obtain window dimensions
		sf::Vector2u windowSize = WindowHandle->getSize();
		WindowHeight = windowSize.y;
		WindowWidth = windowSize.x;

		//Create the various resource pools
		TexturePool::CreateObject();
		FontPool::CreateObject();

		//Create the main rendertarget
		PrimaryRenderTarget = dynamic_cast<RenderTarget*>(RenderTarget::CreateObject());
		PrimaryRenderTarget->SetAssociatedCamera(Entity::Spawn<Camera>());

		bInitialized = true;
	}

	void GraphicsCore::RegisterResourcePool (ResourcePool* newResourcePool)
	{
		RegisteredResourcePools.push_back(newResourcePool);
	}

	void GraphicsCore::RegisterRenderTarget (RenderTarget* newRenderTarget)
	{
		if (!newRenderTarget)
		{
			LOG(LOG_WARNING, TXT("Attempting to register a null render target to the graphics engine."));
			return;
		}

		//make sure this render target is not already in the render list
		for (unsigned int i = 0; i < RenderTargetList.size(); i++)
		{
			if (RenderTargetList.at(i).AssociatedRenderTarget == newRenderTarget)
			{
				LOG1(LOG_WARNING, TXT("Attempting to register %s to the graphics engine when it's already registered."), newRenderTarget->GetUniqueName());
				return;
			}
		}

		SRenderTargetInfo newRenderTargetInfo;
		newRenderTargetInfo.AssociatedRenderTarget = newRenderTarget;
		float frameRateDuration = (newRenderTarget->GetFrameRate() > 0) ? (1.f / newRenderTarget->GetFrameRate().ToFLOAT().Value) : 0;
		newRenderTargetInfo.TargetFrameRate = frameRateDuration;
		newRenderTargetInfo.TimeRemaining = frameRateDuration;
		RenderTargetList.push_back(newRenderTargetInfo);
	}

	void GraphicsCore::UnregisterRenderTarget (RenderTarget* target)
	{
		for (unsigned int i = 0; i < RenderTargetList.size(); i++)
		{
			if (RenderTargetList.at(i).AssociatedRenderTarget == target)
			{
				RenderTargetList.erase(RenderTargetList.begin() + i);
				break;
			}
		}
	}

	void GraphicsCore::BroadcastResolutionChange ()
	{
		INT oldResX = WindowWidth;
		INT oldResY = WindowHeight;

		sf::Vector2u windowSize = WindowHandle->getSize();
		WindowHeight = windowSize.y;
		WindowWidth = windowSize.x;

		for (ObjectIterator iter; iter.SelectedObject; iter++)
		{
			if (dynamic_cast<WindowHandleInterface*>(iter.SelectedObject))
			{
				dynamic_cast<WindowHandleInterface*>(iter.SelectedObject)->HandleResolutionChange(oldResX, oldResY);
			}
		}
	}

	void GraphicsCore::UpdateRenderTargetFrameRate (RenderTarget* renderTarget, bool bResetElapsedTime)
	{
		//safety
		if (!VALID_OBJECT(renderTarget))
		{
			LOG(LOG_WARNING, TXT("Attempting to update a render target's frame rate to the graphics engine when a null pointer was given."));
			return;
		}

		int renderTargetListIndex = -1;

		//Find the render target within the list
		for (unsigned int i = 0; i < RenderTargetList.size(); i++)
		{
			if (RenderTargetList.at(i).AssociatedRenderTarget == renderTarget)
			{
				renderTargetListIndex = i;
				break;
			}
		}

		if (renderTargetListIndex < 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to update %s frame rate to the graphics engine before it was registered."), renderTarget->GetUniqueName());
			return;
		}

		FLOAT frameRateDuration = (renderTarget->GetFrameRate() > 0) ? (1.f / renderTarget->GetFrameRate().ToFLOAT()) : 0;
		RenderTargetList.at(renderTargetListIndex).TargetFrameRate = frameRateDuration;

		if (bResetElapsedTime)
		{
			RenderTargetList.at(renderTargetListIndex).TimeRemaining = frameRateDuration;
		}
	}

	GraphicsCore* GraphicsCore::GetGraphicsCore ()
	{
		return GraphicsInstance;
	}

	void GraphicsCore::GetWindowSize (INT& outWidth, INT& outHeight)
	{
		outWidth = WindowWidth;
		outHeight = WindowHeight;
	}

	void GraphicsCore::GetWindowPosition (INT& outPosX, INT& outPosY, bool bIncludeTitlebar)
	{
		if (!bIncludeTitlebar)
		{
			Rectangle clientWindow = OS_GetClientWindowPos();

			outPosX = clientWindow.Left.ToINT();
			outPosY = clientWindow.Top.ToINT();
			return;
		}

		sf::Vector2i windowPos = WindowHandle->getPosition();

		outPosX = windowPos.x;
		outPosY = windowPos.y;
	}

	const RenderTarget* GraphicsCore::GetPrimaryRenderTarget ()
	{
		return PrimaryRenderTarget;
	}

	void GraphicsCore::Tick (FLOAT deltaTime)
	{
		//Update RenderTargetList and generate any scenes if necessary
		for (unsigned int i = 0; i < RenderTargetList.size(); i++)
		{
			if (RenderTargetList.at(i).TargetFrameRate >= 0)
			{
				RenderTargetList.at(i).TimeRemaining -= deltaTime;
			
				if (RenderTargetList.at(i).TimeRemaining <= 0.f)
				{
					//PreRenderFrame();  Removed for now until I come up with a good solution for split screen.  Also maxframerate is not supported yet
					RenderTargetList.at(i).AssociatedRenderTarget->GenerateScene();
					RenderTargetList.at(i).TimeRemaining = RenderTargetList.at(i).TargetFrameRate;
					//PostRenderFrame();
				}
			}
		}
	}

	void GraphicsCore::PreRenderFrame ()
	{
		WindowHandle->clear(sf::Color::Black);
	}

	void GraphicsCore::PostRenderFrame ()
	{
		WindowHandle->display();
	}

	void GraphicsCore::Shutdown ()
	{

	}
}

#endif