/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIAttributes.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/


#include "GUIClasses.h"

#if INCLUDE_GUI

namespace SD
{
	GUIAttributes::GUIAttributes (bool bNewAutoClamp)
	{
		bAutoClamp = bNewAutoClamp;

		OwningComponent = nullptr;
		MinSizeX = 1;
		MinSizeY = 1;
		AbsSizeX = 1;
		AbsSizeY = 1;

		MaxPositionX = INT_MAX;
		MaxPositionY = INT_MAX;
		MaxSizeX = INT_MAX;
		MaxSizeY = INT_MAX;

		AutoMinPositionX = 0;
		AutoMaxPositionX = INT_MAX;
		AutoMinPositionY = 0;
		AutoMaxPositionY = INT_MAX;
		AutoMaxSizeX = INT_MAX;
		AutoMaxSizeY = INT_MAX;
	}

	GUIAttributes::GUIAttributes (GUIComponent* newOwningComponent)
	{
		bAutoClamp = true;

		OwningComponent = newOwningComponent;
		MinSizeX = 1;
		MinSizeY = 1;
		AbsSizeX = 1;
		AbsSizeY = 1;

		MaxPositionX = INT_MAX;
		MaxPositionY = INT_MAX;
		MaxSizeX = INT_MAX;
		MaxSizeY = INT_MAX;

		AutoMinPositionX = 0;
		AutoMaxPositionX = INT_MAX;
		AutoMinPositionY = 0;
		AutoMaxPositionY = INT_MAX;
		AutoMaxSizeX = INT_MAX;
		AutoMaxSizeY = INT_MAX;
	}

	GUIAttributes::GUIAttributes (const GUIAttributes& copyObject)
	{
		bool bAutoClamp = copyObject.bAutoClamp;

		OwningComponent = copyObject.OwningComponent;
		AbsPositionX = copyObject.AbsPositionX;
		AbsPositionY = copyObject.AbsPositionY;
		RelPositionX = copyObject.RelPositionX;
		RelPositionY = copyObject.RelPositionY;
		AbsSizeX = copyObject.AbsSizeX;
		AbsSizeY = copyObject.AbsSizeY;
		RelSizeX = copyObject.RelSizeX;
		RelSizeY = copyObject.RelSizeY;
		MinPositionX = copyObject.MinPositionX;
		MaxPositionX = copyObject.MaxPositionX;
		MinPositionY = copyObject.MinPositionY;
		MaxPositionY = copyObject.MaxPositionY;
		MinSizeX = copyObject.MinSizeX;
		MaxSizeX = copyObject.MaxSizeX;
		MinSizeY = copyObject.MinSizeY;
		MaxSizeY = copyObject.MaxSizeY;

		AutoMinPositionX = copyObject.AutoMinPositionX;
		AutoMaxPositionX = copyObject.AutoMaxPositionX;
		AutoMinPositionY = copyObject.AutoMinPositionY;
		AutoMaxPositionY = copyObject.AutoMaxPositionY;
		AutoMaxSizeX = copyObject.AutoMaxSizeX;
		AutoMaxSizeY = copyObject.AutoMaxSizeY;
	}

	GUIAttributes::~GUIAttributes ()
	{
	}

	void GUIAttributes::SetOwningComponent (GUIComponent* newOwningComponent)
	{
		OwningComponent = newOwningComponent;
		
		ApplyAutoClampValues();
		RecalculateRelativeValues();
	}

	void GUIAttributes::SetMinPositionX (const INT newMinPos)
	{
		MinPositionX = Utils::Min(newMinPos, MaxPositionX);

		if (AbsPositionX < MinPositionX)
		{
			OwningComponent->SetAbsoluteSize(AbsPositionX, AbsPositionY);
		}
	}

	void GUIAttributes::SetMaxPositionX (const INT newMaxPos)
	{
		MaxPositionX = Utils::Max(newMaxPos, MinPositionX);

		if (AbsPositionX > MaxPositionX)
		{
			OwningComponent->SetAbsoluteSize(AbsPositionX, AbsPositionY);
		}
	}

	void GUIAttributes::SetMinPositionY (const INT newMinPos)
	{
		MinPositionY = Utils::Min(newMinPos, MaxPositionY);

		if (AbsPositionY < MinPositionY)
		{
			OwningComponent->SetAbsoluteSize(AbsPositionX, AbsPositionY);
		}
	}

	void GUIAttributes::SetMaxPositionY (const INT newMaxPos)
	{
		MaxPositionY = Utils::Max(newMaxPos, MinPositionY);

		if (AbsPositionY > MaxPositionY)
		{
			OwningComponent->SetAbsoluteSize(AbsPositionX, AbsPositionY);
		}
	}

	void GUIAttributes::SetMinSizeX (const INT newMinSize)
	{
		MinSizeX = Utils::Min(newMinSize, MaxSizeX);

		if (AbsSizeX < MinSizeX)
		{
			OwningComponent->SetAbsoluteSize(AbsSizeX, AbsSizeY);
		}
	}

	void GUIAttributes::SetMaxSizeX (const INT newMaxSize)
	{
		MaxSizeX = Utils::Max(newMaxSize, MinSizeX);

		if (AbsSizeX > MaxSizeX)
		{
			OwningComponent->SetAbsoluteSize(AbsSizeX, AbsSizeY);
		}
	}

	void GUIAttributes::SetMinSizeY (const INT newMinSize)
	{
		MinSizeY = Utils::Min(newMinSize, MaxSizeY);

		if (AbsSizeY < MinSizeY)
		{
			OwningComponent->SetAbsoluteSize(AbsSizeX, AbsSizeY);
		}
	}

	void GUIAttributes::SetMaxSizeY (const INT newMaxSize)
	{
		MaxSizeY = Utils::Max(newMaxSize, MinSizeY);

		if (AbsSizeY > MaxSizeY)
		{
			OwningComponent->SetAbsoluteSize(AbsSizeX, AbsSizeY);
		}
	}

	void GUIAttributes::ApplyAutoClampValues ()
	{
		if (!bAutoClamp)
		{
			return;
		}

		if (!GraphicsEngineComponent::Get()->GetRenderWindow())
		{
			//The window handle is not yet initialized.  use defaults
			AutoMinPositionX = 0;
			AutoMaxPositionX = INT_MAX;
			AutoMinPositionY = 0;
			AutoMaxPositionY = INT_MAX;
			AutoMaxSizeX = INT_MAX;
			AutoMaxSizeY = INT_MAX;

			return;
		}

		GUIAttributes parentAttributes;
		GetComponentsOwnerAttributes(parentAttributes);

		AutoMinPositionX = parentAttributes.GetAbsPositionX();
		AutoMaxPositionX = parentAttributes.GetRight() - AbsSizeX;
		AutoMinPositionY = parentAttributes.GetAbsPositionY();
		AutoMaxPositionY = parentAttributes.GetBottom() - AbsSizeY;
		AutoMaxSizeX = parentAttributes.GetRight() - AbsPositionX;
		AutoMaxSizeY = parentAttributes.GetBottom() - AbsPositionY;
	}

	void GUIAttributes::RecalculateRelativeValues ()
	{
		CalculateRelPosition();
		CalculateRelSize();
	}

	void GUIAttributes::SetAutoClamp (bool bNewAutoClamp)
	{
		bAutoClamp = bNewAutoClamp;

		if (!bAutoClamp)
		{
			//Restore default auto clamp values
			AutoMinPositionX = 0;
			AutoMaxPositionX = INT_MAX;
			AutoMinPositionY = 0;
			AutoMaxPositionY = INT_MAX;
			AutoMaxSizeX = INT_MAX;
			AutoMaxSizeY = INT_MAX;
			return;
		}

		ApplyAutoClampValues();

		if (AbsPositionX < GetMinPositionX() || AbsPositionX > GetMaxPositionX() ||
			AbsPositionY < GetMinPositionY() || AbsPositionY > GetMaxPositionY())
		{
			//Apply new clamp values
			OwningComponent->SetAbsolutePosition(AbsPositionX, AbsPositionY);
		}

		if (AbsSizeX > GetMaxSizeX() || AbsSizeY > GetMaxSizeY())
		{
			//Apply new clamp values
			OwningComponent->SetAbsoluteSize(AbsSizeX, AbsSizeY);
		}
	}

	void GUIAttributes::GetComponentsOwnerAttributes (GUIAttributes& outAttributesCopy) const
	{
		if (OwningComponent)
		{
			const GUIComponent* targetComponent = dynamic_cast<GUIComponent*>(OwningComponent->GetOwner());
			if (targetComponent)
			{
				outAttributesCopy = targetComponent->Attributes;
				return;
			}
		}

		//Generate attributes based on application's window handle
		INT absSizeX;
		INT absSizeY;
		GraphicsEngineComponent::Get()->GetWindowSize(absSizeX, absSizeY);

		//Don't call set Abs Size/Position to avoid inf recursion
		outAttributesCopy.AbsSizeX = absSizeX;
		outAttributesCopy.AbsSizeY = absSizeY;
		outAttributesCopy.RelSizeX = 1.f;
		outAttributesCopy.RelSizeY = 1.f;
		outAttributesCopy.AbsPositionX = 0;
		outAttributesCopy.AbsPositionY = 0;
		outAttributesCopy.RelPositionX = 0.f;
		outAttributesCopy.RelPositionY = 0.f;
	}

	INT GUIAttributes::GetRight () const
	{
		return (AbsPositionX + AbsSizeX);
	}

	INT GUIAttributes::GetBottom () const
	{
		return (AbsPositionY + AbsSizeY);
	}

	bool GUIAttributes::GetAutoClamp () const
	{
		return bAutoClamp;
	}

	INT GUIAttributes::GetAbsPositionX () const
	{
		return AbsPositionX;
	}

	INT GUIAttributes::GetAbsPositionY () const
	{
		return AbsPositionY;
	}

	FLOAT GUIAttributes::GetRelPositionX () const
	{
		return RelPositionX;
	}

	FLOAT GUIAttributes::GetRelPositionY () const
	{
		return RelPositionY;
	}

	INT GUIAttributes::GetAbsSizeX () const
	{
		return AbsSizeX;
	}

	INT GUIAttributes::GetAbsSizeY () const
	{
		return AbsSizeY;
	}

	FLOAT GUIAttributes::GetRelSizeX () const
	{
		return RelSizeX;
	}

	FLOAT GUIAttributes::GetRelSizeY () const
	{
		return RelSizeY;
	}

	INT GUIAttributes::GetMinPositionX () const
	{
		return Utils::Max(MinPositionX, AutoMinPositionX);
	}

	INT GUIAttributes::GetMaxPositionX () const
	{
		return Utils::Min(MaxPositionX, AutoMaxPositionX);
	}

	INT GUIAttributes::GetMinPositionY () const
	{
		return Utils::Max(MinPositionY, AutoMinPositionY);
	}

	INT GUIAttributes::GetMaxPositionY () const
	{
		return Utils::Min(MaxPositionY, AutoMaxPositionY);
	}

	INT GUIAttributes::GetMinSizeX () const
	{
		return MinSizeX;
	}

	INT GUIAttributes::GetMaxSizeX () const
	{
		return Utils::Min(MaxSizeX, AutoMaxSizeX);
	}

	INT GUIAttributes::GetMinSizeY () const
	{
		return MinSizeY;
	}

	INT GUIAttributes::GetMaxSizeY () const
	{
		return Utils::Min(MaxSizeY, AutoMaxSizeY);
	}

	const GUIComponent* GUIAttributes::GetOwningComponent () const
	{
		return OwningComponent;
	}

	void GUIAttributes::SetAbsolutePosition (INT newPosX, INT newPosY)
	{
		newPosX = Utils::Clamp(newPosX, GetMinPositionX(), GetMaxPositionX());
		newPosY = Utils::Clamp(newPosY, GetMinPositionY(), GetMaxPositionY());

		AbsPositionX = newPosX;
		AbsPositionY = newPosY;

		CalculateRelPosition();
		ApplyAutoClampValues();
	}

	void GUIAttributes::SetRelativePosition (FLOAT newPosX, FLOAT newPosY)
	{
		RelPositionX = newPosX;
		RelPositionY = newPosY;

		CalculateAbsPosition();
		ApplyAutoClampValues();
	}

	void GUIAttributes::SetAbsoluteSize (INT newSizeX, INT newSizeY)
	{
		INT resultSizeX = Utils::Clamp(newSizeX, GetMinSizeX(), GetMaxSizeX());
		INT resultSizeY = Utils::Clamp(newSizeY, GetMinSizeY(), GetMaxSizeY());

		AbsSizeX = resultSizeX;
		AbsSizeY = resultSizeY;

		INT shiftX = newSizeX - resultSizeX;
		INT shiftY = newSizeY - resultSizeY;
		//TODO:  revert shift changes
		if (shiftX != 0 || shiftY != 0)
		{
			//This attributes changed its size due to clamp limits.  
			//Need to shift position by the amount changed to ensure the component did not scale up beyond its clamp boundaries
			//fails if user set abs size to 9999999
			//OwningComponent->SetAbsolutePosition(AbsPositionX - shiftX, AbsPositionY - shiftY);
		}

		CalculateRelSize();
		ApplyAutoClampValues();
	}

	void GUIAttributes::SetRelativeSize (FLOAT newSizeX, FLOAT newSizeY)
	{
		RelSizeX = newSizeX;
		RelSizeY = newSizeY;

		CalculateAbsSize();
		ApplyAutoClampValues();
	}

	void GUIAttributes::CalculateAbsPosition ()
	{
		GUIAttributes parentAttributes;
		GetComponentsOwnerAttributes(parentAttributes);

		AbsPositionX = ((parentAttributes.GetRight() - parentAttributes.AbsPositionX).ToFLOAT() * RelPositionX) + parentAttributes.AbsPositionX;
		AbsPositionY = ((parentAttributes.GetBottom() - parentAttributes.AbsPositionY).ToFLOAT() * RelPositionY) + parentAttributes.AbsPositionY;

		if (AbsPositionX < MinPositionX || AbsPositionX > MaxPositionX || AbsPositionY < MinPositionY || AbsPositionY > MaxPositionY)
		{
			//The current absolute position does not satisfy the clamp requirements, re-apply abs position to apply clamp.
			OwningComponent->SetAbsolutePosition(AbsPositionX, AbsPositionY);
		}
	}

	void GUIAttributes::CalculateRelPosition ()
	{
		GUIAttributes parentAttributes;
		GetComponentsOwnerAttributes(parentAttributes);

		INT xOffset = parentAttributes.AbsPositionX;
		INT yOffset = parentAttributes.AbsPositionY;

		RelPositionX = (AbsPositionX - xOffset).ToFLOAT() / (parentAttributes.GetRight() - xOffset).ToFLOAT();
		RelPositionY = (AbsPositionY - yOffset).ToFLOAT() / (parentAttributes.GetBottom() - yOffset).ToFLOAT();
	}

	void GUIAttributes::CalculateAbsSize ()
	{
		GUIAttributes parentAttributes;
		GetComponentsOwnerAttributes(parentAttributes);

		AbsSizeX = RelSizeX * parentAttributes.AbsSizeX;
		AbsSizeY = RelSizeY * parentAttributes.AbsSizeY;

		if (AbsSizeX < MinSizeX || AbsSizeX > MaxSizeX || AbsSizeY < MinSizeY || AbsSizeY > MaxSizeY)
		{
			//The current absolute size does not satisfy the clamp requirements, re-apply abs size to apply clamp.
			SetAbsoluteSize(AbsSizeX, AbsSizeY);
		}
	}

	void GUIAttributes::CalculateRelSize ()
	{
		GUIAttributes parentAttributes;
		GetComponentsOwnerAttributes(parentAttributes);

		RelSizeX = AbsSizeX.ToFLOAT() / parentAttributes.AbsSizeX.ToFLOAT();
		RelSizeY = AbsSizeY.ToFLOAT() / parentAttributes.AbsSizeY.ToFLOAT();
	}

	bool operator== (const GUIAttributes& left, const GUIAttributes& right)
	{
		//Return true if the SizeX, SizeY, PosX, and PosY are equal
		return (left.GetAbsSizeX() == right.GetAbsSizeX() && left.GetAbsSizeY() == right.GetAbsSizeY() &&
				left.GetAbsPositionX() == right.GetAbsPositionX() && left.GetAbsPositionY() == right.GetAbsPositionY());
	}

	bool operator!= (const GUIAttributes& left, const GUIAttributes& right)
	{
		return !(left == right);
	}
}

#endif