/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FrameRenderComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

using namespace std;

namespace SD
{
	Texture* FrameRenderComponent::DefaultTopBorder = nullptr;
	Texture* FrameRenderComponent::DefaultRightBorder = nullptr;
	Texture* FrameRenderComponent::DefaultBottomBorder = nullptr;
	Texture* FrameRenderComponent::DefaultLeftBorder = nullptr;

	Texture* FrameRenderComponent::DefaultTopRightCorner = nullptr;
	Texture* FrameRenderComponent::DefaultBottomRightCorner = nullptr;
	Texture* FrameRenderComponent::DefaultBottomLeftCorner = nullptr;
	Texture* FrameRenderComponent::DefaultTopLeftCorner = nullptr;

	IMPLEMENT_CLASS(FrameRenderComponent, RenderComponent)

	void FrameRenderComponent::InitializeDefaultProperties ()
	{
		Super::InitializeDefaultProperties();

		TopBorder = DefaultTopBorder;
		RightBorder = DefaultRightBorder;
		BottomBorder = DefaultBottomBorder;
		LeftBorder = DefaultLeftBorder;
		TopRightCorner = DefaultTopRightCorner;
		BottomRightCorner = DefaultBottomRightCorner;
		BottomLeftCorner = DefaultBottomLeftCorner;
		TopLeftCorner = DefaultTopLeftCorner;

		//FillTexture = nullptr;
		//TODO:  Remove FillTexture texture test (back to nullptr)
		FillTexture = Texture::CreateTexture(Engine::GetBaseDirectory() + "Textures\\TextureTest.jpg", "TextureTest");
		FillColor = sf::Color(255, 255, 255);

		OwningFrameComponent = nullptr;
	}

	void FrameRenderComponent::InitializeProperties (const Object* copyObject)
	{
		Super::InitializeProperties(copyObject);

		const FrameRenderComponent* castCopyObject = dynamic_cast<const FrameRenderComponent*>(copyObject);
		if (castCopyObject != nullptr)
		{
			TopBorder = castCopyObject->TopBorder;
			RightBorder = castCopyObject->RightBorder;
			BottomBorder = castCopyObject->BottomBorder;
			LeftBorder = castCopyObject->LeftBorder;
			TopRightCorner = castCopyObject->TopRightCorner;
			BottomRightCorner = castCopyObject->BottomRightCorner;
			BottomLeftCorner = castCopyObject->BottomLeftCorner;
			TopLeftCorner = castCopyObject->TopLeftCorner;

			FillTexture = castCopyObject->FillTexture;
			FillColor = castCopyObject->FillColor;

			if (VALID_OBJECT_CAST(Owner, FrameComponent*))
			{
				OwningFrameComponent = dynamic_cast<FrameComponent*>(Owner);
			}
			else
			{
				OwningFrameComponent = nullptr;
			}
		}
	}

	bool FrameRenderComponent::SetOwner (Entity* newOwner)
	{
		//Only accepts a FrameComponent as owner
		FrameComponent* castedNewOwner = dynamic_cast<FrameComponent*>(newOwner);

		if (VALID_OBJECT(castedNewOwner) && Super::SetOwner(newOwner))
		{
			OwningFrameComponent = castedNewOwner;
			return true;
		}
		
		Warn(newOwner->GetName() + " cannot be the owner of " + GetName() + ".  FrameRenderComponents only expects FrameComponents.");

		return false;
	}

	void FrameRenderComponent::Render (const Camera* targetCamera)
	{
		if (!VALID_OBJECT(OwningFrameComponent))
		{
			return;
		}

		INT iPosX;
		INT iPosY;
		INT iScaleX;
		INT iScaleY;
		FLOAT borderThickness = FLOAT(OwningFrameComponent->GetBorderThickness().ToFLOAT());

		OwningFrameComponent->GetAbsolutePosition(iPosX, iPosY);
		OwningFrameComponent->GetAbsoluteSize(iScaleX, iScaleY);

		FLOAT posX = iPosX.ToFLOAT();
		FLOAT posY = iPosY.ToFLOAT();
		FLOAT scaleX = iScaleX.ToFLOAT();
		FLOAT scaleY = iScaleY.ToFLOAT();

		//Draw the fill first to ensure it's rendered behind the border
		if (FillTexture != nullptr)
		{
			sf::Sprite fillSprite;
			fillSprite.setTexture(*(FillTexture->GetTextureResource()));
			fillSprite.setPosition(posX.Value, posY.Value);
			fillSprite.setScale(scaleX.Value/(float)FillTexture->GetWidth(), scaleY.Value/(float)FillTexture->GetHeight());
			GraphicsCore::WindowHandle->draw(fillSprite);
		}
		else
		{
			sf::RectangleShape background(sf::Vector2f(scaleX.Value, scaleY.Value));
			background.setPosition(sf::Vector2f(posX.Value, posY.Value));
			background.setFillColor(FillColor);
			GraphicsCore::WindowHandle->draw(background);
		}

		//Draw borders
		if (TopBorder != nullptr)
		{
			sf::Sprite topSprite;
			topSprite.setTexture(*(TopBorder->GetTextureResource()));
			topSprite.setPosition(posX.Value, posY.Value);
			topSprite.setScale(scaleX.Value/(float)TopBorder->GetWidth(), 1.f);
			GraphicsCore::WindowHandle->draw(topSprite);
		}

		if (RightBorder != nullptr)
		{
			sf::Sprite rightSprite;
			rightSprite.setTexture(*(RightBorder->GetTextureResource()));
			rightSprite.setPosition(FLOAT(posX + scaleX - borderThickness).Value, posY.Value);
			rightSprite.setScale(1.f, scaleY.Value/(float)RightBorder->GetHeight());
			GraphicsCore::WindowHandle->draw(rightSprite);
		}

		if (BottomBorder != nullptr)
		{
			sf::Sprite bottomSprite;
			bottomSprite.setTexture(*(BottomBorder->GetTextureResource()));
			bottomSprite.setPosition(posX.Value, FLOAT(posY + scaleY - borderThickness).Value);
			bottomSprite.setScale(scaleX.Value/(float)BottomBorder->GetWidth(), 1.f);
			GraphicsCore::WindowHandle->draw(bottomSprite);
		}

		if (LeftBorder != nullptr)
		{
			sf::Sprite leftSprite;
			leftSprite.setTexture(*(LeftBorder->GetTextureResource()));
			leftSprite.setPosition(posX.Value, posY.Value);
			leftSprite.setScale(1.f, scaleY.Value/(float)LeftBorder->GetHeight());
			GraphicsCore::WindowHandle->draw(leftSprite);
		}

		//Draw corners
		if (TopRightCorner != nullptr)
		{
			sf::Sprite topRightSprite;
			topRightSprite.setTexture(*(TopRightCorner->GetTextureResource()));
			topRightSprite.setPosition(FLOAT(posX + scaleX - borderThickness).Value, posY.Value);
			GraphicsCore::WindowHandle->draw(topRightSprite);
		}

		if (BottomRightCorner != nullptr)
		{
			sf::Sprite bottomRightSprite;
			bottomRightSprite.setTexture(*(BottomRightCorner->GetTextureResource()));
			bottomRightSprite.setPosition(FLOAT(posX + scaleX - borderThickness).Value, FLOAT(posY + scaleY - borderThickness).Value);
			GraphicsCore::WindowHandle->draw(bottomRightSprite);
		}

		if (BottomLeftCorner != nullptr)
		{
			sf::Sprite bottomLeftSprite;
			bottomLeftSprite.setTexture(*(BottomLeftCorner->GetTextureResource()));
			bottomLeftSprite.setPosition(posX.Value, FLOAT(posY + scaleY - borderThickness).Value);
			GraphicsCore::WindowHandle->draw(bottomLeftSprite);
		}

		if (TopLeftCorner != nullptr)
		{
			sf::Sprite topLeftSprite;
			topLeftSprite.setTexture(*(TopLeftCorner->GetTextureResource()));
			topLeftSprite.setPosition(posX.Value, posY.Value);
			GraphicsCore::WindowHandle->draw(topLeftSprite);
		}
	}

	void FrameRenderComponent::PostEngineInitialize () const
	{
		Super::PostEngineInitialize();

		DefaultTopBorder = Texture::CreateTexture(Engine::GetBaseDirectory() + "Textures\\InterfaceTextures\\FrameBorderTop.jpg", "DefaultFrameTopBorder");
		DefaultRightBorder = Texture::CreateTexture(Engine::GetBaseDirectory() + "Textures\\InterfaceTextures\\FrameBorderRight.jpg", "DefaultFrameRightBorder");
		DefaultBottomBorder = Texture::CreateTexture(Engine::GetBaseDirectory() + "Textures\\InterfaceTextures\\FrameBorderBottom.jpg", "DefaultFrameBottomBorder");
		DefaultLeftBorder = Texture::CreateTexture(Engine::GetBaseDirectory() + "Textures\\InterfaceTextures\\FrameBorderLeft.jpg", "DefaultFrameLeftBorder");

		DefaultTopRightCorner = Texture::CreateTexture(Engine::GetBaseDirectory() + "Textures\\InterfaceTextures\\FrameBorderTopRight.jpg", "DefaultFrameTopRightCorner");
		DefaultBottomRightCorner = Texture::CreateTexture(Engine::GetBaseDirectory() + "Textures\\InterfaceTextures\\FrameBorderBottomRight.jpg", "DefaultFrameBottomRightCorner");
		DefaultBottomLeftCorner = Texture::CreateTexture(Engine::GetBaseDirectory() + "Textures\\InterfaceTextures\\FrameBorderBottomLeft.jpg", "DefaultFrameBottomLeftCorner");
		DefaultTopLeftCorner = Texture::CreateTexture(Engine::GetBaseDirectory() + "Textures\\InterfaceTextures\\FrameBorderTopLeft.jpg", "DefaultFrameTopLeftCorner");
	}
}