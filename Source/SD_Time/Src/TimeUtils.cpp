/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TimeUtils.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "TimeClasses.h"

#if INCLUDE_SD_TIME

using namespace std;

namespace SD
{
	vector<TimeUtils::SMonthInfo> TimeUtils::MonthInfo = TimeUtils::PopulateMonthInfo();

	IMPLEMENT_ABSTRACT_CLASS(TimeUtils, BaseUtils)

	INT TimeUtils::NumDaysWithinMonth (INT month, INT year)
	{
		if (month < 0 || month > 11)
		{
			LOG1(LOG_WARNING, TXT("%s is not a valid month.  There are only 12 months within a year.  Expected values range from 0 - 11."), month);
			return 0;
		}

		if (month == 1 && year > 0 && IsLeapYear(year))
		{
			return MonthInfo.at(month.Value).NumDays + 1;
		}

		return MonthInfo.at(month.Value).NumDays;
	}

	bool TimeUtils::IsLeapYear (INT year)
	{
		//A leap year is divisable by 4 except for 100.  If divisable by 400, then it's a leap year regardless.
		if ((year % 100) == 0)
		{
			return ((year % 400) == 0);
		}

		return ((year % 4) == 0);
	}

	void TimeUtils::GetMonthInfo (INT monthNumber, SMonthInfo& outMonthInfo)
	{
		if (monthNumber < 0 || monthNumber > 11)
		{
			LOG1(LOG_WARNING, TXT("%s is not a valid month.  There are only 12 months within a year.  Expected values range from 0 - 11."), monthNumber);
			return;
		}

		unsigned int index = monthNumber.Value;
		outMonthInfo.NumDays = MonthInfo.at(index).NumDays;
		outMonthInfo.FullMonthName = MonthInfo.at(index).FullMonthName;
		outMonthInfo.MonthNameAbbreviated = MonthInfo.at(index).MonthNameAbbreviated;
	}

	vector<TimeUtils::SMonthInfo> TimeUtils::PopulateMonthInfo ()
	{
		vector<SMonthInfo> results;
		SMonthInfo newMonthInfo;

		newMonthInfo.FullMonthName = TXT("January");
		newMonthInfo.MonthNameAbbreviated = TXT("Jan");
		newMonthInfo.NumDays = 31;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("Febuary");
		newMonthInfo.MonthNameAbbreviated = TXT("Feb");
		newMonthInfo.NumDays = 28;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("March");
		newMonthInfo.MonthNameAbbreviated = TXT("March");
		newMonthInfo.NumDays = 31;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("April");
		newMonthInfo.MonthNameAbbreviated = TXT("Apr");
		newMonthInfo.NumDays = 30;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("May");
		newMonthInfo.MonthNameAbbreviated = TXT("May");
		newMonthInfo.NumDays = 31;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("June");
		newMonthInfo.MonthNameAbbreviated = TXT("June");
		newMonthInfo.NumDays = 30;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("July");
		newMonthInfo.MonthNameAbbreviated = TXT("Jul");
		newMonthInfo.NumDays = 31;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("August");
		newMonthInfo.MonthNameAbbreviated = TXT("Aug");
		newMonthInfo.NumDays = 31;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("September");
		newMonthInfo.MonthNameAbbreviated = TXT("Sept");
		newMonthInfo.NumDays = 30;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("October");
		newMonthInfo.MonthNameAbbreviated = TXT("Oct");
		newMonthInfo.NumDays = 31;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("November");
		newMonthInfo.MonthNameAbbreviated = TXT("Nov");
		newMonthInfo.NumDays = 30;
		results.push_back(newMonthInfo);

		newMonthInfo.FullMonthName = TXT("December");
		newMonthInfo.MonthNameAbbreviated = TXT("Dec");
		newMonthInfo.NumDays = 31;
		results.push_back(newMonthInfo);

		return results;
	}
}

#endif