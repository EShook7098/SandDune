/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TimerManager.h
  This class is responsible for tracking a list of timer functions,
  and invoke their events when their time expired.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TIMERMANAGER_H
#define TIMERMANAGER_H

#include "SD_Time.h"

#if INCLUDE_SD_TIME

/**
  Quick shortcut to setup a timer.  Expected format for functionCallback is:  "ClassName::EventHandler"
  Semi-colon at the end are not included in case this is used inside a conditional statement.
 */
#define SET_TIMER(targetObject, timeDelay, bRepeat, functionCallback) \
	SD::TimerManager::GetTimerManager()->SetTimer(targetObject, timeDelay, bRepeat, bind(&##functionCallback##, targetObject), #functionCallback)

#define REMOVE_TIMER(targetObject, targetCallback) \
	SD::TimerManager::GetTimerManager()->RemoveTimer(targetObject, #targetCallback)

namespace SD
{
	class TimerManager : public Entity
	{
		DECLARE_CLASS(TimerManager)


		/*
		=====================
		  Data types
		=====================
		*/

	protected:
		struct STimerData
		{
			/* The object that'll handle the event.  This variable is used to check if it's safe to call the function without causing access violations. */
			const Object* OwningObject = nullptr;

			//TODO:  Remove bool return value after tests are successful.
			/* Callback to call when enough time elapsed.  This should return false if the owner was destroyed or if REMOVE_TIMER was called within this handler. */
			std::function<bool()> EventHandler = nullptr;

			/*Unique identifier that describes the callback function (since std::functions do not have == operators). */
			DString EventName;

			/* If greater than 0, then this timer struct will reset its counter
			to this value every time this event handler was called. */
			FLOAT TimeInterval = 0;

			FLOAT TimeRemaining = 0;

			/* If true, then this Timer struct will be deleted at the beginning of next Tick. */
			bool bPendingDelete = false;
		};


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Timer Manager instantiated for the main thread, and to be referenced via SetTimer macros. */
		static TimerManager* MainTimerManager;

		/* List of all registered timers waiting for a callback. */
		std::vector<STimerData> Timers;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void BeginObject () override;
		virtual void Destroy () override;

	protected:
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Retrieves the timer manager that's registered to the engine.
		 */
		static TimerManager* GetTimerManager ();

		/**
		  Registers new timer instance to the vector of timers.
		  Returns true if it replaced an existing entry (when functionCallback matches with another).
		 */
		virtual bool SetTimer (const Object* targetObject, FLOAT timeDelay, bool bRepeat, std::function<bool()> functionCallback, const DString& functionIdentifier);

		/**
		  Removes the function callback from the vector of timers.
		  Returns true if the function was found and removed.
		 */
		virtual bool RemoveTimer (const Object* targetObject, const DString& functionIdentifier);

		/**
		  Returns time remaining of the registered targetCallback.
		  This will return 0, if the targetCallback was not found.
		 */
		virtual FLOAT GetTimeRemaining (const Object* targetObject, const DString& functionIdentifier) const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Iterates through each timer and checks if they still hold valid references to OwningObject.
		  If not, then the Timer is marked for deletion.
		 */
		virtual void EvaluateTimerObjects ();

		/**
		  Returns true if the given Timer struct is valid.  Return false if it should be marked for deletion.
		  Parameter is the index value of the Timers vector.
		 */
		virtual bool IsValidTimerData (unsigned int timerIdx) const;

		/**
		  Flushes PendingDeleteIndices to clean up the Timers vector.
		 */
		virtual void ApplyPendingDelete ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleTick (FLOAT deltaSec);
	};
}

#endif
#endif