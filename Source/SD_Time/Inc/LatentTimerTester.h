/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LatentTimerTester.h
  This class is responsible for conducting unit tests various timer-
  related functionality such as the timer manager.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TIMERUNITTESTER_H
#define TIMERUNITTESTER_H

#include "SD_Time.h"

#if INCLUDE_SD_TIME

#ifdef DEBUG_MODE

namespace SD
{
	class LatentTimerTester : public Entity
	{
		DECLARE_CLASS(LatentTimerTester)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		UnitTester::EUnitTestFlags TestFlags;
		const UnitTester* OwningUnitTester;

		/* Counter incrementing every time an event is triggered. */
		INT NumEventsTriggered;

		/* How many times the repeated event should be triggered. */
		INT MaxNumRepeatedEvents;

		/* Interval at which the timer events are triggered. */
		FLOAT EventInterval;

		/* Time at which this test began. */
		FLOAT StartTime;

		/* Becomes true if this unit tester is about to be destroyed due to a failed test. */
		bool bFailedTest;

		/* Stopwatch to record how long this timer unit tester was running. */
		Stopwatch* UnitTestStopwatch;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void Destroy () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual void LaunchTest (UnitTester::EUnitTestFlags testFlags, const UnitTester* unitTesterOwner);


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Checks the test results to ensure that the events weren't triggered too many times.
		 */
		virtual void VerifyEvents ();

		virtual void RemoveStopwatch ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual bool HandleSingleEvent ();
		virtual bool HandleRepeatedEvent ();

		/**
		  Test event that should never be triggered (testing if the timer manager is able to abort callbacks).
		 */
		virtual bool HandleFalseEvent ();

		/**
		  Checks if the events are not taking too long to trigger.
		 */
		virtual void HandleTick (FLOAT deltaSec);
	};
}

#endif //debug_mode

#endif
#endif