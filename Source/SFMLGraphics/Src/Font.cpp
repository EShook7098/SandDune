/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Font.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "SFMLGraphicsClasses.h"

#if INCLUDE_SFMLGRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(Font, Object)

	void Font::InitProps ()
	{
		Super::InitProps();

		FontName = TXT("UnknownFont");
		FontResource = nullptr;
	}

	DString Font::GetFriendlyName () const
	{
		if (!FontName.IsEmpty())
		{
			return TXT("Font:  ") + FontName;
		}

		return Super::GetFriendlyName();
	}

#if 0
	Font* Font::ImportFont (DString fileName, DString fontName)
	{
		if (fontName.IsEmpty())
		{
			LOG(LOG_WARNING, TXT("Cannot import a font without specifying a name."));
			return nullptr;
		}

		//Check name clashes
		FontPool* fontPool = FontPool::GetFontPool();
		if (fontPool == nullptr)
		{
			LOG1(LOG_WARNING, TXT("Unable to import font (%s).  Cannot obtain a reference to the font pool!"), fontName);
			return nullptr;
		}

		Font* fontReference = fontPool->FindFont(fontName);
		if (fontReference)
		{
			LOG1(LOG_WARNING, TXT("A font with the name %s is already imported."), fontName);
			return fontReference;
		}

		//Import font
		sf::Font* newFont = new sf::Font();

		//if (!newFont->loadFromFile(Engine::GetBaseDirectory() + FONT_LOCATION + DIR_SEPARATOR + fileName))
		if (true) //TODO:  GetBaseDirectory moved to file module
		{
			LOG1(LOG_WARNING, TXT("Unable to import font.  Could not find %s"), fileName);
			return nullptr;
		}

		fontReference = dynamic_cast<Font*>(Font::CreateObject());
		if (!fontReference)
		{
			delete newFont;
			return nullptr;
		}

		fontReference->FontResource = newFont;
		fontReference->FontName = fontName;

		fontPool->RegisterFont(fontReference, fontName);

		return fontReference;
	}
#endif

	void Font::Release ()
	{
		if (FontResource != nullptr)
		{
			delete FontResource;
		}

		FontResource = nullptr;
	}

	void Font::SetResource (sf::Font* newResource)
	{
		FontResource = newResource;
	}

	FLOAT Font::GetCharacterWidth (const TCHAR character, unsigned int charSize) const
	{
		if (FontResource == nullptr)
		{
			LOG2(LOG_WARNING, TXT("Unable to calculate character width for \"%s\" since a font resource is not yet assigned to %s"), DString(character), GetUniqueName());
			return 0.f;
		}

		sf::Glyph glyph = FontResource->getGlyph(character, charSize, false);

		return FLOAT(glyph.advance);
	}

	FLOAT Font::CalculateStringWidth (const DString& line, unsigned int charSize) const
	{
		FLOAT totalWidth = 0.f;

		for (INT i = 0, length = line.Length(); i < length; i++)
		{
			totalWidth += GetCharacterWidth(line.At(i), charSize);
		}

		return totalWidth;
	}

	INT Font::FindCharNearWidthLimit (const DString& line, unsigned int charSize, FLOAT maxWidth) const
	{
		FLOAT curWidth = 0.f;

		for (INT i = 0, length = line.Length(); i < length; i++)
		{
			FLOAT newWidth = GetCharacterWidth(line.At(i), charSize);
			if (curWidth + newWidth >= maxWidth)
			{
				return (i - 1); //return previous character that fits within bounds.
			}

			curWidth += newWidth;
		}

		return line.Length() - 1; //The whole string fits, return the last character.
	}

	DString Font::GetFontName () const
	{
		return FontName;
	}

	const sf::Font* Font::GetFontResource () const
	{
		return FontResource;
	}
}

#endif