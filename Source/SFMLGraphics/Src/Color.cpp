/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Color.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "SFMLGraphicsClasses.h"

#if INCLUDE_SFMLGRAPHICS

using namespace std;

namespace SD
{
	Color::Color ()
	{
		Source = sf::Color();
	}

	Color::Color (const Color& otherColor)
	{
		Source = otherColor.Source;
	}

	Color::Color (const sf::Color& otherColor)
	{
		Source = otherColor;
	}

	Color::Color (INT red, INT green, INT blue, INT alpha)
	{
		sf::Uint8 sfRed = Utils::Clamp(red.Value, 0, 255);
		sf::Uint8 sfGreen = Utils::Clamp(green.Value, 0, 255);
		sf::Uint8 sfBlue = Utils::Clamp(blue.Value, 0, 255);
		sf::Uint8 sfAlpha = Utils::Clamp(alpha.Value, 0, 255);

		Source = sf::Color(sfRed, sfGreen, sfBlue, sfAlpha);
	}

	DString Color::ToString () const
	{
		return DString::Format(TXT("[%s,%s,%s,%s]"), {to_string(Source.r).c_str(), to_string(Source.g).c_str(), to_string(Source.b).c_str(), to_string(Source.a).c_str()});
	}

	bool operator== (const Color &left, const Color &right)
	{
		return left.Source == right.Source;
	}
 
	bool operator!= (const Color &left, const Color &right)
	{
		return left.Source != right.Source;
	}

	Color operator+ (const Color &left, const Color &right)
	{
		return Color(left.Source + right.Source);
	}

	Color operator- (const Color &left, const Color &right)
	{
		return Color(left.Source - right.Source);
	}

	Color operator* (const Color &left, const Color &right)
	{
		return Color(left.Source * right.Source);
	}
 
	Color& operator+= (Color &left, const Color &right)
	{
		left.Source += right.Source;
		return left;
	}

	Color& operator-= (Color &left, const Color &right)
	{
		left.Source -= right.Source;
		return left;
	}
 
	Color& operator*= (Color &left, const Color &right)
	{
		left.Source *= right.Source;
		return left;
	}
}

#endif