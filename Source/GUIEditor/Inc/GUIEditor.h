/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIEditor.h
  Contains important file includes and definitions for the GUIEditor module.

  The GUIEditor module houses utilities and entities that'll make it easier to
  edit/configure existing UIs.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUIEDITOR_H
#define GUIEDITOR_H

#include "Configuration.h"
#if INCLUDE_GUIEDITOR

#if !(INCLUDE_CORE)
#error The GUIEditor module requires the Core module.  Please enable INCLUDE_CORE in Configuration.h.
#endif

#if !(INCLUDE_GUI)
#error The GUIEditor module requires the GUI module.  Please enable INCLUDE_GUI in Configuration.h.
#endif

#if !(INCLUDE_BASEEDITOR)
#error The GUIEditor module requires the Base Editor module.  Please enable INCLUDE_BASEEDITOR in Configuration.h.
#endif

#include "CoreClasses.h"
#include "GUIClasses.h"
#include "BaseEditor.h"

#define GUIEDITOR_TITLE "GUI Editor"
#define LOG_GUIEDITOR "GUI Editor"

#endif
#endif