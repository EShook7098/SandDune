/*
=====================================================================
  <Any copyright stuff here>

  LabelComponentTextThread.h
  An object responsible for deploying a separate thread to handle
  expensive calculations for word wrapping, clamping, and alignment
  for a LabelComponent.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef LABELCOMPONENTTEXTTHREAD_H
#define LABELCOMPONENTTEXTTHREAD_H

#include "LabelComponent.h"

#if INCLUDE_GUI

namespace SD
{
	class LabelComponentTextThread
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Becomes true whenever the TextThread finished calculating PendingTextUpdates. */
		bool bFinishedCalculation;

	protected:
		/* A vector of false sf::Text to handle measurements. These are never rendered,
		and will be deleted after transfering its results to the parent label component.*/
		std::vector<sf::Text> PendingTextUpdates;

		/* Label component to notify when calculations are finished. */
		LabelComponent* ParentLabelComponent;

		/* Actual thread instance that'll run asynchronously from the main thread. */
		std::thread TextThread;

		std::mutex TextMutex;

		/* Various copied variables from LabelComponent.  When the LabelComponent deploys this thread, it'll
		take a snapshot of the LabelComponent's variables so that this thread can run its calculations 
		asynchronously from Sand Dune's main thread. */
		DString DisplayedText;
		const Font* Font;
		INT CharSize;
		bool bWrapText;
		bool bClampText;
		FLOAT LineSpacing;
		std::string TextTrail;
		LabelComponent::eVerticalAlignment VerticalAlignment;
		LabelComponent::eHorizontalAlignment HorizontalAlignment;
		INT MaxNumLines;
		INT AbsPosX;
		INT AbsPosY;
		INT AbsSizeX;
		INT AbsSizeY;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		LabelComponentTextThread (LabelComponent& labelComponent);
		virtual ~LabelComponentTextThread ();


		/*
		=====================
		  Methods
		=====================
		*/

		/**
		  Initializes linear calculation sequence then updates LabelComponent when finished.
		  This function runs on the main thread (not on TextThread).
		 */
		virtual void DeployThread ();

		virtual INT GetBottomBorder () const;
		virtual INT GetRightBorder () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void ExecuteTextThread ();

		/**
		  Instantiates a sf::text and inserts to TextMeasurements vector at index idx.
		 */
		virtual void InsertTextMeasure (DString content, unsigned int idx);


		/**
		  Iterates through all characters.  Every time a new line character is encountered, the text
		  after the new line character is then transfered to the next Text instance within the render component.
		  If there isn't enough space for a new line (vertical absolute size is not large enough), then
		  this function will not add another line (and the remaining text will go beyond the last text's boundaries.
		  Returns true if all text instances fit within the component's boundaries.
		 */
		virtual bool ParseNewLineCharacters ();

		/**
		  Repositions any newline characters to ensure the text fits within the box.
		 */
		virtual void WrapText ();

		/**
		  Removes text that extends beyond the bounding box.
		  For performance purposes, this only considers the last line.
		  Naturally, WrapText would have already taken care of characters beyond the width's boundaries.
		  This function also updates the TrimmedTextTail and TextContent based on the text that was truncated.
		 */
		virtual void ClampText ();

		/**
		  Sets sfml's Text instance so that the text is approximately aligned vertically.
		 */
		virtual void AlignVertically ();

		/**
		  Crudely adds white spaces in front of each line until the text is approximately horizontally aligned.
		 */
		virtual void AlignHorizontally ();

		/**
		  Populates the vector of ints with indices of all new line characters.
		 */
		//virtual void GetAllNewLineCharacterIndices (std::vector<int>& outNewLineChars) const;

		/**
		  Seeks and finds the next left character that is just beyond this component's width.
		  textIndex is the render component's texts' index.
		  This function returns the index of that left character.
		  Return value is negative if all characters are within this component's width.
		 */
		virtual INT FindCharacterBeyondWidth (INT textIndex) const;

		/**
		  Iterates backwards through the line of text starting from startPosition (defaults to end of line)
		  and returns the character index that first matches any of the given targetCharacters.
		  Returns INT_INDEX_NONE, if none of the characters were found within that line.
		 */
		virtual INT FindPreviousCharacter (const std::vector<char>& targetCharacters, unsigned int textLine, INT startPosition = INT_INDEX_NONE) const;

		/**
		  Notifies the label component that the calculations are finished, and this thread is ready to update
		  the label component.
		 */
		virtual void UpdateLabelComponent ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		/**
		  Periodically checks on the calculation progress.  When it's ready, it'll update
		  the parent label component.
		 */
		virtual bool CheckThreadStatus ();
	};
}

#endif
#endif