/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldLabelComponent.h
  A label component that renders text within a bounding box.
  Divided from its parent class (LabelComponent), this class notifies
  the owning TextField about the changed text so that the TextField
  may adjust its cursor position and scroll bar size.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TEXTFIELDLABELCOMPONENT_H
#define TEXTFIELDLABELCOMPONENT_H

#include "LabelComponent.h"

#if INCLUDE_GUI

namespace SD
{
	class ScrollbarComponent;

	class TextFieldLabelComponent : public LabelComponent
	{
		DECLARE_CLASS(TextFieldLabelComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Chunk of FullText that's above the current scroll position.  Becomes empty if scroll position is 0. */
		DString TrimmedTextHead;

		/* Number of lines were deleted because in order to fit the text within the UI boundaries. */
		INT NumTrimmedLines;

	private:
		/* Flag to ignore alignment functionality.  This is only set in SetTextContent function. */
		bool bIgnoreAlignment;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void SetTextContent (DString newContent) override;
		virtual void SetClampText (bool bNewClampText) override;
		virtual DString GetFullText () const override;

	protected:
		virtual void ClampText () override;
		virtual void AlignVertically () override;
		virtual void AlignHorizontally () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Overrides the current TextContent with the given new string.
		  If bChangesAtCursor is true, then this component will only modify text at and after the cursor position.
		 */
		virtual void SetTextContent (DString newContent, INT cursorPosition, bool bChangesAtCursor);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		/**
		  Only gets the text that was cut off from rendering because of positive scroll position.
		 */
		virtual DString GetTrimmedTextHead () const;

		virtual INT GetNumTrimmedLines () const;

		/**
		  Climbs up the owner chain until it finds a reference to a TextFieldComponent.
		 */
		virtual TextFieldComponent* GetOwningTextField () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Calculates the text that should be cut from FullText due to owner's scroll position.
		  This will then update the TrimmedTextHead property with the result.
		 */
		virtual void CalculateTrimmedTextHead (INT targetLineNumber);
	};
}

#endif
#endif