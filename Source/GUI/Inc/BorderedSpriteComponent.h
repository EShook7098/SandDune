/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BorderedSpriteComponent.h
  A component that draws stretched borders around the sprite.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef BORDEREDSPRITECOMPONENT_H
#define BORDEREDSPRITECOMPONENT_H

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	class BorderedSpriteComponent : public SpriteComponent
	{
		DECLARE_CLASS(BorderedSpriteComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Reference to the resources that draws the borders. */
		Texture* TopBorder;
		Texture* RightBorder;
		Texture* BottomBorder;
		Texture* LeftBorder;

		/* Corner images. */
		Texture* TopRightCorner;
		Texture* BottomRightCorner;
		Texture* BottomLeftCorner;
		Texture* TopLeftCorner;

		/* If Sprite is nullptr, then the interior is filled with this color. */
		sf::Color FillColor;

		/* Determines how thick to draw the borders. */
		FLOAT BorderThickness;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void Render (const Camera* targetCamera, const Window* targetWindow) override;
	};
}

#endif
#endif