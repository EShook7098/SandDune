/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIClasses.h
  Contains all header includes for the GUI module.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUICLASSES_H
#define GUICLASSES_H

#include "Configuration.h"
#if INCLUDE_GUI

#define MODULE_GUI 1

#include "GUI.h"
#include "GUIEngineComponent.h"
#include "TextRenderComponent.h"
#include "GUITheme.h"
#include "GUIEntity.h"
#include "GUIComponent.h"
#include "FocusInterface.h"
#include "FocusComponent.h"
#include "HoverComponent.h"
#include "LabelComponent.h"
#include "TextFieldComponent.h"
#include "TextFieldLabelComponent.h"
#include "TextLineLabelComponent.h"
#include "TextFieldRenderComponent.h"
#include "FrameComponent.h"
#include "ButtonComponent.h"
#include "ScrollbarComponent.h"
#include "ScrollbarTrackComponent.h"
#include "DropdownComponent.h"
#include "TreeListComponent.h"
#include "TreeBranchIterator.h"
#include "GUIDataElement.h"
#include "BorderedSpriteComponent.h"
#include "ButtonStateComponent.h"
#include "SingleSpriteButtonState.h"

#ifdef DEBUG_MODE
#include "GUIUnitTester.h"
#include "GUITester.h"
#include "TestMenuButtonList.h"
#include "TestMenuFocusInterfaces.h"
#endif

#endif
#endif