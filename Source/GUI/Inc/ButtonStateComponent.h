/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ButtonStateComponent.h
  A component that contains methods that'll transition the button
  component to various states such as hovered, down, and disabled.

  Note:  The reason why this is seperated from the ButtonComponent is
  to avoid code duplication whenever there's a need to subclass the
  ButtonComponent without having to override transition state functionality.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef BUTTONSTATECOMPONENT_H
#define BUTTONSTATECOMPONENT_H

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	class ButtonComponent;

	class ButtonStateComponent : public EntityComponent
	{
		DECLARE_CLASS(ButtonStateComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Reference to the owning button component. */
		ButtonComponent* OwningButton;


		/*
		=====================
		  Inherited	
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;
		virtual void AttachTo (Entity* newOwner) override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Figures out what state the owning component is and adjusts its appearance accordingly.
		 */
		virtual void RefreshState ();

		/**
		  Notifies the render components of this state component which window to draw to.
		 */
		virtual void DrawToWindow (Window* newRelevantWindow);

		/**
		  Various hooks to change the button appearance based on its state.
		  ie:  Use a single sprite with multi-subdivisions, swapping textures altogether, or changing frame component's borders.
		 */
		virtual void SetDefaultAppearance ();
		virtual void SetDisableAppearance ();
		virtual void SetHoverAppearance ();
		virtual void SetDownAppearance ();


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual ButtonComponent* GetOwningButton () const;
	};
}

#endif
#endif