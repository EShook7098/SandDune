/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DropdownComponent.h
  A component that expands to a list of objects, and the user can specify which object to select.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef DROPDOWNCOMPONENT_H
#define DROPDOWNCOMPONENT_H

#include "GUIComponent.h"

#if INCLUDE_GUI

#include "GUIDataElement.h"
#include "FocusInterface.h"

namespace SD
{
	class FrameComponent;
	class LabelComponent;
	class ButtonComponent;
	class ScrollbarComponent;

	class DropdownComponent : public GUIComponent, public FocusInterface
	{
		DECLARE_CLASS(DropdownComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Callback to invoke whenever an option was selected where the parameter is the index of the list item. */
		SDFunction<void, INT> OnOptionSelected;

	protected:
		/* List of available options.  Each option may have a pointer to an object but does not check if the object is valid. 
		  The list, itself, is a pointer to BaseGUIDataElement in order to support polymorphic types for casting. */
		std::vector<BaseGUIDataElement*> List;

		unsigned int SelectedItemIdx;

		/* Contrast to the SelectedItemIdx, this index shifts along the List (moving the highlight bar) without actually selecting the item. */
		INT BrowseIndex;

		/* Text to display on selected item field when no item is selected. */
		DString NoSelectedItemText;

		/* Becomes true if the list is expanded and visible. */
		bool bExpanded;

		/* Determines the height of a single line within the expanded section. */
		FLOAT ExpandedOptionHeight;

		ButtonComponent* ExpandButton;

		/* Texture to use for the expand button while the dropdown is collapsed. */
		Texture* ButtonTextureCollapsed;

		/* Texture to use for the expand button while the dropdown is expanded. */
		Texture* ButtonTextureExpanded;

		FrameComponent* ExpandBackground;
		FrameComponent* SelectedObjBackground;

		LabelComponent* SelectedItemLabel;
		LabelComponent* ListLabel;

		SolidColorRenderComponent* HighlightBar;
		AbsTransformComponent* HighlightBarTransform;

		ScrollbarComponent* ListScrollbar;

	private:
		/* Timestamp at which this dropdown menu expanded. */
		FLOAT PrevExpandTime;

		/* Threshold that differentiates a drag select and a toggle select.  If the user tapped the dropdown arrow, the menu remains expanded.
		If the user held the mouse button beyond this value in seconds, then the menu will hide as soon as the user releases the button. */
		FLOAT DragToggleThresholdTime;

		/* If true, then the next enter release key event is ignored. */
		bool bIgnoreEnterRelease;

		/* For focused dropdowns, this is the search string the user is currently entering. */
		DString SearchString;

		/* Timestamp when the latest character was added to SearchString. */
		FLOAT SearchStringTimestamp;

		/* Maximum time to elapse before the search string clears. */
		FLOAT SearchStringClearTime;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;
		virtual void SetWindowHandle (Window* newWindowHandle) override;

		virtual bool CanBeFocused () const override;
		virtual void LoseFocus () override;
		virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
		virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

	protected:
		virtual void HandleSizeChange () override;
		virtual bool ShouldHaveExternalInputComponent () const override;
		virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
		virtual bool ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual void Expand ();
		virtual void Collapse ();

		/**
		  Sets the selected item by index value.
		 */
		virtual void SetSelectedItem (unsigned int itemIndex);
		virtual void SetNoSelectedItemText (const DString& newNoSelectedItemText);
		virtual void ClearList ();


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual DString GetNoSelectedItemText () const;


		/*
		=====================
		  Implementation
		=====================
		*/
		
	protected:
		virtual void InitializeExpandButton ();
		virtual void InitializeExpandBackground ();
		virtual void InitializeSelectedObjBackground ();
		virtual void InitializeSelectedItemLabel ();
		virtual void InitializeListLabel ();
		virtual void InitializeHighlightBar ();
		virtual void InitializeListScrollbar ();

		/**
		  Recalculates each components of the dropdown such in a way it maintains its structure.
		 */
		virtual void RefreshComponentTransforms ();

		/**
		  Runs all methods necessary whenever the List changes size.
		 */
		virtual void ImplementChangeInListLength ();

		/**
		  Assigns the BrowseIndex value, and repositions highlight bar based on the new index value.
		  If bAllowScrolling is true, then it'll attempt to scroll the ListScrollbar such that the newBrowseIndex is visible.
		 */
		virtual void SetBrowseIndex (INT newBrowseIndex, bool bAllowScrolling);

		/**
		  Retrieves the option index based on the specified y position.  This considers the scroll position offset.
		  Returns INT_INDEX_NONE, if it's not over any option.
		 */
		virtual INT GetOptionLine (FLOAT yCoordinate) const;

		/**
		  Sets the list label text based on scroll position and list.
		 */
		virtual void PopulateListLabel ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleExpandButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleExpandScrollPosChanged (INT newScrollPosition);


		/*
		=====================
		  Templates
		=====================
		*/

	public:
		/**
		  Adds an item to the list.  The pointer to the object is not validated.  If an object is deleted,
		  it's the calling object's responsibility to clear the dropdown and repopulate it.
		 */
		template <class T>
		void AddOption (const GUIDataElement<T>& inNewDataElement)
		{
			List.push_back(new GUIDataElement<T>(inNewDataElement));
			ImplementChangeInListLength();
		}

		template <class T>
		void SetList (const std::vector<GUIDataElement<T>>& newList)
		{
			ClearList();

			for (unsigned int i = 0; i < newList.size(); i++)
			{
				List.push_back(new GUIDataElement<T>(newList.at(i)));
			}

			ImplementChangeInListLength();
		}

		template <class T>
		GUIDataElement<T>* GetSelectedItem () const
		{
			if (SelectedItemIdx >= 0 && SelectedItemIdx < List.size())
			{
				return dynamic_cast<GUIDataElement<T>*>(List.at(SelectedItemIdx));
			}

			return nullptr;
		}

		/**
		  Attempts to select the data item within the table that matches the specified data element.
		 */
		template <class T>
		void SetSelectedItem (const GUIDataElement<T>& targetData)
		{
			for (unsigned int i = 0; i < List.size(); i++)
			{
				if (List.at(i) == targetData)
				{
					SelectOption(i);
					return;
				}
			}
		}
	};
}

#endif
#endif