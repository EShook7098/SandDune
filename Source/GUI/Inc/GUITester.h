/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUITester.h
  Class that'll instantiate various UI Components so that the user
  may test their functionality without external interference.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUITESTER_H
#define GUITESTER_H

#include "GUI.h"

#if INCLUDE_GUI

#ifdef DEBUG_MODE

namespace SD
{
	class GUIComponent;
	class GUIEntity;
	class TreeListComponent;

	class GUITester : public Entity
	{
		DECLARE_CLASS(GUITester)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		Rectangle<FLOAT> StageDimensions;

		UnitTester::EUnitTestFlags TestFlags;

	protected:
		/* List of GUI Components that are outside of staging area (cleans up at destroy). */
		std::vector<GUIComponent*> BaseComponents;

		/* List of GUI Components that are inside staging area (cleans up at stage change). */
		std::vector<GUIComponent*> StageComponents;

		/* Component that makes up the stage's frame. */
		FrameComponent* StageFrame;

		/* Buttons that make up the button bar.  This list may be used for testing lists (such as dropdown components). */
		std::vector<ButtonComponent*> ButtonBar;

		/* Maximum number of buttons on the ButtonBar is displayed at any given time. */
		INT MaxButtonsPerPage;

		/* Which section of the ButtonBar is the user currently viewing. */
		INT ButtonBarPageIdx;

		ButtonComponent* NextButtonBarPage;
		ButtonComponent* PrevButtonBarPage;

		/* For the frame test, this is the list of frame components that'll be testing size change alignments.
			[0-2]:  Frame within frame test - Alignment: center-center
			[3]: Sibling frames within giant frame - Alignment: top-center
			[4]: top-right, [5]: center-right, [6]: bottom-right, [7]: bottom-center, [8]: bottom-left, [9]: center-left, [10]: top-left, [11]: center-center.
		 */
		FrameComponent* AlignmentFrames[11];
		static const int NumAlignmentFrames = 12;

		/* For the button test, these are the two buttons that'll be toggling each other. */
		ButtonComponent* ToggleButtons[2];

		/* For the scrollbar test, this is the label component describing the labeled scrollbar. */
		LabelComponent* ScrollbarLabel;

		/* For the scrollbar test, this is the label component describing the adjustable scrollbar. */
		LabelComponent* AdjustableScrollbarLabel;

		/* For the scrollbar test, this is the adjustable scrollbar where its properies is based on user input. */
		ScrollbarComponent* AdjustableScrollbar;

		/* For the dropdown test, this is a list of numbers the numeric dropdown menus will be referencing. */
		std::vector<INT> DropdownNumbers;

		/* For the dropdown test, this is the dropdown component that reports the button bar content. */
		DropdownComponent* ButtonBarDropdown;

		/* List of GUIEntities instantiated for the GUIEntity test. */
		std::vector<GUIEntity*> GUIEntities;

		/* For the GUIEntitiy test, this is the dropdown that selects which GUIEntity should be focused. */
		DropdownComponent* SetFocusDropdown;

		/* For the HoverComponent test, this is the label component that will be adjusting its text based on if the user is hovering over it or not. */
		LabelComponent* HoverLabel;

		/* For the HoverComponent test, this is the button component that'll be adjusting its text based on if the user is hovering over it or not. */
		ButtonComponent* HoverButton;

		/* For the TreeList test, this is the class browser tree list that'll notify the user which class they've selected. */
		TreeListComponent* TreeListClassBrowser;

		/* Window handle that'll be rendering objects from this test. */
		InputWindow* TestWindowHandle;

	private:
		/* Counter for the button panels (external from stage). */
		INT PanelButtonCounter;

		/* Render Target that'll be rendering objects for the TestWindowHandle. */
		RenderTarget* TestWindowRenderer;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void Destroy () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual void BeginUnitTest ();


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void InitializeStageFrame ();

		/**
		  Creates and initializes all UI components that make up the components external to the testing stage.
		 */
		virtual void CreateBaseButtons ();

		virtual void ClearStageComponents ();

		/**
		  Creates and initializes a button to the options panel.
		 */
		virtual ButtonComponent* AddPanelButton ();

		/**
		  Sets the relevant buttons on the button bar to be visible based on the current page idx.
		 */
		virtual void UpdateButtonBarPageIdx ();

		/**
		  For Labeled Scrollbar:  this computes the text content for the label based on the scrollbar's current position.
		 */
		virtual void RefreshLabeledScrollbarText (INT newScrollPosition);

		/**
		  For the Adjustable Scrollbar:  this computes the text content for the label based on the scrollbar's current properties.
		 */
		virtual void RefreshAdjustableScrollbarText (INT newScrollPosition);


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleWindowEvent (const sf::Event newWindowEvent);

		virtual void HandleFrameButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleLabelButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleButtonButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleScrollbarButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleDropdownButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleGUIEntityButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleHoverButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleTreeListButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleCloseClicked (ButtonComponent* uiComponent);
		virtual void HandleNextPageClicked (ButtonComponent* uiComponent);
		virtual void HandlePrevPageClicked (ButtonComponent* uiComponent);

		virtual void HandleFrameSizeChange ();

		virtual void HandleTestButtonPressed (ButtonComponent* uiComponent);
		virtual void HandleTestButtonReleased (ButtonComponent* uiComponent);
		virtual void HandleToggleButtonReleased (ButtonComponent* uiComponent);

		virtual void HandleBasicScrollbarIdxChange (INT newScrollPosition);
		virtual void HandleLabeledScrollbarIdxChange (INT newScrollPosition);
		virtual void HandleAdjustableScrollbarIdxChange (INT newScrollPosition);
		virtual void HandleDecMaxScrollPosReleased (ButtonComponent* uiComponent);
		virtual void HandleIncMaxScrollPosReleased (ButtonComponent* uiComponent);
		virtual void HandleDecNumVisPosReleased (ButtonComponent* uiComponent);
		virtual void HandleIncNumVisPosReleased (ButtonComponent* uiComponent);
		virtual void HandleDecScrollIntervalReleased (ButtonComponent* uiComponent);
		virtual void HandleIncScrollIntervalReleased (ButtonComponent* uiComponent);
		virtual void HandleToggleScrollVisBasedPositionReleased (ButtonComponent* uiComponent);

		virtual void HandleBasicDropdownOptionClicked (INT newOptionIdx);
		virtual void HandleFullBasicDropdownOptionClicked (INT newOptionIdx);
		virtual void HandleButtonBarDropdownOptionClicked (INT newOptionIdx);

		virtual void HandleFocusChange (INT newOptionIdx);

		virtual void HandleHoverFrameRolledOver (GUIComponent* uiComponent);
		virtual void HandleHoverFrameRolledOut (GUIComponent* uiComponent);
		virtual void HandleHoverLabelRolledOver (GUIComponent* uiComponent);
		virtual void HandleHoverLabelRolledOut (GUIComponent* uiComponent);
		virtual void HandleHoverButtonRolledOver (GUIComponent* uiComponent);
		virtual void HandleHoverButtonRolledOut (GUIComponent* uiComponent);

		virtual void HandleTreeListClassSelected (INT newOptionIdx);
	};
}

#endif //debug mode
#endif //INCLUDE_GUI
#endif //include guard