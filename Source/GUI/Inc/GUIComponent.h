/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIComponent.h
  Contains base functionality for all UI Components.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUICOMPONENT_H
#define GUICOMPONENT_H

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	class GUIComponent : public EntityComponent
	{
		DECLARE_CLASS(GUIComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Component determining this GUI's scale and position. */
		AbsTransformComponent* Transformation;

		/* InputComponent that's passing in input events to this component.  NOTE:  Only the parent GUIComponent will contain an InputComponent by default. */
		InputComponent* Input;

		/* InputComponent that's strictly used to capture mouse events when the pointer is outside of bounds.  By default, mouse events that are
		external to this component's borders will not be executed.  Events from this component does not invoke ExecuteMouseMove to Sub GUI Components. */
		InputComponent* ExternalBorderInput;

		/* Value at which SetDrawOrder was last called. */
		FLOAT CurrentDrawOrder;

		/* OS window handle this GUIComponent is rendering to, and capturing input from.  This spreads to all sub GUIComponents.  Although not likely, sub Components may override this. 
		A single GUI entity cannot be shared amongst multiple windows without overriding behavior of their render and input components. */
		Window* WindowHandle;

		/* If true then this particular GUI Component is visible.  Note:  When checking for visibility, refer to IsVisible function since the owning GUIComponent may be invisible and this flag is still true. */
		bool bVisible;

	private:
		/* Becomes true if this Component needs to unregister the window handle's polling delegate before its destruction. */
		bool bBoundWindowPollDelegate;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;

	protected:
		virtual void AttachTo (Entity* newOwner) override;
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Returns true if this GUIComponent is the GUIComponent closest to the owning entity within the scope of local component chain (see diagram below).
			Entity
				OtherComponent
					GUIComponent <---- Outermost
					GUIComponent <---- Outermost
						SubGUIComponent <---- Not outermost
							SubGUIComponent <---- Innermost
					GUIComponent
						Component
							SubGUIComponent <---- Innermost
				GUIComponent <---- Outermost
		 */
		virtual bool IsOutermostGUIComponent () const;

		/**
		  Updates draw order for this component's render component and all children components' render components.
		  All children component's draw order will be one value higher than its parent.
		 */
		virtual void SetDrawOrder (FLOAT newDrawOrder);

		/**
		  Checks the ShouldHaveInputComponent function and adds/removes the Input variable accordingly.
		 */
		virtual void ConditionallySetInputComponent ();

		/**
		  Creates and initializes an InputComponent that'll notify this GUIComponent of mouse events beyond the transformation's borders.
		 */
		virtual void SetupExternalBorderInput ();

		/**
		  Checks the ShouldHaveExternalInputComponent function and adds/removes the external input component accordingly.
		 */
		virtual void ConditionallySetExternalBorderInput ();

		/**
		  Removes the external border InputComponent.
		 */
		virtual void RemoveExternalBorderInput ();

		/**
		  Updates which window handle this component will render to and capture input from.
		 */
		virtual void SetWindowHandle (Window* newWindowHandle);

		/**
		  Retrieves the mouse pointer instance that'll most likely interact with this GUIComponent.
		 */
		virtual MousePointer* GetMousePointer () const;

		virtual void SetVisibility (bool bNewVisibility);

		/**
		  Returns true if this component and all of its parent GUI components are visible.
		  If bCheckVisChain is false, then it'll only check the visibility on this component.
		 */
		virtual bool IsVisible (bool bCheckVisChain = true) const;


		/*
		=====================
		  Accessors
		=====================
		*/

		/**
		  Returns the GUIComponent that owns this component.  Returns self, if no GUIComponent owns this.
		 */
		virtual GUIComponent* GetParentGUIComponent ();

		virtual AbsTransformComponent* GetTransform () const;

		virtual Window* GetWindowHandle () const;

		/**
		  Climbs up the ownership chain until it finds a GUIComponent containing an InputComponent.
		 */
		virtual InputComponent* GetInput () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Invoked from the root's InputComponent so that not all GUIComponents have to contain InputComponents.
		  Instead the parent GUIComponents will only redirect the mouse events to its components if the mouse event is relevant.
		 */
		virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
		virtual bool ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
		virtual bool ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

		/**
		  Returns true, if mouse events are relevant for this component and its subcomponents.
		 */
		virtual bool AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const;

		/**
		  Creates and initializes an InputComponent for this component.
		 */
		virtual void InitializeInputComponent ();

		/**
		  Returns true if the conditions are right for this component to possess an InputComponent.
		 */
		virtual bool ShouldHaveInputComponent () const;

		/**
		  Returns true if the conditions are right for this component to create an InputComponent to invoke mouse events that are external from this component's borders.
		 */
		virtual bool ShouldHaveExternalInputComponent () const;

		/**
		  Iterates through all RenderComponents attached to this component (not sub components), 
		  and set their draw order to this value.  Should be overridden if there's a strict draw
		  order required for a GUIComponent with multiple render components.
		 */
		virtual void SetRenderComponentsDrawOrder (FLOAT newDrawOrder);

		/**
		  Iterates through all RenderComponents attached to this component (not sub components),
		  and set their visibility state based on this UIComponent's resulting visibility state.
		  Then it'll notify any sub GUI Components to check their visibility state.
		 */
		virtual void RefreshRenderComponentVisibility ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		/**
		  Invoked whenever its Transformation changed in size.
		 */
		virtual void HandleSizeChange ();

		/**
		  Invoked whenever its Transformation changed in position.
		 */
		virtual void HandlePositionChange (const Vector2& deltaMove);

		/**
		  Polling events from the window handle.
		 */
		void HandleWindowPollEvent (const sf::Event newWindowEvent);

		virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
		virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
		virtual bool HandleMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

		/**
		  Various mouse event events invoked from the ExternalBorderInput component.  These typically invoke execute mouse event functions when the mouse pointer is beyond this component's borders.
		 */
		virtual void HandleExternalMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
		virtual bool HandleExternalMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
		virtual bool HandleExternalMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);
	};
}

#endif
#endif