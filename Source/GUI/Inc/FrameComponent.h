/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FrameComponent.h
  A component that implements a resizable bounding box.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef FRAMECOMPONENT_H
#define FRAMECOMPONENT_H

#include "GUIComponent.h"

#if INCLUDE_GUI

namespace SD
{
	class BorderedSpriteComponent;

	class FrameComponent : public GUIComponent
	{
		DECLARE_CLASS(FrameComponent)


		/*
		=====================
		  Data types
		=====================
		*/

	protected:
		enum EBorder
		{
			B_None,
			B_Top,
			B_Right,
			B_Bottom,
			B_Left
		};


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Reference to the component that's responsible for displaying the frames. */
		BorderedSpriteComponent* RenderComponent;		

	protected:
		/* Various flags that are associated with a EBorder type. */
		static const INT HorizontalBorderFlag;
		static const INT VerticalBorderFlag;

		/* If true, then this frame component will not resize when dragged. */
		bool bLockedFrame;

		/* The BorderThickness determines the how far the mouse pointer may be from the edge of this component and still grab the borders (in pixels). */
		FLOAT BorderThickness;

		/* Reference to the border that was grabbed for resizing. */
		EBorder HorGrabbedBorder; //horizontal borders
		EBorder VertGrabbedBorder; //vertical borders

		INT HoverFlags;


		/*
		=====================
		  Inherited	
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void SetWindowHandle (Window* newWindowHandle) override;
		virtual void Destroy () override;

	protected:
		virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
		virtual bool ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
		virtual bool ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;
		virtual bool ShouldHaveExternalInputComponent () const override;
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual void SetLockedFrame (bool bLockedFrame);
		virtual void SetBorderThickness (FLOAT newBorderThickness);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual bool GetLockedFrame () const;
		virtual FLOAT GetBorderThickness () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Evaluates the current cursor position and updates the mouse icon if necessary.
		  Returns false if this UI component is no longer overriding the mouse icon.
		 */
		virtual bool ShouldOverrideMouseIcon (const sf::Event::MouseMoveEvent& sfmlEvent);

		/**
		  Calculates the frame's transformation attributes based on the mouse's coordinates (for dragging borders).
		 */
		virtual void CalculateTransformationChanges (FLOAT mousePosX, FLOAT mousePosY);

		/**
		  Calculates the min/max coordinates and size of the frame component.
		  Separated from the Transformation since it's need to be calculated at a per drag update since the scale limits is based on coordinates and size.
		 */
		virtual void CalculateTransformationLimits (Range<FLOAT>& outAbsCoordinatesXLimits, Range<FLOAT>& outAbsCoordinatesYLimits, Range<FLOAT>& outSizeXLimits, Range<FLOAT>& outSizeYLimits) const;


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		/**
		  Function callback to see if it should continue to override the mouse pointer icon.
		  Returns false if this instance should no longer override the icon.
		 */
		virtual bool HandleMouseIconOverride (const sf::Event::MouseMoveEvent& sfmlEvent);
	};
}

#endif
#endif