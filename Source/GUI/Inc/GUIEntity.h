/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIEntity.h
  Entity that contains a series of GUIComponents and maintains relationships with other GUIEntities.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUIENTITY_H
#define GUIENTITY_H

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	class GUIComponent;
	class FocusComponent;

	class GUIEntity : public Entity
	{
		DECLARE_CLASS(GUIEntity)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* List of all instanced GUIEntities.  The last element is the GUIEntity with highest draw and input priority. */
		static std::vector<GUIEntity*> GUIList;

		/* Component that'll pass input events to focused entity.  This also handles tab ordering. */
		FocusComponent* Focus;

		AbsTransformComponent* Transformation;

	private:
		/* Determines how many draw order slots GUIEntity may consume.  Used for setting the draw order of the GUIEntity and its components based on its index position within GUIList. */
		static FLOAT MaxDrawOrderPerEntity;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;

	protected:
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Removes focus from this GUIEntity and its components.
		 */
		virtual void LoseFocus ();

		/**
		  Adds focus to this GUIEntity and its components.
		 */
		virtual void GainFocus ();

		/**
		  Updates which window handle this GUI should reside in.
		 */
		virtual void SetWindowHandle (Window* newWindowHandle);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual AbsTransformComponent* GetTransform () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Construct GUIComponents that'll assemble this UI instance.
		 */
		virtual void ConstructUI () = 0;

		/**
		  Calculates the transformation attributes for individual components.
		 */
		virtual void RefreshTransformations () = 0;

		/**
		  Creates and initializes the Transformation component.
		 */
		virtual void InitializeTransformation ();

		/**
		  Creates and initializes the focus component.  This is where the tab order of the focus component is specified.
		 */
		virtual void InitializeFocusComponent ();

		/**
		  Determine the index value this entity belongs in the GUIList.
		 */
		virtual unsigned int CalcGUIListIdx () const;

		/**
		  Adds this GUIEntity to the GUIList.
		 */
		virtual void RegisterToGUIList ();

		/**
		  Refreshes all GUIEntities' draw order and input priority based on its position in the GUIList.
		 */
		virtual void RefreshPriorityForAll ();

		/**
		  Sets all GUIComponents within this GUIEntity to base their draw order values from this specified value.
		 */
		virtual void SetBaseDrawOrder (FLOAT newDrawOrder);
		virtual void SetInputPriority (INT newInputPriority);


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleSizeChange ();
		virtual void HandlePositionChange (const Vector2& deltaMove);
	};
}

#endif
#endif