/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextRenderComponent.h
  A component that communicates with SFML to render
  text with specified parameters.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TEXTRENDERCOMPONENT_H
#define TEXTRENDERCOMPONENT_H

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	class TextRenderComponent : public RenderComponent
	{
		DECLARE_CLASS(TextRenderComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Reference to all instantiated SFML Text objects.  Each instance represents a new line. */
		std::vector<sf::Text*> Texts;

	protected:
		/* Determines the draw offsets for the Text instances.  Each element cooresponds to
		each Text instance.  This is primarily used for alignment. */
		std::vector<sf::Vector2f> DrawOffsets;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void Destroy () override;
		virtual void Render (const Camera* targetCamera, const Window* targetWindow) override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Deletes all text pointers and clears the Texts vector.
		 */
		virtual void DeleteAllText ();

		/**
		  Removes an element from the DrawOffsets vector.
		  count determines how many elements to clear after the index value.
		 */
		virtual void ClearDrawOffset (unsigned int index, unsigned int count);

		virtual void ClearAllDrawOffsets ();

		/**
		  Changes a particular draw offset that corresponds to the relative draw position of a Text pointer.
		  If index is greater than the drawOffsets vector size, then it'll append elements.
		 */
		virtual void SetDrawOffset (unsigned int index, FLOAT posX, FLOAT posY);
		virtual void SetDrawOffset (unsigned int index, const sf::Vector2f& newOffset);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		/**
		  Populates the second parameter with the value of DrawOffsets.at(index).
		  Function returns false if the index of DrawOffsets does not exist.
		 */
		virtual bool GetDrawOffset (unsigned int index, sf::Vector2f& outResultOffset) const;

		virtual size_t GetDrawOffsetLength () const;
	};
}

#endif
#endif