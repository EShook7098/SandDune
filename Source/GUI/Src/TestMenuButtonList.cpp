/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TestMenuButtonList.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI
#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TestMenuButtonList, GUIEntity)

	void TestMenuButtonList::InitProps ()
	{
		Super::InitProps();

		MenuFrame = nullptr;
	}

	void TestMenuButtonList::LoseFocus ()
	{
		Super::LoseFocus();

		LOG1(LOG_DEBUG, TXT("%s has lost focus."), GetUniqueName());
	}

	void TestMenuButtonList::GainFocus ()
	{
		Super::GainFocus();

		LOG1(LOG_DEBUG, TXT("%s has gained focus."), GetUniqueName());
	}

	void TestMenuButtonList::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(MenuFrame)

		CLEANUP_INVALID_POINTER_ARRAY(ButtonList)
	}

	void TestMenuButtonList::ConstructUI ()
	{
		MenuFrame = FrameComponent::CreateObject();
		if (AddComponent(MenuFrame))
		{
			MenuFrame->SetLockedFrame(true);
			MenuFrame->GetTransform()->SetRelativeTo(GetTransform());

			for (unsigned int i = 0; i < 10; i++)
			{
				ButtonComponent* newButton = InitializeButton();
				CHECK(newButton != nullptr)

				newButton->SetCaptionText(TXT("Button ") + INT(i + 1).ToString());

				//Disable button 5 to test if the focus component will skip it
				if (i == 4)
				{
					newButton->SetEnabled(false);
				}
			}
		}
	}

	void TestMenuButtonList::RefreshTransformations ()
	{
		if (VALID_OBJECT(MenuFrame))
		{
			MenuFrame->GetTransform()->SetAbsCoordinates(Vector2());
			MenuFrame->GetTransform()->SetBaseSize(GetTransform()->GetBaseSize());

			Vector2 buttonSize = Vector2(GetTransform()->GetCurrentSizeX() * 0.6f, 16.f);
			FLOAT buttonSpacing = 8.f;
			for (unsigned int i = 0; i < ButtonList.size(); i++)
			{
				ButtonList.at(i)->GetTransform()->SetBaseSize(buttonSize);
				ButtonList.at(i)->GetTransform()->SetAbsCoordinates(Vector2(0, ((buttonSize.Y + buttonSpacing) * FLOAT::MakeFloat(i)) + MenuFrame->GetBorderThickness()));
				ButtonList.at(i)->GetTransform()->SnapCenterHorizontally();
			}
		}
	}

	void TestMenuButtonList::InitializeFocusComponent ()
	{
		Super::InitializeFocusComponent();
		CHECK(Focus != nullptr)

		for (unsigned int i = 0; i < ButtonList.size(); i++)
		{
			Focus->TabOrder.push_back(ButtonList.at(i));
		}
	}

	ButtonComponent* TestMenuButtonList::InitializeButton ()
	{
		ButtonComponent* newButton = ButtonComponent::CreateObject();
		if (!MenuFrame->AddComponent(newButton))
		{
			return nullptr;
		}

		newButton->RenderComponent->SetSpriteTexture(GUITheme::GetGUITheme()->ButtonSingleSpriteBackground);
		newButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		dynamic_cast<SingleSpriteButtonState*>(newButton->GetButtonStateComponent())->Sprite = newButton->RenderComponent;
		newButton->GetButtonStateComponent()->RefreshState();
		newButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TestMenuButtonList, HandleButtonReleased, void, ButtonComponent*));
		ButtonList.push_back(newButton);

		return newButton;
	}

	void TestMenuButtonList::HandleButtonReleased (ButtonComponent* uiComponent)
	{
		LOG1(LOG_DEBUG, TXT("Button \"%s\" was released."), uiComponent->GetCaptionText());
	}
}

#endif
#endif