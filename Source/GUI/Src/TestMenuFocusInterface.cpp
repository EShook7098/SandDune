/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TestMenuFocusInterface.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI
#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TestMenuFocusInterface, GUIEntity)

	void TestMenuFocusInterface::InitProps ()
	{
		Super::InitProps();

		PageIdx = 0;

		MenuFrame = nullptr;
		NextPage = nullptr;
		PrevPage = nullptr;

		FocusButton = nullptr;
		FocusDropdown = nullptr;
		FocusTreeList = nullptr;

		DropdownEntries.push_back(TXT("First Option"));
		DropdownEntries.push_back(TXT("Some other option"));
		DropdownEntries.push_back(TXT("DString entries"));
		DropdownEntries.push_back(TXT("Press Up and Down"));
		DropdownEntries.push_back(TXT("to browse to an option"));
		DropdownEntries.push_back(TXT("Enter characters to"));
		DropdownEntries.push_back(TXT("jump to an option"));
		DropdownEntries.push_back(TXT("Press enter to expand"));
		DropdownEntries.push_back(TXT("and again to select item"));
		DropdownEntries.push_back(TXT("Testing Search"));
		DropdownEntries.push_back(TXT("Testing Search Entries"));
		DropdownEntries.push_back(TXT("Testing Search Fruits"));
		DropdownEntries.push_back(TXT("Testing Search Apples"));
		DropdownEntries.push_back(TXT("TESTING SEARCH BANANAS"));
		DropdownEntries.push_back(TXT("testing search oranges"));
		DropdownEntries.push_back(TXT("Testing search pears"));
		DropdownEntries.push_back(TXT("Testing search grapes"));
		DropdownEntries.push_back(TXT("Testing search Avocados"));
		DropdownEntries.push_back(TXT("Last option"));

		TreeListEntries.push_back(TXT("Root"));
		TreeListEntries.push_back(TXT("A"));
		TreeListEntries.push_back(TXT("A1"));
		TreeListEntries.push_back(TXT("A2"));
		TreeListEntries.push_back(TXT("A2a"));
		TreeListEntries.push_back(TXT("A2b"));
		TreeListEntries.push_back(TXT("A2bi"));
		TreeListEntries.push_back(TXT("A2bii"));
		TreeListEntries.push_back(TXT("A2biii"));
		TreeListEntries.push_back(TXT("A2biv"));
		TreeListEntries.push_back(TXT("A2c"));
		TreeListEntries.push_back(TXT("A2d"));
		TreeListEntries.push_back(TXT("A3"));
		TreeListEntries.push_back(TXT("A4"));
		TreeListEntries.push_back(TXT("A4a"));
		TreeListEntries.push_back(TXT("A4b"));
		TreeListEntries.push_back(TXT("A4bi"));
		TreeListEntries.push_back(TXT("A4bii"));
		TreeListEntries.push_back(TXT("B"));
		TreeListEntries.push_back(TXT("B1"));
		TreeListEntries.push_back(TXT("B1a"));
		TreeListEntries.push_back(TXT("B1b"));
		TreeListEntries.push_back(TXT("B1bi"));
		TreeListEntries.push_back(TXT("B1bii"));
		TreeListEntries.push_back(TXT("B1biii"));
		TreeListEntries.push_back(TXT("B1c"));
		TreeListEntries.push_back(TXT("B2"));
		TreeListEntries.push_back(TXT("B3"));
		TreeListEntries.push_back(TXT("C"));
		TreeListEntries.push_back(TXT("CA"));
	}

	void TestMenuFocusInterface::LoseFocus ()
	{
		Super::LoseFocus();

		LOG1(LOG_DEBUG, TXT("%s has lost focus."), GetUniqueName());
	}

	void TestMenuFocusInterface::GainFocus ()
	{
		Super::GainFocus();

		LOG1(LOG_DEBUG, TXT("%s has gained focus."), GetUniqueName());
	}

	void TestMenuFocusInterface::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(MenuFrame)
		CLEANUP_INVALID_POINTER(NextPage)
		CLEANUP_INVALID_POINTER(PrevPage)

		CLEANUP_INVALID_POINTER(FocusButton)
		CLEANUP_INVALID_POINTER(FocusDropdown)
		CLEANUP_INVALID_POINTER(FocusTreeList)
	}

	void TestMenuFocusInterface::ConstructUI ()
	{
		MenuFrame = FrameComponent::CreateObject();
		if (AddComponent(MenuFrame))
		{
			MenuFrame->SetLockedFrame(true);
			MenuFrame->GetTransform()->SetRelativeTo(GetTransform());

			NextPage = ButtonComponent::CreateObject();
			if (!MenuFrame->AddComponent(NextPage))
			{
				NextPage = nullptr;
				return;
			}

			NextPage->RenderComponent->SetSpriteTexture(GUITheme::GetGUITheme()->ButtonSingleSpriteBackground);
			NextPage->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			dynamic_cast<SingleSpriteButtonState*>(NextPage->GetButtonStateComponent())->Sprite = NextPage->RenderComponent;
			NextPage->GetButtonStateComponent()->RefreshState();
			NextPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandleNextPageReleased, void, ButtonComponent*));
			NextPage->SetCaptionText(TXT("Next"));

			PrevPage = ButtonComponent::CreateObject();
			if (!MenuFrame->AddComponent(PrevPage))
			{
				PrevPage = nullptr;
				return;
			}

			PrevPage->RenderComponent->SetSpriteTexture(GUITheme::GetGUITheme()->ButtonSingleSpriteBackground);
			PrevPage->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			dynamic_cast<SingleSpriteButtonState*>(PrevPage->GetButtonStateComponent())->Sprite = PrevPage->RenderComponent;
			PrevPage->GetButtonStateComponent()->RefreshState();
			PrevPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandlePrevPageReleased, void, ButtonComponent*));
			PrevPage->SetCaptionText(TXT("Previous"));

			PageData.push_back(SPageComponents());
			FocusButton = ButtonComponent::CreateObject();
			if (!MenuFrame->AddComponent(FocusButton))
			{
				FocusButton = nullptr;
				return;
			}

			FocusButton->RenderComponent->SetSpriteTexture(GUITheme::GetGUITheme()->ButtonSingleSpriteBackground);
			FocusButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			dynamic_cast<SingleSpriteButtonState*>(FocusButton->GetButtonStateComponent())->Sprite = FocusButton->RenderComponent;
			FocusButton->GetButtonStateComponent()->RefreshState();
			FocusButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandleButtonReleased, void, ButtonComponent*));
			FocusButton->SetCaptionText(TXT("Focus Button"));
			PageData.at(PageData.size() - 1).Components.push_back(FocusButton);

			FocusDropdown = DropdownComponent::CreateObject();
			if (!MenuFrame->AddComponent(FocusDropdown))
			{
				FocusDropdown = nullptr;
				return;
			}
			FocusDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandleDropdownOptionSelected, void, INT);
			FocusDropdown->SetNoSelectedItemText(TXT("<Nothing selected>"));
			PageData.at(PageData.size() - 1).Components.push_back(FocusDropdown);
			for (unsigned int i = 0; i < DropdownEntries.size(); i++)
			{
				FocusDropdown->AddOption<DString>(GUIDataElement<DString>(&DropdownEntries.at(i), DropdownEntries.at(i)));
			}

			PageData.push_back(SPageComponents());
			FocusTreeList = TreeListComponent::CreateObject();
			if (!MenuFrame->AddComponent(FocusTreeList))
			{
				FocusTreeList = nullptr;
				return;
			}
			FocusTreeList->OnOptionSelected = SDFUNCTION_1PARAM(this, TestMenuFocusInterface, HandleTreeListOptionSelected, void, INT);
			PageData.at(PageData.size() - 1).Components.push_back(FocusTreeList);
			vector<TreeListComponent::SDataBranch*> branches;
			for (unsigned int i = 0; i < TreeListEntries.size(); i++)
			{
				branches.push_back(new TreeListComponent::SDataBranch(new GUIDataElement<DString>(&TreeListEntries.at(i), TreeListEntries.at(i))));
			}
			/*
			Manually link branches to this format
			[01]A
			[02]	A1
			[03]	A2
			[04]		A2a
			[05]		A2b
			[06]			A2bi
			[07]			A2bii
			[08]			A2biii
			[09]			A2biv
			[10]		A2c
			[11]		A2d
			[12]	A3
			[13]	A4
			[14]		A4a
			[15]		A4b
			[16]			A4bi
			[17]			A4bii
			[18]B
			[19]	B1
			[20]		B1a
			[21]		B1b
			[22]			B1bi
			[23]			B1bii
			[24]			B1biii
			[25]		B1c
			[26]	B2
			[27]	B3
			[28]C
			[29]	CA
			*/
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(0), *branches.at(1));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(1), *branches.at(2));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(1), *branches.at(3));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(3), *branches.at(4));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(3), *branches.at(5));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(5), *branches.at(6));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(5), *branches.at(7));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(5), *branches.at(8));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(5), *branches.at(9));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(3), *branches.at(10));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(3), *branches.at(11));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(1), *branches.at(12));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(1), *branches.at(13));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(13), *branches.at(14));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(13), *branches.at(15));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(15), *branches.at(16));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(15), *branches.at(17));
			
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(0), *branches.at(18));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(18), *branches.at(19));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(19), *branches.at(20));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(19), *branches.at(21));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(21), *branches.at(22));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(21), *branches.at(23));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(21), *branches.at(24));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(19), *branches.at(25));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(18), *branches.at(26));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(18), *branches.at(27));

			TreeListComponent::SDataBranch::LinkBranches(*branches.at(0), *branches.at(28));
			TreeListComponent::SDataBranch::LinkBranches(*branches.at(28), *branches.at(29));
			FocusTreeList->SetTreeList(branches);
		}

		RefreshPageVisibility();
	}

	void TestMenuFocusInterface::RefreshTransformations ()
	{
		if (VALID_OBJECT(MenuFrame))
		{
			MenuFrame->GetTransform()->SetBaseSize(GetTransform()->GetBaseSize());
			MenuFrame->GetTransform()->SetAbsCoordinates(Vector2());

			if (VALID_OBJECT(NextPage))
			{
				NextPage->GetTransform()->SetBaseSize(Vector2(96, 16));
				NextPage->GetTransform()->SetAbsCoordinates(Vector2(MenuFrame->GetTransform()->GetCurrentSizeX() - NextPage->GetTransform()->GetCurrentSizeX() - MenuFrame->GetBorderThickness(), MenuFrame->GetTransform()->GetCurrentSizeY() - NextPage->GetTransform()->GetCurrentSizeY() - MenuFrame->GetBorderThickness()));
			}

			if (VALID_OBJECT(PrevPage))
			{
				PrevPage->GetTransform()->SetBaseSize(Vector2(96, 16));
				PrevPage->GetTransform()->SetAbsCoordinates(Vector2(MenuFrame->GetBorderThickness(), MenuFrame->GetTransform()->GetCurrentSizeY() - PrevPage->GetTransform()->GetCurrentSizeY() - MenuFrame->GetBorderThickness()));
			}

			Vector2 posOffset = Vector2(MenuFrame->GetBorderThickness(), MenuFrame->GetBorderThickness());
			if (VALID_OBJECT(FocusButton))
			{
				FocusButton->GetTransform()->SetBaseSize(Vector2(128, 16));
				FocusButton->GetTransform()->SetAbsCoordinates(posOffset);
				posOffset.Y += FocusButton->GetTransform()->GetCurrentSizeY();
			}

			posOffset.Y += 8.f;

			if (VALID_OBJECT(FocusDropdown))
			{
				FocusDropdown->GetTransform()->SetBaseSize(Vector2(200, 200));
				FocusDropdown->GetTransform()->SetAbsCoordinates(posOffset);
				posOffset.Y += FocusDropdown->GetTransform()->GetCurrentSizeY();
			}

			if (VALID_OBJECT(FocusTreeList))
			{
				FocusTreeList->GetTransform()->SetAbsCoordinates(Vector2(MenuFrame->GetBorderThickness(), MenuFrame->GetBorderThickness()));
				FocusTreeList->GetTransform()->SetBaseSize(MenuFrame->GetTransform()->GetCurrentSize() - Vector2(MenuFrame->GetBorderThickness() * 2, (MenuFrame->GetBorderThickness() * 2) + NextPage->GetTransform()->GetCurrentSizeY()));
			}
		}
	}

	void TestMenuFocusInterface::InitializeFocusComponent ()
	{
		Super::InitializeFocusComponent();
		CHECK(Focus != nullptr)

		Focus->TabOrder.push_back(FocusButton);
		Focus->TabOrder.push_back(FocusDropdown);
		Focus->TabOrder.push_back(PrevPage);
		Focus->TabOrder.push_back(NextPage);
		Focus->TabOrder.push_back(FocusTreeList);
	}

	void TestMenuFocusInterface::RefreshPageVisibility ()
	{
		for (unsigned int i = 0; i < PageData.size(); i++)
		{
			bool bIsVisible = (PageIdx == i);
			for (unsigned int j = 0; j < PageData.at(i).Components.size(); j++)
			{
				PageData.at(i).Components.at(j)->SetVisibility(bIsVisible);
			}
		}
	}

	void TestMenuFocusInterface::HandleNextPageReleased (ButtonComponent* uiComponent)
	{
		if (++PageIdx >= static_cast<int>(PageData.size()))
		{
			PageIdx = 0;
		}

		RefreshPageVisibility();
	}

	void TestMenuFocusInterface::HandlePrevPageReleased (ButtonComponent* uiComponent)
	{
		if (--PageIdx < 0)
		{
			PageIdx = static_cast<int>(PageData.size()) - 1;
		}

		RefreshPageVisibility();
	}

	void TestMenuFocusInterface::HandleButtonReleased (ButtonComponent* uiComponent)
	{
		LOG1(LOG_DEBUG, TXT("Button \"%s\" was released."), uiComponent->GetCaptionText());
	}

	void TestMenuFocusInterface::HandleDropdownOptionSelected (INT newIdx)
	{
		LOG1(LOG_DEBUG, TXT("Focus Dropdown selected new item:  %s"), FocusDropdown->GetSelectedItem<DString>()->GetLabelText());
	}

	void TestMenuFocusInterface::HandleTreeListOptionSelected (INT optionIdx)
	{
		TreeListComponent::SDataBranch* selectedItem = FocusTreeList->GetBranchFromIdx(optionIdx);
		if (selectedItem != nullptr && selectedItem->Data != nullptr)
		{
			LOG1(LOG_DEBUG, TXT("Focus TreeList selected new item:  %s"), selectedItem->Data->GetLabelText());
		}
		else
		{
			LOG(LOG_DEBUG, TXT("Focus TreeList cleared item selection."));
		}
	}
}

#endif
#endif