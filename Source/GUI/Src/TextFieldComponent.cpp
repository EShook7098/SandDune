/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	//TODO:  Don't update text on scroll, instead cache a vector of all lines, and only render a
	//	selected number of lines to be rendered on UI.
	IMPLEMENT_CLASS(TextFieldComponent, GUIComponent)

	void TextFieldComponent::InitProps ()
	{
		Super::InitProps();

		ScrollJumpInterval = 4;
		TextLabel = nullptr;
		Background = nullptr;
		RenderComponent = nullptr;
		Scrollbar = nullptr;
		bEditable = true;
		bSelectable = true;
		bSingleLine = false;
		HorizontalScrollPosition = 0;
		FullText = TXT("");
		CursorPosition = -1;
		ActiveLineIndex = -1;
		HighlightEndPosition = -1;
		bDraggingOverText = false;
		bOverridingMousePointerIcon = false;
	}

	void TextFieldComponent::BeginObject ()
	{
		Super::BeginObject();

		//Instantiate the proper label component based on the single line property
		SetSingleLine(bSingleLine);

		InitializeTextLabelComponent();
		InitializeScrollbarComponent();
		InitializeBackgroundComponent();
		InitializeRenderComponent();
		LinkComponents();

		//TODO:  This is potentially causing a dead lock. (at init)
		//SetFullText(FullText, false);
	}

	void TextFieldComponent::SetWindowHandle (Window* newWindowHandle)
	{
		Super::SetWindowHandle(newWindowHandle);

		if (VALID_OBJECT(RenderComponent) && WindowHandle != nullptr)
		{
			RenderComponent->RelevantRenderWindows.clear();
			RenderComponent->RelevantRenderWindows.push_back(WindowHandle);
		}
	}

	void TextFieldComponent::Destroy ()
	{
		GetMousePointer()->RemoveIconOverride(SDFUNCTION_1PARAM(this, TextFieldComponent, HandleMouseIconOverride, bool, const sf::Event::MouseMoveEvent&));
		bOverridingMousePointerIcon = false;

		Super::Destroy();
	}

	void TextFieldComponent::InitializeInputComponent ()
	{
		Super::InitializeInputComponent();

		if (VALID_OBJECT(Input))
		{
			Input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, TextFieldComponent, HandleCaptureInput, bool, const sf::Event&);
			Input->CaptureTextDelegate = SDFUNCTION_1PARAM(this, TextFieldComponent, HandleCaptureText, bool, const sf::Event&);
		}
	}

	bool TextFieldComponent::ShouldHaveInputComponent () const
	{
		if (bEditable || bSelectable)
		{
			return true;
		}

		return ShouldHaveInputComponent();
	}

	void TextFieldComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

		if (!bOverridingMousePointerIcon)
		{
			UpdateMousePointerIcon(sfmlEvent);
		}

		if (bDraggingOverText)
		{
			INT charIndex = FindCharacterIndex(sfmlEvent.x, sfmlEvent.y);

			if (charIndex >= 0)
			{
				SetHighlightEndPosition(charIndex);
			}
		}
	}

	bool TextFieldComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		if (!bSelectable || sfmlEvent.button != sf::Mouse::Left)
		{
			//Only listen to left mouse button events, but still capture the mouse event if over current component
			return Transformation->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
		}

		if (eventType == sf::Event::MouseButtonReleased)
		{
			bool bOldDraggingOverText = bDraggingOverText;
			bDraggingOverText = false;

			return bOldDraggingOverText;
		}
		else if (Transformation->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)))
		{
			INT charIndex = FindCharacterIndex(INT(sfmlEvent.x), INT(sfmlEvent.y));

			if (charIndex >= 0)
			{
				SetCursorPosition(charIndex);
			}

			SetHighlightEndPosition(-1);
			bDraggingOverText = true;

			return true;
		}

		//deselect
		SetCursorPosition(-1);
		SetHighlightEndPosition(-1);
		return false;
	}

	void TextFieldComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		//May need to check sub component pointers since those are publicly accessable via Getter functions
		CLEANUP_INVALID_POINTER(TextLabel)
		CLEANUP_INVALID_POINTER(Background)
		CLEANUP_INVALID_POINTER(RenderComponent)
		CLEANUP_INVALID_POINTER(Scrollbar)
	}

	bool TextFieldComponent::CursorIsVisible () const
	{
		if (CursorPosition < 0)
		{
			return false;
		}

		if (bSingleLine)
		{
			if (VALID_OBJECT(TextLabel))
			{
				return (CursorPosition >= TextLabel->GetTrimmedTextHead().Length() &&
						CursorPosition < TextLabel->GetFullText().Length() - TextLabel->GetTrimmedTextTail().Length());
			}
		}
		else if (VALID_OBJECT(Scrollbar))
		{
			return (ActiveLineIndex >= 0 && ActiveLineIndex >= Scrollbar->GetScrollPosition() &&
					ActiveLineIndex < Scrollbar->GetScrollPosition() + Scrollbar->GetNumVisibleScrollPositions());
		}

		return false;
	}

	void TextFieldComponent::ReplaceTextLabel (TextFieldLabelComponent* newTextLabel)
	{
		if (VALID_OBJECT(TextLabel))
		{
			RemoveComponent(TextLabel);
			TextLabel->Destroy();
			TextLabel = nullptr;
		}

		if (AddComponent(newTextLabel))
		{
			TextLabel = newTextLabel;
			TextLabel->SetMaxNumLinesChanged(bind(&TextFieldComponent::HandleMaxNumLinesChanged, this, placeholders::_1));
		}

		if (VALID_OBJECT(Scrollbar))
		{
			UpdateScrollbarLength();
		}
	}

	void TextFieldComponent::ReplaceBackground (FrameComponent* newBackground)
	{
		if (VALID_OBJECT(Background))
		{
			//Preserve the label component
			if (VALID_OBJECT(TextLabel))
			{
				TextLabel->DetachSelfFromOwner();
			}

			if (VALID_OBJECT(RenderComponent))
			{
				RenderComponent->DetachSelfFromOwner();
			}

			RemoveComponent(Background);
			Background->Destroy();
			Background = nullptr;
		}

		GUIComponent* guiOwner = this;

		if (AddComponent(newBackground))
		{
			Background = newBackground;
			guiOwner = Background;
		}

		if (VALID_OBJECT(TextLabel))
		{
			guiOwner->AddComponent(TextLabel);
		}

		if (VALID_OBJECT(RenderComponent))
		{
			guiOwner->AddComponent(RenderComponent);
		}
	}

	void TextFieldComponent::ReplaceRenderComponent (TextFieldRenderComponent* newRenderComponent)
	{
		if (VALID_OBJECT(RenderComponent))
		{
			RemoveComponent(RenderComponent);
			RenderComponent->Destroy();
			RenderComponent = nullptr;
		}

		if (AddComponent(newRenderComponent))
		{
			RenderComponent = newRenderComponent;
		}
	}

	void TextFieldComponent::SetScrollbar (ScrollbarComponent* newScrollbar)
	{
		if (VALID_OBJECT(Scrollbar))
		{
			RemoveComponent(Scrollbar);
			Scrollbar->Destroy();
			Scrollbar = nullptr;
		}

		if (AddComponent(newScrollbar))
		{
			Scrollbar = newScrollbar;
			Scrollbar->SetScrollPositionChanged(bind(&TextFieldComponent::HandleScrollPositionChanged, this, placeholders::_1));
			Scrollbar->SetNumVisibleScrollPositions(TextLabel->GetMaxNumLines());
		}
	}

	void TextFieldComponent::SetFullText (DString newContent, bool bChangesAtCursor)
	{
		TextLabel->SetTextContent(newContent, CursorPosition, bChangesAtCursor);
		FullText = TextLabel->GetFullText();

		if (VALID_OBJECT(Scrollbar))
		{
			UpdateScrollbarLength();
		}
	}

	void TextFieldComponent::SetEditable (bool bNewEditable)
	{
		if (bEditable == bNewEditable)
		{
			return; //Nothing needs changing
		}

		bEditable = bNewEditable;

		if (!bEditable)
		{
			//deselect
			SetHighlightEndPosition(-1);
			SetCursorPosition(-1);

			if (bOverridingMousePointerIcon)
			{
				GetMousePointer()->EvaluateMouseIconOverrides();
				bOverridingMousePointerIcon = false;
			}
		}

		ConditionallySetInputComponent();
	}

	void TextFieldComponent::SetSelectable (bool bNewSelectable)
	{
		bSelectable = bNewSelectable;

		if (!bSelectable)
		{
			//deselect
			SetHighlightEndPosition(-1);
			SetCursorPosition(-1);
		}

		ConditionallySetInputComponent();
	}

	void TextFieldComponent::SetSingleLine (bool bNewSingleLine)
	{
		if (bSingleLine == bNewSingleLine)
		{
			return;
		}

		bSingleLine = bNewSingleLine;

		if (VALID_OBJECT(Scrollbar))
		{
			UpdateScrollbarLength();
			Scrollbar->SetVisibility(!bSingleLine);
			Scrollbar->SetEnabled(!bSingleLine);
		}

		if (VALID_OBJECT(TextLabel))
		{
			TextLabel->DetachSelfFromOwner();
			TextLabel->Destroy();
			TextLabel = nullptr;
		}

		//Refresh text label component since we need to swap classes
		InitializeTextLabelComponent();
		LinkComponents();
		SetFullText(FullText, false);
	}

	void TextFieldComponent::SetHorizontalScrollPosition (INT newHorizontalScrollPosition)
	{
		HorizontalScrollPosition = newHorizontalScrollPosition;
	}

	void TextFieldComponent::SetCursorPosition (INT newPosition)
	{
		CursorPosition = Utils::Min(newPosition, FullText.Length());
		CalculateActiveLineIndex();

		if (CursorPosition >= 0 && !bSingleLine && !CursorIsVisible())
		{
			if (bSingleLine)
			{
				bool bCursorOnLeft = (CursorPosition < GetScrollPosition());
				INT newScrollPosition = (bCursorOnLeft) ? CursorPosition - ScrollJumpInterval : CursorPosition + TextLabel->GetTextContent().Length() + ScrollJumpInterval;
				SetHorizontalScrollPosition(newScrollPosition);
			}
			else
			{
				//Update the scroll position so that the cursor becomes visible
				bool bCursorOnTop = (ActiveLineIndex < GetScrollPosition() + Scrollbar->GetNumVisibleScrollPositions());
				INT newScrollPosition = (bCursorOnTop) ? ActiveLineIndex - ScrollJumpInterval : ActiveLineIndex + Scrollbar->GetNumVisibleScrollPositions() + ScrollJumpInterval;
				Scrollbar->SetScrollPosition(newScrollPosition);
			}
		}
	}

	void TextFieldComponent::SetHighlightEndPosition (INT newPosition)
	{
		HighlightEndPosition = Utils::Min(newPosition, FullText.Length());
	}

	LabelComponent* TextFieldComponent::GetTextLabel ()
	{
		return TextLabel;
	}

	FrameComponent* TextFieldComponent::GetBackground ()
	{
		return Background;
	}

	TextFieldRenderComponent* TextFieldComponent::GetRenderComponent ()
	{
		return RenderComponent;
	}

	ScrollbarComponent* TextFieldComponent::GetScrollbar ()
	{
		return Scrollbar;
	}

	bool TextFieldComponent::GetEditable () const
	{
		return bEditable;
	}

	bool TextFieldComponent::GetSelectable () const
	{
		return bSelectable;
	}

	bool TextFieldComponent::GetSingleLine () const
	{
		return bSingleLine;
	}

	INT TextFieldComponent::GetHorizontalScrollPosition () const
	{
		return HorizontalScrollPosition;
	}

	DString TextFieldComponent::GetFullText () const
	{
		return TextLabel->GetFullText();
	}

	INT TextFieldComponent::GetCursorPosition () const
	{
		return CursorPosition;
	}

	INT TextFieldComponent::GetHighlightEndPosition () const
	{
		return HighlightEndPosition;
	}

	/**
	 If bSingleLine is true, then it retrieves the horizontal scroll position.
	 Otherwise, it'll retrieve the vertical scroll position from the scroll bar.
	*/
	INT TextFieldComponent::GetScrollPosition () const
	{
		if (!bSingleLine)
		{
			if (VALID_OBJECT(Scrollbar))
			{
				return Scrollbar->GetScrollPosition();
			}

			return 0;
		}

		return HorizontalScrollPosition;
	}

	DString TextFieldComponent::GetSelectedText () const
	{
		if (HighlightEndPosition < 0 || HighlightEndPosition == CursorPosition || CursorPosition < 0)
		{
			return TXT(""); //Nothing is selected
		}

		if (HighlightEndPosition < CursorPosition)
		{
			return FullText.SubString(HighlightEndPosition.Value, CursorPosition.Value - HighlightEndPosition.Value);
		}
		else
		{
			return FullText.SubString(CursorPosition.Value, HighlightEndPosition.Value - CursorPosition.Value);
		}
	}

	void TextFieldComponent::InitializeTextLabelComponent ()
	{
		if (VALID_OBJECT(TextLabel))
		{
			return;
		}

		TextLabel = (bSingleLine) ? TextLineLabelComponent::CreateObject() : TextFieldLabelComponent::CreateObject();
		if (VALID_OBJECT(TextLabel))
		{
			TextLabel->SetMaxNumLinesChanged(bind(&TextFieldComponent::HandleMaxNumLinesChanged, this, placeholders::_1));
		}
	}

	void TextFieldComponent::InitializeScrollbarComponent ()
	{
		if (VALID_OBJECT(Scrollbar))
		{
			return; //already initialized (or copied from another text field)
		}

		Scrollbar = ScrollbarComponent::CreateObject();
		if (VALID_OBJECT(Scrollbar))
		{
			AddComponent(Scrollbar);
			Scrollbar->SetScrollPositionChanged(bind(&TextFieldComponent::HandleScrollPositionChanged, this, placeholders::_1));
		}
	}

	void TextFieldComponent::InitializeBackgroundComponent ()
	{
		if (VALID_OBJECT(Background))
		{
			return; //already initialized (or copied from another text field)
		}

		Background = FrameComponent::CreateObject();
		if (VALID_OBJECT(Background))
		{
			AddComponent(Background);
			Background->SetLockedFrame(true);
			Background->SetBorderThickness(2);
		}
	}

	void TextFieldComponent::InitializeRenderComponent ()
	{
		if (VALID_OBJECT(RenderComponent))
		{
			return; //already initialized (or copied from another text field)
		}

		RenderComponent = TextFieldRenderComponent::CreateObject();
		if (VALID_OBJECT(RenderComponent))
		{
			RenderComponent->OwningTextField = this;
			RenderComponent->TransformationInterface = Transformation;
			RenderComponent->RelevantRenderWindows.clear();
			RenderComponent->RelevantRenderWindows.push_back(GetWindowHandle());
		}
	}

	void TextFieldComponent::LinkComponents ()
	{
		if (!VALID_OBJECT(TextLabel) || !VALID_OBJECT(Background) || !VALID_OBJECT(RenderComponent))
		{
			LOG1(LOG_WARNING, TXT("Unable to link components for %s since either the TextLabel, Background, or RenderComponent does not exist."), GetUniqueName());
			return;
		}

		//Expected component chain for TextFieldComponents
		//Scrollbar
		//Background
		//	TextLabel (or TextLine)
		//		TextRender

		//Linking TextLabel
		TextLabel->AddComponent(RenderComponent);

		//Replace the label component's render component with this text field's render component
		TextRenderComponent* oldRenderComponent = TextLabel->RenderComponent;
		if (VALID_OBJECT(oldRenderComponent) && oldRenderComponent != RenderComponent)
		{
			TextLabel->RemoveComponent(TextLabel->RenderComponent);
			oldRenderComponent->Destroy();
			TextLabel->AddComponent(RenderComponent);
			TextLabel->RenderComponent = RenderComponent;
		}

		//Linking Scrollbar
		if (VALID_OBJECT(Scrollbar))
		{
			Scrollbar->SetNumVisibleScrollPositions(TextLabel->GetMaxNumLines());
		}

		//Linking Background
		Background->AddComponent(TextLabel);
	}

	void TextFieldComponent::UpdateMousePointerIcon (const sf::Event::MouseMoveEvent& sfmlEvent)
	{
		bool bHoveringOverText = (bEditable && TextLabel->GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));

		if (bHoveringOverText)
		{
			GetMousePointer()->PushMouseIconOverride(GUITheme::GetGUITheme()->TextPointer, SDFUNCTION_1PARAM(this, TextFieldComponent, HandleMouseIconOverride, bool, const sf::Event::MouseMoveEvent&));
			bOverridingMousePointerIcon = true;
		}
	}

	INT TextFieldComponent::FindCharacterIndex (INT absX, INT absY)
	{
		if (!VALID_OBJECT(TextLabel))
		{
			LOG1(LOG_WARNING, TXT("%s is attempting to find character index from an invalid text label component."), GetUniqueName());
			return -1;
		}

		if (!VALID_OBJECT(RenderComponent))
		{
			LOG1(LOG_WARNING, TXT("%s is attempting to find character index from an invalid render component."), GetUniqueName());
			return -1;
		}

		if (FullText.IsEmpty())
		{
			return 0;
		}

		AbsTransformComponent* textTransformation = TextLabel->GetTransform();
		INT lineIndex = 0; 

		if (!bSingleLine)
		{
			FLOAT yPercent = (FLOAT(absY) - textTransformation->GetAbsCoordinatesY()) / (textTransformation->GetCurrentSize().Y - textTransformation->GetAbsCoordinatesY());

			if (yPercent > 1.f)
			{
				return -1; //Out of bounds
			}

			FLOAT linesPerPercent = (1.f/TextLabel->GetMaxNumLines().ToFLOAT());

			//Calculate which line was clicked on
			INT lineIndex = Utils::Min(INT(RenderComponent->Texts.size()), (linesPerPercent * yPercent).ToINT());
		}

		//Count how many characters are about to be skipped
		INT skippedChars = TextLabel->GetTrimmedTextHead().Length();
		for (unsigned int i = 0; i < lineIndex; i++)
		{
			skippedChars += static_cast<int>(RenderComponent->Texts.at(i)->getString().getSize());
		}

		//Measure the width of the first 8 characters to get an estimate how wide a character is
		sf::Text* labelRender = RenderComponent->Texts.at(lineIndex.ToUnsignedInt());

		if (labelRender->getString().getSize() < 8)
		{
			FLOAT bestDist = 9001; //over 9 thousand!
			INT bestIndex = -1;
			//It may be faster and simpler to iterate through each character than to estimate position and iterate from estimation
			for (unsigned int i = 0; i < labelRender->getString().getSize(); i++)
			{
				sf::Vector2f charLocation = labelRender->findCharacterPos(i);
				if (FLOAT::Abs(FLOAT(charLocation.x) - absX.ToFLOAT()) < bestDist)
				{	
					bestDist = FLOAT(charLocation.x) - FLOAT::Abs(absX.ToFLOAT());
					bestIndex = i;
				}
			}

			return skippedChars + bestIndex;
		}

		//Approximate the width of a single character through taking the average width of 8 characters
		sf::Vector2f drawOffset;
		RenderComponent->GetDrawOffset(lineIndex.ToUnsignedInt(), drawOffset);
		sf::Vector2f charPosReference = labelRender->findCharacterPos(8);

		FLOAT approxCharWidth = (FLOAT(charPosReference.x - drawOffset.x) - textTransformation->GetAbsCoordinatesX()) / 8.f;

		INT curCharIndex = 0;
		sf::Vector2f curCharPosition;
		curCharPosition.x = textTransformation->GetAbsCoordinatesX().Value + drawOffset.x;

		//Continuously jump ahead based on approximations until we passed the x position
		do
		{
			curCharIndex += Utils::Max<INT>(1, FLOAT::Round(FLOAT(absX.ToFLOAT() - curCharPosition.x) / approxCharWidth).ToINT());
			curCharIndex = Utils::Min(curCharIndex, FullText.Length() - 1);
			curCharPosition = labelRender->findCharacterPos(curCharIndex.Value);
		}
		while(FLOAT(curCharPosition.x) < absX.ToFLOAT() && curCharIndex < FullText.Length() - 1);

		//Decrement one character at a time until we passed the threshold
		while (FLOAT(curCharPosition.x) > absX.ToFLOAT() && curCharIndex > 0)
		{
			--curCharIndex;
			curCharPosition = labelRender->findCharacterPos(curCharIndex.Value);
		}

		return (skippedChars + curCharIndex);
	}

	void TextFieldComponent::CalculateActiveLineIndex ()
	{
		if (bSingleLine || CursorPosition < 0)
		{
			ActiveLineIndex = -1;
		}

		INT accumulatedChars = 0;
		for(ActiveLineIndex = 0; ActiveLineIndex.ToUnsignedInt() < RenderComponent->Texts.size(); ActiveLineIndex++)
		{
			accumulatedChars += static_cast<int>(RenderComponent->Texts.at(ActiveLineIndex.ToUnsignedInt())->getString().getSize());

			if (accumulatedChars > CursorPosition)
			{
				return;
			}
		}

		ActiveLineIndex = -1; //Invalid cursor position
	}

	void TextFieldComponent::DeleteSelectedText ()
	{
		DString result = GetFullText();

		if (HighlightEndPosition < 0 || CursorPosition < 0 || HighlightEndPosition == CursorPosition
				|| HighlightEndPosition >= FullText.Length() || CursorPosition >= FullText.Length())
		{
			return;
		}
		else if (HighlightEndPosition < CursorPosition)
		{
			result.Remove(HighlightEndPosition.Value, (CursorPosition - HighlightEndPosition).Value);
		}
		else
		{
			result.Remove(CursorPosition.Value, (HighlightEndPosition - CursorPosition).Value);
		}

		SetFullText(result, true);
	}

	void TextFieldComponent::UpdateScrollbarLength ()
	{
		INT maxPosition = static_cast<int>(RenderComponent->Texts.size());
		maxPosition += GetScrollPosition() + TextLabel->GetNumTrimmedLines(); //include text head and text trail

		Scrollbar->SetMaxScrollPosition(maxPosition);
	}

	void TextFieldComponent::UpdateHighlightPositionPriorToMove (bool bShouldHighlight)
	{
		if (bShouldHighlight && HighlightEndPosition < 0)
		{
			SetHighlightEndPosition(CursorPosition);
		}
		else if (!bShouldHighlight)
		{
			SetHighlightEndPosition(-1);
		}
	}

	void TextFieldComponent::JumpCursorToPrevWord ()
	{
		INT curCharIndex = Utils::Max<INT>(CursorPosition, 0);

		while (curCharIndex > 0 && FullText.At(curCharIndex.Value) != ' ')
		{
			curCharIndex--;
		}

		SetCursorPosition(curCharIndex);
	}

	void TextFieldComponent::JumpCursorToNextWord ()
	{
		INT curCharIndex = Utils::Max<INT>(CursorPosition, 0);

		while (curCharIndex < FullText.Length() && FullText.At(curCharIndex.Value) != ' ')
		{
			curCharIndex++;
		}

		SetCursorPosition(curCharIndex);
	}

	void TextFieldComponent::JumpCursorToBeginningLine ()
	{
		INT skippedChars = 0;

		for (unsigned int i = 0; i < ActiveLineIndex; i++)
		{
			skippedChars += static_cast<int>(RenderComponent->Texts.at(i)->getString().getSize());
		}

		SetCursorPosition(skippedChars + 1);
	}

	void TextFieldComponent::JumpCursorToEndLine ()
	{
		INT skippedChars = 0;

		for (unsigned int i = 0; i < ActiveLineIndex; i++)
		{
			skippedChars += static_cast<int>(RenderComponent->Texts.at(i)->getString().getSize());
		}

		DString curLine = RenderComponent->Texts.at(ActiveLineIndex.ToUnsignedInt())->getString();

		SetCursorPosition(skippedChars + curLine.Length());
	}

	bool TextFieldComponent::HandleCaptureInput (sf::Event keyEvent)
	{
		if (CursorPosition < 0)
		{
			return false;
		}

		if (!bSelectable && !bEditable)
		{
			return false;
		}

		if (keyEvent.key.control)
		{
			switch (keyEvent.key.code)
			{
				case(sf::Keyboard::C):
					OS_CopyToClipboard(GetSelectedText());
					return true;

				case(sf::Keyboard::V):
					//TODO:  Paste
					//OS_PasteFromClipboard();
					return true;

				case(sf::Keyboard::X):
					OS_CopyToClipboard(GetSelectedText());
					DeleteSelectedText();
					return true;

				case(sf::Keyboard::End):
					//Select text from current position to the end
					UpdateHighlightPositionPriorToMove(keyEvent.key.shift);
					SetCursorPosition(INT(GetFullText().Length() + 1));
					return true;

				case(sf::Keyboard::Home):
					UpdateHighlightPositionPriorToMove(keyEvent.key.shift);
					SetCursorPosition(0);
					return true;

				case(sf::Keyboard::Left):
					UpdateHighlightPositionPriorToMove(keyEvent.key.shift);
					JumpCursorToPrevWord();
					return true;

				case(sf::Keyboard::Right):
					UpdateHighlightPositionPriorToMove(keyEvent.key.shift);
					JumpCursorToNextWord();
					return true;

				case(sf::Keyboard::Up):
					//TODO:  Set scroll position up one (don't modify cursor)
					return true;

				case(sf::Keyboard::Down):
					//TODO:  Set scroll position down one (don't modify cursor)
					return true;
			}
		}

		switch (keyEvent.key.code)
		{
			case(sf::Keyboard::Escape):
				SetCursorPosition(-1);
				SetHighlightEndPosition(-1);
				return true;

			case(sf::Keyboard::Home):
				UpdateHighlightPositionPriorToMove(keyEvent.key.shift);
				JumpCursorToBeginningLine();
				return true;

			case(sf::Keyboard::End):
				UpdateHighlightPositionPriorToMove(keyEvent.key.shift);
				JumpCursorToEndLine();
				return true;

			case(sf::Keyboard::Left):
				UpdateHighlightPositionPriorToMove(keyEvent.key.shift);
				SetCursorPosition(Utils::Max(CursorPosition - 1, INT(0)));
				return true;

			case(sf::Keyboard::Right):
				UpdateHighlightPositionPriorToMove(keyEvent.key.shift);
				SetCursorPosition(CursorPosition + 1);
				return true;

			//TODO:  Handle Up/down navigation
			//TODO:  Handle page up/page down navigation (jump one increment by numVisibleIndices)
		}

		//Capture input anyway since this text field is focused
		return true;
	}

	bool TextFieldComponent::HandleCaptureText (sf::Event keyEvent)
	{
		if (CursorPosition < 0)
		{
			return false;
		}

		if (HighlightEndPosition >= 0)
		{
			DeleteSelectedText();
			SetHighlightEndPosition(-1);

			if (keyEvent.key.code == sf::Keyboard::BackSpace || keyEvent.key.code == sf::Keyboard::Delete)
			{
				return true;
			}
		}

		char* msg = new char[1];
		msg[0] = static_cast<char>(keyEvent.text.unicode);

		if (msg[0] == '\r')
		{
			msg[0] = '\n';
		}

		DString fullTextCopy = GetFullText();

		//Interp delete characters
		bool bDeletedCharacter = false;
		switch (keyEvent.key.code)
		{
			case(sf::Keyboard::BackSpace):
				//delete prev character
				fullTextCopy.Remove(CursorPosition.Value - 1);
				bDeletedCharacter = true;
				SetCursorPosition(Utils::Max(CursorPosition-1, INT(0)));
				break;

			case (sf::Keyboard::Delete):
				//delete next character
				fullTextCopy.Remove(CursorPosition.Value);
				bDeletedCharacter = true;
				break;
		}

		if (!bDeletedCharacter)
		{
			DString strMsg(msg);
			fullTextCopy.Insert(CursorPosition.Value, strMsg);
			SetCursorPosition(CursorPosition + 1);
		}

		delete[] msg;
		SetFullText(fullTextCopy, true);
		return true;
	}

	void TextFieldComponent::HandleScrollPositionChanged (INT newScrollPosition)
	{
		//Refresh the label component to recompute the TrimmedTextHead
		SetFullText(FullText, false);
	}

	void TextFieldComponent::HandleMaxNumLinesChanged (INT newMaxNumLines)
	{
		if (VALID_OBJECT(Scrollbar))
		{
			Scrollbar->SetNumVisibleScrollPositions(newMaxNumLines);
		}
	}

	bool TextFieldComponent::HandleMouseIconOverride (const sf::Event::MouseMoveEvent& sfmlEvent)
	{
		if (!bOverridingMousePointerIcon)
		{
			return false;
		}

		bool bResult = ((bEditable || bSelectable) && TextLabel->GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));	

		if (!bResult)
		{
			bOverridingMousePointerIcon = false;
		}

		return bResult;
	}
}

#endif