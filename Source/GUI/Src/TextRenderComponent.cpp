/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextRenderComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TextRenderComponent, RenderComponent)

	void TextRenderComponent::InitProps ()
	{
		Super::InitProps(); //Prevent information hiding
	}

	void TextRenderComponent::Destroy ()
	{	
		DeleteAllText();

		Super::Destroy();
	}

	void TextRenderComponent::Render (const Camera* targetCamera, const Window* targetWindow)
	{
		sf::Transformable transformationResults;
		if (TransformationInterface != nullptr)
		{
			TransformationInterface->ApplyDrawPosition(&transformationResults, targetCamera);
		}

		for (unsigned int i = 0; i < Texts.size(); i++)
		{
			if (Texts.at(i) != nullptr)
			{
				sf::Vector2f drawOffset = (i < DrawOffsets.size()) ? DrawOffsets.at(i) : sf::Vector2f(0,0);
				Texts.at(i)->setPosition(drawOffset + transformationResults.getPosition());
				targetWindow->Resource->draw(*Texts.at(i));
			}
		}
	}

	void TextRenderComponent::DeleteAllText ()
	{
		for (unsigned int i = 0; i < Texts.size(); i++)
		{
			delete Texts.at(i);
		}

		Texts.clear();
	}

	void TextRenderComponent::ClearDrawOffset (unsigned int index, unsigned int count)
	{
		if (index < DrawOffsets.size())
		{
			DrawOffsets.erase(DrawOffsets.begin() + index, DrawOffsets.begin() + count);
		}
	}

	void TextRenderComponent::ClearAllDrawOffsets ()
	{
		DrawOffsets.clear();
	}

	void TextRenderComponent::SetDrawOffset (unsigned int index, FLOAT posX, FLOAT posY)
	{
		while (index >= DrawOffsets.size())
		{
			DrawOffsets.push_back(sf::Vector2f());
		}

		DrawOffsets.at(index) = sf::Vector2f(posX.Value, posY.Value);
	}

	void TextRenderComponent::SetDrawOffset (unsigned int index, const sf::Vector2f& newOffset)
	{
		SetDrawOffset(index, FLOAT(newOffset.x), FLOAT(newOffset.y));
	}

	bool TextRenderComponent::GetDrawOffset (unsigned int index, sf::Vector2f& outResultOffset) const
	{
		if (index < DrawOffsets.size())
		{
			outResultOffset = DrawOffsets.at(index);
			return true;
		}

		return false;
	}

	size_t TextRenderComponent::GetDrawOffsetLength () const
	{
		return DrawOffsets.size();
	}
}

#endif