/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScrollbarTrackComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(ScrollbarTrackComponent, GUIComponent)

	void ScrollbarTrackComponent::InitProps ()
	{
		Super::InitProps();

		OwningScrollbar = nullptr;
		TrackColor = nullptr;
		TravelTrackColor = nullptr;
		Thumb = nullptr;
		TravelDelay = 0.75f;
		TravelInterval = 0.2f;

		bEnabled = true;
		bScaleThumb = true;
		DraggingThumbPointerOffset = -1;
		TravelDirection = 0;
		OnThumbChangedPosition = nullptr;
	}

	void ScrollbarTrackComponent::BeginObject ()
	{
		Super::BeginObject();

		if (!VALID_OBJECT(TrackColor) && VALID_OBJECT(Transformation))
		{
			TrackColor = SolidColorRenderComponent::CreateObject();

			TrackColor->TransformationInterface = Transformation;
			TrackColor->SolidColor = sf::Color(96, 96, 96, 255);

			AddComponent(TrackColor);
		}

		if (!VALID_OBJECT(TravelTrackColor))
		{
			TravelTrackColor = SolidColorRenderComponent::CreateObject();
			TravelTrackColor->SetVisibility(false);
			TravelTrackColor->SetDrawOrder(TrackColor->GetDrawOrder() + 1.f); //Ensure this draws over the track
			TravelTrackColor->SolidColor = sf::Color(48, 48, 48, 255);

			AbsTransformComponent* absTransComp = AbsTransformComponent::CreateObject();
			if (TravelTrackColor->AddComponent(absTransComp))
			{
				TravelTrackColor->TransformationInterface = absTransComp;
			}
			else
			{
				absTransComp->Destroy();
			}

			AddComponent(TravelTrackColor);
		}

		InitializeThumb();

		RefreshScrollbarTrack();
	}

	void ScrollbarTrackComponent::AttachTo (Entity* newOwner)
	{
		Super::AttachTo(newOwner);

		OwningScrollbar = dynamic_cast<ScrollbarComponent*>(newOwner);
	}

	void ScrollbarTrackComponent::RefreshRenderComponentVisibility ()
	{
		Super::RefreshRenderComponentVisibility();

		//Ensure the TravelTrackColor is only visible when it's necessary
		if (VALID_OBJECT(TravelTrackColor))
		{
			TravelTrackColor->SetVisibility(IsVisible() && TravelDirection != 0);
		}
	}

	void ScrollbarTrackComponent::SetWindowHandle (Window* newWindowHandle)
	{
		Super::SetWindowHandle(newWindowHandle);

		if (WindowHandle == nullptr)
		{
			return;
		}

		if (VALID_OBJECT(TrackColor))
		{
			TrackColor->RelevantRenderWindows.clear();
			TrackColor->RelevantRenderWindows.push_back(WindowHandle);
		}

		if (VALID_OBJECT(TravelTrackColor))
		{
			TravelTrackColor->RelevantRenderWindows.clear();
			TravelTrackColor->RelevantRenderWindows.push_back(WindowHandle);
		}
	}

	void ScrollbarTrackComponent::Destroy ()
	{
		REMOVE_TIMER(this, ScrollbarTrackComponent::HandleTravel);

		Super::Destroy();
	}

	void ScrollbarTrackComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

		if (TravelDirection != 0)
		{
			if (VALID_OBJECT(TravelTrackColor))
			{
				CalculateTravelTrackAttributes(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
			}
			return;
		}

		if (DraggingThumbPointerOffset < 0 || !VALID_OBJECT(Transformation) || !VALID_OBJECT(Thumb->GetTransform()))
		{
			return;
		}

		FLOAT newThumbPosY = (sfmlEvent.y - DraggingThumbPointerOffset).ToFLOAT();
		Thumb->GetTransform()->SetAbsCoordinates(Vector2(Transformation->GetAbsCoordinatesX(), newThumbPosY));
		OnThumbChangedPosition(CalculateScrollIndex());
	}

	bool ScrollbarTrackComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		if (eventType == sf::Event::MouseButtonReleased)
		{
			if (DraggingThumbPointerOffset < 0 && TravelDirection == 0)
			{
				return false;
			}

			DraggingThumbPointerOffset = -1;
			TravelDirection = 0;
			ConditionallySetExternalBorderInput();

			if (VALID_OBJECT(TravelTrackColor))
			{
				TravelTrackColor->SetVisibility(false);
			}

			return false;
		}

		if (sfmlEvent.button != sf::Mouse::Left)
		{
			return false;
		}

		if (!bEnabled || !Transformation->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)) || !VALID_OBJECT(Thumb))
		{
			return false;
		}

		TravelDirection = (sfmlEvent.y > Thumb->GetTransform()->CalcFinalCoordinatesY().ToINT()) ? -1 : 1;
		ConditionallySetExternalBorderInput();

		if (VALID_OBJECT(TravelTrackColor))
		{
			TravelTrackColor->SetVisibility(true);
			CalculateTravelTrackAttributes(INT(sfmlEvent.x).ToFLOAT(), INT(sfmlEvent.y).ToFLOAT());
		}
		
		SET_TIMER(this, TravelDelay, false, ScrollbarTrackComponent::HandleTravel);
		return true;
	}

	bool ScrollbarTrackComponent::ShouldHaveInputComponent () const
	{
		return (Super::ShouldHaveInputComponent() && bEnabled);
	}

	bool ScrollbarTrackComponent::ShouldHaveExternalInputComponent () const
	{
		//Return true if the user is currently dragging the thumb around or holding on travel track.
		return (DraggingThumbPointerOffset >= 0 || TravelDirection != 0);
	}

	void ScrollbarTrackComponent::SetRenderComponentsDrawOrder (FLOAT newDrawOrder)
	{
		//Update other attached render components
		Super::SetRenderComponentsDrawOrder(newDrawOrder);

		FLOAT drawOrderModifier = newDrawOrder + 1.f;

		if (VALID_OBJECT(TravelTrackColor))
		{
			TravelTrackColor->SetDrawOrder(drawOrderModifier);
			drawOrderModifier++;
		}

		if (VALID_OBJECT(Thumb))
		{
			Thumb->SetDrawOrder(drawOrderModifier);
		}
	}

	bool ScrollbarTrackComponent::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
	{
		return (bEnabled && Super::AcceptsMouseEvents(mousePosX, mousePosY));
	}

	void ScrollbarTrackComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(OwningScrollbar)
		CLEANUP_INVALID_POINTER(TrackColor)
		CLEANUP_INVALID_POINTER(TravelTrackColor)
		CLEANUP_INVALID_POINTER(Thumb)
	}

	void ScrollbarTrackComponent::HandleSizeChange ()
	{
		Super::HandleSizeChange();

		//Need to readjust scaling, position, and position clamps for the thumb.
		RefreshScrollbarTrack();
	}

	void ScrollbarTrackComponent::RefreshScrollbarTrack ()
	{
		if (GetMousePointer() == nullptr)
		{
			return;
		}

		if (VALID_OBJECT(Thumb))
		{
			CalculateThumbScale();
			CalculateThumbPositionClamps();
			CalculateThumbPosition();
			ReevaluateThumbVisibility();
		}

		INT mousePosX;
		INT mousePosY;
		GetMousePointer()->GetMousePosition(mousePosX, mousePosY);

		CalculateTravelTrackAttributes(mousePosX.ToFLOAT(), mousePosY.ToFLOAT());
	}

	void ScrollbarTrackComponent::SetThumbChangedPositionHandler (function<void(INT newScrollIndex)> newHandler)
	{
		OnThumbChangedPosition = newHandler;
	}

	void ScrollbarTrackComponent::SetEnabled (bool bNewEnabled)
	{
		bEnabled = bNewEnabled;

		if (!bEnabled)
		{
			if (TravelDirection != 0)
			{
				REMOVE_TIMER(this, ScrollbarTrackComponent::HandleTravel);
			}
			TravelDirection = 0;
			
			if (VALID_OBJECT(TravelTrackColor))
			{
				TravelTrackColor->SetVisibility(false);
			}

			DraggingThumbPointerOffset = -1;
			ConditionallySetExternalBorderInput();
		}

		ReevaluateThumbVisibility();
	}

	void ScrollbarTrackComponent::SetScaleThumb (bool bNewScaleThumb)
	{
		bScaleThumb = bNewScaleThumb;

		if (!bScaleThumb && VALID_OBJECT(Thumb))
		{
			const Texture* thumbTexture = Thumb->RenderComponent->GetSpriteTexture();
			if (VALID_OBJECT(thumbTexture) && VALID_OBJECT(Thumb->GetTransform()))
			{
				//restore default scale
				Thumb->GetTransform()->SetBaseSize(Vector2(thumbTexture->GetWidth().ToFLOAT(), thumbTexture->GetHeight().ToFLOAT()));
			}
		}
		else
		{
			CalculateThumbScale();
		}
	}

	bool ScrollbarTrackComponent::ReversedDirections () const
	{
		FLOAT mousePosX;
		FLOAT mousePosY;
		GetMousePointer()->GetMousePosition(mousePosX, mousePosY);

		//bReversedDirection becomes true when a user clicks above/below the thumb, then switches sides as the thumb travels.
		bool bReversedDir = (TravelDirection > 0 && mousePosY > Thumb->GetTransform()->CalcFinalCoordinatesY()); //mouse below the thumb as thumb travels up
		bReversedDir = bReversedDir || (TravelDirection < 0 && mousePosY < Thumb->GetTransform()->CalcFinalBottomBounds()); //mouse above thumb as thumb travels down

		return bReversedDir;
	}

	bool ScrollbarTrackComponent::GetEnabled () const
	{
		return bEnabled;
	}

	bool ScrollbarTrackComponent::GetScaleThumb () const
	{
		return bScaleThumb;
	}

	void ScrollbarTrackComponent::InitializeThumb ()
	{
		if (!VALID_OBJECT(Thumb))
		{
			Thumb = ButtonComponent::CreateObject();
			Thumb->RenderComponent->SetSpriteTexture(GUITheme::GetGUITheme()->ScrollbarTrackFill);
			Thumb->RenderComponent->TopBorder = GUITheme::GetGUITheme()->ScrollbarTrackBorderTop;
			Thumb->RenderComponent->RightBorder = GUITheme::GetGUITheme()->ScrollbarTrackBorderRight;
			Thumb->RenderComponent->BottomBorder = GUITheme::GetGUITheme()->ScrollbarTrackBorderBottom;
			Thumb->RenderComponent->LeftBorder = GUITheme::GetGUITheme()->ScrollbarTrackBorderLeft;
			Thumb->RenderComponent->TopRightCorner = GUITheme::GetGUITheme()->ScrollbarTrackBorderTopRight;
			Thumb->RenderComponent->BottomRightCorner = GUITheme::GetGUITheme()->ScrollbarTrackBorderBottomRight;
			Thumb->RenderComponent->BottomLeftCorner = GUITheme::GetGUITheme()->ScrollbarTrackBorderBottomLeft;
			Thumb->RenderComponent->TopLeftCorner = GUITheme::GetGUITheme()->ScrollbarTrackBorderTopLeft;
			Thumb->SetBorderThickness(2);
			Thumb->RenderComponent->FillColor = sf::Color(64, 64, 64, 255);

			Thumb->SetButtonPressedHandler(bind(&ScrollbarTrackComponent::HandleThumbPressed, this, placeholders::_1));
			Thumb->SetButtonReleasedHandler(bind(&ScrollbarTrackComponent::HandleThumbReleased, this, placeholders::_1));
			AddComponent(Thumb);

			if (VALID_OBJECT(TravelTrackColor))
			{
				//Ensure the thumb is drawn over the track and the solid color renderers
				Thumb->SetDrawOrder(TravelTrackColor->GetDrawOrder() + 2.f);
			}

			LabelComponent* buttonCaption = Thumb->CaptionComponent;
			if (buttonCaption != nullptr && Thumb->RemoveComponent(buttonCaption))
			{
				buttonCaption->Destroy();
			}
		}
	}

	void ScrollbarTrackComponent::ReevaluateThumbVisibility ()
	{
		if (VALID_OBJECT(OwningScrollbar))
		{
			Thumb->SetVisibility(bEnabled && OwningScrollbar->GetNumVisibleScrollPositions() < OwningScrollbar->GetMaxScrollPosition());
		}
	}

	void ScrollbarTrackComponent::CalculateThumbPositionClamps ()
	{
		//Ensure the thumb remains between the two buttons.
		FLOAT thumbHeight = Thumb->GetTransform()->GetCurrentSizeY();
		Thumb->GetTransform()->SetAbsCoordinatesYClamps(Range<FLOAT>(0.f, GetTransform()->GetCurrentSizeY() - thumbHeight));
	}

	void ScrollbarTrackComponent::CalculateThumbPosition ()
	{
		if (!VALID_OBJECT(OwningScrollbar))
		{
			return;
		}

		if (OwningScrollbar->GetMaxScrollPosition() <= 0)
		{
			//There's nothing to scroll here (the Thumb should be invisible at this point)
			return;
		}

		FLOAT percentScrolled = (OwningScrollbar->GetScrollPosition().ToFLOAT() / OwningScrollbar->GetMaxScrollPosition().ToFLOAT());
		FLOAT thumbAbsPosY = (percentScrolled * (GetTransform()->GetCurrentSizeY()));
		Thumb->GetTransform()->SetAbsCoordinates(Vector2(0.f, thumbAbsPosY));
	}

	void ScrollbarTrackComponent::CalculateThumbScale ()
	{
		if (!bScaleThumb || !VALID_OBJECT(OwningScrollbar))
		{
			return;
		}

		if (OwningScrollbar->GetMaxScrollPosition() <= 0)
		{
			//There's nothing to scroll here
			return;
		}

		FLOAT scaleRatio = Utils::Clamp<FLOAT>((OwningScrollbar->GetNumVisibleScrollPositions().ToFLOAT() / OwningScrollbar->GetMaxScrollPosition().ToFLOAT()), 0.05f, 1.f);
		FLOAT newSize = (scaleRatio * Transformation->GetCurrentSize().Y);
		Thumb->GetTransform()->SetBaseSize(Vector2(Transformation->GetBaseSize().X, newSize));
	}

	INT ScrollbarTrackComponent::CalculateScrollIndex ()
	{
		if (!VALID_OBJECT(Thumb->GetTransform()))
		{
			return 0;
		}

		FLOAT thumbPosY = Thumb->GetTransform()->GetAbsCoordinatesY();
		FLOAT percentScrolled = thumbPosY / Transformation->GetCurrentSize().Y;

		//TODO:  It's impossible to scroll for very small thumb due to the 5% minimum scale requirement.
		return FLOAT(round((OwningScrollbar->GetMaxScrollPosition().ToFLOAT() * percentScrolled).Value)).ToINT();
	}

	bool ScrollbarTrackComponent::CalculateTravelTrackAttributes (FLOAT mousePosX, FLOAT mousePosY)
	{
		bool bStopped = (TravelDirection == 0 || ReversedDirections());
		TravelTrackColor->SetVisibility(!bStopped);

		if (bStopped)
		{
			//Thumb finished traveling to mouse pointer
			return false;
		}

		//clamp mousePosY to be within track size
		mousePosY = Utils::Clamp(mousePosY, Transformation->CalcFinalCoordinatesY(), Transformation->CalcFinalBottomBounds());

		AbsTransformComponent* travelTransform = dynamic_cast<AbsTransformComponent*>(TravelTrackColor->TransformationInterface);
		if (!travelTransform)
		{
			return false;
		}

		if (TravelDirection > 0) //scrolling up
		{
			FLOAT absSizeY = Thumb->GetTransform()->CalcFinalCoordinatesY() - mousePosY;
			travelTransform->SetBaseSize(Vector2(Transformation->GetBaseSizeX(), absSizeY));
			travelTransform->SetAbsCoordinates(Vector2(Transformation->CalcFinalCoordinatesX(), mousePosY));
		}
		else //scrolling down
		{
			FLOAT absSizeY = mousePosY - Thumb->GetTransform()->CalcFinalBottomBounds();
			travelTransform->SetBaseSize(Vector2(Transformation->GetBaseSizeX(), absSizeY));
			travelTransform->SetAbsCoordinates(Vector2(Transformation->CalcFinalCoordinatesX(), Thumb->GetTransform()->CalcFinalBottomBounds()));
		}

		return true;
	}

	void ScrollbarTrackComponent::HandleThumbPressed (ButtonComponent* uiComponent)
	{
		if (!VALID_OBJECT(uiComponent->GetTransform()))
		{
			return;
		}

		INT mousePosX;
		INT mousePosY;
		GetMousePointer()->GetMousePosition(mousePosX, mousePosY);

		DraggingThumbPointerOffset = mousePosY - uiComponent->GetTransform()->GetAbsCoordinatesY().ToINT();
		ConditionallySetExternalBorderInput();
	}

	void ScrollbarTrackComponent::HandleThumbReleased (ButtonComponent* uiComponent)
	{
		DraggingThumbPointerOffset = -1;
		ConditionallySetExternalBorderInput();
	}

	bool ScrollbarTrackComponent::HandleTravel ()
	{
		if (TravelDirection == 0 || !VALID_OBJECT(OwningScrollbar))
		{
			return true;
		}

		if (!ReversedDirections())
		{
			OwningScrollbar->IncrementScrollPosition(OwningScrollbar->GetNumVisibleScrollPositions() * TravelDirection * -1);
		}

		SET_TIMER(this, TravelInterval, false, ScrollbarTrackComponent::HandleTravel);
		return true;
	}
}

#endif