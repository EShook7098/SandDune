/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HoverComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(HoverComponent, EntityComponent)

	void HoverComponent::InitProps ()
	{
		Super::InitProps();

		bIsHoveringOwner = false;
		OwningGUI = nullptr;
		Input = nullptr;
	}

	void HoverComponent::BeginObject ()
	{
		Super::BeginObject();

		Input = InputComponent::CreateObject();
		if (AddComponent(Input))
		{
			Input->MouseMoveDelegate = SDFUNCTION_3PARAM(this, HoverComponent, HandleMouseMovement, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
		}
		else
		{
			Input = nullptr;
		}
	}

	bool HoverComponent::CanBeAttachedTo (Entity* ownerCandidate) const
	{
		return (dynamic_cast<GUIComponent*>(ownerCandidate) != nullptr && Super::CanBeAttachedTo(ownerCandidate));
	}

	void HoverComponent::AttachTo (Entity* newOwner)
	{
		Super::AttachTo(newOwner);

		OwningGUI = dynamic_cast<GUIComponent*>(newOwner);
		CHECK(OwningGUI != nullptr)

		RegisterInputToWindow(OwningGUI->GetWindowHandle());
	}

	

	void HoverComponent::ComponentDetached ()
	{
		OwningGUI = nullptr;

		Super::ComponentDetached();
	}

	void HoverComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(OwningGUI)
		CLEANUP_INVALID_POINTER(Input)
	}

	bool HoverComponent::GetIsHovering () const
	{
		return bIsHoveringOwner;
	}

	void HoverComponent::RegisterInputToWindow (Window* window)
	{
		CHECK(Input != nullptr)

		if (VALID_OBJECT(window) && InputEngineComponent::Get() != nullptr)
		{
			InputBroadcaster* broadcaster = InputEngineComponent::Get()->FindBroadcasterForWindow(window);
			if (broadcaster != nullptr && broadcaster != Input->GetInputMessenger())
			{
				broadcaster->AddInputComponent(Input);
			}
		}
	}

	void HoverComponent::EvaluateHoverState (const sf::Event::MouseMoveEvent& sfmlEvent)
	{
		bool bOldHovering = bIsHoveringOwner;
		bIsHoveringOwner = OwningGUI->GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));

		if (bOldHovering != bIsHoveringOwner)
		{
			//Hover state change detected, invoke callback
			if (bIsHoveringOwner && OnRollOver.IsBounded())
			{
				OnRollOver(OwningGUI);
			}
			else if (!bIsHoveringOwner && OnRollOut.IsBounded())
			{
				OnRollOut(OwningGUI);
			}
		}
	}

	void HoverComponent::HandleMouseMovement (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		if (OwningGUI != nullptr)
		{
			EvaluateHoverState(sfmlEvent);
		}
	}
}

#endif