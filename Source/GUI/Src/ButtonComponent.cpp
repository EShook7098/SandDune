/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ButtonComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(ButtonComponent, FrameComponent)

	void ButtonComponent::InitProps ()
	{
		Super::InitProps();

		//Override defaults from FrameComponent properties
		bLockedFrame = true;
		SetBorderThickness(0);

		OnButtonPressed = nullptr;
		bEnabled = true;
		CaptionComponent = nullptr;
		StateComponent = nullptr;
		bPressedDown = false;
		bHovered = false;
		OnButtonPressed = nullptr;
		OnButtonReleased = nullptr;
	}

	void ButtonComponent::BeginObject ()
	{
		Super::BeginObject();

		if (VALID_OBJECT(RenderComponent))
		{
			//Clear all borders since it's going to be a simple sprite
			RenderComponent->TopBorder = nullptr;
			RenderComponent->RightBorder = nullptr;
			RenderComponent->BottomBorder = nullptr;
			RenderComponent->LeftBorder = nullptr;
			RenderComponent->TopRightCorner = nullptr;
			RenderComponent->BottomRightCorner = nullptr;
			RenderComponent->BottomLeftCorner = nullptr;
			RenderComponent->TopLeftCorner = nullptr;
			RenderComponent->SetSpriteTexture(GUITheme::GetGUITheme()->ButtonFill);
		}

		if (!VALID_OBJECT(CaptionComponent))
		{
			InitializeCaptionComponent(LabelComponent::SStaticClass());
		}
	}

	void ButtonComponent::SetWindowHandle (Window* newWindowHandle)
	{
		Super::SetWindowHandle(newWindowHandle);

		if (VALID_OBJECT(StateComponent) && WindowHandle != nullptr)
		{
			StateComponent->DrawToWindow(newWindowHandle);
		}
	}

	bool ButtonComponent::CanBeFocused () const
	{
		return GetEnabled() && IsVisible();
	}

	void ButtonComponent::LoseFocus ()
	{
		FocusInterface::LoseFocus();

		if (bPressedDown) //Cancel out of selection (Pressing tab while Return key is held)
		{
			SetButtonUp(false);
		}
	}

	bool ButtonComponent::CaptureFocusedInput (const sf::Event& keyEvent)
	{
		if (keyEvent.key.code == sf::Keyboard::Return)
		{
			if (keyEvent.type == sf::Event::KeyPressed)
			{
				SetButtonDown(true);
				return true;
			}
			else if (keyEvent.type == sf::Event::KeyReleased)
			{
				SetButtonUp(true);
				return true;
			}
		}

		return false;
	}

	bool ButtonComponent::CaptureFocusedText (const sf::Event& keyEvent)
	{
		return false;
	}

	void ButtonComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

		bool bNewHovered = VALID_OBJECT(Transformation) ? Transformation->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)) : false;

		if (bNewHovered != bHovered)
		{
			bHovered = bNewHovered;
			ConditionallySetExternalBorderInput();

			if (VALID_OBJECT(StateComponent))
			{
				if (bHovered)
				{
					(bPressedDown) ? StateComponent->SetDownAppearance() : StateComponent->SetHoverAppearance();
				}
				else
				{
					StateComponent->SetDefaultAppearance();
				}
			}
		}
	}

	bool ButtonComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		if (Super::ExecuteMouseClick(mouse, sfmlEvent, eventType))
		{
			return true;
		}

		//Buttons only care for left mouse button
		if (sfmlEvent.button != sf::Mouse::Left || !IsVisible())
		{
			return false;
		}

		if (!VALID_OBJECT(Transformation))
		{
			LOG1(LOG_WARNING, TXT("Unable to execute mouse click for %s since it does not have a transformation component to determine if the event is relevant or not."), GetName());
			return false;
		}

		if (!bPressedDown && eventType == sf::Event::MouseButtonPressed && Transformation->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)))
		{
			SetButtonDown(true);
			return true;
		}
		else if (bPressedDown && eventType == sf::Event::MouseButtonReleased)
		{
			SetButtonUp(Transformation->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));

			//fallthrough is intended.  All events listening for the mouse release should be notified since mouse release is typically associated with unlocking something.
			//For example, the scrollbar's thumb's hover state (for rendering) and the scrollbar track (for positioning thumb) cares for mouse release events from a single mouse release event.
		}

		return false;
	}

	bool ButtonComponent::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
	{
		return (bEnabled && Super::AcceptsMouseEvents(mousePosX, mousePosY));
	}

	bool ButtonComponent::ShouldHaveInputComponent () const
	{
		//Skip FrameComponent's ShouldRegisterInterface evaluation
		return (GUIComponent::ShouldHaveInputComponent() && bEnabled);
	}

	bool ButtonComponent::ShouldHaveExternalInputComponent () const
	{
		if (bHovered || bPressedDown)
		{
			return true;
		}

		return Super::ShouldHaveExternalInputComponent();
	}

	void ButtonComponent::HandleSizeChange ()
	{
		Super::HandleSizeChange();

		if (VALID_OBJECT(CaptionComponent))
		{
			CaptionComponent->GetTransform()->SetAbsCoordinates(Vector2(GetBorderThickness(), GetBorderThickness()));
			CaptionComponent->GetTransform()->SetBaseSize(GetTransform()->GetBaseSize() - Vector2(GetBorderThickness() * 2.f, GetBorderThickness() * 2.f));
		}
	}

	void ButtonComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		//Double check publicly accessible pointers
		CLEANUP_INVALID_POINTER(CaptionComponent)
		CLEANUP_INVALID_POINTER(StateComponent)
	}

	void ButtonComponent::SetButtonDown (bool bInvokeDelegate)
	{
		if (VALID_OBJECT(StateComponent))
		{
			StateComponent->SetDownAppearance();
		}

		if (bInvokeDelegate && OnButtonPressed != nullptr)
		{
			OnButtonPressed(this);
		}

		bPressedDown = true;
	}

	void ButtonComponent::SetButtonUp (bool bInvokeDelegate)
	{
		if (VALID_OBJECT(StateComponent))
		{
			//Restore button state appearance regardless if the button was released beyond bounds or not
			(bHovered) ? StateComponent->SetHoverAppearance() : StateComponent->SetDefaultAppearance();
		}

		bPressedDown = false;
		ConditionallySetExternalBorderInput();

		if (bInvokeDelegate && OnButtonReleased != nullptr)
		{
			OnButtonReleased(this);
		}
	}

	void ButtonComponent::SetButtonPressedHandler (std::function<void(ButtonComponent* uiComponent)> newHandler)
	{
		OnButtonPressed = newHandler;
	}

	void ButtonComponent::SetButtonReleasedHandler (std::function<void(ButtonComponent* uiComponent)> newHandler)
	{
		OnButtonReleased = newHandler;
	}

	void ButtonComponent::SetCaptionText (DString newText)
	{
		if (VALID_OBJECT(CaptionComponent))
		{
			CaptionComponent->SetTextContent(newText);
		}
	}

	void ButtonComponent::SetEnabled (bool bNewEnabled)
	{
		bEnabled = bNewEnabled;
		bHovered = false;
		bPressedDown = false;

		if (bEnabled)
		{
			FLOAT mouseX;
			FLOAT mouseY;
			GetMousePointer()->GetMousePosition(mouseX, mouseY);

			bHovered = (VALID_OBJECT(Transformation)) ? Transformation->WithinBounds(mouseX, mouseY) : false;
		}

		if (VALID_OBJECT(StateComponent))
		{
			if (bEnabled)
			{
				(bHovered) ? StateComponent->SetHoverAppearance() : StateComponent->SetDefaultAppearance();
			}
			else
			{
				StateComponent->SetDisableAppearance();
			}
		}

		ConditionallySetInputComponent();
	}

	ButtonStateComponent* ButtonComponent::ReplaceStateComponent (const DClass* newButtonStateClass)
	{
		if (newButtonStateClass == nullptr)
		{
			//Simply destroy the old
			if (VALID_OBJECT(StateComponent))
			{
				StateComponent->Destroy();
				StateComponent = nullptr;
			}

			return nullptr;
		}

		const ButtonStateComponent* defaultButtonState = dynamic_cast<const ButtonStateComponent*>(newButtonStateClass->GetDefaultObject());
		if (!defaultButtonState)
		{
			return nullptr;
		}

		ButtonStateComponent* oldButtonState = StateComponent;
		StateComponent = defaultButtonState->CreateObjectOfMatchingClass();
		if (!VALID_OBJECT(StateComponent))
		{
			StateComponent = oldButtonState;
			return nullptr;
		}

		if (!AddComponent(StateComponent))
		{
			StateComponent->Destroy();
			StateComponent = oldButtonState;
			return nullptr;
		}

		if (VALID_OBJECT(oldButtonState))
		{
			RemoveComponent(oldButtonState);
			oldButtonState->Destroy();
		}

		StateComponent->DrawToWindow(GetWindowHandle());
		return StateComponent;
	}

	void ButtonComponent::InitializeCaptionComponent (const DClass* labelClass)
	{
		if (VALID_OBJECT(CaptionComponent))
		{
			return;
		}

		const LabelComponent* defaultObject = dynamic_cast<const LabelComponent*>(labelClass->GetDefaultObject());
		if (!defaultObject)
		{
			return; //not valid class
		}

		CaptionComponent = defaultObject->CreateObjectOfMatchingClass();
		if (VALID_OBJECT(CaptionComponent))
		{
			AddComponent(CaptionComponent);
			CaptionComponent->SetAutoRefresh(false);

			//Setting scale equal to this component's scale
			if (VALID_OBJECT(CaptionComponent->GetTransform()))
			{
				CaptionComponent->GetTransform()->SetBaseSize(Transformation->GetBaseSize());
				CaptionComponent->GetTransform()->SetAbsCoordinates(Vector2(BorderThickness, BorderThickness));
			}

			CaptionComponent->SetLineSpacing(0);
			CaptionComponent->SetHorizontalAlignment(LabelComponent::HA_Center);
			CaptionComponent->SetVerticalAlignment(LabelComponent::VA_Center);
			CaptionComponent->SetAutoRefresh(true);
			CaptionComponent->RenderComponent->SetDrawOrder(RenderComponent->GetDrawOrder() + 1.f);
			SetCaptionText(TXT("Button"));
		}
	}

	bool ButtonComponent::GetEnabled () const
	{
		return bEnabled;
	}

	bool ButtonComponent::GetHovered () const
	{
		return bHovered;
	}

	bool ButtonComponent::GetPressedDown () const
	{
		return bPressedDown;
	}

	DString ButtonComponent::GetCaptionText () const
	{
		if (VALID_OBJECT(CaptionComponent))
		{
			return CaptionComponent->GetFullText();
		}

		return TXT("");
	}

	ButtonStateComponent* ButtonComponent::GetButtonStateComponent () const
	{
		return StateComponent;
	}
}

#endif