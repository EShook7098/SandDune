/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIEngineComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_ENGINE_COMPONENT(GUIEngineComponent)

	void GUIEngineComponent::InitializeComponent ()
	{
		Super::InitializeComponent();

		ImportGUIEssentialTextures();
		ImportGUIEssentialFonts();

		GUIThemeClass = GetGUIThemeClass();
		if (!GUIThemeClass->IsChildOf(GUITheme::SStaticClass()))
		{
			Engine::GetEngine()->FatalError(TXT("Failed to initialize GUIEngineComponent.  The static class of GUIThemeClass is not a GUITheme."));
			return;
		}

		RegisteredGUITheme = dynamic_cast<GUITheme*>(GUIThemeClass->GetDefaultObject()->CreateObjectOfMatchingClass());
		RegisteredGUITheme->InitializeTheme();
	}

	void GUIEngineComponent::ShutdownComponent ()
	{
		if (VALID_OBJECT(RegisteredGUITheme))
		{
			RegisteredGUITheme->Destroy();
		}

		Super::ShutdownComponent();
	}

	void GUIEngineComponent::SetGUITheme (GUITheme* newGUITheme)
	{
		RegisteredGUITheme = newGUITheme;
	}

	GUITheme* GUIEngineComponent::GetGUITheme () const
	{
		return RegisteredGUITheme;
	}

	void GUIEngineComponent::ImportGUIEssentialTextures ()
	{
		//Textures for BorderedSpriteComponent
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("FrameBorderTop.jpg"), TXT("Interface.FrameBorderTop"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("FrameBorderRight.jpg"), TXT("Interface.FrameBorderRight"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("FrameBorderBottom.jpg"), TXT("Interface.FrameBorderBottom"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("FrameBorderLeft.jpg"), TXT("Interface.FrameBorderLeft"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("FrameBorderTopRight.jpg"), TXT("Interface.FrameBorderTopRight"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("FrameBorderBottomRight.jpg"), TXT("Interface.FrameBorderBottomRight"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("FrameBorderBottomLeft.jpg"), TXT("Interface.FrameBorderBottomLeft"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("FrameBorderTopLeft.jpg"), TXT("Interface.FrameBorderTopLeft"));

		//ButtonComponent
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("Buttons") + DString(DIR_SEPARATOR) + TXT("TextureTestStates.jpg"), TXT("Interface.Buttons.TextureTestStates"));

		//FrameComponent
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("TextureTest.jpg"), TXT("Interface.DefaultFrameComponentFill"));

		//Mouse Icon Overrides
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("CursorText.png"), TXT("Interface.CursorText"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("CursorResizeVertical.png"), TXT("Interface.CursorResizeVertical"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("CursorResizeHorizontal.png"), TXT("Interface.CursorResizeHorizontal"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("CursorResizeTopLeft.png"), TXT("Interface.CursorResizeTopLeft"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("CursorResizeBottomLeft.png"), TXT("Interface.CursorResizeBottomLeft"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("CursorScrollbarPan.png"), TXT("Interface.CursorScrollbarPan"));

		//ScrollbarComponent
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("Buttons") + DString(DIR_SEPARATOR) + TXT("UpButton.jpg"), TXT("Interface.Buttons.UpButton"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("Buttons") + DString(DIR_SEPARATOR) + TXT("DownButton.jpg"), TXT("Interface.Buttons.DownButton"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarMiddleMouseSprite.png"), TXT("Interface.ScrollbarMiddleMouseSprite"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarThumbFill.jpg"), TXT("Interface.ScrollbarThumbFill"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarThumbBorderBottom.jpg"), TXT("Interface.ScrollbarThumbBorderBottom"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarThumbBorderBottomLeft.jpg"), TXT("Interface.ScrollbarThumbBorderBottomLeft"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarThumbBorderBottomRight.jpg"), TXT("Interface.ScrollbarThumbBorderBottomRight"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarThumbBorderLeft.jpg"), TXT("Interface.ScrollbarThumbBorderLeft"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarThumbBorderRight.jpg"), TXT("Interface.ScrollbarThumbBorderRight"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarThumbBorderTop.jpg"), TXT("Interface.ScrollbarThumbBorderTop"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarThumbBorderTopLeft.jpg"), TXT("Interface.ScrollbarThumbBorderTopLeft"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("ScrollbarThumbBorderTopRight.jpg"), TXT("Interface.ScrollbarThumbBorderTopRight"));

		//DropdownComponent
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("Buttons") + DString(DIR_SEPARATOR) + TXT("CollapsedButton.jpg"), TXT("Interface.Buttons.CollapsedButton"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("Buttons") + DString(DIR_SEPARATOR) + TXT("ExpandedButton.jpg"), TXT("Interface.Buttons.ExpandedButton"));

		//Tree List
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("Buttons") + DString(DIR_SEPARATOR) + TXT("TreeListAddButton.png"), TXT("Interface.Buttons.TreeListAddButton"));
		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface") + DString(DIR_SEPARATOR) + TXT("Buttons") + DString(DIR_SEPARATOR) + TXT("TreeListSubtractButton.png"), TXT("Interface.Buttons.TreeListSubtractButton"));
	}

	void GUIEngineComponent::ImportGUIEssentialFonts ()
	{
		//FontPool::GetFontPool()->CreateAndImportFont(TXT("QuattrocentoSans-Regular.ttf"), TXT("DefaultFont"));
	}

	const DClass* GUIEngineComponent::GetGUIThemeClass ()
	{
		return GUITheme::SStaticClass();
	}
}

#endif