/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldRenderComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TextFieldRenderComponent, TextRenderComponent)

	void TextFieldRenderComponent::InitProps ()
	{
		Super::InitProps();

		//HighlightingShader = nullptr;
		OwningTextField = nullptr;
		CursorBlinkInterval = 0.75f;

		CursorColor = sf::Color(0,0,0,255);
		CursorWidth = 1.f;
	}

	void TextFieldRenderComponent::Render (const Camera* targetCamera, const Window* targetWindow)
	{
		Super::Render(targetCamera, targetWindow);

		if (!VALID_OBJECT(OwningTextField) || Texts.empty())
		{
			return;
		}

		INT highlightEndPos = OwningTextField->GetHighlightEndPosition();
		INT cursorPos = OwningTextField->GetCursorPosition();

		if (highlightEndPos < 0 && cursorPos >= 0) //render cursor blinker
		{
			if (CursorBlinkInterval <= 0)
			{
				return; //invisible cursor
			}

			if (sin((Engine::GetEngine()->GetElapsedTime().Value * PI) / CursorBlinkInterval.Value) > 0)
			{
				sf::Vector2f cursorSize;
				cursorSize.x = CursorWidth.Value;
				cursorSize.y = static_cast<float>(Texts.at(0)->getCharacterSize());

				sf::RectangleShape cursor;
				cursor.setSize(cursorSize);
				cursor.setPosition(FindCursorCoordinates(cursorPos));
				targetWindow->Resource->draw(cursor);
			}
		}
		else if (highlightEndPos >= 0) //Render highlight shader
		{
			//TODO:  Set highlight parameters here
			//TODO:  Render shader?
		}
	}

	void TextFieldRenderComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(OwningTextField)
	}

	sf::Vector2f TextFieldRenderComponent::FindCursorCoordinates (INT cursorIndex)
	{
		INT scrollPos = OwningTextField->GetScrollPosition();

		//Offset cursorIndex based on the amount of characters that were scrolled through
		if (scrollPos > 0 && VALID_OBJECT_CAST(OwningTextField, TextFieldLabelComponent*))
		{
			DString untrimmedText = dynamic_cast<TextFieldLabelComponent*>(OwningTextField->GetTextLabel())->GetTrimmedTextHead();
			cursorIndex -= untrimmedText.Length();
		}

		//Find the proper Texts index based on cursorIndex
		INT accumulatedChars = 0;
		unsigned int lineIdx;
		for (lineIdx = 0; lineIdx < Texts.size(); lineIdx++)
		{
			if (accumulatedChars + static_cast<int>(Texts.at(lineIdx)->getString().getSize()) > cursorIndex )
			{
				break; //found the line that contains the cursor
			}

			accumulatedChars += static_cast<int>(Texts.at(lineIdx)->getString().getSize());
		}

		return (Texts.at(lineIdx)->findCharacterPos((cursorIndex - accumulatedChars).ToUnsignedInt()));
	}
}

#endif