/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TreeListComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	INT TreeListComponent::NumPendingDeleteComponents = 0;
	INT TreeListComponent::MaxNumPendingDeleteComponents = 4096;

	IMPLEMENT_CLASS(TreeListComponent, GUIComponent)

	void TreeListComponent::SDataBranch::LinkBranches (SDataBranch& outParent, SDataBranch& outChild)
	{
		outParent.Children.push_back(&outChild);
		outChild.ParentBranch = &outParent;
	}

	bool TreeListComponent::SDataBranch::IsValid () const
	{
		return Data != nullptr;
	}

	bool TreeListComponent::SDataBranch::IsVisible () const
	{
		if (ParentBranch == nullptr)
		{
			return true; //Root branches are always visible
		}
		else if (!ParentBranch->bExpanded)
		{
			return false;
		}
		else
		{
			return ParentBranch->IsVisible();
		}
	}

	INT TreeListComponent::SDataBranch::GetNumParentBranches () const
	{
		if (ParentBranch != nullptr)
		{
			return ParentBranch->GetNumParentBranches() + 1;
		}

		return 0;
	}

	INT TreeListComponent::SDataBranch::CountNumChildren (bool bIncludeCollapsed) const
	{
		INT result = -1; //negative one to ignore this branch.  This branch should not be included in count.

		TreeBranchIterator<const TreeListComponent::SDataBranch> iter(this);
		while (iter.GetSelectedBranch() != nullptr)
		{
			result++;
			(bIncludeCollapsed) ? iter++ : iter.SelectNextVisibleBranch();		
		};

		return result;
	}

	void TreeListComponent::InitProps ()
	{
		Super::InitProps();

		BrowseJumpAmount = 10;
		IndentSize = 14.f;
		BranchHeight = 16.f;
		ConnectionThickness = 4.f;
		ConnectionColor = sf::Color::Black; //TODO:  Get color from GUITheme

		SelectedItemIdx = INT_INDEX_NONE;
		SelectedBranch = nullptr;
		BrowseIndex = INT_INDEX_NONE;
		ExpandButtonTexture = nullptr;
		CollapseButtonTexture = nullptr;
		SelectedBar = nullptr;
		HoverBar = nullptr;
		SelectedTransform = nullptr;
		HoverTransform = nullptr;
		bIgnoreHoverMovement = false;
		ListScrollbar = nullptr;
	}

	void TreeListComponent::BeginObject ()
	{
		Super::BeginObject();

		if (!VALID_OBJECT(ExpandButtonTexture))
		{
			ExpandButtonTexture = GUITheme::GetGUITheme()->TreeListAddButton;
		}

		if (!VALID_OBJECT(CollapseButtonTexture))
		{
			CollapseButtonTexture = GUITheme::GetGUITheme()->TreeListSubtractButton;
		}

		InitializeScrollbar();
		InitializeSolidColors();
	}

	void TreeListComponent::Destroy ()
	{
		ClearTreeList(); //Need to deallocate the list of pointers

		Super::Destroy();
	}

	void TreeListComponent::SetWindowHandle (Window* newWindowHandle)
	{
		Super::SetWindowHandle(newWindowHandle);

		for (unsigned int i = 0; i < BranchConnections.size(); i++)
		{
			BranchConnections.at(i)->RelevantRenderWindows.clear();
			BranchConnections.at(i)->RelevantRenderWindows.push_back(WindowHandle);
		}

		if (VALID_OBJECT(SelectedBar))
		{
			SelectedBar->RelevantRenderWindows.clear();
			SelectedBar->RelevantRenderWindows.push_back(WindowHandle);
		}

		if (VALID_OBJECT(HoverBar))
		{
			HoverBar->RelevantRenderWindows.clear();
			HoverBar->RelevantRenderWindows.push_back(WindowHandle);
		}
	}

	bool TreeListComponent::CanBeFocused () const
	{
		return IsVisible() && TreeList.size() > 0;
	}

	void TreeListComponent::GainFocus ()
	{
		FocusInterface::GainFocus();

		if (VALID_OBJECT(HoverBar))
		{
			HoverBar->SetVisibility(true);
			SetBrowseIndex(Utils::Max<INT>(SelectedItemIdx, 0));
		}
	}

	void TreeListComponent::LoseFocus ()
	{
		FocusInterface::LoseFocus();

		if (VALID_OBJECT(HoverBar))
		{
			HoverBar->SetVisibility(false);
		}
	}

	bool TreeListComponent::CaptureFocusedInput (const sf::Event& keyEvent)
	{
		if (keyEvent.type == sf::Event::KeyReleased)
		{
			if (keyEvent.key.code == sf::Keyboard::Return)
			{
				SetSelectedItem(BrowseIndex);
				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::Up)
			{
				InputBroadcaster* mainBroadcaster = InputEngineComponent::Get()->GetMainBroadcaster();
				INT scrollAmount = (mainBroadcaster != nullptr && mainBroadcaster->GetCtrlHeld()) ? BrowseJumpAmount : 1;
				INT newIndex = FindBranchIdxFromAnchor(BrowseIndex, -scrollAmount, false);
				if (newIndex < 0)
				{
					newIndex = Utils::Max<INT>(FindLastVisibleBranchIdx(), 0); //select last visible entry
				}

				SetBrowseIndex(newIndex);
				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::Down)
			{
				InputBroadcaster* mainBroadcaster = InputEngineComponent::Get()->GetMainBroadcaster();
				INT scrollAmount = (mainBroadcaster != nullptr && mainBroadcaster->GetCtrlHeld()) ? BrowseJumpAmount : 1;
				INT newIndex = FindBranchIdxFromAnchor(BrowseIndex, scrollAmount, false);
				if (newIndex >= static_cast<int>(TreeList.size()) || newIndex == INT_INDEX_NONE)
				{
					newIndex = 0;
				}

				SetBrowseIndex(newIndex);
				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::Left)
			{
				//Collapse button
 				CollapseBranch(BrowseIndex);
				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::Right)
			{
				//Expand button
				ExpandBranch(BrowseIndex);
				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::Space)
			{
				//Toggle button
				ButtonComponent* selectedButton = FindExpandButtonAtIdx(BrowseIndex);
				if (selectedButton != nullptr)
				{
					selectedButton->SetButtonUp(true);
				}

				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::PageUp && VALID_OBJECT(ListScrollbar))
			{
				INT scrollAmount = ListScrollbar->GetNumVisibleScrollPositions();
				INT newIndex = FindBranchIdxFromAnchor(BrowseIndex, -scrollAmount, false);
				if (newIndex == INT_INDEX_NONE)
				{
					newIndex = Utils::Max<INT>(FindLastVisibleBranchIdx(), 0); //select last visible entry
				}

				SetBrowseIndex(newIndex);
				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::PageDown && VALID_OBJECT(ListScrollbar))
			{
				INT scrollAmount = ListScrollbar->GetNumVisibleScrollPositions();
				INT newIndex = FindBranchIdxFromAnchor(BrowseIndex, scrollAmount, false);
				if (newIndex >= static_cast<int>(TreeList.size()) || newIndex == INT_INDEX_NONE)
				{
					newIndex = 0;
				}

				SetBrowseIndex(newIndex);
				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::Home)
			{
				SetBrowseIndex(0);
				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::End)
			{
				INT newIndex = Utils::Max<INT>(FindLastVisibleBranchIdx(), 0); //select last visible entry
				SetBrowseIndex(newIndex);
				return true;
			}

		}

		return false;
	}

	bool TreeListComponent::CaptureFocusedText (const sf::Event& keyEvent)
	{
		return false;
	}

	void TreeListComponent::HandleSizeChange ()
	{
		Super::HandleSizeChange();

		RefreshComponentTransforms();
	}

	bool TreeListComponent::ShouldHaveExternalInputComponent () const
	{
		return true; //Always be true since we may need to adjust the browsing highlight bar based on mouse pointer beyond borders.
	}

	void TreeListComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);
		if (!IsVisible() || bIgnoreHoverMovement)
		{
			return;
		}

		//Update browsing highlight bar and region
		bool bIsHovering = GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
		if (VALID_OBJECT(HoverBar))
		{
			HoverBar->SetVisibility(bIsHovering);
		}

		if (!bIsHovering)
		{
			return;
		}

		LabelComponent* hoveredLabel = FindLabelAtCoordinates(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
		if (hoveredLabel == nullptr)
		{
			HoverBar->SetVisibility(false);
			return;
		}

		CHECK(HoverTransform != nullptr)
		Vector2 newSize(hoveredLabel->GetTransform()->GetCurrentSizeX(), BranchHeight);
		HoverTransform->SetAbsCoordinates(hoveredLabel->GetTransform()->GetAbsCoordinates());
		HoverTransform->SetBaseSize(newSize);
	}

	bool TreeListComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		if (IsVisible() && eventType == sf::Event::MouseButtonReleased)
		{
			bool bWithinBounds = GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
			if (!bWithinBounds)
			{
				return false;
			}

			SDataBranch* selectedBranch = nullptr;
			LabelComponent* hoveredLabel = FindLabelAtCoordinates(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
			if (hoveredLabel != nullptr)
			{
				CHECK(TreeList.size() > 0) //If we have clicked on a label, we should have some tree list data.

				INT labelIdx = INT_INDEX_NONE;
				for (unsigned int i = 0; i < TreeLabels.size(); i++)
				{
					if (TreeLabels.at(i) == hoveredLabel)
					{
						labelIdx = INT(i);
						break;
					}
				}

				CHECK(labelIdx != INT_INDEX_NONE)

				//Find branch index from the label idx
				INT scrollPosition = (VALID_OBJECT(ListScrollbar)) ? ListScrollbar->GetScrollPosition() : 0;
				INT branchIdx = 0;
				for (TreeBranchIterator<TreeListComponent::SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter.SelectNextVisibleBranch())
				{
					if ((branchIdx - scrollPosition) == labelIdx)
					{
						selectedBranch = iter.GetSelectedBranch();
						break;
					}

					branchIdx++;
				}
			}

			SetSelectedItem(selectedBranch);
			return true;
		}

		return false;
	}

	void TreeListComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		NumPendingDeleteComponents = 0;

		CLEANUP_INVALID_POINTER(ExpandButtonTexture)
		CLEANUP_INVALID_POINTER(CollapseButtonTexture)
		
		CLEANUP_INVALID_POINTER(SelectedBar)
		CLEANUP_INVALID_POINTER(HoverBar)

		CLEANUP_INVALID_POINTER(SelectedTransform)
		CLEANUP_INVALID_POINTER(HoverTransform)

		CLEANUP_INVALID_POINTER(ListScrollbar)
	}

	void TreeListComponent::SortDataList (std::vector<SDataBranch*>& outDataList)
	{
		//Find the root branch
		unsigned int rootIdx = UINT_INDEX_NONE;
		for (unsigned int i = 0; i < outDataList.size(); i++)
		{
			if (outDataList.at(i)->ParentBranch == nullptr)
			{
				rootIdx = i;
				break;
			}
		}

		if (rootIdx == UINT_INDEX_NONE)
		{
			LOG(LOG_WARNING, TXT("Failed to sort data list.  Unable to find root branch."));
			return;
		}

		unsigned int i = 0;
		for (TreeBranchIterator<SDataBranch> iter(outDataList.at(rootIdx)); iter.GetSelectedBranch() != nullptr; iter++)
		{
			outDataList.at(i) = iter.GetSelectedBranch();
			i++;
		}
	}

	void TreeListComponent::SetMaxNumPendingDeleteComponents (INT newMaxNumPendingDeleteComponents)
	{
		MaxNumPendingDeleteComponents = newMaxNumPendingDeleteComponents;
		if (MaxNumPendingDeleteComponents > 0 && NumPendingDeleteComponents > MaxNumPendingDeleteComponents)
		{
			NumPendingDeleteComponents = 0; //Set this var early to protect against inf recursion (if some other object set this function during garbage collection).
			Engine::GetEngine()->CollectGarbage();
		}
	}

	void TreeListComponent::ExpandBranch (INT branchIdx)
	{
		SDataBranch* branch = GetBranchFromIdx(branchIdx);
		if (branch != nullptr)
		{
			ExpandBranch(branch);
		}
	}

	void TreeListComponent::ExpandBranch (SDataBranch* branch)
	{
		branch->bExpanded = true;

		if (VALID_OBJECT(ListScrollbar))
		{
			ListScrollbar->SetMaxScrollPosition(CountNumVisibleEntries());
		}

		RefreshTree();
		for (unsigned int i = 0; i < ExpandButtons.size(); i++)
		{
			if (ExpandButtons.at(i).Branch == branch)
			{
				CalcHoverTransform(i);
				break;
			}
		}
	}

	void TreeListComponent::CollapseBranch (INT branchIdx)
	{
		SDataBranch* branch = GetBranchFromIdx(branchIdx);
		if (branch != nullptr)
		{
			CollapseBranch(branch);
		}
	}

	void TreeListComponent::CollapseBranch (SDataBranch* branch)
	{
		branch->bExpanded = false;

		if (VALID_OBJECT(ListScrollbar))
		{
			ListScrollbar->SetMaxScrollPosition(CountNumVisibleEntries());
		}

		RefreshTree();

		//Clear hover transform that's hovering over children branches
		CalcHoverTransform(UINT_INDEX_NONE);
	}

	void TreeListComponent::ExpandAll ()
	{
		SetExpandForAllBranches(true);
	}

	void TreeListComponent::CollapseAll ()
	{
		SetExpandForAllBranches(false);
	}

	void TreeListComponent::JumpScrollbarToViewBranch (INT branchIdx)
	{
		if (TreeList.size() <= 0 || !VALID_OBJECT(ListScrollbar))
		{
			return;
		}

		INT scrollIdx = 0;
		INT numVisibleBranches = 0;
		for (TreeBranchIterator<const SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter++)
		{
			if (scrollIdx == branchIdx)
			{
				if (ListScrollbar->IsIndexVisible(numVisibleBranches))
				{
					return; //already within view
				}
				else if (numVisibleBranches < ListScrollbar->GetScrollPosition())
				{
					//Scroll to up
					ListScrollbar->SetScrollPosition(numVisibleBranches - 3);
					return;
				}
				else
				{
					//Scroll down
					ListScrollbar->SetScrollPosition(numVisibleBranches - ListScrollbar->GetNumVisibleScrollPositions() + 3);
					return;
				}
			}

			scrollIdx++;
			if (iter.GetSelectedBranch()->IsVisible())
			{
				numVisibleBranches++;
			}
		}
	}

	void TreeListComponent::SetBrowseIndex (INT newBrowseIndex)
	{
		BrowseIndex = newBrowseIndex;

		if (BrowseIndex >= 0 && BrowseIndex < static_cast<int>(TreeList.size()))
		{
			JumpScrollbarToViewBranch(BrowseIndex);
			CalcHoverTransformAtIdx(BrowseIndex);
		}
	}

	void TreeListComponent::SetSelectedItem (INT itemIndex)
	{
		INT oldSelectedItemIdx = SelectedItemIdx;
		SelectedItemIdx = itemIndex;
		SelectedBranch = GetBranchFromIdx(SelectedItemIdx);
		if (SelectedBranch == nullptr)
		{
			//The item index was not valid index
			SelectedItemIdx = INT_INDEX_NONE;
		}

		UpdateSelectedItem(oldSelectedItemIdx != SelectedItemIdx);
	}

	void TreeListComponent::SetSelectedItem (SDataBranch* item)
	{
		INT oldSelectedItemIdx = SelectedItemIdx;

		if (item == nullptr || TreeList.size() <= 0)
		{
			SelectedItemIdx = INT_INDEX_NONE;
			SelectedBranch = nullptr;
		}
		else
		{
			//Verify the item is within the tree list
			INT index = -1;
			for (TreeBranchIterator<TreeListComponent::SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter++)
			{
				index++;
				if (iter.GetSelectedBranch() == item)
				{
					SelectedBranch = item;
					break;
				}
			}

			SelectedItemIdx = index;
		}

		UpdateSelectedItem(oldSelectedItemIdx != SelectedItemIdx);
	}

	void TreeListComponent::SetTreeList (const std::vector<SDataBranch*>& newTreeList)
	{
		ClearTreeList();

		TreeList = newTreeList;

		RefreshTree();
	}

	void TreeListComponent::ClearTreeList ()
	{
		while (TreeList.size() > 0)
		{
			delete TreeList.at(TreeList.size() - 1)->Data;
			delete TreeList.at(TreeList.size() - 1);
			TreeList.pop_back();
		}

		DestroyTreeComponents();

		if (VALID_OBJECT(ListScrollbar))
		{
			ListScrollbar->SetMaxScrollPosition(0);
		}
	}

	SolidColorRenderComponent* TreeListComponent::GetSelectedBar () const
	{
		return SelectedBar;
	}

	SolidColorRenderComponent* TreeListComponent::GetHoverBar () const
	{
		return HoverBar;
	}

	INT TreeListComponent::GetBrowseIndex () const
	{
		return BrowseIndex;
	}

	TreeListComponent::SDataBranch* TreeListComponent::GetBranchFromIdx (INT treeIdx) const
	{
		if (treeIdx < 0 || TreeList.size() <= 0)
		{
			LOG2(LOG_WARNING, TXT("Failed to find branch from Idx %s.  Out of bounds from TreeList of size %s."), treeIdx, INT(TreeList.size()));
			return nullptr;
		}

		return TreeList.at(treeIdx.ToUnsignedInt());
	}

	void TreeListComponent::InitializeScrollbar ()
	{
		ListScrollbar = ScrollbarComponent::CreateObject();
		if (!AddComponent(ListScrollbar))
		{
			ListScrollbar = nullptr;
			return;
		}

		ListScrollbar->SetMaxScrollPosition(CountNumVisibleEntries());
		ListScrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, TreeListComponent, HandleScrollPosChanged, void, INT));
	}

	void TreeListComponent::InitializeSolidColors ()
	{
		//TODO:  Get colors from GUITheme

		//Initialize selected bar
		SelectedBar = SolidColorRenderComponent::CreateObject();
		if (!AddComponent(SelectedBar))
		{
			SelectedBar = nullptr;
		}
		else
		{
			SelectedBar->SolidColor = sf::Color::Cyan;
			SelectedBar->SetVisibility(false); //This will become visible upon selecting an item

			SelectedTransform = AbsTransformComponent::CreateObject();
			if (!AddComponent(SelectedTransform))
			{
				SelectedTransform = nullptr;
			}
			else
			{
				SelectedBar->TransformationInterface = SelectedTransform;
				SelectedTransform->SetRelativeTo(GetTransform());
				//The transformation attributes will be set upon selecting an item
			}
		}

		HoverBar = SolidColorRenderComponent::CreateObject();
		if (!AddComponent(HoverBar))
		{
			HoverBar = nullptr;
		}
		else
		{
			HoverBar->SolidColor = sf::Color(200, 200, 200, 96);
			HoverBar->SetVisibility(false); //This will become visible upon hovering or browsing items

			HoverTransform = AbsTransformComponent::CreateObject();
			if (!AddComponent(HoverTransform))
			{
				HoverTransform = nullptr;
			}
			else
			{
				HoverBar->TransformationInterface = HoverTransform;
				HoverTransform->SetRelativeTo(GetTransform());
				//The transformation attributes will be set upon hovering or browsing items.
			}
		}
	}

	void TreeListComponent::UpdateSelectedItem (bool bInvokeCallback)
	{
		UpdateSelectedBar();		

		if (bInvokeCallback && OnOptionSelected.IsBounded())
		{
			OnOptionSelected(SelectedItemIdx);
		}
	}

	LabelComponent* TreeListComponent::FindLabelAtCoordinates (FLOAT x, FLOAT y) const
	{
		for (unsigned int i = 0; i < TreeLabels.size(); i++)
		{
			//If the labels are in sorted order, keep going down the list until the label's bottom bounds is beyond the mouse.
			if (TreeLabels.at(i)->GetTransform()->CalcFinalBottomBounds() >= y)
			{
				return TreeLabels.at(i);
			}
		}

		return nullptr;
	}

	INT TreeListComponent::CountNumVisibleEntries () const
	{
		if (TreeList.size() <= 0)
		{
			return 0;
		}

		INT result = 0;
		TreeBranchIterator<const TreeListComponent::SDataBranch> iter(TreeList.at(0));
		while (iter.GetSelectedBranch() != nullptr)
		{
			result++;
			iter.SelectNextVisibleBranch();
		}

		return result;
	}

	INT TreeListComponent::FindBranchIdxFromAnchor (INT anchorBranchIdx, INT distanceFromAnchor, bool bIncludeCollapsed) const
	{
		if (bIncludeCollapsed) //No need to check for collapsed branches; use fast solution rather than iteration
		{
			INT result = anchorBranchIdx + distanceFromAnchor;
			if (result < 0)
			{
				result = INT_INDEX_NONE;
			}

			return result;
		}

		INT curIdx = Utils::Min(anchorBranchIdx, INT(TreeList.size() - 1));
		INT stepsFromAnchor = 0;
		while (true)
		{
			(distanceFromAnchor > 0) ? curIdx++ : curIdx--;

			if (curIdx < 0) //Reached beyond idx 0 in TreeList
			{
				return INT_INDEX_NONE;
			}
			else if (curIdx >= static_cast<int>(TreeList.size()))
			{
				return INT_INDEX_NONE;
			}

			if (TreeList.at(curIdx.ToUnsignedInt())->IsVisible())
			{
				stepsFromAnchor++;
				if (stepsFromAnchor == INT::Abs(distanceFromAnchor))
				{
					return curIdx;
				}
			}
		}

		return INT_INDEX_NONE;
	}

	TreeListComponent::SDataBranch* TreeListComponent::FindFirstVisibleBranch () const
	{
		if (TreeList.size() <= 0)
		{
			return nullptr;
		}

		if (ListScrollbar->GetNumVisibleScrollPositions() >= ListScrollbar->GetMaxScrollPosition())
		{
			return TreeList.at(0);
		}

		INT scrollIdx = -1;
		TreeBranchIterator<TreeListComponent::SDataBranch> iter(TreeList.at(0));
		while (iter.GetSelectedBranch() != nullptr)
		{
			scrollIdx++;
			if (scrollIdx >= ListScrollbar->GetScrollPosition())
			{
				return iter.GetSelectedBranch();
			}

			iter.SelectNextVisibleBranch();
		}

		LOG3(LOG_WARNING, TXT("Unable to find first visible branch since the scroll bar is at an invalid scroll position:  %s out of %s with num visible scroll positions being %s"), ListScrollbar->GetScrollPosition(), ListScrollbar->GetMaxScrollPosition(), ListScrollbar->GetNumVisibleScrollPositions());
		return nullptr;
	}

	INT TreeListComponent::FindLastVisibleBranchIdx () const
	{
		for (INT i = INT(TreeList.size() - 1); i >= 0; i--)
		{
			if (TreeList.at(i.ToUnsignedInt())->IsVisible())
			{
				return i;
			}
		}

		LOG1(LOG_WARNING, TXT("Unable to find last visible branch from %s since all of the branches are collapsed."), GetUniqueName());
		return INT_INDEX_NONE;
	}

	ButtonComponent* TreeListComponent::FindExpandButtonAtIdx (INT branchIdx) const
	{
		SDataBranch* branch = GetBranchFromIdx(branchIdx);
		if (branch == nullptr)
		{
			return nullptr;
		}

		for (unsigned int i = 0; i < ExpandButtons.size(); i++)
		{
			if (ExpandButtons.at(i).Branch == branch)
			{
				return ExpandButtons.at(i).Button;
			}
		}

		return nullptr;
	}

	void TreeListComponent::SetExpandForAllBranches (bool bExpanded)
	{
		if (TreeList.size() <= 0)
		{
			return;
		}

		for (TreeBranchIterator<SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter++)
		{
			iter.GetSelectedBranch()->bExpanded = bExpanded;
		}

		if (VALID_OBJECT(ListScrollbar))
		{
			ListScrollbar->SetMaxScrollPosition(CountNumVisibleEntries());
		}

		RefreshTree();
	}

	void TreeListComponent::RefreshComponentTransforms ()
	{
		//Reconstruct the tree entries first to place the buttons, connections, and labels
		RefreshTree();

		UpdateSelectedBar();

		//Simply hide the hover bar since that's based on mouse position which is likely not relevant to the new change in transform dimensions.
		if (VALID_OBJECT(HoverBar))
		{
			HoverBar->SetVisibility(false);
		}

		if (VALID_OBJECT(ListScrollbar))
		{
			//TODO:  Get scrollbar width from theme
			ListScrollbar->GetTransform()->SetBaseSize(Vector2(16.f, GetTransform()->GetCurrentSizeY()));
			ListScrollbar->GetTransform()->SnapRight();
			ListScrollbar->GetTransform()->SnapTop();
			ListScrollbar->SetNumVisibleScrollPositions((GetTransform()->GetCurrentSizeY() / BranchHeight).ToINT());
		}
	}

	void TreeListComponent::CalcHoverTransform (unsigned int expandButtonIdx)
	{
		if (expandButtonIdx == UINT_INDEX_NONE)
		{
			bIgnoreHoverMovement = false;
			HoverTransform->SetBaseSize(Vector2(HoverTransform->GetBaseSizeX(), BranchHeight));
			return;
		}

		bIgnoreHoverMovement = true;
		INT numOptions = ExpandButtons.at(expandButtonIdx).Branch->CountNumChildren(false);
		numOptions++; //include parent branch

		//Set hover bar over the label component next to button
		Vector2 newPosition = ExpandButtons.at(expandButtonIdx).Button->GetTransform()->GetAbsCoordinates();
		newPosition.X += ExpandButtons.at(expandButtonIdx).Button->GetTransform()->GetCurrentSizeX();
		HoverTransform->SetAbsCoordinates(newPosition);

		//Ensure the hover bar doesn't go beyond the bottom bounds
		const FLOAT maxSizeY = GetTransform()->GetCurrentSizeY() - HoverTransform->GetAbsCoordinatesY();
		HoverTransform->SetBaseSize(Vector2(HoverTransform->GetBaseSizeX(), Utils::Min(BranchHeight * numOptions.ToFLOAT(), maxSizeY)));
	}

	void TreeListComponent::CalcHoverTransformAtIdx (INT branchIdx)
	{
		if (!VALID_OBJECT(HoverBar))
		{
			return;
		}

		SDataBranch* branch = GetBranchFromIdx(branchIdx);
		if (branch != nullptr)
		{
			bool bWithinScrollRange = CalcTransformAtIdx(HoverTransform, branch);
			HoverBar->SetVisibility(bWithinScrollRange);
		}
	}

	void TreeListComponent::UpdateSelectedBar ()
	{
		if (!VALID_OBJECT(SelectedBar))
		{
			return;
		}

		bool bWithinScrollRange = CalcTransformAtIdx(SelectedTransform, SelectedBranch);
		SelectedBar->SetVisibility(bWithinScrollRange);
	}

	bool TreeListComponent::CalcTransformAtIdx (AbsTransformComponent* transform, TreeListComponent::SDataBranch* branch)
	{
		if (TreeList.size() <= 0 || branch == nullptr)
		{
			return false;
		}

		//Find out how many branches the selected bar must offset from
		INT scrollIdx = 0;
		INT maxVisibleScrollIdx = VALID_OBJECT(ListScrollbar) ? ListScrollbar->GetLastVisibleScrollPosition() : INT_MAX;
		for (TreeBranchIterator<const TreeListComponent::SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter.SelectNextVisibleBranch())
		{
			if (iter.GetSelectedBranch() == branch)
			{
				break;
			}

			scrollIdx++;
			if (scrollIdx > maxVisibleScrollIdx)
			{
				//Selected branch is either collapsed or beyond current scroll position
				return false;
			}
		}

		INT topScrollIdx = (VALID_OBJECT(ListScrollbar)) ? ListScrollbar->GetScrollPosition() : 0;
		if (scrollIdx < topScrollIdx)
		{
			//selected branch is above current scroll position.
			return false;
		}

		//Now we can finally calculate the selected bar's transformation
		FLOAT absX = branch->GetNumParentBranches().ToFLOAT() * IndentSize;
		FLOAT absY = (scrollIdx - topScrollIdx).ToFLOAT() * BranchHeight;

		transform->SetAbsCoordinates(Vector2(absX, absY));
		transform->SetBaseSize(Vector2(GetTransform()->GetCurrentSizeX() - transform->GetAbsCoordinatesX(), BranchHeight));

		return true;
	}

	void TreeListComponent::DestroyTreeComponents ()
	{
		NumPendingDeleteComponents += static_cast<int>(ExpandButtons.size() + BranchConnections.size() + ConnectionTransforms.size() + TreeLabels.size());

		for (unsigned int i = 0; i < ExpandButtons.size(); i++)
		{
			ExpandButtons.at(i).Button->Destroy();
		}
		ExpandButtons.clear();
		bIgnoreHoverMovement = false; //buttons cleared, we can ignore their hover states

		for (unsigned int i = 0; i < BranchConnections.size(); i++)
		{
			BranchConnections.at(i)->Destroy();
		}
		BranchConnections.clear();

		for (unsigned int i = 0; i < ConnectionTransforms.size(); i++)
		{
			ConnectionTransforms.at(i)->Destroy();
		}
		ConnectionTransforms.clear();

		for (unsigned int i = 0; i < TreeLabels.size(); i++)
		{
			TreeLabels.at(i)->Destroy();
		}
		TreeLabels.clear();
	}

	void TreeListComponent::RefreshTree ()
	{
		//Destroy old buttons, labels, and connectors
		DestroyTreeComponents();
		if (TreeList.size() <= 0)
		{
			return;
		}

		INT topBranchScrollIdx = (VALID_OBJECT(ListScrollbar)) ? ListScrollbar->GetScrollPosition() : 0;
		INT curScrollIdx = -1;
		FLOAT expandButtonSize = BranchHeight;
		const Vector2 treeFinalCoordinates = GetTransform()->CalcFinalCoordinates();
		vector<SDataBranch*> parentsAboveScroll; //List of expanded branches that are above the current scroll position
		vector<SBranchConnectionInfo> orphans; //List of branches that needs to be connected to their parent.
		TreeBranchIterator<TreeListComponent::SDataBranch> iter(TreeList.at(0));

		//Construct the tree
		while (iter.GetSelectedBranch() != nullptr)
		{
			curScrollIdx++;
			SDataBranch* curBranch = iter.GetSelectedBranch();
			Vector2 curBranchPos = Vector2(curBranch->GetNumParentBranches().ToFLOAT() * IndentSize, (curScrollIdx - topBranchScrollIdx).ToFLOAT() * BranchHeight);
			curBranchPos += treeFinalCoordinates;

			if (curScrollIdx < topBranchScrollIdx)
			{
				//Keep track of this branch in case we need to draw a connection to it
				if (curBranch->bExpanded && curBranch->Children.size() > 0)
				{
					//Handle case where the expand button is above the scroll position but the child is below the scroll position
					if (curScrollIdx + curBranch->CountNumChildren(false) >= ListScrollbar->GetScrollPosition() + ListScrollbar->GetNumVisibleScrollPositions())
					{
						//Draw a straight line from cur branch to bottom of transform
						Vector2 parentPos = Vector2(curBranchPos.X, treeFinalCoordinates.Y);
						parentPos.X += (expandButtonSize * 0.5f); //center horizontally relative to the button beyond scroll position.

						Vector2 childPos = parentPos;
						childPos.Y = GetTransform()->CalcFinalBottomBounds();

						CreateConnections(parentPos, childPos);
					}

					//At least one child may be visible within scroll position.  Store a reference to this branch since one of the children may need to link to this.
					parentsAboveScroll.push_back(curBranch);
				}

				//This branch is not visible since the scrollbar is not at the top.  Skip this branch.
				iter.SelectNextVisibleBranch();
				continue;
			}

			//Check if we need to draw a connection from cur branch to a branch above current scoll position
			for (unsigned int i = 0; i < parentsAboveScroll.size(); i++)
			{
				if (parentsAboveScroll.at(i) == curBranch->ParentBranch)
				{
					Vector2 parentPos = Vector2(curBranchPos.X, treeFinalCoordinates.Y);
					parentPos.X += (expandButtonSize * 0.5f); //center horizontally relative to the button beyond scroll position.
					parentPos.X -= IndentSize; //The parent branch would be one unit to the left.

					Vector2 childPos = curBranchPos;
					childPos.X += (expandButtonSize * 0.75f); //Stretch through the button to remove the gap between horizontal bar and label for branches without expand buttons.
					childPos.Y += (BranchHeight * 0.5f); //Vertical center align

					CreateConnections(parentPos, childPos);

					break;
				}
			}

			//Create the expand/collapse button
			if (curBranch->Children.size() > 0)
			{
				ButtonComponent* expandButton = ButtonComponent::CreateObject();
				if (AddComponent(expandButton))
				{
					expandButton->RenderComponent->SetSpriteTexture(curBranch->bExpanded ? CollapseButtonTexture : ExpandButtonTexture);
					expandButton->GetTransform()->SetAbsCoordinates(curBranchPos - treeFinalCoordinates);
					expandButton->GetTransform()->SetBaseSize(Vector2(expandButtonSize, expandButtonSize));
					expandButton->CaptionComponent->Destroy();
					expandButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
					dynamic_cast<SingleSpriteButtonState*>(expandButton->GetButtonStateComponent())->Sprite = expandButton->RenderComponent;
					expandButton->GetButtonStateComponent()->RefreshState();
					expandButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TreeListComponent, HandleExpandButtonClicked, void, ButtonComponent*));
					ExpandButtons.push_back(SButtonMapping(expandButton, curBranch));

					HoverComponent* expandHover = HoverComponent::CreateObject();
					if (expandButton->AddComponent(expandHover))
					{
						expandHover->OnRollOver = SDFUNCTION_1PARAM(this, TreeListComponent, HandleExpandButtonRolledOver, void, GUIComponent*);
						expandHover->OnRollOut = SDFUNCTION_1PARAM(this, TreeListComponent, HandleExpandButtonRolledOut, void, GUIComponent*);
					}

					//Draw connection between current branch and child
					if (curBranch->bExpanded)
					{
						Vector2 connectionPoint = expandButton->GetTransform()->CalcFinalCoordinates();
						connectionPoint += Vector2(expandButton->GetTransform()->GetCurrentSizeX() * 0.5f, expandButton->GetTransform()->GetCurrentSizeY());
						for (unsigned int i = 0; i < curBranch->Children.size(); i++)
						{
							orphans.push_back(SBranchConnectionInfo(curBranch, curBranch->Children.at(i), connectionPoint));
						}
					}
				}
			}

			LabelComponent* branchLabel = LabelComponent::CreateObject();
			if (AddComponent(branchLabel))
			{
				branchLabel->SetWrapText(false);
				Vector2 labelPos = curBranchPos;
				labelPos.X += expandButtonSize;
				branchLabel->GetTransform()->SetAbsCoordinates(labelPos - treeFinalCoordinates);
				branchLabel->GetTransform()->SetBaseSize(Vector2(GetTransform()->GetCurrentSizeX() - branchLabel->GetTransform()->GetAbsCoordinatesX(), expandButtonSize));
				branchLabel->SetCharacterSize(expandButtonSize.ToINT());
				branchLabel->SetTextContent(curBranch->Data->GetLabelText());
				TreeLabels.push_back(branchLabel);
			}

			//Check if curBranch is an orphan
			for (unsigned int i = 0; i < orphans.size(); i++)
			{
				if (orphans.at(i).ChildBranch != curBranch)
				{
					continue;
				}

				//Create a connection between child and parent
				Vector2 parentPos = orphans.at(i).ParentConnectPosition;
				Vector2 childPos = curBranchPos;
				childPos.X += (expandButtonSize * 0.75f); //Stretch through the button to remove the gap between horizontal bar and label for branches without expand buttons.
				childPos.Y += (BranchHeight * 0.5f); //Vertical center align
				CreateConnections(parentPos, childPos);

				//Connection successful, remove orphan reference
				orphans.erase(orphans.begin() + i);
				break;
			}

			if (VALID_OBJECT(ListScrollbar) && !ListScrollbar->IsIndexVisible(curScrollIdx + 1))
			{
				//Any orphans remaining implies that they're beyond the current visible scroll position.  Draw connections to the bottom from each parent.
				for (unsigned int i = 0; i < orphans.size(); i++)
				{
					Vector2 childPos;
					childPos.X = orphans.at(i).ParentConnectPosition.X;
					childPos.Y = GetTransform()->CalcFinalBottomBounds(); //Child position will be straight down from parent
					CreateConnections(orphans.at(i).ParentConnectPosition, childPos);
				}
				orphans.clear();

				//Reached at bottom of scrollbar... stop constructing new branches
				break;
			}

			iter.SelectNextVisibleBranch();
		}

		UpdateSelectedBar();

		//Reset hover transform's height to a single line in case it was hovering over a button.
		if (VALID_OBJECT(HoverTransform))
		{
			HoverTransform->SetBaseSize(Vector2(HoverTransform->GetCurrentSizeX(), BranchHeight));
		}

		//Check if we need to run garbage collection
		if (NumPendingDeleteComponents > 0 && NumPendingDeleteComponents > MaxNumPendingDeleteComponents)
		{
			NumPendingDeleteComponents = 0; //Set variable early to protect against inf recursion
			Engine::GetEngine()->CollectGarbage();
		}
	}

	void TreeListComponent::CreateConnections (const Vector2& parentPt, const Vector2 childPt)
	{
		FLOAT highestPt = Utils::Min(parentPt.Y, childPt.Y);
		FLOAT lowestPt = Utils::Max(parentPt.Y, childPt.Y);
		FLOAT leftestPt = Utils::Min(parentPt.X, childPt.X);
		FLOAT rightestPt = Utils::Max(parentPt.X, childPt.X);

		//Center align with connection thickness into consideration
		lowestPt += (ConnectionThickness * 0.5f);

		//Vertical line
		SolidColorRenderComponent* verticalConnection = SolidColorRenderComponent::CreateObject();
		if (!AddComponent(verticalConnection))
		{
			return;
		}

		AbsTransformComponent* vertTransform = AbsTransformComponent::CreateObject();
		if (!AddComponent(vertTransform))
		{
			verticalConnection->Destroy();
			return;
		}

		verticalConnection->SolidColor = ConnectionColor;
		verticalConnection->TransformationInterface = vertTransform;
		verticalConnection->RelevantRenderWindows.clear();
		verticalConnection->RelevantRenderWindows.push_back(GetWindowHandle());
		verticalConnection->SetDrawOrder(CurrentDrawOrder);

		vertTransform->SetAbsCoordinates(Vector2(leftestPt - (ConnectionThickness * 0.5f), highestPt)); //Align horizontally over source
		vertTransform->SetBaseSize(Vector2(ConnectionThickness, lowestPt - highestPt));

		BranchConnections.push_back(verticalConnection);
		ConnectionTransforms.push_back(vertTransform);

		//Horizontal line
		if (childPt.X - parentPt.X < ConnectionThickness)
		{
			return; //Don't bother drawing a horizontal line since the two positions are directly vertical from each other
		}

		SolidColorRenderComponent* horConnection = SolidColorRenderComponent::CreateObject();
		if (!AddComponent(horConnection))
		{
			return;
		}

		AbsTransformComponent* horTransform = AbsTransformComponent::CreateObject();
		if (!AddComponent(horTransform))
		{
			horConnection->Destroy();
			return;
		}


		horConnection->SolidColor = ConnectionColor;
		horConnection->TransformationInterface = horTransform;
		horConnection->RelevantRenderWindows.clear();
		horConnection->RelevantRenderWindows.push_back(GetWindowHandle());
		horConnection->SetDrawOrder(CurrentDrawOrder);

		horTransform->SetAbsCoordinates(Vector2(leftestPt, lowestPt - ConnectionThickness));
		horTransform->SetBaseSize(Vector2(rightestPt - leftestPt, ConnectionThickness));

		BranchConnections.push_back(horConnection);
		ConnectionTransforms.push_back(horTransform);
	}

	void TreeListComponent::HandleExpandButtonClicked (ButtonComponent* uiComponent)
	{
		//Identify which button was clicked - then toggle bExpanded based on the button's associated branch.
		for (unsigned int i = 0; i < ExpandButtons.size(); i++)
		{
			if (ExpandButtons.at(i).Button == uiComponent)
			{
				SDataBranch* branch = ExpandButtons.at(i).Branch;
				(branch->bExpanded) ? CollapseBranch(branch) : ExpandBranch(branch);
				break;
			}
		}
	}

	void TreeListComponent::HandleExpandButtonRolledOver (GUIComponent* uiComponent)
	{
		if (!VALID_OBJECT(HoverTransform))
		{
			return;
		}

		ButtonComponent* expandButton = dynamic_cast<ButtonComponent*>(uiComponent);
		if (expandButton == nullptr)
		{
			LOG2(LOG_WARNING, TXT("Expand button rolled over callback for %s is not an expand button.  Instead it's a %s"), GetUniqueName(), uiComponent->GetUniqueName());
			return;
		}

		//Find the databranch this button is associated with
		for (unsigned int i = 0; i < ExpandButtons.size(); i++)
		{
			if (expandButton == ExpandButtons.at(i).Button)
			{
				CalcHoverTransform(i);
				break;
			}
		}
	}

	void TreeListComponent::HandleExpandButtonRolledOut (GUIComponent* uiComponent)
	{
		MousePointer* mouse = GetMousePointer();
		CHECK(mouse != nullptr)
		Vector2 mousePos = mouse->GetMousePosition();

		//Identify if user is hovering over a different expand button
		for (unsigned int i = 0; i < ExpandButtons.size(); i++)
		{
			if (ExpandButtons.at(i).Button->GetTransform()->WithinBounds(mousePos.X, mousePos.Y))
			{
				CalcHoverTransform(i);
				return;
			}
		}

		//Not hovering over any button, remove children hover state.
		CalcHoverTransform(UINT_INDEX_NONE);
	}

	void TreeListComponent::HandleScrollPosChanged (INT newScrollPosition)
	{
		RefreshTree();
	}
}

#endif