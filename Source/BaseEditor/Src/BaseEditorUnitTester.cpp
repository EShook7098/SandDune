/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BaseEditorUnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

#if INCLUDE_BASEEDITOR

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(BaseEditorUnitTester, UnitTester)

	bool BaseEditorUnitTester::RunTests (EUnitTestFlags testFlags) const
	{
		if ((testFlags & UTF_Manual) > 0 && (testFlags & UTF_NeverFails) > 0 && (testFlags & UTF_Asynchronous) > 0)
		{
			return TestPropertyLine(testFlags);
		}

		return true;
	}

	bool BaseEditorUnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
	{
		if (!Super::MetRequirements(completedTests))
		{
			return false;
		}

		//Requires Graphics Engine and Input unit tests
		vector<const Object*> requiredTests;
		requiredTests.push_back(GraphicsUnitTester::SStaticClass()->GetDefaultObject());
		requiredTests.push_back(InputUnitTester::SStaticClass()->GetDefaultObject());
		
		for (unsigned int i = 0; i < completedTests.size(); i++)
		{
			for (unsigned int j = 0; j < requiredTests.size(); j++)
			{
				if (requiredTests.at(j) == completedTests.at(i))
				{
					requiredTests.erase(requiredTests.begin() + j);
					break;
				}
			}
		}

		return (requiredTests.size() == 0);
	}

	bool BaseEditorUnitTester::TestPropertyLine (EUnitTestFlags testFlags) const
	{
		UNITTESTER_LOG(testFlags, TXT("Begin Property Line testing. . ."));
		BaseEditorTester* tester = BaseEditorTester::CreateObject(); //The base editor tester will destroy itself whenever the user flips one of its bools.
		tester->TestFlags = testFlags;
		UNITTESTER_LOG(testFlags, TXT("Editable property lines have been instantiated.  This unit test will terminate upon user input."));

		return true;
	}
}

#endif
#endif