/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableFLOAT.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

#if INCLUDE_BASEEDITOR

using namespace std;

namespace SD
{
	EditableFLOAT::EditableFLOAT () : EditableProperty()
	{
		bMinBounded = false;
		bMaxBounded = false;
		MinValue = -MAXFLOAT;
		MaxValue = MAXFLOAT;
		OwningProperty = nullptr;
	}

	EditableFLOAT::EditableFLOAT (const EditableFLOAT& copyProperty) : EditableProperty(copyProperty)
	{
		bMinBounded = copyProperty.bMinBounded;
		bMaxBounded = copyProperty.bMaxBounded;
		MinValue = copyProperty.MinValue;
		MaxValue = copyProperty.MaxValue;
	}

	void EditableFLOAT::SetOwningProperty (DProperty* newOwningProperty)
	{
		EditableProperty::SetOwningProperty(newOwningProperty);

		OwningProperty = dynamic_cast<FLOAT*>(newOwningProperty);
		if (OwningProperty == nullptr)
		{
			Engine::GetEngine()->FatalError(TXT("EditableFLOATs may only describe FLOAT datatypes."));
		}
	}

	void EditableFLOAT::EditVariable (DString& outNewValue)
	{
		if (OnPrePropertyChange != nullptr)
		{
			OnPrePropertyChange(outNewValue);
		}

		OwningProperty->Value = FLOAT(outNewValue).Value;
		ApplyClamps();
		outNewValue = OwningProperty->ToString();

		if (OnPostPropertyChange != nullptr)
		{
			OnPostPropertyChange(outNewValue);
		}
	}

	DString EditableFLOAT::GetStringValue () const
	{
		return OwningProperty->ToString();
	}

	DString EditableFLOAT::ExportVariable () const
	{
		return PropertyName + TXT("=") + OwningProperty->ToString();
	}

	void EditableFLOAT::ApplyClamps ()
	{
		if (bMinBounded && OwningProperty->Value < MinValue)
		{
			OwningProperty->Value = MinValue.Value;
		}

		if (bMaxBounded && OwningProperty->Value > MaxValue)
		{
			OwningProperty->Value = MaxValue.Value;
		}
	}
}

#endif