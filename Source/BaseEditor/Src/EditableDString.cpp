/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableDString.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

#if INCLUDE_BASEEDITOR

using namespace std;

namespace SD
{
	EditableDString::EditableDString () : EditableProperty()
	{
		bApplyCharLimit = false;
		MaxCharacters = 8;
		OwningProperty = nullptr;
	}

	EditableDString::EditableDString (const EditableDString& copyProperty) : EditableProperty(copyProperty)
	{
		bApplyCharLimit = copyProperty.bApplyCharLimit;
		MaxCharacters = copyProperty.MaxCharacters;
	}

	void EditableDString::SetOwningProperty (DProperty* newOwningProperty)
	{
		EditableProperty::SetOwningProperty(newOwningProperty);

		OwningProperty = dynamic_cast<DString*>(newOwningProperty);
		if (OwningProperty == nullptr)
		{
			Engine::GetEngine()->FatalError(TXT("EditableDStrings may only describe DString datatypes."));
		}
	}

	void EditableDString::EditVariable (DString& outNewValue)
	{
		if (OnPrePropertyChange != nullptr)
		{
			OnPrePropertyChange(outNewValue);
		}

		OwningProperty->String = outNewValue.String;
		ApplyCharLimit();
		outNewValue = *OwningProperty;

		if (OnPostPropertyChange != nullptr)
		{
			OnPostPropertyChange(outNewValue);
		}
	}

	DString EditableDString::GetStringValue () const
	{
		return *OwningProperty;
	}

	DString EditableDString::ExportVariable () const
	{
		return PropertyName + TXT("=") + *OwningProperty;
	}

	void EditableDString::ApplyCharLimit ()
	{
		if (bApplyCharLimit && OwningProperty->Length() > MaxCharacters)
		{
			*OwningProperty = OwningProperty->SubStringCount(0, MaxCharacters);
		}
	}
}

#endif