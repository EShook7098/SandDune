/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PropertyLineRenderComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

#if INCLUDE_BASEEDITOR

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(PropertyLineRenderComponent, RenderComponent)

	const INT PropertyLineRenderComponent::RENDER_PROPERTY_DIVIDER = 1;
	const INT PropertyLineRenderComponent::RENDER_PROPERTY_NAME = 2;
	const INT PropertyLineRenderComponent::RENDER_PROPERTY_VALUE = 4;
	const INT PropertyLineRenderComponent::RENDER_TEXT_CURSOR = 8;
	const INT PropertyLineRenderComponent::RENDER_READ_ONLY_FOREGROUND = 16;
	const INT PropertyLineRenderComponent::RENDER_ALL = RENDER_PROPERTY_DIVIDER | RENDER_PROPERTY_NAME | RENDER_PROPERTY_VALUE | RENDER_TEXT_CURSOR | RENDER_READ_ONLY_FOREGROUND;

	void PropertyLineRenderComponent::InitProps ()
	{
		Super::InitProps();

		BackgroundColor = sf::Color(40, 40, 40, 255);
		ForegroundColor = sf::Color(0,0,0, 128);
		TextColor = sf::Color(230, 230, 230, 255);
		bPropertyReadOnly = false;
		TextSize = 14;
		DividerWidth = 2.f;

		PropertyNameWidth = 128.f;
		bUserEnteringValue = false;
		PropertyName = TXT("Unbounded Property");
		PropertyValue = TXT("Property Value not found");

		PrevPosXOffset = 0.f;
	}

	void PropertyLineRenderComponent::Render (const Camera* targetCamera, const Window* targetWindow)
	{
		if (TransformationInterface == nullptr)
		{
			LOG1(LOG_WARNING, TXT("Unable to draw %s since it does not have a TransformationInterface affiliated with it."), GetUniqueName());
			return;
		}

		sf::Transformable transformResults;
		TransformationInterface->ApplyDrawPosition(&transformResults, targetCamera);
		TransformationInterface->ApplyDrawSize(&transformResults, targetCamera);

		//Detect if there was any change in width
		if (PrevWidth.Value != transformResults.getScale().x)
		{
			PrevWidth = transformResults.getScale().x;
			CalculateClampedPropertyName();
			CalculateClampedPropertyValue();
		}

		//Render background
		sf::RectangleShape background;
		background.setSize(transformResults.getScale());
		background.setPosition(transformResults.getPosition());
		background.setFillColor(BackgroundColor);
		targetWindow->Resource->draw(background);

		//Give the property, itself, an opportunity to override how it should render
		INT renderFlags = RENDER_ALL;
		FLOAT nameOffset = 0.f;
		if (OnRender.IsBounded())
		{
			OnRender.Execute(this, transformResults, targetWindow, renderFlags, nameOffset);
		}

		if ((renderFlags & RENDER_PROPERTY_DIVIDER) > 0)
		{
			//Render divider
			sf::RectangleShape divider;
			divider.setSize(sf::Vector2f(DividerWidth.Value, transformResults.getScale().y));
			divider.setPosition(sf::Vector2f(transformResults.getPosition().x + PropertyNameWidth.Value, transformResults.getPosition().y));
			background.setFillColor(sf::Color::Black);
			targetWindow->Resource->draw(divider);
		}

		if ((renderFlags & RENDER_PROPERTY_NAME) > 0)
		{
			if (nameOffset != PrevPosXOffset)
			{
				PrevPosXOffset = nameOffset;
				CalculateClampedPropertyName();
			}

			sf::Text propertyName;
			propertyName.setCharacterSize(TextSize.ToUnsignedInt());
			propertyName.setColor(TextColor);
			propertyName.setFont(*FontPool::GetFontPool()->GetDefaultFont()->GetFontResource());
			sf::Vector2f namePosition = transformResults.getPosition();
			namePosition.x += nameOffset.Value;
			propertyName.setPosition(namePosition);
			propertyName.setString(ClampedPropertyName.String);
			targetWindow->Resource->draw(propertyName);
		}

		if ((renderFlags & RENDER_PROPERTY_VALUE) > 0)
		{
			sf::Text propertyValue;
			propertyValue.setCharacterSize(TextSize.ToUnsignedInt());
			propertyValue.setColor(TextColor);
			propertyValue.setFont(*FontPool::GetFontPool()->GetDefaultFont()->GetFontResource());
			propertyValue.setPosition(sf::Vector2f(transformResults.getPosition().x + PropertyNameWidth.Value + 6.f, transformResults.getPosition().y));
			propertyValue.setString(ClampedPropertyValue.String);
			targetWindow->Resource->draw(propertyValue);

			if (bUserEnteringValue && (renderFlags & RENDER_TEXT_CURSOR) > 0 && Engine::GetEngine()->GetElapsedTime().ToINT()%2 == 0)
			{
				//Render text cursor
				sf::RectangleShape cursor;
				cursor.setSize(sf::Vector2f(2.f, transformResults.getScale().y));
				sf::Vector2f lastCharPos = propertyValue.findCharacterPos((ClampedPropertyValue.Length() - 1).Value);
				cursor.setPosition(sf::Vector2f(lastCharPos.x + (propertyValue.getCharacterSize() * 0.75f), transformResults.getPosition().y));
				cursor.setFillColor(TextColor);
				targetWindow->Resource->draw(cursor);
			}
		}

		if ((renderFlags & RENDER_READ_ONLY_FOREGROUND) > 0 && bPropertyReadOnly)
		{
			sf::RectangleShape foreground;
			foreground.setSize(sf::Vector2f(transformResults.getScale().x - PropertyNameWidth.Value, transformResults.getScale().y));
			foreground.setPosition(sf::Vector2f(transformResults.getPosition().x + PropertyNameWidth.Value, transformResults.getPosition().y));
			foreground.setFillColor(ForegroundColor);
			targetWindow->Resource->draw(foreground);
		}
	}

	void PropertyLineRenderComponent::CalculateClampedPropertyValue ()
	{
		Font* defaultFont = FontPool::GetFontPool()->GetDefaultFont();
		INT maxCharIdx = defaultFont->FindCharNearWidthLimit(PropertyValue, TextSize.ToUnsignedInt(), PrevWidth - PropertyNameWidth);

		if (maxCharIdx < PropertyValue.Length())
		{
			ClampedPropertyValue = PropertyValue.SubStringCount(0, maxCharIdx + 1);
		}
		else
		{
			ClampedPropertyValue = PropertyValue;
		}
	}

	void PropertyLineRenderComponent::CalculateClampedPropertyName ()
	{
		Font* defaultFont = FontPool::GetFontPool()->GetDefaultFont();
		INT maxCharIdx = defaultFont->FindCharNearWidthLimit(PropertyName, TextSize.ToUnsignedInt(), PropertyNameWidth - PrevPosXOffset);

		if (maxCharIdx < PropertyName.Length())
		{
			ClampedPropertyName = PropertyName.SubStringCount(0, maxCharIdx + 1);
		}
		else
		{
			ClampedPropertyName = PropertyName;
		}
	}

	void PropertyLineRenderComponent::SetPropertyNameWidth (FLOAT newPropertyNameWidth)
	{
		PropertyNameWidth = newPropertyNameWidth;
	}

	void PropertyLineRenderComponent::SetPropertyName (const DString& newPropertyName)
	{
		PropertyName = newPropertyName;
		CalculateClampedPropertyName();
	}

	void PropertyLineRenderComponent::SetPropertyValue (const DString& newPropertyValue)
	{
		PropertyValue = newPropertyValue;
		ClampedPropertyValue = newPropertyValue;
		//Not clamping since the user could be entering data into this field.
	}

	void PropertyLineRenderComponent::SetUserEnteringValue (bool bIsEnteringValue)
	{
		bUserEnteringValue = bIsEnteringValue;
	}

	void PropertyLineRenderComponent::AddPropertyValue (const DString& addedText)
	{
		PropertyValue += addedText;
		ClampedPropertyValue = PropertyValue;
	}

	FLOAT PropertyLineRenderComponent::GetPropertyNameWidth () const
	{
		return PropertyNameWidth;
	}

	DString PropertyLineRenderComponent::GetPropertyName () const
	{
		return PropertyName;
	}

	DString PropertyLineRenderComponent::GetPropertyValue () const
	{
		return PropertyValue;
	}

	bool PropertyLineRenderComponent::GetUserEnteringValue () const
	{
		return bUserEnteringValue;
	}
}

#endif