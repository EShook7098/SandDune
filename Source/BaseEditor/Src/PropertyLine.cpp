/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PropertyLine.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

#if INCLUDE_BASEEDITOR

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(PropertyLine, Entity)

	void PropertyLine::InitProps ()
	{
		Super::InitProps();

		bEnableDividerDragging = true;

		RepresentedProperty = nullptr;
		bDraggingBorder = false;
		bOverDivider = false;
		bActive = true;
		Transform = nullptr;
		LineRender = nullptr;
		PrevTextValue = TXT("");
	}

	void PropertyLine::BeginObject ()
	{
		Super::BeginObject();

		InputComponent* input = InputComponent::CreateObject();
		if (AddComponent(input))
		{
			input->CaptureTextDelegate = SDFUNCTION_1PARAM(this, PropertyLine, HandleText, bool, const sf::Event&);
			input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, PropertyLine, HandleKeyInput, bool, const sf::Event&);
			input->MouseMoveDelegate = SDFUNCTION_3PARAM(this, PropertyLine, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
			input->MouseClickDelegate = SDFUNCTION_3PARAM(this, PropertyLine, HandleMouseButton, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		}

		if (!VALID_OBJECT(Transform))
		{
			Transform = AbsTransformComponent::CreateObject();
			if (!AddComponent(Transform))
			{
				Transform = nullptr;
				Destroy();
				return;
			}
		}

		Transform->SetBaseSize(Vector2(384, 16));
		PropertyHeight = Transform->GetBaseSizeY();

		if (!VALID_OBJECT(LineRender))
		{
			LineRender = PropertyLineRenderComponent::CreateObject();
			if (!AddComponent(LineRender))
			{
				LineRender = nullptr;
				return;
			}
		}

		CHECK(LineRender != nullptr)
		LineRender->TransformationInterface = Transform;
		//Update the renderer to use the represented variable (if this was copied from another object)
		if (RepresentedProperty != nullptr)
		{
			BindPropertyVariable(RepresentedProperty);
		}
	}

	void PropertyLine::Destroy ()
	{
		BindPropertyVariable(nullptr);
		if (OnDestroy.IsBounded())
		{
			OnDestroy.Execute();
		}

		Super::Destroy();
	}

	void PropertyLine::SetPropertyLineWidth (FLOAT newWidth)
	{
		Transform->SetBaseSize(Vector2(newWidth, Transform->GetBaseSizeY()));
	}

	void PropertyLine::SetPropertyTextValue (const DString& newValue)
	{
		if (VALID_OBJECT(LineRender))
		{
			LineRender->SetPropertyValue(newValue);
			LineRender->CalculateClampedPropertyValue();
		}
	}

	void PropertyLine::BindPropertyVariable (EditableProperty* targetVariable)
	{
		if (LineRender == nullptr)
		{
			return;
		}

		if (RepresentedProperty != nullptr)
		{
			RepresentedProperty->OnHeightChange = nullptr;
			RepresentedProperty->SetPropertyEditor(nullptr);
		}

		RepresentedProperty = targetVariable;
		if (RepresentedProperty == nullptr)
		{
			return;
		}

		RepresentedProperty->OnHeightChange = SDFUNCTION_1PARAM(this, PropertyLine, HandlePropertyHeightChange, void, INT);
		RepresentedProperty->SetPropertyEditor(this);
		LineRender->SetPropertyName(targetVariable->PropertyName);
		LineRender->SetPropertyValue(targetVariable->GetStringValue());
		LineRender->CalculateClampedPropertyValue();
		LineRender->bPropertyReadOnly = RepresentedProperty->GetReadOnly();
		LineRender->OnRender = SDFUNCTION_5PARAM(RepresentedProperty, EditableProperty, HandleRender, void, PropertyLineRenderComponent*, const sf::Transformable&, const Window*, INT&, FLOAT&);
	}

	void PropertyLine::SetTextFocus (bool bOnlyCallback)
	{
		if (VALID_OBJECT(LineRender))
		{
			if (!bOnlyCallback)
			{
				LineRender->SetUserEnteringValue(true);
				PrevTextValue = LineRender->GetPropertyValue();
			}

			if (OnBeginTextEdit.IsBounded())
			{
				OnBeginTextEdit(this);
			}
		}
	}

	void PropertyLine::ApplyEdits ()
	{
		if (!VALID_OBJECT(LineRender) || !LineRender->GetUserEnteringValue())
		{
			return;
		}

		DString convertedPropertyValue = LineRender->GetPropertyValue();
		if (RepresentedProperty != nullptr)
		{
			RepresentedProperty->EditVariable(convertedPropertyValue);
		}

		//Update line render's text in case (ie:  removing all letters when assigning a numeric datatype).
		if (convertedPropertyValue != LineRender->GetPropertyValue())
		{
			LineRender->SetPropertyValue(convertedPropertyValue);
		}

		LineRender->CalculateClampedPropertyValue();
		LineRender->SetUserEnteringValue(false);
	}

	void PropertyLine::SetActive (bool bNewActive)
	{
		bActive = bNewActive;
		if (VALID_OBJECT(LineRender))
		{
			LineRender->SetVisibility(bActive);
		}
	}

	EditableProperty* PropertyLine::GetRepresentedProperty () const
	{
		return RepresentedProperty;
	}

	AbsTransformComponent* PropertyLine::GetTransform () const
	{
		return Transform;
	}

	PropertyLineRenderComponent* PropertyLine::GetLineRender () const
	{
		return LineRender;
	}

	bool PropertyLine::IsActive () const
	{
		return bActive;
	}

	INT PropertyLine::AreCoordinatesOverValue (FLOAT x, FLOAT y) const
	{
		Vector2 absCoordinates = Transform->CalcFinalCoordinates();

		//Check y-axis
		if (y < absCoordinates.Y || y > Transform->CalcFinalBottomBounds())
		{
			return 0;
		}

		if (x > Transform->CalcFinalRightBounds())
		{
			return 0;
		}

		//Must be on or after the LineRedner's property value width
		if (x < absCoordinates.X + LineRender->GetPropertyNameWidth())
		{
			return 0;
		}

		//Distinguish between after or on the divider
		if (x < absCoordinates.X + LineRender->GetPropertyNameWidth() + LineRender->DividerWidth)
		{
			return 1; //On the divider
		}

		return 2;
	}

	bool PropertyLine::HandleText (const sf::Event& newTextEvent)
	{
		if (LineRender == nullptr || !LineRender->GetUserEnteringValue() || !IsActive())
		{
			return false;
		}

		switch (newTextEvent.text.unicode)
		{
			case(8): //backspace
			case(9): //tab
			case(13): //return
			case(27): //escape
				return false;
		}

		LineRender->AddPropertyValue(DString(sf::String(newTextEvent.text.unicode)));
		return true;
	}

	bool PropertyLine::HandleKeyInput (const sf::Event& newKeyEvent)
	{
		if (LineRender == nullptr || !LineRender->GetUserEnteringValue() || !IsActive())
		{
			return false;
		}

		if (newKeyEvent.type != sf::Event::KeyPressed)
		{
			return true;
		}

		switch(newKeyEvent.key.code)
		{
			case(sf::Keyboard::Escape):
				LineRender->SetPropertyValue(PrevTextValue);
				LineRender->SetUserEnteringValue(false);
				LineRender->CalculateClampedPropertyValue();
				break;

			case(sf::Keyboard::Return):
				ApplyEdits();
				break;

			case(sf::Keyboard::BackSpace):
			{
				DString newPropValue = LineRender->GetPropertyValue();
				if (newPropValue.Length() > 0)
				{
					newPropValue = newPropValue.SubStringCount(0, newPropValue.Length() - 1); //Remove last character
					LineRender->SetPropertyValue(newPropValue);
				}

				break;
			}
		}

		return true;
	}

	void PropertyLine::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent, const Vector2& deltaMove)
	{
		if (LineRender == nullptr || !IsActive() || !bEnableDividerDragging)
		{
			return;
		}

		//Check hovering states
		bool bCurOverDivider = (bDraggingBorder || AreCoordinatesOverValue(FLOAT::MakeFloat(sfEvent.x), FLOAT::MakeFloat(sfEvent.y)) == 1);
		if (bCurOverDivider != bOverDivider)
		{
			bOverDivider = bCurOverDivider;
			if (bOverDivider)
			{
				Texture* hoverIcon = TexturePool::GetTexturePool()->FindTexture(TXT("Editor.CursorResizeHorizontal"));
				if (VALID_OBJECT(hoverIcon))
				{
					mouse->PushMouseIconOverride(hoverIcon, SDFUNCTION_1PARAM(this, PropertyLine, HandleMouseIconOverride, bool, const sf::Event::MouseMoveEvent&));
				}
			}
		}
		
		if (!bDraggingBorder)
		{
			return;
		}

		FLOAT newWidth = FLOAT::MakeFloat(sfEvent.x) - Transform->CalcFinalCoordinatesX();
		//clamp width (in case the user drags the border beyond transformation
		newWidth = Utils::Clamp<FLOAT>(newWidth, 8.f, Transform->GetCurrentSizeX() - 8.f);

		LineRender->SetPropertyNameWidth(newWidth);
	}

	bool PropertyLine::HandleMouseButton (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType)
	{
		if (RepresentedProperty == nullptr || Transform == nullptr || LineRender == nullptr || !IsActive())
		{
			return false;
		}

		INT mouseRelevance = AreCoordinatesOverValue(FLOAT::MakeFloat(sfEvent.x), FLOAT::MakeFloat(sfEvent.y));
		if (RepresentedProperty->OverrideMouseClickEvent(sfEvent, eventType, mouseRelevance))
		{
			return true;
		}

		if (eventType == sf::Event::MouseButtonReleased)
		{
			if (bDraggingBorder)
			{
				bDraggingBorder = false;
				LineRender->CalculateClampedPropertyValue();
				LineRender->CalculateClampedPropertyName();
				return false;
			}

			if (RepresentedProperty->GetReadOnly())
			{
				return false; //Property does not allow edits.
			}
			
			if (mouseRelevance > 1)
			{
				SetTextFocus(false);

				return true;
			}
			else if (LineRender->GetUserEnteringValue())
			{
				ApplyEdits();
			}

			return false;
		}

		if (eventType == sf::Event::MouseButtonPressed && mouseRelevance == 1 && bEnableDividerDragging)
		{
			bDraggingBorder = true;
			return true;
		}

		return false;
	}

	bool PropertyLine::HandleMouseIconOverride (const sf::Event::MouseMoveEvent& sfEvent)
	{
		return bOverDivider && bEnableDividerDragging;
	}

	void PropertyLine::HandlePropertyHeightChange (INT newNumSlots)
	{
		if (newNumSlots <= 0)
		{
			LOG2(LOG_WARNING, TXT("Received property height changed value with invalid parameters (%s) on object %s"), newNumSlots, GetUniqueName());
			return;
		}
	}
}

#endif