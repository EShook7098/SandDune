/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableVector2.h
  Defines Vector2-specific functionality to assist developers when making changes to Vector2s.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef EDITABLEVECTOR2_H
#define EDITABLEVECTOR2_H

#include "EditableProperty.h"
#include "CollapsibleProperty.h"

#if INCLUDE_BASEEDITOR

namespace SD
{
	class EditableFLOAT;

	class EditableVector2 : public EditableProperty, public CollapsibleProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Various callbacks for each axis. */
		std::function<void(DString&)> OnXPreChange;
		std::function<void(DString&)> OnYPreChange;
		std::function<void(DString&)> OnXPostChange;
		std::function<void(DString&)> OnYPostChange;

	protected:
		/* Weak pointer reference to the data type owning this editable property object. */
		Vector2* OwningProperty;

		/* Entities instantiated to render individual axis. */
		PropertyLine* PropertyLineX;
		PropertyLine* PropertyLineY;

		/* Editable variables for the x,y axis. */
		EditableFLOAT* EditableX;
		EditableFLOAT* EditableY;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		EditableVector2 ();
		EditableVector2 (const EditableVector2& copyProperty);
		virtual ~EditableVector2 ();


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void SetOwningProperty (DProperty* newOwningProperty) override;
		virtual void EditVariable (DString& outNewValue) override;
		virtual DString GetStringValue () const override;
		virtual DString ExportVariable () const override;
		virtual bool OverrideMouseClickEvent (const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType, INT mouseRelevance) override;
		virtual void SetPropertyEditor (PropertyLine* newPropertyEditor) override;
		virtual void SetReadOnly (bool bNewReadOnly) override;
		virtual INT GetMaxHeight () const override;
		virtual void HandleRender (PropertyLineRenderComponent* propertyRenderer, const sf::Transformable& renderTransform, const Window* targetWindow, INT& outRenderFlags, FLOAT& outPropNameOffset) override;

	protected:
		virtual void ExpandProperty () override;
		virtual void CollapseProperty () override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Initializes each axis of the vector to be editable.
		 */
		virtual void InitializeAxis ();

		/**
		  Removes PropertyLines for the X and Y axis if they are valid entities.
		 */
		virtual void RemoveAxisPropertyLines ();

		/**
		  Calculates the height value based on this variable's current status.
		 */
		virtual void CalculateCurrentHeight ();

		virtual DProperty* GetXAxis () const;
		virtual DProperty* GetYAxis () const;
		virtual DProperty* GetOwningProperty () const;


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleAxisBeginTextEdit (PropertyLine* focusedLine);
		virtual void HandleXPreChange (DString& outPropertyValue);
		virtual void HandleXPostChange (DString& outPropertyValue);
		virtual void HandleYPreChange (DString& outPropertyValue);
		virtual void HandleYPostChange (DString& outPropertyValue);
		virtual void HandleXLineDestroyed ();
		virtual void HandleYLineDestroyed ();
	};
}

#endif
#endif