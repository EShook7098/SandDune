/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PropertyLine.h
  An entity containing components necessary to render a property line and
  handle input.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef PROPERTYLINE_H
#define PROPERTYLINE_H

#include "BaseEditor.h"

#if INCLUDE_BASEEDITOR

namespace SD
{
	class PropertyLineRenderComponent;

	class PropertyLine : public Entity
	{
		DECLARE_CLASS(PropertyLine)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Callback to invoke whenever this entity is destroyed. */
		SDFunction<void> OnDestroy;

		/* Callback when this PropertyLine gained text edit focus. */
		SDFunction<void, PropertyLine*> OnBeginTextEdit;

		/* If true, then the user is able to drag the property name/value divider around. */
		bool bEnableDividerDragging;

	protected:
		/* Variable that is represented here, and will be updated whenever this property line's text field was updated. */
		EditableProperty* RepresentedProperty;

		/* Becomes true if the user is currently dragging the divider between the property name and the value. */
		bool bDraggingBorder;

		/* Becomes true if the mouse is currently over the divider. */
		bool bOverDivider;

		/* Absolute height needed to render a single line (One property could require more than 1 vertical slot). */
		FLOAT PropertyHeight;

		/* If false, then this entity will not be capturing input nor render. */
		bool bActive;

		/* Transformation component that defines this entity's size and position. */
		AbsTransformComponent* Transform;

		/* Component that'll be rendering the property line. */
		PropertyLineRenderComponent* LineRender;

		/* Text of property value prior to user edits (in case user cancels out of edits). */
		DString PrevTextValue;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Sets how wide this property line may render across.
		 */
		virtual void SetPropertyLineWidth (FLOAT newWidth);

		virtual void SetPropertyTextValue (const DString& newValue);

		/**
		  Binds an EditableProperty to this PropertyLine so this entity knows which property to update on changes.
		 */
		virtual void BindPropertyVariable (EditableProperty* targetVariable);

		/**
		  Give PropertyLine permission to record text events to the line renderer.
		  If bOnlyCallback is true, this will only invoke the OnBeginTextEdit callback without having to notify the line renderer about recording values.
		 */
		virtual void SetTextFocus (bool bOnlyCallback);

		/**
		  Writes the current changes from the renderer to the property, itself.
		  This also clears the text cursor to prevent the user from making further changes until they click on this field again.
		 */
		virtual void ApplyEdits ();

		virtual void SetActive (bool bNewActive);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual EditableProperty* GetRepresentedProperty () const;
		virtual AbsTransformComponent* GetTransform () const;
		virtual PropertyLineRenderComponent* GetLineRender () const;
		virtual bool IsActive () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Returns a value based given pixel coordinates are over the LineRender's property value or not.
		  0 = Not over property value.
		  1 = On the divider between property name and property value.
		  2 = Over the property value region
		 */
		virtual INT AreCoordinatesOverValue (FLOAT x, FLOAT y) const;


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual bool HandleText (const sf::Event& newTextEvent);
		virtual bool HandleKeyInput (const sf::Event& newKeyEvent);
		virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent, const Vector2& deltaMove);
		virtual bool HandleMouseButton (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType);
		virtual bool HandleMouseIconOverride (const sf::Event::MouseMoveEvent& sfEvent);

		/**
		  Invoked whenever the represented property has changed the number of lines it takes.
		  For example, a vector2 may initially take up only one slot, but when the user clicks on the expand button, the Vector2 will need 3 slots (base + x + y).
		 */
		virtual void HandlePropertyHeightChange (INT newNumSlots);
	};
}

#endif
#endif