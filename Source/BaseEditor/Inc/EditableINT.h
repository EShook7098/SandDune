/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableINT.h
  Defines INT-specific functionality to assist developers when making changes to INTs.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef EDITABLEINT_H
#define EDITABLEINT_H

#include "EditableProperty.h"

#if INCLUDE_BASEEDITOR

namespace SD
{
	class EditableINT : public EditableProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* If true, then the user cannot specify a value lower than min value. */
		bool bMinBounded;

		/* If true, then the user cannot specify a value higher than max value. */
		bool bMaxBounded;

		INT MinValue;
		INT MaxValue;

	protected:
		/* Weak pointer reference to the data type owning this editable property object. */
		INT* OwningProperty;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		EditableINT ();
		EditableINT (const EditableINT& copyProperty);


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void SetOwningProperty (DProperty* newOwningProperty) override;
		virtual void EditVariable (DString& outNewValue) override;
		virtual DString GetStringValue () const override;
		virtual DString ExportVariable () const override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void ApplyClamps ();
	};
}

#endif
#endif