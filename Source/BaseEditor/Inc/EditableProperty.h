/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableProperty.h
  An interface implementing editing-specific capabilities.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef EDITABLEPROPERTY_H
#define EDITABLEPROPERTY_H

#include "BaseEditor.h"

#if INCLUDE_BASEEDITOR

/* Macro to create and initialize an editable property, and to bind it to a specified member variable for an object. */
#define INIT_EDITABLE_PROP(dataType, memberVar, propOwner, bInReadOnly) \
	Editable##dataType##* memberVar##Extension = new Editable##dataType##(); \
	memberVar##Extension->SetReadOnly(bInReadOnly); \
	memberVar##Extension->PropertyName = TXT(#memberVar##); \
	memberVar##Extension->SetOwningProperty(&##memberVar##); \
	memberVar##Extension->BindPropertyToObject(##propOwner##);

namespace SD
{
	class PropertyLineRenderComponent;
	class PropertyLine;

	class EditableProperty : public DPropertyExtension
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Human-readable name of this property.  Typically this is equal to the member variable name. */
		DString PropertyName;

		/* Callback to invoke prior to the user making a change to the property. */
		std::function<void(DString&)> OnPrePropertyChange;

		/* Callback to invoke after this object changed the owning property's value. */
		std::function<void(DString&)> OnPostPropertyChange;

		/* Callback to invoke whenever the property has changed the number of vertical slots it needs for rendering. */
		std::function<void(INT)> OnHeightChange;

	protected:
		/* If true, this property can only be viewed.  Otherwise this property can be edited at runtime. */
		bool bReadOnly;

		/* The number of slots to render this editable property. */
		INT Height;

		/* The entity that may be editing this variable. */
		PropertyLine* PropertyEditor;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		EditableProperty ();
		EditableProperty (const EditableProperty& copyProperty);
		virtual ~EditableProperty ();


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Exports the value of the variable in string representation.
		 */
		virtual DString GetStringValue () const = 0;

		/**
		  Exports the full contents of this EditableProperty to a human readable string (includes variable name).
		 */
		virtual DString ExportVariable () const = 0;

		/**
		  The user entered text in the property field to be assigned to this variable.
		  The string should be edited to remove any invalid characters.
		 */
		virtual void EditVariable (DString& outNewValue) = 0;

		/**
		  Returns true if this property should override the mouse click event which prevents the property line from running its normal mouse logic.
		  mouseRelevance is 0 if the click event is not property value region, 1 if the click event is on the property divider, 2 is when the mouse is over the property value region.
		 */
		virtual bool OverrideMouseClickEvent (const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType, INT mouseRelevance);

		/**
		  Informs this variable which PropertyLine object that could change this variable's value.
		 */
		virtual void SetPropertyEditor (PropertyLine* newPropertyEditor);

		virtual void SetReadOnly (bool bNewReadOnly);
		virtual void SetHeight (INT newHeight);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual bool GetReadOnly () const;
		virtual INT GetHeight () const;

		/**
		  Retrieves the maximum height value this property may achieve.  Becomes negative if the max height is undeterministic.
		 */
		virtual INT GetMaxHeight () const;


		/*
		=====================
		  Event Handlers
		=====================
		*/

	public:
		/**
		  Informs the external editor how this property should be rendered.
		  @Param:  propertyRenderer - render component that would handle default rendering.
		  @Param:  renderTransform - transform results calculated using the renderer's transform and camera location.
		  @Param:  targetWindow - window object to draw sf objects to.
		  @Param:  outRenderFlags - various flags to override which aspects should be rendered or not.  Set to 0 to completely override render behavior.
		 */
		virtual void HandleRender (PropertyLineRenderComponent* propertyRenderer, const sf::Transformable& renderTransform, const Window* targetWindow, INT& outRenderFlags, FLOAT& outPropNameOffset);
	};
}

#endif
#endif