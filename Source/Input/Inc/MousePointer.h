/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MousePointer.h
  Object that's responsible for drawing the mouse cursor.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef MOUSEPOINTER_H
#define MOUSEPOINTER_H

#include "Configuration.h"

#if INCLUDE_INPUT

#include "Input.h"

namespace SD
{
	//TODO:  Readjust current mouse limit region whenever the window changed positions.

	class MousePointer : public Entity
	{
		DECLARE_CLASS(MousePointer)


		/*
		=====================
		  Data types
		=====================
		*/

	protected:
		struct SMouseIconOverride
		{
			/* Function to callback that should return true if this element should continue to override the mouse pointer. */
			SDFunction<bool, const sf::Event::MouseMoveEvent&> OverrideCallback; 

			/* Mouse Icon to set when this element is on top of stack. */
			const Texture* MousePointerIcon = nullptr;
		};

		struct SMouseLimit
		{
			/* Function callback that'll return false whenever this region is no longer applicable. */
			SDFunction<bool, const sf::Event::MouseMoveEvent&> LimitCallback;

			/* Limit region to apply while this MouseLimit is active. */
			Rectangle<INT> LimitRegion;
		};


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* If true, the mouse pointer will continuously evaluate clamped mouse positions on mouse move.  Otherwise, external classes are expected to invoke EvaluateClampedMousePointer during relevant times. */
		bool bContinuouslyEvalClampPos;

		/* If true, then the mouse pointer will evaluate mouse icon override on mouse move event. */
		bool bContinuouslyEvalMouseIcon;

	protected:
		/* Counter that determines if the mouse should be visible or not.  It's hidden when this value is greater than 0. */
		INT ShowMouseValue;

		/* If true, then this mouse pointer cannot move. */
		bool bLocked;

		SpriteComponent* MouseIcon;
		InputComponent* MouseInput;

		AbsTransformComponent* Transformation;

		/* Texture to use when displaying mouse pointer without any IconOverrides. */
		Texture* DefaultIcon;

		/* Stack of clampped mouse positions.  The vector on top of the stack determines the clamp limits of the mouse pointer. */
		std::vector<SMouseLimit> ClampPositionsStack;

		/* Stack of icons that are overriding the mouse pointer icon.  Only the element on top of the stack is considered.
		It will only pop the stack if the callback function on top returned false. */
		std::vector<SMouseIconOverride> MouseIconOverrideStack;

		/* Determines which window handle is this mouse pointer rendering to and capturing events for. */
		Window* OwningWindowHandle;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Invokes the current MouseIconOverride callback to see if it's still relevant or not.  It'll conditionally invoke the callback
		  if the current callback is equal to the specified callback.  Generally you'll either want to call this on a particular event,
		  on mouse move, or on tick.  To evaluate mouse icon regardless of what's on top of stack, see EvaluateMouseIconOverride.
		 */
		void ConditionallyEvaluateMouseIconOverride (SDFunction<bool, const sf::Event::MouseMoveEvent&> targetCallback);

		/**
		  Pushes a new mouse position limit to the clamped positions stack.
		  The limitCallback is the function callback to refer to that should return false whenever the limit region should no longer apply.
		 */
		virtual void PushPositionLimit (Rectangle<INT> newLimitRegion, SDFunction<bool, const sf::Event::MouseMoveEvent&> limitCallback);

		/**
		  Removes the specified position limit.  Returns true if found and removed.
		 */
		virtual bool RemovePositionLimit (SDFunction<bool, const sf::Event::MouseMoveEvent&> targetCallback);

		/**
		  Pushes a new mouse icon override to the top of the stack.  Every mouse pointer icon evaluation
		  will call the callback function to see if this element should continue overriding the mouse pointer or not.
		  Only the element on top of the stack is evaluated.
		 */
		virtual void PushMouseIconOverride (const Texture* newIcon, SDFunction<bool, const sf::Event::MouseMoveEvent&> callbackFunction);

		/**
		  Removes the specified MouseIconOverride.  Returns true if found and removed.
		 */
		virtual bool RemoveIconOverride (SDFunction<bool, const sf::Event::MouseMoveEvent&> targetCallback);

		/**
		  Returns true if the icon override is currently viewed (on top of stack).
		 */
		virtual bool IsIconOverrideCurrentlyViewed (SDFunction<bool, const sf::Event::MouseMoveEvent&> targetCallback) const;

		virtual void SetDefaultIcon (Texture* newIcon);

		virtual void SetMousePosition (INT newX, INT newY);

		/**
		  Increments ShowMouseValue when bVisible is false.  Decrements ShowMouseValue when bVisible is true.
		 */
		virtual void SetMouseVisibility (bool bVisible);

		virtual void SetLocked (bool bNewLocked);

		virtual void SetOwningWindowHandle (Window* newOwningWindowHandle);

		/**
		  Looks at the top of the MouseIconOverrides stack to see if a new icon needs to be applied or not.
		 */
		virtual void EvaluateMouseIconOverrides ();

		/**
		  Looks at the top of the ClampPositions stack to see if a new mouse region limit needs to be applied or not.
		 */
		virtual void EvaluateClampedMousePointer ();


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		static MousePointer* GetMousePointer ();
		virtual void GetMousePosition (INT& outX, INT& outY) const;
		virtual void GetMousePosition (FLOAT& outX, FLOAT& outY) const;
		virtual Vector2 GetMousePosition () const;

		virtual bool GetMouseVisibility () const;
		virtual bool GetLocked () const;

		/**
		  Returns the size of ClampPositionsStack
		 */
		virtual INT GetClampSize () const;

		virtual Texture* GetDefaultIcon () const;

		virtual SpriteComponent* GetMouseIcon () const;
		virtual InputComponent* GetMouseInput () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Offsets the rectangle by the main window's position coordinates.
		 */
		virtual void AddWindowOffsetTo (Rectangle<INT>& outRectangle) const;

		/**
		  Refreshes the Mouse Icon's draw offset based on the current sprite's texture.
		 */
		virtual void UpdateSpriteOffset ();

		/**
		  Returns the default mouse icon size (in pixels).
		 */
		virtual Vector2 GetDefaultMouseSize () const;


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	};
}

#endif
#endif