/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputUnitTester.h
  Tests various aspects of the Input Module.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef INPUTUNITTESTER_H
#define INPUTUNITTESTER_H

#include "Input.h"

#if INCLUDE_INPUT

#ifdef DEBUG_MODE
namespace SD
{
	class InputBroadcaster;

	class InputUnitTester : public UnitTester
	{
		DECLARE_CLASS(InputUnitTester)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Broadcaster used for testing (to be destroyed when user presses Esc) */
		mutable InputBroadcaster* TestBroadcaster;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual bool RunTests (EUnitTestFlags testFlags) const override;
		virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual bool TestInputBroadcaster (EUnitTestFlags testFlags) const;
		virtual bool TestInputComponent (EUnitTestFlags testFlags) const;
		virtual bool TestMousePointer (EUnitTestFlags testFlags) const;

	};
}

#endif
#endif
#endif