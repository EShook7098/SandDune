/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputClasses.h
  Contains all header includes for the Input module.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef INPUTCLASSES_H
#define INPUTCLASSES_H

#include "Configuration.h"
#if INCLUDE_INPUT

#define MODULE_INPUT 1

#include "Input.h"
#include "InputEngineComponent.h"
#include "InputBroadcaster.h"
#include "InputComponent.h"
#include "MousePointer.h"
#include "InputWindow.h"

#ifdef DEBUG_MODE
#include "InputUnitTester.h"
#include "InputTester.h"
#include "MouseTester.h"
#include "MouseTesterRender.h"
#endif

#endif
#endif