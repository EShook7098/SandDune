/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MousePointer.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "InputClasses.h"

#if INCLUDE_INPUT

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(MousePointer, Entity)

	void MousePointer::InitProps ()
	{
		Super::InitProps();

		bContinuouslyEvalClampPos = true;
		bContinuouslyEvalMouseIcon = true;

		ShowMouseValue = 0;
		bLocked = false;
		MouseIcon = nullptr;
		MouseInput = nullptr;
		Transformation = nullptr;
		DefaultIcon = nullptr;
		OwningWindowHandle = nullptr;
	}

	void MousePointer::BeginObject ()
	{
		Super::BeginObject();

		MouseInput = InputComponent::CreateObject();
		if (!AddComponent(MouseInput))
		{
			Engine::GetEngine()->FatalError(TXT("Unable to attach input component to Mouse Pointer object."));
			return;
		}
		MouseInput->MouseMoveDelegate = SDFUNCTION_3PARAM(this, MousePointer, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
		//This should be the first interface to receive mouse input (so that mouse positions are updated first).
		InputEngineComponent::Get()->GetMainBroadcaster()->SetInputPriority(MouseInput, 1000000);

		Transformation = AbsTransformComponent::CreateObject();
		if (!VALID_OBJECT(Transformation))
		{
			Engine::GetEngine()->FatalError(TXT("Unable to attach absolute transformation component to Mouse Pointer object."));
			return;
		}
		AddComponent(Transformation);

		MouseIcon = SpriteComponent::CreateObject();
		if (!VALID_OBJECT(MouseIcon))
		{
			Engine::GetEngine()->FatalError(TXT("Unable to attach sprite component to Mouse Pointer object."));
			return;
		}

		AddComponent(MouseIcon);
		Transformation->SetPivotPoint((MouseIcon->GetSpriteSize() * 0.5f)); //Center sprite over center
		Transformation->SetBaseSize(GetDefaultMouseSize());
		MouseIcon->TransformationInterface = Transformation;
		MouseIcon->SetDrawOrder(GraphicsEngineComponent::Get()->DrawOrderUI + 90000.f); //Very high priority

		//Hide OS Mouse pointer
		//GraphicsEngineComponent::Get()->GetRenderWindow()->setMouseCursorVisible(false);
	}

	void MousePointer::Destroy ()
	{
		SetOwningWindowHandle(nullptr);

		Super::Destroy();
	}

	void MousePointer::ConditionallyEvaluateMouseIconOverride (SDFunction<bool, const sf::Event::MouseMoveEvent&> targetCallback)
	{
		if (IsIconOverrideCurrentlyViewed(targetCallback))
		{
			EvaluateMouseIconOverrides();
		}
	}

	void MousePointer::PushPositionLimit (Rectangle<INT> newRegionLimit, SDFunction<bool, const sf::Event::MouseMoveEvent&> limitCallback)
	{
		if (!limitCallback.IsBounded())
		{
			LOG1(LOG_WARNING, TXT("Cannot set a mouse limit region to (%s) without associating a callback for this region.  The role of the callback is notifiy the MousePointer when the region is no longer relevant."), newRegionLimit);
			return;
		}

		SMouseLimit newLimitEntry;
		newLimitEntry.LimitRegion = newRegionLimit;
		newLimitEntry.LimitCallback = limitCallback;
		ClampPositionsStack.push_back(newLimitEntry);

		Rectangle<INT> newRegion = newLimitEntry.LimitRegion;
		//Set the curLimit relative to the OS's borders.
		AddWindowOffsetTo(newRegion);

		OS_ClampMousePointer(newRegion);
	}

	bool MousePointer::RemovePositionLimit (SDFunction<bool, const sf::Event::MouseMoveEvent&> targetCallback)
	{
		for (unsigned int i = 0; i < ClampPositionsStack.size(); i++) //This is the reason why ClampPositionsStack is a vector instead of a stack
		{
			if (targetCallback == ClampPositionsStack.at(i).LimitCallback)
			{
				ClampPositionsStack.erase(ClampPositionsStack.begin() + i);
				if (i >= ClampPositionsStack.size())
				{
					//Last element was removed, reapply new limits
					EvaluateClampedMousePointer();
				}

				return true;
			}
		}

		return false;
	}

	void MousePointer::PushMouseIconOverride (const Texture* newIcon, SDFunction<bool, const sf::Event::MouseMoveEvent&> callbackFunction)
	{
		if (!callbackFunction.IsBounded())
		{
			LOG1(LOG_WARNING, TXT("Cannot set a mouse pointer icon (%s) without associating a callback for this override.  The role of the callback is notifiy the MousePointer when it can stop using this new icon."), newIcon->GetTextureName());
			return;
		}

		SMouseIconOverride newOverride;
		newOverride.OverrideCallback = callbackFunction;
		newOverride.MousePointerIcon = newIcon;

		MouseIconOverrideStack.push_back(newOverride);
		MouseIcon->SetSpriteTexture(newOverride.MousePointerIcon);
		UpdateSpriteOffset();
	}

	bool MousePointer::RemoveIconOverride (SDFunction<bool, const sf::Event::MouseMoveEvent&> targetCallback)
	{
		for (unsigned int i = 0; i < MouseIconOverrideStack.size(); i++) //This is the reason why MouseIconOverrideStack is a vector instead of a stack
		{
			if (targetCallback == MouseIconOverrideStack.at(i).OverrideCallback)
			{
				MouseIconOverrideStack.erase(MouseIconOverrideStack.begin() + i);
				if (i >= MouseIconOverrideStack.size())
				{
					//Last element was removed, reapply new limits
					EvaluateMouseIconOverrides();
				}

				return true;
			}
		}

		return false;
	}

	bool MousePointer::IsIconOverrideCurrentlyViewed (SDFunction<bool, const sf::Event::MouseMoveEvent&> targetCallback) const
	{
		if (MouseIconOverrideStack.size() <= 0)
		{
			return false;
		}

		return (MouseIconOverrideStack.back().OverrideCallback == targetCallback);
	}

	void MousePointer::SetDefaultIcon (Texture* newIcon)
	{
		DefaultIcon = newIcon;

		if (VALID_OBJECT(MouseIcon) && MouseIconOverrideStack.size() <= 0)
		{
			MouseIcon->SetSpriteTexture(DefaultIcon);
			UpdateSpriteOffset();
		}
	}

	void MousePointer::SetMousePosition (INT newX, INT newY)
	{
		if (bLocked)
		{
			return;
		}

		Transformation->SetAbsCoordinates(Vector2(newX.ToFLOAT(), newY.ToFLOAT()));

		if (bContinuouslyEvalClampPos)
		{
			EvaluateClampedMousePointer();
		}

		if (bContinuouslyEvalMouseIcon)
		{
			EvaluateMouseIconOverrides();
		}
	}

	void MousePointer::EvaluateMouseIconOverrides ()
	{
		if (MouseIconOverrideStack.size() > 0)
		{
			sf::Event::MouseMoveEvent newMoveEvent;
			Vector2 absCoordinates = Transformation->CalcFinalCoordinates();
			newMoveEvent.x = absCoordinates.X.ToINT().Value;
			newMoveEvent.y = absCoordinates.Y.ToINT().Value;

			SMouseIconOverride curOverride = MouseIconOverrideStack.at(MouseIconOverrideStack.size() - 1);
			if (!curOverride.OverrideCallback.IsBounded() || !curOverride.OverrideCallback(newMoveEvent))
			{
				//Remove the back element and try the next one
				MouseIconOverrideStack.erase(MouseIconOverrideStack.begin() + MouseIconOverrideStack.size() - 1);
				EvaluateMouseIconOverrides(); //Recursively check the top of the stack until a callback returns true (or when there's nothing)

				if (MouseIconOverrideStack.size() > 0)
				{
					curOverride = MouseIconOverrideStack.at(MouseIconOverrideStack.size() - 1);
					MouseIcon->SetSpriteTexture((curOverride.MousePointerIcon) ? curOverride.MousePointerIcon : DefaultIcon);
				}
				else
				{
					MouseIcon->SetSpriteTexture(DefaultIcon);
				}

				UpdateSpriteOffset();
			}
		}
	}

	void MousePointer::EvaluateClampedMousePointer ()
	{
		if (ClampPositionsStack.size() > 0)
		{
			sf::Event::MouseMoveEvent newMoveEvent;
			Vector2 absCoordinates = Transformation->CalcFinalCoordinates();
			newMoveEvent.x = absCoordinates.X.ToINT().Value;
			newMoveEvent.y = absCoordinates.Y.ToINT().Value;

			SMouseLimit curLimit = ClampPositionsStack.at(ClampPositionsStack.size() - 1);

			if (!curLimit.LimitCallback.IsBounded() || !curLimit.LimitCallback(newMoveEvent))
			{
				//Remove the back element and try the next one
				ClampPositionsStack.erase(ClampPositionsStack.begin() + ClampPositionsStack.size() - 1);
				EvaluateClampedMousePointer(); //Recursively check the top of the stack until a callback returns true (or when there's nothing)

				if (ClampPositionsStack.size() <= 0)
				{
					//Remove clamped borders
					OS_ClampMousePointer(Rectangle<INT>());
				}
				else
				{
					Rectangle<INT> curLimitRect = curLimit.LimitRegion;
					//Set the curLimit relative to the OS's borders.
					AddWindowOffsetTo(curLimitRect);

					OS_ClampMousePointer(curLimitRect);
				}
			}
		}
	}

	void MousePointer::SetMouseVisibility (bool bVisible)
	{
		ShowMouseValue = (bVisible) ? Utils::Max<INT>(ShowMouseValue - 1, 0) : ShowMouseValue + 1;

		if (VALID_OBJECT(MouseIcon))
		{
			MouseIcon->SetVisibility(GetMouseVisibility());
		}
	}

	void MousePointer::SetLocked (bool bNewLocked)
	{
		bLocked = bNewLocked;
	}

	void MousePointer::SetOwningWindowHandle (Window* newOwningWindowHandle)
	{
		//Restore window's cursor visible state
		if (OwningWindowHandle != nullptr && OwningWindowHandle->Resource != nullptr)
		{
			//Restore OS Mouse Pointer if there are no other instantiated MousePointer classes
			OwningWindowHandle->Resource->setMouseCursorVisible(true);

			for (ObjectIterator iter(GetObjectHash()); iter.SelectedObject; iter++)
			{
				if (VALID_OBJECT_CAST(iter.SelectedObject, MousePointer*) && iter.SelectedObject != this)
				{
					//Although it's not likely, there's another mouse pointer instance that is also drawing a cursor
					OwningWindowHandle->Resource->setMouseCursorVisible(false);
					break;
				}
			}
		}

		OwningWindowHandle = newOwningWindowHandle;
		if (OwningWindowHandle != nullptr && OwningWindowHandle->Resource != nullptr)
		{
			OwningWindowHandle->Resource->setMouseCursorVisible(false);
		}
	}

	MousePointer* MousePointer::GetMousePointer ()
	{
		if (InputEngineComponent::Get() != nullptr)
		{
			return InputEngineComponent::Get()->GetMouse();
		}

		return nullptr;
	}

	void MousePointer::GetMousePosition (INT& outX, INT& outY) const
	{
		const Vector2 absCoordinates = Transformation->CalcFinalCoordinates();
		outX = absCoordinates.X.ToINT();
		outY = absCoordinates.Y.ToINT();
	}

	void MousePointer::GetMousePosition (FLOAT& outX, FLOAT& outY) const
	{
		const Vector2 absCoordinates = Transformation->CalcFinalCoordinates();
		outX = absCoordinates.X;
		outY = absCoordinates.Y;
	}

	Vector2 MousePointer::GetMousePosition () const
	{
		return Transformation->CalcFinalCoordinates();
	}

	bool MousePointer::GetMouseVisibility () const
	{
		return (ShowMouseValue == 0);
	}

	bool MousePointer::GetLocked () const
	{
		return bLocked;
	}

	INT MousePointer::GetClampSize () const
	{
		return INT(static_cast<int>(ClampPositionsStack.size()));
	}

	Texture* MousePointer::GetDefaultIcon () const
	{
		return DefaultIcon;
	}

	SpriteComponent* MousePointer::GetMouseIcon () const
	{
		return MouseIcon;
	}

	InputComponent* MousePointer::GetMouseInput () const
	{
		return MouseInput;
	}

	void MousePointer::AddWindowOffsetTo (Rectangle<INT>& outRectangle) const
	{
		if (!VALID_OBJECT(OwningWindowHandle))
		{
			return;
		}

		INT windowPosX;
		INT windowPosY;
		OwningWindowHandle->GetWindowPosition(windowPosX, windowPosY);

		outRectangle.Left += windowPosX;
		outRectangle.Top += windowPosY;
		outRectangle.Right += windowPosX;
		outRectangle.Bottom += windowPosY;
	}

	void MousePointer::UpdateSpriteOffset ()
	{
		Transformation->SetPivotPoint((MouseIcon->GetSpriteSize() * 0.5f));
	}

	Vector2 MousePointer::GetDefaultMouseSize () const
	{
		return Vector2(30.f, 50.f);
	}

	void MousePointer::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		SetMousePosition(sfmlEvent.x, sfmlEvent.y);
	}
}

#endif