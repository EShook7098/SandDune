/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MouseWindow.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "InputClasses.h"

#if INCLUDE_INPUT

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(InputWindow, Window)

	void InputWindow::InitProps ()
	{
		Super::InitProps();

		Pointer = nullptr;
		Broadcaster = nullptr;
	}

	void InputWindow::BeginObject ()
	{
		Super::BeginObject();

		InitializeBroadcaster();
		InitializePointer();
	}

	void InputWindow::Destroy ()
	{
		if (VALID_OBJECT(Pointer))
		{
			Pointer->Destroy();
			Pointer = nullptr;
		}

		if (VALID_OBJECT(Broadcaster))
		{
			Broadcaster->Destroy();
			Broadcaster = nullptr;
		}

		Super::Destroy();
	}

	void InputWindow::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(Pointer)
		CLEANUP_INVALID_POINTER(Broadcaster)
	}

	MousePointer* InputWindow::GetPointer () const
	{
		return Pointer;
	}

	InputBroadcaster* InputWindow::GetBroadcaster () const
	{
		return Broadcaster;
	}

	void InputWindow::InitializeBroadcaster ()
	{
		Broadcaster = InputBroadcaster::CreateObject();
		CHECK(VALID_OBJECT(Broadcaster))

		Broadcaster->SetInputSource(this);
	}

	void InputWindow::InitializePointer ()
	{
		Pointer = MousePointer::CreateObject();
		CHECK(VALID_OBJECT(Pointer))

		Pointer->SetOwningWindowHandle(this);
		Pointer->GetMouseIcon()->RelevantRenderWindows.clear();
		Pointer->GetMouseIcon()->RelevantRenderWindows.push_back(this);

		Broadcaster->AddInputComponent(Pointer->GetMouseInput(), 1000000);
		Broadcaster->SetAssociatedMouse(Pointer);

		//Default icon should match what the main mouse pointer's icon is.
		Texture* defaultIcon = MousePointer::GetMousePointer()->GetDefaultIcon();
		Pointer->SetDefaultIcon(defaultIcon);
	}
}

#endif