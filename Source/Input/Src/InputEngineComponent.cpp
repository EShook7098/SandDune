/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputEngineComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "InputClasses.h"

#if INCLUDE_INPUT

using namespace std;

namespace SD
{
	IMPLEMENT_ENGINE_COMPONENT(InputEngineComponent)

	InputEngineComponent::InputEngineComponent () : Super()
	{
		MainBroadcaster = nullptr;
		Mouse = nullptr;
	}

	void InputEngineComponent::PreInitializeComponent ()
	{
		Super::PreInitializeComponent();

		MainBroadcaster = InputBroadcaster::CreateObject();
		CHECK(MainBroadcaster != nullptr)

		Mouse = MousePointer::CreateObject();
		CHECK(Mouse != nullptr)
	}

	void InputEngineComponent::InitializeComponent ()
	{
		Super::InitializeComponent();

		Texture* defaultIcon = TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface/CursorPointer.png"), TXT("Interface.CursorPointer"));
		if (!VALID_OBJECT(defaultIcon))
		{
			Engine::GetEngine()->FatalError(TXT("Unable to import essential texture for InputEngineComponent (Interface/CursorPointer)"));
			return;
		}

		Mouse->SetDefaultIcon(defaultIcon);
		Mouse->SetOwningWindowHandle(GraphicsEngineComponent::Get()->GetWindowHandle());
		MainBroadcaster->SetInputSource(GraphicsEngineComponent::Get()->GetWindowHandle());
		MainBroadcaster->SetAssociatedMouse(Mouse);
	}

	void InputEngineComponent::ShutdownComponent ()
	{
		Super::ShutdownComponent();

		if (VALID_OBJECT(MainBroadcaster))
		{
			MainBroadcaster->Destroy();
			MainBroadcaster = nullptr;
		}

		if (VALID_OBJECT(Mouse))
		{
			Mouse->Destroy();
			Mouse = nullptr;
		}
	}

	InputBroadcaster* InputEngineComponent::FindBroadcasterForWindow (Window* window) const
	{
		for (InputBroadcaster* curBroadcaster : InputBroadcasters)
		{
			if (curBroadcaster->GetInputSource() == window)
			{
				return curBroadcaster;
			}
		}

		return nullptr;
	}

	void InputEngineComponent::RegisterInputBroadcaster (InputBroadcaster* newBroadcaster)
	{
		InputBroadcasters.push_back(newBroadcaster);
	}

	void InputEngineComponent::UnregisterInputBroadcaster (InputBroadcaster* oldBroadcaster)
	{
		for (unsigned int i = 0; i < InputBroadcasters.size(); i++)
		{
			if (InputBroadcasters.at(i) == oldBroadcaster)
			{
				InputBroadcasters.erase(InputBroadcasters.begin() + i);
				return;
			}
		}
	}

	InputBroadcaster* InputEngineComponent::GetMainBroadcaster () const
	{
		return MainBroadcaster;
	}

	MousePointer* InputEngineComponent::GetMouse () const
	{
		return Mouse;
	}
}

#endif