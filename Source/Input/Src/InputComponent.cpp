/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "InputClasses.h"

#if INCLUDE_INPUT

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(InputComponent, EntityComponent)

	void InputComponent::InitProps ()
	{
		Super::InitProps();

		InputMessenger = nullptr;
		PendingInputMessenger = nullptr;
		InputPriority = 0;
	}

	void InputComponent::BeginObject ()
	{
		Super::BeginObject();

		//By default, this component will register to the main render window since 90% of input components will be handled there
		InputEngineComponent::Get()->GetMainBroadcaster()->AddInputComponent(this);
	}

	void InputComponent::Destroy ()
	{
		if (VALID_OBJECT(InputMessenger))
		{
			InputMessenger->RemoveInputComponent(this);
			InputMessenger = nullptr;
		}

		Super::Destroy();
	}

	void InputComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(InputMessenger)
		CLEANUP_INVALID_POINTER(PendingInputMessenger)
	}

	bool InputComponent::CaptureInput (const sf::Event& keyEvent)
	{
		if (CaptureInputDelegate.IsBounded())
		{
			return CaptureInputDelegate(keyEvent);
		}

		return false;
	}

	bool InputComponent::CaptureText (const sf::Event& keyEvent)
	{
		if (CaptureTextDelegate.IsBounded())
		{
			return CaptureTextDelegate(keyEvent);
		}

		return false;
	}

	void InputComponent::MouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		if (MouseMoveDelegate.IsBounded())
		{
			MouseMoveDelegate(mouse, sfmlEvent, deltaMove);
		}
	}

	bool InputComponent::MouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		if (MouseClickDelegate.IsBounded())
		{
			return MouseClickDelegate(mouse, sfmlEvent, eventType);
		}

		return false;
	}

	bool InputComponent::MouseWheelScroll (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
	{
		if (MouseWheelScrollDelegate.IsBounded())
		{
			return MouseWheelScrollDelegate(mouse, sfmlEvent);
		}

		return false;
	}

	void InputComponent::SetInputPriority (INT newPriority)
	{
		InputBroadcaster* broadcaster = (VALID_OBJECT(PendingInputMessenger)) ? PendingInputMessenger : InputMessenger;
		broadcaster->SetInputPriority(this, newPriority);
	}

	InputBroadcaster* InputComponent::GetInputMessenger () const
	{
		return InputMessenger;
	}

	INT InputComponent::GetInputPriority () const
	{
		return InputPriority;
	}
}

#endif