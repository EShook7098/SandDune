/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputUnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "InputClasses.h"

#if INCLUDE_INPUT

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(InputUnitTester, UnitTester)

	bool InputUnitTester::RunTests (EUnitTestFlags testFlags) const
	{
		if ((testFlags & UTF_Manual) > 0 && (testFlags & UTF_NeverFails) > 0 && (testFlags & UTF_Asynchronous) > 0)
		{
			return (TestInputBroadcaster(testFlags) && TestInputComponent(testFlags) && TestMousePointer(testFlags));
		}

		return true;
	}

	bool InputUnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
	{
		if (!Super::MetRequirements(completedTests))
		{
			return false;
		}

		//Requires File unit test
		for (unsigned int i =0; i < completedTests.size(); i++)
		{
			if (FileUnitTester::SGetDefaultObject() == completedTests.at(i))
			{
				return true;
			}
		}

		return false;
	}

	bool InputUnitTester::TestInputBroadcaster (EUnitTestFlags testFlags) const
	{
		TestBroadcaster = InputBroadcaster::CreateObject();
		TestBroadcaster->SetInputSource(GraphicsEngineComponent::Get()->GetWindowHandle());
		TestBroadcaster->SetAssociatedMouse(MousePointer::GetMousePointer());
		TestBroadcaster->bDebugKeyEvent = true;
		TestBroadcaster->bDebugTextEvent = true;
		TestBroadcaster->bDebugMouseMove = true;
		TestBroadcaster->bDebugMouseButton = true;
		TestBroadcaster->bDebugMouseWheel = true;
		return true;
	}

	bool InputUnitTester::TestInputComponent (EUnitTestFlags testFlags) const
	{
		UNITTESTER_LOG(testFlags, TXT("Begin Input Component unit test. . ."));
		InputTester* inputTester = InputTester::CreateObject(); //This object will destroy the TestBroadcaster and itself when the user presses the Esc character.
		inputTester->TestFlags = testFlags;
		inputTester->SetTestBroadcaster(TestBroadcaster);
		UNITTESTER_LOG(testFlags, TXT("This test does not automatically terminate.  Press the Escape character to end test."));

		return true;
	}

	bool InputUnitTester::TestMousePointer (EUnitTestFlags testFlags) const
	{
		//MouseTester will destroy itself when escape is pressed
		UNITTESTER_LOG(testFlags, TXT("Begin Mouse Test. . ."));
		MouseTester* mouseTester = MouseTester::CreateObject();
		mouseTester->TestFlags = testFlags;
		mouseTester->bActive = false; //Disabled for now.  The InputTester will activate this tester when it's destroyed.
		UNITTESTER_LOG(testFlags, TXT("This test does not automatically terminate.  Press the Escape character once to begin the test, and again to end it."));

		return true;
	}

}

#endif
#endif