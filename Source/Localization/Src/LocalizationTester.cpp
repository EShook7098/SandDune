/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LocalizationTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "Configuration.h"

#if INCLUDE_LOCALIZATION

#include "LocalizationClasses.h"

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(LocalizationTester, UnitTester)

	bool LocalizationTester::RunTests (EUnitTestFlags testFlags) const
	{
		if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
		{
			return (TestTextTranslator(testFlags));
		}

		return true;
	}

	bool LocalizationTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
	{
		if (!Super::MetRequirements(completedTests))
		{
			return false;
		}

		//File module testing is required
		for (unsigned int i = 0; i < completedTests.size(); i++)
		{
			if (FileUnitTester::SStaticClass()->GetDefaultObject() == completedTests.at(i))
			{
				return Super::MetRequirements(completedTests);
			}
		}

		return false;
	}

	bool LocalizationTester::TestTextTranslator (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Text Translator"));

		UNITTESTER_LOG(testFlags, TXT("The engine's localization directory is:  ") + DString(LOCALIZATION_LOCATION));
		UNITTESTER_LOG(testFlags, TXT("The localization files' file extension is:  ") + DString(LOCALIZATION_EXT));

		TextTranslator* translator = LocalizationEngineComponent::Get()->GetTranslator();
		if (!VALID_OBJECT(translator))
		{
			UnitTestError(testFlags, TXT("Text Translator test failed.  Unable to get a handle to the translator from LocalizationEngineComponent."));
			return false;
		}

		vector<DString> engAnswerKey;
		engAnswerKey.push_back(TXT("Translated to English."));
		engAnswerKey.push_back(TXT("Testing various \"Characters\" such as ; semicolon, and commas"));
		engAnswerKey.push_back(TXT("Testing multiple sections within .loc"));

		vector<DString> engArrayAnswerKey;
		engArrayAnswerKey.push_back(TXT("Value1"));
		engArrayAnswerKey.push_back(TXT("Value2"));
		engArrayAnswerKey.push_back(TXT("Value3"));

		vector<DString> xxxAnswerKey;
		xxxAnswerKey.push_back(TXT("Translated to Debugging language."));
		xxxAnswerKey.push_back(TXT("XXXXXXXX"));
		xxxAnswerKey.push_back(TXT("XXXXXXXX"));

		TextTranslator::ELanguages oldLanguage = translator->GetSelectedLanguage();
		UNITTESTER_LOG(testFlags, TXT("Setting current language to English"));
		translator->SelectLanguage(TextTranslator::English);

		DString key1 = translator->TranslateText(TXT("Key1"), TXT("UnitTests"), TXT("SectionA"));
		UNITTESTER_LOG1(testFlags, TXT("Translated Key1 to \"%s\""), key1);
		if (key1 != engAnswerKey.at(0))
		{
			UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key1 within SectionA does not match expected value \"%s\".  Instead it translated to \"%s\"."), {engAnswerKey.at(0), key1});
			return false;
		}

		DString key2 = translator->TranslateText(TXT("Key2"), TXT("UnitTests"), TXT("SectionB"));
		UNITTESTER_LOG1(testFlags, TXT("Translated Key2 to \"%s\""), key2);
		if (key2 != engAnswerKey.at(1))
		{
			UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key2 within SectionB does not match expected value \"%s\".  Instead it translated to \"%s\"."), {engAnswerKey.at(1), key2});
			return false;
		}

		DString key3 = translator->TranslateText(TXT("Key3"), TXT("UnitTests"), TXT("SectionC"));
		UNITTESTER_LOG1(testFlags, TXT("Translated Key3 to \"%s\""), key3);
		if (key3 != engAnswerKey.at(2))
		{
			UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key3 within SectionC does not match expected value \"%s\".  Instead it translated to \"%s\"."), {engAnswerKey.at(2), key3});
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Retrieving array from english localization."));
		vector<DString> engArray;
		translator->GetTranslations(TXT("ArrayKey"), engArray, TXT("UnitTests"), TXT("SectionD"));
		if (engArray.size() != engArrayAnswerKey.size())
		{
			UnitTestError(testFlags, TXT("Text translator test failed.  Translated text for ArrayKey should have returned %s results.  Instead it found %s results."), {INT(engArrayAnswerKey.size()).ToString(), INT(engArray.size()).ToString()});
			return false;
		}

		for (unsigned int i = 0; i < engArray.size(); i++)
		{
			UNITTESTER_LOG2(testFlags, TXT("    [%s] = \"%s\""), INT(i), engArray.at(i));
			if (engArray.at(i) != engArrayAnswerKey.at(i))
			{
				UnitTestError(testFlags, TXT("Text translator test failed.  Translated ArrayKey element %s within SectionD does not match the expected value \"%s\".  Instead it translated to \"%s\"."), {INT(i).ToString(), engArrayAnswerKey.at(i), engArray.at(i)});
				return false;
			}
		}

		UNITTESTER_LOG(testFlags, TXT("Setting current language to XXX"));
		translator->SelectLanguage(TextTranslator::Debugging);

		key1 = translator->TranslateText(TXT("Key1"), TXT("UnitTests"));
		UNITTESTER_LOG1(testFlags, TXT("Translated Key1 to \"%s\""), key1);
		if (key1 != xxxAnswerKey.at(0))
		{
			UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key1 does not match expected value \"%s\".  Instead it translated to \"%s\"."), {xxxAnswerKey.at(0), key1});
			return false;
		}

		key2 = translator->TranslateText(TXT("Key2"), TXT("UnitTests"));
		UNITTESTER_LOG1(testFlags, TXT("Translated Key2 to \"%s\""), key2);
		if (key2 != xxxAnswerKey.at(1))
		{
			UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key2 does not match expected value \"%s\".  Instead it translated to \"%s\"."), {xxxAnswerKey.at(1), key2});
			return false;
		}

		key3 = translator->TranslateText(TXT("Key3"), TXT("UnitTests"));
		UNITTESTER_LOG1(testFlags, TXT("Translated Key3 to \"%s\""), key3);
		if (key3 != xxxAnswerKey.at(2))
		{
			UnitTestError(testFlags, TXT("Text Translator test failed.  Translated text for Key3 does not match expected value \"%s\".  Instead it translated to \"%s\"."), {xxxAnswerKey.at(2), key3});
			return false;
		}

		vector<DString> debugArray;
		translator->GetTranslations(TXT("ArrayKey"), debugArray, TXT("UnitTests"));
		if (debugArray.size() != engArray.size())
		{
			UnitTestError(testFlags, TXT("Text translator test failed.  The expected size for ArrayKey within debugging localization is %s.  Instead it found %s translations."), {INT(engArray.size()).ToString(), INT(debugArray.size()).ToString()});
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Translated the following text from debugging localization file. . ."));
		for (unsigned int i = 0; i < debugArray.size(); i++)
		{
			UNITTESTER_LOG2(testFlags, TXT("    [%s] = \"%s\""), INT(i), debugArray.at(i));
			if (debugArray.at(i).IsEmpty())
			{
				UnitTestError(testFlags, TXT("Text translator test failed.  The translator is expected to find some text within the debugging localization array.  Instead it found an empty entry."));
				return false;
			}

			if (debugArray.at(i) == engArray.at(i))
			{
				UnitTestError(testFlags, TXT("Text translator test failed.  The debugging array and english array are expected to contain different results.  Instead they both returned %s at index %s."), {debugArray.at(i), INT(i).ToString()});
				return false;
			}
		}

		UNITTESTER_LOG(testFlags, TXT("Restoring current language to defaults."));
		translator->SelectLanguage(oldLanguage);

		ExecuteSuccessSequence(testFlags, TXT("Text Translator"));
		return true;
	}
}

#endif
#endif