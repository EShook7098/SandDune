/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextTranslator.h
  Object responsible for tracking current selected language, and retrieve
  localized text from identifiers.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TEXTTRANSLATOR_H
#define TEXTTRANSLATOR_H

#include "Configuration.h"

#if INCLUDE_LOCALIZATION

#include "Localization.h"

/**
  Default language to select whenever this application launches (and if there's no default language to load from an ini).
  The Language should map to one of the enumerators in ELanguages.
 */
#ifndef DEFAULT_LANGUAGE
#define DEFAULT_LANGUAGE English
#endif

namespace SD
{
	class TextTranslator : public Object
	{
		DECLARE_CLASS(TextTranslator)


		/*
		=====================
		  Enums
		=====================
		*/

		enum ELanguages
		{
			None,
			English,
			Debugging
		};

		/**
		  Maps a language to an associated text (to associate config files to a specific language.
		 */
		struct SLanguageMapping
		{
		public:
			ELanguages Language;

			/* Text appearance at the end of file name to find files specific to this language.  This does not include extension. */
			DString TextSuffix;

			/* Human-readable text to language. */
			DString FriendlyName;

			SLanguageMapping ()
			{
				Language = None;
				TextSuffix = TXT("");
				FriendlyName = TXT("None");
			}
		};


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		ELanguages SelectedLanguage;

		/* Language mappings to associate config file suffixes with a specific language.  The AssociatedText should be unique to avoid shared configs. */
		std::vector<SLanguageMapping> LanguageMappings;

		/* The default file reader to use if baseFileName is not specified in TranslateText. */
		ConfigWriter* DefaultReader;

	private:
		/* Cached index to LanguageMapping affiliated with the SelectedLanguage. */
		unsigned int SelectedLanguageMappingIdx;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		static TextTranslator* GetTranslator ();

		/**
		  Finds an associated text within the specified section of specified file based on selected language.
		  localizationKey is the text to search for in the localization file to find associated translated text.
		  baseFileName is the name of the file without the language suffix and file extension.  If it's not specified, then it'll default to PROJECT_NAME-LANG.ini file.
		  sectionName is an optional variable to help narrow down where localizationKey is found.  If not specified, then it'll search the entire file.
		 */
		virtual DString TranslateText (const DString& localizationKey, const DString& baseFileName = TXT(""), const DString& sectionName = TXT("")) const;

		/**
		  Returns all translations of the given localizationKey.  This is needed to translate words with multiple meanings.
		  For example:  String to bool could return true for text like "True", "Yes", or "On".
		 */
		virtual void GetTranslations (const DString& localizationKey, std::vector<DString>& outTranslations, const DString& baseFileName = TXT(""), const DString& sectionName = TXT("")) const;

		virtual void SelectLanguage (ELanguages newLanguage);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		ELanguages GetSelectedLanguage () const;
		DString GetSelectedLanguageName () const;
		void GetLanguageMappings (std::vector<SLanguageMapping>& outLanguageMappings) const;
		SLanguageMapping GetSelectedLanguageData () const;
		const ConfigWriter* GetDefaultReader () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Adds file location before the file name, and appends the language suffix plus file extension.
		 */
		virtual DString GetFullFileFormat (const DString& baseFileName) const;

		/**
		  Initializes selected language variable.  Defaults to DEFAULT_LANGUAGE.  This is the function
		  to override when loading a selected language from an ini saved from an earlier session.
		 */
		virtual void SelectDefaultLanguage ();

		/**
		  Obtains a file handle based on the specified name.  If none, then it'll return DefaultReader.
		  Note:  This function will create a ConfigWriter instance if it's not the DefaultReader.
		 */
		virtual ConfigWriter* GetFileReader (const DString& fullFileName) const;
	};
}

#endif
#endif