/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Localization.h
  Contains important file includes and definitions for the Localization module.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef LOCALIZATION_H
#define LOCALIZATION_H

#include "Configuration.h"
#if INCLUDE_LOCALIZATION

#if !(INCLUDE_CORE)
#error The Localization module requires the Core module.  Please enable INCLUDE_CORE in Configuration.h.
#endif

#if !(INCLUDE_FILE)
#error The Localization module requires the File module.  Please enable INCLUDE_FILE in Configuration.h.
#endif

//Include required modules
#include "CoreClasses.h"
#include "FileClasses.h"

/* Directory location relative to base directory where Localization files may be located. */
#define LOCALIZATION_LOCATION "Localization"

/* LocalizationExt is the file extension all localization files use.  Although they're the same as config files,
they use a different extension to notify the user that this is not meant for edits unless they plan to mod the system. */
#define LOCALIZATION_EXT ".loc"

#define LOG_LOCALIZATION "Localization"

#endif
#endif