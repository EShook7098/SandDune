/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EngineIntegrityTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(EngineIntegrityTester, Object)

	void EngineIntegrityTester::Destroy ()
	{
		//At least try to remove left over objects if one of the tests failed
		for (unsigned int i = 0; i < InstantiatedObjects.size(); i++)
		{
			InstantiatedObjects.at(i)->Destroy();
		}

		Super::Destroy();
	}

	bool EngineIntegrityTester::TestClassIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
	{
		tester->BeginTestSequence(testFlags, TXT("Class Iterator"));

		//Populate classes that should be found
		vector<const DClass*> requiredObjectClasses;
		vector<const DClass*> requiredEngineClasses;
		requiredObjectClasses.push_back(Object::SStaticClass());
		requiredObjectClasses.push_back(Entity::SStaticClass());
		requiredObjectClasses.push_back(EntityComponent::SStaticClass());
		requiredObjectClasses.push_back(DatatypeUnitTester::SStaticClass());
		requiredObjectClasses.push_back(EngineIntegrityUnitTester::SStaticClass());
		requiredObjectClasses.push_back(UnitTester::SStaticClass());
		requiredObjectClasses.push_back(Utils::SStaticClass());
		requiredEngineClasses.push_back(EngineComponent::SStaticClass());

		UNITTESTER_LOG(testFlags, TXT("Launching object class iterator."));
		INT classCounter = 0;
		for (ClassIterator iter; iter.SelectedClass; iter++)
		{
			classCounter++;
			UNITTESTER_LOG(testFlags, TXT("Class Iterator found class:  ") + iter.SelectedClass->Name());
			if (iter.SelectedClass->ClassType != DClass::CT_Object)
			{
				tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The object class iterator found a class that's not an object named %s."), {iter.SelectedClass->Name()});
				return false;
			}

			const Object* curObject = static_cast<const Object*>(iter.SelectedClass->GetDefaultObject());
			if (VALID_OBJECT(curObject))
			{
				UNITTESTER_LOG(testFlags, TXT("    Default object is:  ") + curObject->GetName());
			}

			for (unsigned int i = 0; i < requiredObjectClasses.size(); i++)
			{
				if (requiredObjectClasses.at(i) == iter.SelectedClass)
				{
					requiredObjectClasses.erase(requiredObjectClasses.begin() + i);
					break;
				}
			}
		}

		UNITTESTER_LOG1(testFlags, TXT("Total number of found classes:  %s."), classCounter);
		if (classCounter <= 0)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed!  Class iterator was unable to find any registered classes."));
			return false;
		}

		if (requiredObjectClasses.size() > 0)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed!  The iterator failed to find some of the expected object classes.  See the log for a list of classes that were not found:"));
			for (unsigned int i = 0; i < requiredObjectClasses.size(); i++)
			{
				UNITTESTER_LOG1(testFlags, TXT("    [Unit Test failure - Missing class] %s"), requiredObjectClasses.at(i)->Name());
			}

			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Launching engine component class iterator."));
		INT numEngineClasses = 0;
		for (ClassIterator iter(EngineComponent::SStaticClass()); iter.SelectedClass; iter++)
		{
			UNITTESTER_LOG(testFlags, TXT("Engine class Iterator found class:  ") + iter.SelectedClass->Name());
			numEngineClasses++;
			if (iter.SelectedClass->ClassType != DClass::CT_EngineComponent)
			{
				tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The engine component class iterator found a class that's not an engine component named %s."), {iter.SelectedClass->Name()});
				return false;
			}

			const EngineComponent* cdo = iter.SelectedClass->GetDefaultEngineComponent();
			if (cdo != nullptr)
			{
				UNITTESTER_LOG(testFlags, TXT("    Default engine component is:  ") + cdo->GetName());
			}

			for (unsigned int i = 0; i < requiredEngineClasses.size(); i++)
			{
				if (requiredEngineClasses.at(i) == iter.SelectedClass)
				{
					requiredEngineClasses.erase(requiredEngineClasses.begin() + i);
					break;
				}
			}
		}

		if (requiredEngineClasses.size() > 0)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed!  The iterator failed to find some of the expected engine component classes.  See the logs for a list of classes that were not found."));
			for (unsigned int i = 0; i < requiredEngineClasses.size(); i++)
			{
				UNITTESTER_LOG1(testFlags, TXT("    [Unit test failure - missing class] %s"), requiredEngineClasses.at(i)->Name());
			}

			return false;
		}

		tester->ExecuteSuccessSequence(testFlags, TXT("Class Iterator"));
		return true;
	}

	bool EngineIntegrityTester::TestObjectInstantiation (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
	{
		tester->BeginTestSequence(testFlags, TXT("Object Instantiation"));

		//Count how many objects use to exist
		TotalObjectsBeforeTest = 0;
		for (ObjectIterator iter; iter.SelectedObject; iter++)
		{
			TotalObjectsBeforeTest++;
		}

		if (tester->ShouldHaveDebugLogs(testFlags))
		{
			Engine::GetEngine()->LogObjectHashTable();
			UNITTESTER_LOG1(testFlags, TXT("There are a total of %s objects before this unit test created an object."), TotalObjectsBeforeTest);
		}

		for (ClassIterator iter; iter.SelectedClass; iter++)
		{
			const Object* defaultObject = (iter.SelectedClass->GetDefaultObject());
			if (defaultObject != nullptr)
			{
				Object* newObject = defaultObject->CreateObjectOfMatchingClass();

				if (VALID_OBJECT(newObject))
				{
					UNITTESTER_LOG1(testFlags, TXT("Created a new object:  %s"), newObject->GetUniqueName());
					InstantiatedObjects.push_back(newObject);
				}
			}
		}

		UNITTESTER_LOG1(testFlags, TXT("Total number of instantiated objects:  %s."), INT(static_cast<int>(InstantiatedObjects.size())));
		if (InstantiatedObjects.size() <= 0)
		{
			tester->UnitTestError(testFlags, TXT("Object instantiation test failed!  Unit tester was unable to create an object."));
			return false;
		}

		if (tester->ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("Displaying Engine's hash table. . ."));
			Engine::GetEngine()->LogObjectHashTable();
		}

		tester->ExecuteSuccessSequence(testFlags, TXT("Object Instantiation"));
		return true;
	}

	bool EngineIntegrityTester::TestObjectIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
	{
		tester->BeginTestSequence(testFlags, TXT("Object Iterator"));

		INT numFoundObjects;
		for (ObjectIterator iter; iter.SelectedObject; iter++)
		{
			UNITTESTER_LOG1(testFlags, TXT("Object iterator found %s"), iter.SelectedObject->GetUniqueName());
			numFoundObjects++;
		}

		UNITTESTER_LOG1(testFlags, TXT("Total number of found objects:  %s"), numFoundObjects);
		if (numFoundObjects.ToUnsignedInt() < InstantiatedObjects.size())
		{
			tester->UnitTestError(testFlags, TXT("Object iterator test failed!  The iterator is expected to find at least %s objects.  It only found %s."), {INT(static_cast<int>(InstantiatedObjects.size())).ToString(), numFoundObjects.ToString()});
			return false;
		}

		tester->ExecuteSuccessSequence(testFlags, TXT("Object Iterator"));
		return true;
	}

	bool EngineIntegrityTester::TestPropertyIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
	{
		tester->BeginTestSequence(testFlags, TXT("Property Iterator"));

		//Initialize variables to test if iterator can retrieve values.
		PropIterINT = 105;
		PropIterFLOAT = 208.f;
		PropIterString = TXT("Random value to string.");
		PropIterVector = Vector3(10.f, 30.f, 91.f);
		UNITTESTER_LOG4(testFlags, TXT("Initialized member variables to:  (INT) %s, (FLOAT) %s, (DString) \"%s\", (Vector3) %s"), PropIterINT, PropIterFLOAT, PropIterString, PropIterVector);

		INT originalPropINT = PropIterINT;
		FLOAT originalPropFLOAT = PropIterFLOAT;
		DString originalPropString = PropIterString;
		Vector3 originalPropVector = PropIterVector;

		bool bFoundINTExt = false;
		bool bFoundFLOATExt = false;
		bool bFoundStringExt = false;
		bool bFoundVectorExt = false;

		//Add property extensions to enable property iterator to find these variables
		DPropertyExtension* intExtension = new DPropertyExtension();
		intExtension->BindPropertyToObject(this);
		intExtension->SetOwningProperty(&PropIterINT);

		DPropertyExtension* floatExtension = new DPropertyExtension();
		floatExtension->BindPropertyToObject(this);
		floatExtension->SetOwningProperty(&PropIterFLOAT);

		DPropertyExtension* stringExtension = new DPropertyExtension();
		stringExtension->BindPropertyToObject(this);
		stringExtension->SetOwningProperty(&PropIterString);

		DPropertyExtension* vectorExtension = new DPropertyExtension();
		vectorExtension->BindPropertyToObject(this);
		vectorExtension->SetOwningProperty(&PropIterVector);
		UNITTESTER_LOG(testFlags, TXT("Attached property extensions to those four member variables.  Launching property iterator to see if it can find all four variables."));

		for (PropertyIterator<DPropertyExtension> iter(this); iter.GetSelectedProperty() != nullptr; iter++)
		{
			DPropertyExtension* curProp = iter.GetSelectedProperty();
			if (curProp->GetOwningProperty() == &PropIterINT)
			{
				if (bFoundINTExt)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found PropIterINT variable more than once."));
					return false;
				}

				INT* curINT = dynamic_cast<INT*>(curProp->GetOwningProperty());
				if (curINT == nullptr)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterINT, but it failed to dynamically cast the property as an INT."));
					return false;
				}

				if (curINT->Value != originalPropINT.Value)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterINT, but the value is different from the original INT value.  It should have been equal to %s.  Instead it's currently %s."), {originalPropINT.ToString(), PropIterINT.ToString()});
					return false;
				}

				UNITTESTER_LOG1(testFlags, TXT("Property iterator found INT variable:  %s"), (*curINT));
				bFoundINTExt = true;
			}
			else if (curProp->GetOwningProperty() == &PropIterFLOAT)
			{
				if (bFoundFLOATExt)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found PropIterFLOAT variable more than once."));
					return false;
				}

				FLOAT* curFLOAT = dynamic_cast<FLOAT*>(curProp->GetOwningProperty());
				if (curFLOAT == nullptr)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterFLOAT, but it failed to dynamically cast the property as a FLOAT."));
					return false;
				}

				if (curFLOAT->Value != originalPropFLOAT.Value)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterFLOAT, but the value is different from the original FLOAT value.  It should have been equal to %s.  Instead it's currently %s."), {originalPropFLOAT.ToString(), PropIterFLOAT.ToString()});
					return false;
				}

				UNITTESTER_LOG1(testFlags, TXT("Property iterator found FLOAT variable:  %s"), (*curFLOAT));
				bFoundFLOATExt = true;
			}
			else if (curProp->GetOwningProperty() == &PropIterString)
			{
				if (bFoundStringExt)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found PropIterString variable more than once."));
					return false;
				}

				DString* curString = dynamic_cast<DString*>(curProp->GetOwningProperty());
				if (curString == nullptr)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterString, but it failed to dynamically cast the property as a String."));
					return false;
				}

				if (*curString != originalPropString)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterString, but the value is different from the original String value.  It should have been equal to \"%s\".  Instead it's currently \"%s\"."), {originalPropString.ToString(), PropIterString.ToString()});
					return false;
				}

				UNITTESTER_LOG1(testFlags, TXT("Property iterator found DString variable:  \"%s\""), (*curString));
				bFoundStringExt = true;
			}
			else if (curProp->GetOwningProperty() == &PropIterVector)
			{
				if (bFoundVectorExt)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found PropIterVector variable more than once."));
					return false;
				}

				Vector3* curVector = dynamic_cast<Vector3*>(curProp->GetOwningProperty());
				if (curVector == nullptr)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterVector, but it failed to dynamically cast the property as a Vector3."));
					return false;
				}

				if (*curVector != originalPropVector)
				{
					tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterVector, but the values are different from the original Vector3 values.  It should have been equal to %s.  Instead it's currently %s."), {originalPropVector.ToString(), PropIterVector.ToString()});
					return false;
				}

				UNITTESTER_LOG1(testFlags, TXT("Property iterator found Vector3 variable:  %s"), (*curVector));
				bFoundVectorExt = true;
			}
		}

		UNITTESTER_LOG(testFlags, TXT("Property iterator finished iterating through properties."));
		if (!bFoundINTExt)
		{
			tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator failed to find the INT extension."));
			return false;
		}

		if (!bFoundFLOATExt)
		{
			tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator failed to find the FLOAT extension."));
			return false;
		}

		if (!bFoundStringExt)
		{
			tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator failed to find the DString extension."));
			return false;
		}

		if (!bFoundVectorExt)
		{
			tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator failed to find the Vector3 extension."));
			return false;
		}

		tester->ExecuteSuccessSequence(testFlags, TXT("Property Iterator"));
		return true;
	}

	bool EngineIntegrityTester::TestCleanUp (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
	{
		tester->BeginTestSequence(testFlags, TXT("Object Cleanup"));

		for (unsigned int i = 0; i < InstantiatedObjects.size(); i++)
		{
			UNITTESTER_LOG1(testFlags, TXT("Destroying object:  %s"), InstantiatedObjects.at(i)->GetUniqueName());
			InstantiatedObjects.at(i)->Destroy();
		}

		//Force garbage collector for immediate updates to the hash table
		Engine::GetEngine()->CollectGarbage();

		if (tester->ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("Displaying Engine's hash table. . ."));
			Engine::GetEngine()->LogObjectHashTable();
		}
		InstantiatedObjects.clear();

		//Count all objects to see if there were any left remaining
		INT totalNumObjects = 0;
		for (ObjectIterator iter; iter.SelectedObject; iter++)
		{
			totalNumObjects++;
		}
		UNITTESTER_LOG1(testFlags, TXT("There are now a total of %s objects after unit test clean up."), totalNumObjects);

		if (TotalObjectsBeforeTest != totalNumObjects)
		{
			tester->UnitTestError(testFlags, TXT("The total number of objects before test (%s) differs from total number of objects after test (%s)!"), {TotalObjectsBeforeTest.ToString(), totalNumObjects.ToString()});
			UNITTESTER_LOG2(testFlags, TXT("The total number of objects before test (%s) differs from total number of objects after test (%s)!"), TotalObjectsBeforeTest, totalNumObjects);
			UNITTESTER_LOG(testFlags, TXT("Created objects that automatically spawn other objects should also clean up after themselves."));
			return false;
		}

		tester->ExecuteSuccessSequence(testFlags, TXT("Object Cleanup"));
		return true;
	}

	bool EngineIntegrityTester::TestComponents (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
	{
		tester->BeginTestSequence(testFlags, TXT("Component"));

		UNITTESTER_LOG(testFlags, TXT("Creating an entity, and attaching a Tick Component and attaching a Life Span component to the tick component."));
		Entity* testEntity = Entity::CreateObject();
		TickComponent* testTick = TickComponent::CreateObject();
		LifeSpanComponent* testLifeSpan = LifeSpanComponent::CreateObject();
		if (!testEntity->AddComponent(testTick) || !testTick->AddComponent(testLifeSpan))
		{
			tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to attach either Tick Component to entity or Life Span Component to Tick Component."));
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Attempting to retrieve components from Entity"));
		testTick = dynamic_cast<TickComponent*>(testEntity->FindSubComponent(Engine::GetEngine()->GetTickComponentHashNumber()));
		if (!VALID_OBJECT(testTick))
		{
			tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to find Tick Component attached to %s."), {testEntity->ToString()});
			return false;
		}

		testLifeSpan = dynamic_cast<LifeSpanComponent*>(testEntity->FindSubComponent(LifeSpanComponent::SStaticClass(), true));
		if (!VALID_OBJECT(testLifeSpan))
		{
			tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to find Life Span Component from %s."), {testEntity->ToString()});
			return false;
		}

		/*
		Entity
			Tick0
				LifeSpan0
					tick [inherited from LifeSpan]
					tick3
			tick1
			tick2
		*/

		UNITTESTER_LOG(testFlags, TXT("Attaching two more Tick Components to owning entity, and another Tick component to the life span component."));
		TickComponent* tick1 = TickComponent::CreateObject();
		TickComponent* tick2 = TickComponent::CreateObject();
		TickComponent* tick3 = TickComponent::CreateObject();
		if (!testEntity->AddComponent(tick1) || !testEntity->AddComponent(tick2) || !testLifeSpan->AddComponent(tick3))
		{
			tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to attach multiple tick components to the test entity."));
			return false;
		}
		vector<EntityComponent*> componentsToFind;
		componentsToFind.push_back(testTick);
		componentsToFind.push_back(testLifeSpan);
		componentsToFind.push_back(tick1);
		componentsToFind.push_back(tick2);
		componentsToFind.push_back(tick3);

		for (ComponentIterator iter(testEntity, true); iter.GetSelectedComponent() != nullptr; iter++)
		{
			for (unsigned int i = 0; i < componentsToFind.size(); i++)
			{
				if (componentsToFind.at(i) == iter.GetSelectedComponent())
				{
					componentsToFind.erase(componentsToFind.begin() + i);
					break;
				}
			}
		}

		if (componentsToFind.size() > 0)
		{
			tester->UnitTestError(testFlags, TXT("Components test failed.  The component iterator was not able to find all components attached to %s."), {testEntity->GetUniqueName()});
			UNITTESTER_LOG(testFlags, TXT("The following components were not found. . ."));
			for (unsigned int i = 0; i < componentsToFind.size(); i++)
			{
				UNITTESTER_LOG2(testFlags, TXT("    [%s] = %s"), INT(i), componentsToFind.at(i)->GetUniqueName());
			}
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Detaching components from their owners."));
		testEntity->RemoveComponent(testTick);
		testEntity->RemoveComponent(tick1);
		testEntity->RemoveComponent(tick3);
		testEntity->RemoveComponent(tick2); //out of order is intended

		UNITTESTER_LOG1(testFlags, TXT("Component test passed!  Was able to find all Tick and Life Span components from %s."), testEntity->ToString());
		testEntity->Destroy();
		testTick->Destroy();
		tick1->Destroy();
		tick2->Destroy();
		tick3->Destroy();

		tester->ExecuteSuccessSequence(testFlags, TXT("Component"));
		return true;
	}
}

#endif
#endif