/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DString.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	DString::DString ()
	{
		String = TXT("");
	}

	DString::DString (const TCHAR* inText)
	{
		String = TString(inText);
	}

	DString::DString (const DString& inString)
	{
		String = inString.String;
	}

	DString::DString (const TString& inString)
	{
		String = inString;
	}

	DString::DString (const sf::String& inString)
	{
#if USE_WIDE_STRINGS
		String = inString.toWideString();
#elif USE_UTF32
		String = static_cast<u32string>(inString.toUtf32());
#elif USE_UTF16
		String = inString.toUtf16();
#elif USE_UTF8
		String = inString; //TODO:  Research subject on convert string encodings
		//String = inString.toUtf8();
#else
		String = toAnsiString();
#endif
	}

	DString::DString (const unsigned int num)
	{
		String = to_string(num);
	}

	DString::DString (const int num)
	{
		String = to_string(num);
	}

	DString::DString (const float num)
	{
		String = to_string(num);
	}

	DString::~DString ()
	{
	
	}

	void DString::operator= (const DString& inString)
	{
		String = inString.String;
	}

	void DString::operator= (const TCHAR* inStr)
	{
		String = TString(inStr);
	}

	DString DString::ToString () const
	{
		return DString(String);
	}

	int DString::Compare (const DString& otherString, bool bCaseSensitive) const
	{
		if (!bCaseSensitive)
		{
			//Create copies of strings to adjust casing
			DString localCopy = String;
			localCopy.ToLower();
			DString localOther = otherString;
			localOther.ToLower();

			return localCopy.String.compare(localOther.String);
		}

		return String.compare(otherString.String);
	}

	INT DString::FindSubText (const DString& search, INT startPos, bool bCaseSensitive) const
	{
#if SAFE_STRINGS
		if (search.IsEmpty())
		{
			LOG1(LOG_WARNING, TXT("Search query for %s is empty.  Cannot find sub text."), ToString());
			return 0;
		}
#endif
		if (!bCaseSensitive)
		{
			DString localCopy = String;
			localCopy.ToLower();
			DString localSearch = search;
			localSearch.ToLower();

			return localCopy.FindSubText(localSearch, startPos, true);
		}

#if SAFE_STRINGS
		startPos = Utils::Clamp<INT>(startPos, 0, Length());
#endif
		return static_cast<int>(String.find(search.String, startPos.Value));
	}

	void DString::SplitString (INT splitIndex, DString& outFirstSegment, DString& outSecondSegment) const
	{
#if SAFE_STRINGS
		splitIndex = Utils::Clamp<INT>(splitIndex, 0, Length() - 1);
#endif

		outFirstSegment = String.substr(0, splitIndex.Value);
		outSecondSegment = String.substr(splitIndex.Value + 1);
	}

	void DString::ParseString (TCHAR delimiter, vector<DString>& outSegments) const
	{
		outSegments.clear();

		INT startIdx = 0;
		for (unsigned int i = 0; i < String.size(); i++)
		{
			if (String.at(i) == delimiter)
			{
				DString newEntry = SubString(startIdx, i - 1); //Minus 1 to not include the delimiter
				outSegments.push_back(newEntry);

				//skip over delimiter
				startIdx = i + 1;
			}
		}

		//Push the last segment
		outSegments.push_back(SubString(startIdx));
	}

	DString DString::SubString (INT startIdx, INT endIdx) const
	{
		if (startIdx > endIdx)
		{
			//return the rest of the string after startIdx
			endIdx = Length() - 1;
		}

#if SAFE_STRINGS
		startIdx = Utils::Max<INT>(startIdx, 0);
		endIdx = Utils::Clamp<INT>(endIdx, startIdx, Length() - 1);

		if (startIdx >= Length())
		{
			LOG2(LOG_WARNING, TXT("Invalid SubString range.  The starting index (%s) is beyond the string length:  %s"), startIdx, DString(String));
			return TXT("");
		}
#endif

		//+1 to include endIdx
		return String.substr(startIdx.Value, 1 + (endIdx - startIdx).Value);
	}

	DString DString::SubStringCount (INT startIdx, INT count) const
	{
#if SAFE_STRINGS
		startIdx = Utils::Clamp<INT>(startIdx, 0, Length());
		count = Utils::Clamp<INT>(count, 0, Length() - startIdx);
#endif

		return String.substr(startIdx.Value, count.Value);
	}

	DString DString::Left (INT count) const
	{
#if SAFE_STRINGS
		count = Utils::Min(count, Length());
#endif
		return String.substr(0, count.Value);
	}

	DString DString::Right (INT count) const
	{
#if SAFE_STRINGS
		count = Utils::Min(count, Length());
#endif
		return String.substr((Length() - count).Value); 
	}

	INT DString::Length () const
	{
		return static_cast<int>(String.length());
	}

	bool DString::IsEmpty () const
	{
		return String.empty();
	}

	void DString::Clear()
	{
		String.clear();
	}

	void DString::Insert (INT idx, const TCHAR character)
	{
#if SAFE_STRINGS
		idx = Utils::Clamp<INT>(idx, 0, Length() - 1);
#endif
		String.insert(String.begin() + idx.Value, character);
	}

	void DString::Insert (INT idx, DString text)
	{
#if SAFE_STRINGS
		idx = Utils::Clamp<INT>(idx, 0, Length());
#endif
		String.insert(idx.Value, text.String);
	}

	void DString::Remove (INT idx, INT count)
	{
#if SAFE_STRINGS
		idx = Utils::Clamp<INT>(idx, 0, Length() - 1);
		count = Utils::Clamp<INT>(count, 1, Length() - idx);
#endif
		String.erase(idx.Value, count.Value);
	}

	void DString::StrReplace (const DString& searchFor, const DString& replaceWith, bool bCaseSensitive)
	{
		INT startIdx = FindSubText(searchFor, 0, bCaseSensitive);
		while (startIdx >= 0)
		{
			String.replace(startIdx.Value, searchFor.Length().Value, replaceWith.String);
			startIdx += replaceWith.Length();
			startIdx = FindSubText(searchFor, startIdx, bCaseSensitive);
		}
	}

	DString DString::FormatString (DString text, const vector<DString>& values)
	{
		INT searchIdx = 0;
		for (unsigned int valueIdx = 0; valueIdx < values.size(); valueIdx++)
		{
			INT macroPos = text.FindSubText(TXT("%s"), searchIdx);
			if (macroPos == INT_INDEX_NONE)
			{
				return text; //Ran out of %s macros
			}

			searchIdx = macroPos + values.at(valueIdx).Length();

			//Remove the '%s' text
			text.Remove(macroPos, 2);
			
			//Insert the content from values
			text.Insert(macroPos, values.at(valueIdx));
		}

		return text;
	}

	const TCHAR* DString::ToCString () const
	{
		return String.c_str();
	}

	const TCHAR DString::At (INT idx) const
	{
#if SAFE_STRINGS
		idx = Utils::Clamp<INT>(idx, 0, Length() - 1);
#endif
		return String.at(idx.Value);
	}

#if !INCLUDE_LOCALIZATION
	bool DString::ToBool () const
	{
		vector<DString> stringsToCmp;
		stringsToCmp.push_back(TXT("true"));
		stringsToCmp.push_back(TXT("1"));
		stringsToCmp.push_back(TXT("yes"));

		for (unsigned int i = 0; i < stringsToCmp.size(); i++)
		{
			if (Compare(stringsToCmp.at(i), false) == 0)
			{
				return true;
			}
		}

		return false;
	}
#endif

	void DString::ToUpper ()
	{
		transform(String.begin(), String.end(), String.begin(), toupper);
	}

	void DString::ToLower ()
	{
		transform(String.begin(), String.end(), String.begin(), tolower);
	}

	void DString::TrimSpaces ()
	{
		if (IsEmpty())
		{
			return;
		}

		size_t firstCharIdx = String.find_first_not_of(TEXT(' '));
		if (firstCharIdx == string::npos)
		{
			//String is all spaces
			Clear();
			return;
		}

		size_t lastCharIdx = String.find_last_not_of(TEXT(' '));
		String = String.substr(firstCharIdx, lastCharIdx - firstCharIdx + 1);
	}

	INT DString::FindFirstMismatchingIdx (const DString& compare, bool bCaseSensitive) const
	{
		if (!bCaseSensitive)
		{
			DString insensitiveString(String);
			insensitiveString.ToUpper();

			DString insensitiveSearch(compare);
			insensitiveSearch.ToUpper();
			return insensitiveString.FindFirstMismatchingIdx(insensitiveSearch, true);
		}

		for (INT i = 0; i < Length(); i++)
		{
			if (i >= compare.Length())
			{
				return i; //Compare is too short
			}

			if (At(i) != compare.At(i))
			{
				return i;
			}
		}

		if (compare.Length() > Length())
		{
			return Length(); //Index that's one character beyond length of this string
		}

		return INT_INDEX_NONE; //identical strings
	}

	wstring DString::ToWStr () const
	{
#if USE_WIDE_STRINGS
		return String; //already a wstring
#else
		//Convert string to wstring
		return wstring(String.begin(), String.end());
#endif
	}

	string DString::ToStr () const
	{
#if USE_WIDE_STRINGS
		//Convert wstring to string
		return string(String.begin(), String.end());
#else
		return String; //already a string
#endif
	}

#pragma region "External Operators"
	bool operator== (const DString& left, const DString& right)
	{
		return (left.Compare(right, true) == 0);
	}

	bool operator== (const DString& left, const TString& right)
	{
		return (left.Compare(DString(right), true) == 0);
	}

	bool operator== (const TString& left, const DString& right)
	{
		return (right.Compare(DString(left), true) == 0);
	}

	bool operator== (const DString& left, const TCHAR* right)
	{
		return (left.Compare(DString(right), true) == 0);
	}

	bool operator== (const TCHAR* left, const DString& right)
	{
		return (right.Compare(DString(left), true) == 0);
	}

	bool operator!= (const DString& left, const DString& right)
	{
		return (left.Compare(right, true) != 0);
	}

	bool operator!= (const DString& left, const TString& right)
	{
		return (left.Compare(DString(right), true) != 0);
	}

	bool operator!= (const TString& left, const DString& right)
	{
		return (right.Compare(DString(left), true) != 0);
	}

	bool operator!= (const DString& left, const TCHAR* right)
	{
		return (left.Compare(DString(right), true) != 0);
	}

	bool operator!= (const TCHAR* left, const DString& right)
	{
		return (right.Compare(DString(left), true) != 0);
	}

	DString operator+ (const DString& left, const DString& right)
	{
		return DString(left.String + right.String);
	}

	DString operator+ (const DString& left, const TString& right)
	{
		return DString(left.String + right);
	}

	DString operator+ (const TString& left, const DString& right)
	{
		return DString(left + right.String);
	}

	DString operator+ (const DString& left, const TCHAR* right)
	{
		return DString(left.String + right);
	}

	DString operator+ (const TCHAR* left, const DString& right)
	{
		return DString(left + right.String);
	}

	DString& operator+= (DString& left, const DString& right)
	{
		left.String += right.String;
		return left;
	}

	DString& operator+= (DString& left, const TString& right)
	{
		left.String += right;
		return left;
	}

	TString& operator+= (TString& left, const DString& right)
	{
		left += right.String;
		return left;
	}

	DString& operator+= (DString& left, const TCHAR* right)
	{
		left.String += right;
		return left;
	}
#pragma endregion
}

#endif