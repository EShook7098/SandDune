/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Vector2.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	Vector2::Vector2 ()
	{
		X = 0;
		Y = 0;
	}

	Vector2::Vector2 (const float x, const float y)
	{
		X = x;
		Y = y;
	}

	Vector2::Vector2 (const FLOAT x, const FLOAT y)
	{
		X = x;
		Y = y;
	}

	Vector2::Vector2 (const Vector2& copyVector)
	{
		X = copyVector.X;
		Y = copyVector.Y;
	}

	void Vector2::operator= (const Vector2& copyVector)
	{
		X = copyVector.X;
		Y = copyVector.Y;
	}

	DString Vector2::ToString () const
	{
		return (TXT("(") + X.ToString() + TXT(",") + Y.ToString() + TXT(")"));
	}

	Vector2 Vector2::SFMLtoSD (const sf::Vector2f sfVector)
	{
		return Vector2(sfVector.x, sfVector.y);
	}

	sf::Vector2f Vector2::SDtoSFML (const Vector2 sdVector)
	{
		return sf::Vector2f(sdVector.X.Value, sdVector.Y.Value);
	}

	FLOAT Vector2::VSize () const
	{
		return (sqrt(pow(X.Value, 2) + pow(Y.Value, 2)));
	}

	void Vector2::SetLengthTo (FLOAT targetLength)
	{
		Normalize();

		X *= targetLength;
		Y *= targetLength;
	}

	FLOAT Vector2::Dot (const Vector2& otherVector) const
	{
		return ((X * otherVector.X) + (Y * otherVector.Y)).Value;
	}

	void Vector2::Normalize ()
	{
		FLOAT magnitude = VSize();

		if (magnitude == 0)
		{
#ifdef DEBUG_MODE
			LOG(LOG_WARNING, TXT("Attempting to normalize a void 2D vector!"));
#endif
			return;
		}

		X /= magnitude;
		Y /= magnitude;
	}

	bool Vector2::IsUniformed () const
	{
		return (X == Y);
	}

	bool Vector2::IsEmpty () const
	{
		return (X == 0.f && Y == 0.f);
	}

	Vector3 Vector2::ToVector3 () const
	{
		return Vector3(X, Y, 0);
	}

#pragma region "External Operators"
	bool operator== (const Vector2& left, const Vector2& right)
	{
		return (left.X == right.X && left.Y == right.Y);
	}

	bool operator!= (const Vector2& left, const Vector2& right)
	{
		return !(left == right);
	}

	Vector2 operator+ (const Vector2& left, const Vector2& right)
	{
		return Vector2(left.X + right.X, left.Y + right.Y);	
	}

	Vector2& operator+= (Vector2& left, const Vector2& right)
	{
		left.X += right.X;
		left.Y += right.Y;
		return left;
	}

	Vector2 operator- (const Vector2& left, const Vector2& right)
	{
		return Vector2(left.X - right.X, left.Y - right.Y);
	}

	Vector2& operator-= (Vector2& left, const Vector2& right)
	{
		left.X -= right.X;
		left.Y -= right.Y;
		return left;
	}

	Vector2 operator* (const Vector2& left, const Vector2& right)
	{
		return Vector2(left.X * right.X, left.Y * right.Y);
	}

	Vector2& operator*= (Vector2& left, const Vector2& right)
	{
		left.X *= right.X;
		left.Y *= right.Y;
		return left;
	}

	Vector2 operator/ (const Vector2& left, const Vector2& right)
	{
		Vector2 result(left);
		result /= right;
		return result;
	}

	Vector2& operator/= (Vector2& left, const Vector2& right)
	{
		if (right.X != 0.f)
		{
			left.X /= right.X;
		}

		if (right.Y != 0.f)
		{
			left.Y /= right.Y;
		}
		
		return left;
	}
#pragma endregion
}

#endif