/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DatatypeUnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(DatatypeUnitTester, UnitTester)

	bool DatatypeUnitTester::RunTests (EUnitTestFlags testFlags) const
	{
		if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
		{
			return (TestDString(testFlags) && TestBOOL(testFlags) && TestINT(testFlags) && TestFLOAT(testFlags) && TestRange(testFlags) && TestVector2(testFlags) &&
				TestVector3(testFlags) && TestRectangle(testFlags) && TestSDFunction(testFlags) && TestMatrix(testFlags));
		}
		
		return true;
	}

	bool DatatypeUnitTester::MetRequirements (const vector<const UnitTester*>& completedTests) const
	{
		return true;
	}

	//It's important to run the DString test first since all of the other tests depends on DStrings to work (for logging).
	bool DatatypeUnitTester::TestDString (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("DString"));

		DString blankString;
		DString initString(TXT("Initialized string"));
		DString copiedString(initString);

		//Need to test formatted string early since the logs depend on that functionality to work
		DString formattedString = DString::Format(TXT("Formatted string (%s)"), {copiedString.ToCString()});
		DString superFormattedString = DString::Format(TXT("Super Formatted string (%s), (%s), (%s), (%s)"), {TXT("CStringA"), TXT("CStringB"), TXT("CStringC"), TXT("CStringD")});

		DString formatParam1 = TXT("Some Value");
		DString formatParam2 = TXT("Insert a string with '%s' macro within");
		DString formatParam3 = TXT("");
		DString formatParam4 = TXT("(Final Value with number 7)");
		DString otherFormatString = DString::FormatString(TXT("Testing other Format String function with %s where we %s another string.  Empty String=\"%s\". %s.  -=End=-"), {formatParam1, formatParam2, formatParam3, formatParam4});
		
		UNITTESTER_LOG(testFlags, TXT("Constructor tests.  Note that these logs may appear incorrectly since the FormatString and ToCString was not yet tested."));
		UNITTESTER_LOG1(testFlags, TXT("    blankString=%s"), blankString);
		UNITTESTER_LOG1(testFlags, TXT("    initString=%s"), initString);
		UNITTESTER_LOG1(testFlags, TXT("    copiedString=%s"), copiedString);
		UNITTESTER_LOG1(testFlags, TXT("    formattedString=%s"), formattedString);
		UNITTESTER_LOG1(testFlags, TXT("    superFormattedString=%s"), superFormattedString);
		UNITTESTER_LOG1(testFlags, TXT("    otherFormatString=%s"), otherFormatString);

		SetTestCategory(testFlags, TXT("Comparison"));
		if (initString != TXT("Initialized string"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The initialized string should have been equal to \"Initialized string\".  Instead it's \"%s\""), {initString});
			UNITTESTER_LOG(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
			return false;
		}

		if (formattedString != TXT("Formatted string (Initialized string)"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The formattedString should have been equal to \"Formatted string (Initialized string)\".  Instead it's \"%s\""), {formattedString});
			UNITTESTER_LOG(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
			return false;
		}

		DString superFormatResult = TXT("Super Formatted string (CStringA), (CStringB), (CStringC), (CStringD)");
		if (superFormattedString != superFormatResult)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The superFormattedString should have been equal to \"%s\".  Instead it's \"%s\""), {superFormatResult, superFormattedString});
			UNITTESTER_LOG(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
			return false;
		}

		DString otherFormatStringResult = TXT("Testing other Format String function with Some Value where we Insert a string with '%s' macro within another string.  Empty String=\"\". (Final Value with number 7).  -=End=-");
		if (otherFormatString != otherFormatStringResult)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The otherFormatString should have been equal to \"%s\".  Instead it's \"%s\""), {otherFormatStringResult, otherFormatString});
			UNITTESTER_LOG(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
			return false;
		}

		//compare CStrings
		if (strcmp(initString.ToCString(), TXT("Initialized string")) != 0)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The CString of initialized string should have been equal to \"Initialized string\".  Instead it's \"%s\""), {initString});
			UNITTESTER_LOG(testFlags, TXT("Note:  The logs may be displayed incorrectly since ToCString was not yet tested."));
			return false;
		}

		DString compareString = initString;
		compareString.ToLower();
		if (compareString != TXT("initialized string"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The lower case of initialized string should have been equal to \"initialized string\".  Instead it's \"%s\""), {compareString});
			return false;
		}

		compareString.ToUpper();
		if (compareString != TXT("INITIALIZED STRING"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The upper case of initialized string should have been equal to \"INITIALIZED STRING\".  Instead it's \"%s\""), {compareString});
			return false;
		}

		if (initString.Compare(compareString, false) != 0)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The case insensitive compare of \"%s\" and \"%s\" should have returned true."), {initString, compareString});
			return false;
		}

		if (!blankString.IsEmpty())
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The blank string should be considered empty.  Instead IsEmpty returned false.  Value of blankString is:  \"%s\""), {blankString});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("DString utilities"));
		DString main = TXT("main");
		UNITTESTER_LOG1(testFlags, TXT("Adding \" appended text\" to \"%s\""), main);
		main += TXT(" appended text");

		if (main != TXT("main appended text"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The resulting appended string should have been \"main appended text\".  Instead it's \"%s\""), {main});
			return false;
		}

		if (main.At(3) != TEXT('n'))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Index 3 of \"%s\" should have been \'n\'.  Instead it returned:  \"%s\""), {main, DString(main.At(3))});
			return false;
		}

		UNITTESTER_LOG2(testFlags, TXT("Finding \"appended\" within \"%s\".  The subtext is found at index %s"), main, main.FindSubText(TXT("appended")));
		if (main.FindSubText(TXT("appended")) != 5)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The index at which \"appended\" appears within \"%s\" should have been 5.  Instead it returned:  \"%s\""), {main, main.FindSubText(TXT("appended")).ToString()});
			return false;
		}

		if (main.FindSubText(TXT("APPENDED"), 0, true) >= 0)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The index at which \"APPENDED\" appears within \"%s\" (case sensitive) should have been negative.  Instead it returned:  %s"), {main, main.FindSubText(TXT("APPENDED"), 0, true).ToString()});
			return false;
		}

		if (main.FindSubText(TXT("APPENDED")) != 5)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The index at which \"APPENDED\" appears within \"%s\" (case insensitive) should have been 5.  Instead it returned:  %s"), {main, main.FindSubText(TXT("APPENDED")).ToString()});
			return false;
		}

		if (main.FindSubText(TXT("appended"), 7) >= 0)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Searching for \"appended\" within \"%s\" starting from index 7 should have returned negative results.  Instead it returned:  %s"), {main, main.FindSubText(TXT("appended"), 7).ToString()});
			return false;
		}

		main = TXT("Sand Dune project:  Transcendence");
		DString lastWord = main.SubString(20);
		UNITTESTER_LOG2(testFlags, TXT("Retrieving the last word from \"%s\" returns \"%s\""), main, lastWord);
		if (lastWord != TXT("Transcendence"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The last word of \"%s\" should have been \"Transcendence\".  Instead it returned \"%s\""), {main.ToString(), lastWord.ToString()});
			return false;
		}

		DString secondWord = main.SubString(5, 8);
		UNITTESTER_LOG2(testFlags, TXT("Retrieving the second word from \"%s\" returns \"%s\""), main, secondWord);
		if (secondWord != TXT("Dune"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The second word of \"%s\" should have been \"Dune\".  Instead it returned \"%s\""), {main, secondWord});
			return false;
		}

		DString duneProject = main.SubStringCount(5, 12);
		UNITTESTER_LOG2(testFlags, TXT("Retrieving \"Dune Project\" from \"%s\" using SubStringCount returns \"%s\""), main, duneProject);
		if (duneProject != TXT("Dune project"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The 2nd and 3rd word of \"%s\" should have been \"Dune project\".  Instead it returned \"%s\""), {main, duneProject});
			return false;
		}

		main = TXT("Segment1|Segment2");
		INT splitIndex = 8; //at '|' character
		UNITTESTER_LOG1(testFlags, TXT("Splitting \"%s\" at | character into two other strings."), main);
		DString firstSegment;
		DString secondSegment;
		main.SplitString(splitIndex, firstSegment, secondSegment);
		if (firstSegment != TXT("Segment1") || secondSegment != TXT("Segment2"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Splitting \"%s\" at the | character (index %s) should have constructed strings:  \"Segment1\" and \"Segment2\".  Instead \"%s\" and \"%s\" was constructed."), {main, splitIndex.ToString(), firstSegment, secondSegment});
			return false;
		}

		main = TXT("2,5,2,29,918,-192,29,38,61");
		vector<DString> numbers;
		vector<DString> correctNumbers = {TXT("2"), TXT("5"), TXT("2"), TXT("29"), TXT("918"), TXT("-192"), TXT("29"), TXT("38"), TXT("61")};
		UNITTESTER_LOG1(testFlags, TXT("Parsing \"%s\" into a vector of strings."), main);
		main.ParseString(TEXT(','), numbers);
		if (numbers.size() != correctNumbers.size())
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The total number of elements of the number vector constructed from parsing \"%s\" should have been %s.  Instead the vector size is %s"), {main, DString(static_cast<int>(correctNumbers.size())), DString(static_cast<int>(numbers.size()))});
			return false;
		}

		//validate each entry
		for (unsigned int i = 0; i < correctNumbers.size(); i++)
		{
			if (correctNumbers.at(i) != numbers.at(i))
			{
				UnitTestError(testFlags, TXT("DString tests failed.  The numbers vector at index %s does not match the expected value:  %s.  Instead it's %s"), {DString(i), correctNumbers.at(i), numbers.at(i)});
				return false;
			}
		}

		main = TXT("Alpha _ Omega");
		DString edgeString = main.Left(5);
		UNITTESTER_LOG2(testFlags, TXT("The left part of \"%s\" is \"%s\""), main, edgeString);
		if (edgeString != TXT("Alpha"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The left part of \"%s\" should have returned \"Alpha\".  Instead it returned \"%s\"."), {main, edgeString});
			return false;
		}

		edgeString = main.Right(5);
		UNITTESTER_LOG2(testFlags, TXT("The right part of \"%s\" is \"%s\""), main, edgeString);
		if (edgeString != TXT("Omega"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The right part of \"%s\" should have returned \"Omega\".  Instead it returned \"%s\"."), {main, edgeString});
			return false;
		}

		UNITTESTER_LOG2(testFlags, TXT("The length of \"%s\" is %s"), main, main.Length());
		if (main.Length() != 13)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The length of \"%s\" should have returned 13. Instead it returned %s"), {main, main.Length().ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Inserting \" _ Beta\" after alpha in \"%s\""), main);
		main.Insert(5, TXT(" _ Beta"));
		if (main != TXT("Alpha _ Beta _ Omega"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of inserting \" _ Beta\" after \"Alpha\" within \"Alpha _ Omega\" should have resulted in \"Alpha _ Beta _ Omega\".  Instead it's:  \"%s\"."), {main});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Removing \" _ Omega\" from \"%s\""), main);
		main.Remove(12, 8);
		if (main != TXT("Alpha _ Beta"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of removing \" _ Omega\" from \"Alpha _ Beta _ Omega\" should have resulted in \"Alpha _ Beta\".  Instead it's:  \"%s\"."), {main});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Replacing spaces with \"[ ]\" in \"%s\""), main);
		main.StrReplace(TXT(" "), TXT("[ ]"));
		if (main != TXT("Alpha[ ]_[ ]Beta"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of replacing \" \" with \"[ ]\" should have resulted in \"Alpha[ ]_[ ]Beta\".  Instead it's \"%s\"."), {main});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Replacing all a's with @'s in \"%s\" (case insensitive)."), main);
		main.StrReplace(TXT("a"), TXT("aAa"), false);
		if (main != TXT("aAalphaAa[ ]_[ ]BetaAa"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of replacing a's with \"aAa\" should have resulted in \"aAalphaAa[ ]_[ ]BetaAa\".  Instead it's \"%s\"."), {main});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Replacing all lower-case a's with nothing in \"%s\""), main);
		main.StrReplace(TXT("a"), TXT(""), true);
		if (main != TXT("AlphA[ ]_[ ]BetA"))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The result of replacing all lower case a's with nothing should have resulted in \"AlphA[ ]_[ ]BetA\".  Instead it's \"%s\"."), {main});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Clearing \"%s\""), main);
		main.Clear();
		if (main != TXT(""))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Clearing a string should have resulted in \"\".  Instead it's:  \"%s\"."), {main});
			return false;
		}

		DString fatString = TXT("            My custom string.        ");
		main = fatString;
		UNITTESTER_LOG1(testFlags, TXT("Trimming spaces on the ends of \"%s\""), main);
		main.TrimSpaces();
		if (main != TXT("My custom string."))
		{
			UnitTestError(testFlags, TXT("DString tests failed.  Trimming spaces on the end of \"%s\" should have resulted in \"My custom string.\".  Instead it's:  \"%s\"."), {fatString, main});
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Testing search functions for DString. . ."));
		DString searchString = TXT("Sand Dune");
		compareString = TXT("Sand Pit");
		INT mismatchIdx = searchString.FindFirstMismatchingIdx(compareString, true);
		UNITTESTER_LOG3(testFlags, TXT("The first mismatching index when searching %s within %s is %s."), searchString, compareString, mismatchIdx);
		if (mismatchIdx != 5)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s within %s should be 5.  Instead it returned %s."), {searchString, compareString, mismatchIdx.ToString()});
			return false;
		}

		compareString = TXT("SaNd DuNe");
		mismatchIdx = searchString.FindFirstMismatchingIdx(compareString, true);
		UNITTESTER_LOG3(testFlags, TXT("The first mismatching index when searching %s within %s is %s."), searchString, compareString, mismatchIdx);
		if (mismatchIdx != 2)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s within %s should be 2.  Instead it returned %s."), {searchString, compareString, mismatchIdx.ToString()});
			return false;
		}

		mismatchIdx = searchString.FindFirstMismatchingIdx(compareString, false);
		UNITTESTER_LOG3(testFlags, TXT("The first mismatching index (case insensitive) when searching %s within %s is %s."), searchString, compareString, mismatchIdx);
		if (mismatchIdx != INT_INDEX_NONE)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index (case insensitive) when searching %s within %s should be " STRINGIFY(INT_INDEX_NONE) ".  Instead it returned %s."), {searchString, compareString, mismatchIdx.ToString()});
			return false;
		}

		searchString = TXT("Entity");
		compareString = TXT("EntityComponent");
		mismatchIdx = searchString.FindFirstMismatchingIdx(compareString, true);
		UNITTESTER_LOG3(testFlags, TXT("The first mismatching index when searching %s within %s is %s."), searchString, compareString, mismatchIdx);
		if (mismatchIdx != 6)
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s within %s should be 6.  Instead it returned %s."), {searchString, compareString, mismatchIdx.ToString()});
			return false;
		}

		CompleteTestCategory(testFlags);
		ExecuteSuccessSequence(testFlags, TXT("DString"));

		return true;
	}

	bool DatatypeUnitTester::TestBOOL (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Bool"));

		UNITTESTER_LOG(testFlags, TXT("NOTE:  booleans may be displayed incorrectly since the localization module was not yet tested."));

		BOOL blankBool;
		BOOL initBool(true);
		BOOL copiedBool(initBool);
		BOOL stringBool(TXT("True"));

		UNITTESTER_LOG1(testFlags, TXT("Constructor tests:  BlankBool=%s"), blankBool);
		UNITTESTER_LOG1(testFlags, TXT("    initializedBool(true)=%s"), initBool);
		UNITTESTER_LOG1(testFlags, TXT("    copiedBool from initializedBool=%s"), copiedBool);
		UNITTESTER_LOG1(testFlags, TXT("    stringBool(\"true\")=%s"), stringBool);

		SetTestCategory(testFlags, TXT("Comparison"));
		BOOL trueBool = true;
		BOOL falseBool = false;
		UNITTESTER_LOG1(testFlags, TXT("True is returning %s"), trueBool);
		if (!trueBool || falseBool)
		{
			UnitTestError(testFlags, TXT("Comparison check failed.  Expected bool conditions (true) and (false) did not return the right conditions.  They returned  %s and %s."), {trueBool.ToString(), falseBool.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Testing bool comparison (true == false)?"), BOOL(trueBool == falseBool));
		if (trueBool == falseBool)
		{
			UnitTestError(testFlags, TXT("Comparison check failed.  True should not equal to false."));
			return false;
		}

		if (trueBool != true)
		{
			UnitTestError(testFlags, TXT("Comparison check failed.  True should be equal to true."));
			return false;
		}

		if (true != trueBool)
		{
			UnitTestError(testFlags, TXT("Comparison check failed.  True should be equal to true."));
			return false;
		}

		CompleteTestCategory(testFlags);
		ExecuteSuccessSequence(testFlags, TXT("BOOL"));

		return true;
	}

	bool DatatypeUnitTester::TestINT (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("INT"));

		INT blankInt;
		INT initializedInt(15);
		INT copiedInt(initializedInt);

		UNITTESTER_LOG(testFlags, TXT("Constructor tests:  BlankInt=") + blankInt.ToString());
		UNITTESTER_LOG(testFlags, TXT("    initializedInt(15)= ") + initializedInt.ToString());
		UNITTESTER_LOG(testFlags, TXT("    copiedInt from initializedInt= ") + copiedInt.ToString());
		if (sizeof(blankInt.Value) != 4)
		{
			UnitTestError(testFlags, TXT("INTs are not 32 bit."));
			return false;
		}

		SetTestCategory(testFlags, TXT("Comparison"));
		UNITTESTER_LOG1(testFlags, TXT("blankInt != 0?  %s"), BOOL(blankInt != 0));
		if (blankInt != 0)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  blankInt is not zero.  Instead it's:  ") + blankInt.ToString());
			return false;
		}

		UNITTESTER_LOG3(testFlags, TXT("initializedInt(%s) != copiedInt(%s) ? %s"), initializedInt, copiedInt, BOOL(initializedInt != copiedInt));
		if (initializedInt != copiedInt)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedInt != copiedInt.  Values are:  %s and %s"), {initializedInt.ToString(), copiedInt.ToString()});
			return false;
		}

		INT testInt = 5;
		UNITTESTER_LOG1(testFlags, TXT("5 < 6 ? %s"), BOOL(testInt < 6));
		if (!(testInt < 6))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is not less than 6."), {testInt.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("5 <= 5 ? %s"), BOOL(testInt <= 5));
		if (!(testInt <= 5))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is not less than or equal to 5."), {testInt.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("5 > 3 ? %s"), BOOL(testInt > 3));
		if (!(testInt > 3))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is not greater than 3."), {testInt.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("5 >= 7 ? %s"), BOOL(testInt >= 7));
		if (testInt >= 7)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is greater than or equal to 7."), {testInt.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("5 >= 3 && 5 < 7 && 5 == 5 && (5 == 10 || 5 != 9) ? %s"), BOOL(testInt >= 3 && testInt < 7 && testInt == 5 && (testInt == 10 || testInt != 9)));
		if (!(testInt >= 3 && testInt < 7 && testInt == 5 && (testInt == 10 || testInt != 9)))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testInt is:  %s"), {testInt.ToString()});
			return false;
		}

		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Arithmetic"));
		testInt = 10 + 8;
		UNITTESTER_LOG1(testFlags, TXT("10 + 8 = %s"), testInt);
		if (testInt != 18)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 18.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt = 10 + initializedInt;
		UNITTESTER_LOG1(testFlags, TXT("10 + 15 = %s"), testInt);
		if (testInt != 25)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 25.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt++;
		UNITTESTER_LOG1(testFlags, TXT("Increment from 25 leads to:  %s"), testInt);
		if (testInt != 26)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 26.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		--testInt;
		UNITTESTER_LOG1(testFlags, TXT("Decrement from 26 leads to:  %s"), testInt);
		if (testInt != 25)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 25.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt = initializedInt - 5;
		UNITTESTER_LOG1(testFlags, TXT("15 - 5 = %s"), testInt);
		if (testInt != 10)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 10.  It's currently:  %s"),  {testInt.ToString()});
			return false;
		}

		testInt += 13;
		UNITTESTER_LOG1(testFlags, TXT("10 += 13 ---> %s"), testInt);
		if (testInt != 23)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 10.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt = initializedInt * 2;
		UNITTESTER_LOG1(testFlags, TXT("15 * 2 = %s"), testInt);
		if (testInt != 30)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 30.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt /= 3;
		UNITTESTER_LOG1(testFlags, TXT("30 /= 3 ---> %s"), testInt);
		if (testInt != 10)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 10.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt += ((20 - initializedInt) * 4) + 3;
		UNITTESTER_LOG1(testFlags, TXT("10 += ((20 - 15) * 4) + 3 results in:  %s"), testInt);
		if (testInt != 33)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 33.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt = 20;
		testInt %= 3;
		UNITTESTER_LOG1(testFlags, TXT("20 % 3 = %s"), testInt);
		if (testInt != 2)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 2.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Bitwise Operation"));
		testInt = 14;
		testInt = testInt >> 2;
		UNITTESTER_LOG1(testFlags, TXT("14 >> 2 = %s"), testInt);
		if (testInt != 3)
		{
			UnitTestError(testFlags, TXT("Bit shifting failed.  Expected value for testInt is 3.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt = 9;
		testInt = testInt << 2;
		UNITTESTER_LOG1(testFlags, TXT("9 << 2 = %s"), testInt);
		if (testInt != 36)
		{
			UnitTestError(testFlags, TXT("Bit shifting failed.  Expected value for testInt is 36.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt = 13;
		testInt &= 11;
		UNITTESTER_LOG1(testFlags, TXT("13 & 11 = %s"), testInt);
		if (testInt != 9)
		{
			UnitTestError(testFlags, TXT("Bitwise operation failed.  Expected value for testInt is 9.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt = 13;
		testInt = testInt | 3;
		UNITTESTER_LOG1(testFlags, TXT("13 | 3 = %s"), testInt);
		if (testInt != 15)
		{
			UnitTestError(testFlags, TXT("Bitwise operation failed.  Expected value for testInt is 15.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}

		testInt = 13;
		testInt ^= 3;
		UNITTESTER_LOG1(testFlags, TXT("13 ^ 3 = %s"), testInt);
		if (testInt != 14)
		{
			UnitTestError(testFlags, TXT("Bitwise operation failed.  Expected value for testInt is 14.  It's currently:  %s"), {testInt.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Typecasting"));
		UNITTESTER_LOG(testFlags, TXT("INT to unsigned int check. . ."));
		testInt = -5;
		unsigned int testUnsignedInt = testInt.ToUnsignedInt();
		UNITTESTER_LOG1(testFlags, TXT("INT(-5) converted to unsigned int is:  %s"), DString::MakeString(testUnsignedInt));
		if (testUnsignedInt != 0)
		{
			UnitTestError(testFlags, TXT("Typecasting failed.  Expected resulting unsigned int is:  0"));
			return false;
		}

		testInt = 9001;
		testUnsignedInt = testInt.ToUnsignedInt();
		UNITTESTER_LOG1(testFlags, TXT("INT(9001) converted to unsigned int is:  %s"),DString::MakeString(testUnsignedInt));
		if (testUnsignedInt != 9001)
		{
			UnitTestError(testFlags, TXT("Typcasting failed.  Expected resulting unsigned int is:  9001"));
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Unsigned int to INT check. . ."));
		testUnsignedInt = 3456789012;
		testInt = testUnsignedInt;
		UNITTESTER_LOG1(testFlags, TXT("UnsignedInt(3456789012) to INT is:  %s.  Expected overflow"), testInt);
		if (testInt != INT_MAX)
		{
			UnitTestError(testFlags, TXT("Typcasting failed.  Expected resulting INT is:  %s"), {INT(INT_MAX).ToString()});
			return false;
		}

		testUnsignedInt = 1337;
		testInt = testUnsignedInt;
		UNITTESTER_LOG1(testFlags, TXT("UnsignedInt(1337) to INT is:  %s"), testInt);
		if (testInt != 1337)
		{
			UnitTestError(testFlags, TXT("Typecasting failed.  Expected resulting INT is:  1337"));
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("INT Utils"));
		testInt = 4;
		UNITTESTER_LOG2(testFlags, TXT("Is %s even?  %s"), testInt, BOOL(testInt.IsEven()));
		if (!testInt.IsEven() || testInt.IsOdd())
		{
			UnitTestError(testFlags, TXT("INT utils test failed.  %s should be considered an even number."), {testInt.ToString()});
			return false;
		}

		testInt++;
		UNITTESTER_LOG2(testFlags, TXT("Is %s odd?  %s"), testInt, BOOL(testInt.IsOdd()));
		if (!testInt.IsOdd() || testInt.IsEven())
		{
			UnitTestError(testFlags, TXT("INT utils test failed.  %s should be considered an odd number."), {testInt.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);
		ExecuteSuccessSequence(testFlags, TXT("INT"));
		
		return true;
	}

	bool DatatypeUnitTester::TestFLOAT (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("FLOAT"));

		FLOAT blankFloat;
		FLOAT initializedFloat(14.5f);
		FLOAT copiedFloat(initializedFloat);

		UNITTESTER_LOG1(testFlags, TXT("Constructor tests:  blankFloat = %s"), blankFloat);
		UNITTESTER_LOG1(testFlags, TXT("    initializedFloat(14.5) = %s"), initializedFloat);
		UNITTESTER_LOG1(testFlags, TXT("    copiedFloat from initializedInt = %s"), copiedFloat);

		SetTestCategory(testFlags, TXT("Comparison"));
		UNITTESTER_LOG1(testFlags, TXT("blankFloat != 0?  %s"), BOOL(blankFloat != 0));
		if (blankFloat != 0)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  blankFloat is not zero.  Instead it's:  %s"), {blankFloat.ToString()});
			return false;
		}

		UNITTESTER_LOG3(testFlags, TXT("initializedFloat(%s) != copiedFloat(%s) ? %s"), initializedFloat, copiedFloat, BOOL(initializedFloat != copiedFloat));
		if (initializedFloat != copiedFloat)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedFloat != copiedInt.  Values are:  %s and %s"), {initializedFloat.ToString(), copiedFloat.ToString()});
			return false;
		}

		FLOAT testFloat = 5.95f;
		UNITTESTER_LOG1(testFlags, TXT("5.95 < 6 ? %s"), BOOL(testFloat < 6));
		if (!(testFloat < 6))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is not less than 6."), {testFloat.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("5.95 <= 5.95 ? %s"), BOOL(testFloat <= 5.95f));
		if (!(testFloat <= 5.95f))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is not less than or equal to 5.95."), {testFloat.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("5.95 > 3 ? %s"), BOOL(testFloat > 3));
		if (!(testFloat > 3))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is not greater than 3."), {testFloat.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("5.95 >= 7.2 ? %s"), BOOL(testFloat >= 7.2f));
		if (testFloat >= 7.2f)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is greater than or equal to 7.2."), {testFloat.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("5.95 >= 3.14 && 5.95 < 7.8 && 5.95 == 5.95 && (5.95 == 10.05 || 5.95 != 9) ? %s"), BOOL(testFloat >= 3.14f && testFloat < 7.8f && testFloat == FLOAT(5.95f) && (testFloat == 10.05f || testFloat != INT(9).ToFLOAT())));
		if (!(testFloat >= 3.14f && testFloat < 7.8f && testFloat == FLOAT(5.95f) && (testFloat == 10.05f || testFloat != INT(9).ToFLOAT())))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testFloat is:  %s"), {testFloat.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Arithmetic"));
		testFloat = 10.2 + 8.8;
		UNITTESTER_LOG1(testFlags, TXT("10.2 + 8.8 = %s"), testFloat);
		if (testFloat != 19)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 19.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}

		testFloat = 10.f + initializedFloat;
		UNITTESTER_LOG1(testFlags, TXT("10 + 14.5 = %s"), testFloat);
		if (testFloat != 24.5)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 24.5.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}

		testFloat++;
		UNITTESTER_LOG1(testFlags, TXT("Increment from 24.5 leads to:  %s"), testFloat);
		if (testFloat != 25.5)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 25.5.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}

		--testFloat;
		UNITTESTER_LOG1(testFlags, TXT("Decrement from 25.5 leads to:  %s"), testFloat);
		if (testFloat != 24.5)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 24.5.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}

		testFloat = initializedFloat - 1.11115f;
		UNITTESTER_LOG1(testFlags, TXT("14.5 - 1.11115 = %s"), testFloat);
		if (testFloat != 13.38885f)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 13.38885.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}

		testFloat += 13.794f;
		UNITTESTER_LOG1(testFlags, TXT("13.38885 += 13.794  ---> %s"), testFloat);
		if (testFloat != 27.18285f)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 27.18285.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}

		testFloat = initializedFloat * 2;
		UNITTESTER_LOG1(testFlags, TXT("14.5 * 2 = %s"), testFloat);
		if (testFloat != 29)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 29.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}

		testFloat /= 3;
		UNITTESTER_LOG1(testFlags, TXT("29 /=3  ---> %s"), testFloat);
		if (abs(testFloat.Value - 9.666667) > 0.000001)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is ~9.666667.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}

		testFloat = 10;
		testFloat += ((20.25f - 14.5f) * 4.f) + 3.1f;
		UNITTESTER_LOG1(testFlags, TXT("10 += ((20.25 - 14.5) * 4) + 3.1 results in:  %s"), testFloat);
		if (testFloat != 36.1f)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 36.1.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}

		testFloat = 20.5;
		testFloat %= 3;
		UNITTESTER_LOG1(testFlags, TXT("20.5 % 3 = %s"), testFloat);
		if (testFloat != 2.5)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 2.5.  It's currently:  %s"), {testFloat.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Typcasting"));
		testFloat = -5.84f;
		INT testInt = testFloat.ToINT();
		UNITTESTER_LOG1(testFlags, TXT("FLOAT(-5.84) converted to INT is:  %s"), testInt);
		if (testInt != -5)
		{
			UnitTestError(testFlags, TXT("Typecasting failed.  Expected resulting INT is:  -5"));
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("FLOAT Utils"));
		FLOAT minBounds = 0.f;
		FLOAT maxBounds = 10.f;
		testFloat = 0.5f;
		UNITTESTER_LOG4(testFlags, TXT("Lerping %s between %s and %s yields %s"), testFloat, minBounds, maxBounds, Utils::Lerp(testFloat, minBounds, maxBounds));
		if (Utils::Lerp(testFloat, minBounds, maxBounds) != 5.f)
		{
			UnitTestError(testFlags, TXT("Lerping test failed.  Lerping %s between %s and %s should have returned 5.  Instead it returned %s"), {testFloat.ToString(), minBounds.ToString(), maxBounds.ToString(), Utils::Lerp(testFloat, minBounds, maxBounds).ToString()});
			return false;
		}

		minBounds = 10.f;
		maxBounds = 1010.f;
		testFloat = 0.75f;
		UNITTESTER_LOG4(testFlags, TXT("Lerping %s between %s and %s yields %s"), testFloat, minBounds, maxBounds, Utils::Lerp(testFloat, minBounds, maxBounds));
		if (Utils::Lerp(testFloat, minBounds, maxBounds) != 760.f)
		{
			UnitTestError(testFlags, TXT("Lerping test failed.  Lerping %s between %s and %s should have returned 760.  Instead it returned %s"), {testFloat.ToString(), minBounds.ToString(), maxBounds.ToString(), Utils::Lerp(testFloat, minBounds, maxBounds).ToString()});
			return false;
		}

		testFloat = 10.75f;
		UNITTESTER_LOG2(testFlags, TXT("Rounding %s results in %s"), testFloat, FLOAT::Round(testFloat));
		if (FLOAT::Round(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::Round(testFloat).ToString()});
			return false;
		}

		testFloat = 10.5f;
		UNITTESTER_LOG2(testFlags, TXT("Rounding %s results in %s"), testFloat, FLOAT::Round(testFloat));
		if (FLOAT::Round(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::Round(testFloat).ToString()});
			return false;
		}

		testFloat = 10.25f;
		UNITTESTER_LOG2(testFlags, TXT("Rounding %s results in %s"), testFloat, FLOAT::Round(testFloat));
		if (FLOAT::Round(testFloat) != 10.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::Round(testFloat).ToString()});
			return false;
		}

		testFloat = 10.75f;
		UNITTESTER_LOG2(testFlags, TXT("Rounding up from %s results in %s"), testFloat, FLOAT::RoundUp(testFloat));
		if (FLOAT::RoundUp(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundUp(testFloat).ToString()});
			return false;
		}

		testFloat = 10.5f;
		UNITTESTER_LOG2(testFlags, TXT("Rounding up from %s results in %s"), testFloat, FLOAT::RoundUp(testFloat));
		if (FLOAT::RoundUp(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundUp(testFloat).ToString()});
			return false;
		}

		testFloat = 10.25f;
		UNITTESTER_LOG2(testFlags, TXT("Rounding up from %s results in %s"), testFloat, FLOAT::RoundUp(testFloat));
		if (FLOAT::RoundUp(testFloat) != 11.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundUp(testFloat).ToString()});
			return false;
		}

		testFloat = 10.75f;
		UNITTESTER_LOG2(testFlags, TXT("Rounding down from %s results in %s"), testFloat, FLOAT::RoundDown(testFloat));
		if (FLOAT::RoundDown(testFloat) != 10.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundDown(testFloat).ToString()});
			return false;
		}

		testFloat = 10.5f;
		UNITTESTER_LOG2(testFlags, TXT("Rounding down from %s results in %s"), testFloat, FLOAT::RoundDown(testFloat));
		if (FLOAT::RoundDown(testFloat) != 10.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundDown(testFloat).ToString()});
			return false;
		}

		testFloat = 10.25f;
		UNITTESTER_LOG2(testFlags, TXT("Rounding down from %s results in %s"), testFloat, FLOAT::RoundDown(testFloat));
		if (FLOAT::RoundDown(testFloat) != 10.f)
		{
			UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundDown(testFloat).ToString()});
			return false;
		}

		CompleteTestCategory(testFlags);
		ExecuteSuccessSequence(testFlags, TXT("FLOAT"));
		
		return true;
	}

	bool DatatypeUnitTester::TestRange (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Range"));

		Range<INT> blankRange;
		Range<INT> initializedRange(0, 100);
		Range<INT> copiedRange(initializedRange);

		UNITTESTER_LOG1(testFlags, TXT("Constructor tests:  blankRange= %s"), blankRange);
		UNITTESTER_LOG1(testFlags, TXT("    initializedRange(0,100)= %s"), initializedRange);
		UNITTESTER_LOG1(testFlags, TXT("    copiedRange from initializedRange= %s"), copiedRange);

		SetTestCategory(testFlags, TXT("Comparison"));
		Range<INT> comparisonRange(25, 100);
		UNITTESTER_LOG3(testFlags, TXT("initializedRange(%s) == comparisonRange(%s)?  %s"), initializedRange, comparisonRange, BOOL(initializedRange == comparisonRange));
		if (initializedRange == comparisonRange)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRange %s should not be equal to %s."), {initializedRange.ToString(), comparisonRange.ToString()});
			return false;
		}

		UNITTESTER_LOG3(testFlags, TXT("initializedRange(%s) == copiedRange(%s) ? %s"), initializedRange, copiedRange, BOOL(initializedRange == copiedRange));
		if (initializedRange != copiedRange)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRange %s != copiedRange %s."), {initializedRange.ToString(), copiedRange.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Utility"));
		Range<INT> testRange(0, 100);
		Range<INT> otherRange(50, 125);
		UNITTESTER_LOG3(testFlags, TXT("Does %s intersect %s?  %s"), testRange, otherRange, BOOL(testRange.Intersects(otherRange)));
		if (!testRange.Intersects(otherRange))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should be intersecting %s."), {testRange.ToString(), otherRange.ToString()});
			return false;
		}

		otherRange.Min = 125;
		otherRange.Max = 200;
		UNITTESTER_LOG3(testFlags, TXT("Does %s intersect %s?  %s"), testRange, otherRange, BOOL(testRange.Intersects(otherRange)));
		if (testRange.Intersects(otherRange))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be intersecting %s."), {testRange.ToString(), otherRange.ToString()});
			return false;
		}

		otherRange.Min = 50;
		otherRange.Max = 200;
		Range<INT> intersection = testRange.GetIntersectionFrom(otherRange);
		UNITTESTER_LOG3(testFlags, TXT("The intersection of %s and %s is %s"), testRange, otherRange, intersection);
		if (intersection != Range<INT>(50, 100))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  The intersection of %s and %s should be 50-100.  Instead it returned %s."), {testRange.ToString(), otherRange.ToString(), intersection.ToString()});
			return false;
		}

		UNITTESTER_LOG3(testFlags, TXT("Is %s completely within %s?  %s"), testRange, otherRange, BOOL(testRange.IsWithin(otherRange)));
		if (testRange.IsWithin(otherRange))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be completely within %s."), {testRange.ToString(), otherRange.ToString()});
			return false;
		}

		otherRange.Min = -100;
		otherRange.Max = 200;
		UNITTESTER_LOG3(testFlags, TXT("Is %s completely within %s?  %s"), testRange, otherRange, BOOL(testRange.IsWithin(otherRange)));
		if (!testRange.IsWithin(otherRange))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should be completely within %s."), {testRange.ToString(), otherRange.ToString()});
			return false;
		}

		otherRange.Min = 100;
		otherRange.Max = 0;
		UNITTESTER_LOG2(testFlags, TXT("Is %s inverted?  %s"), otherRange, BOOL(otherRange.IsInverted()));
		if (!otherRange.IsInverted())
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should be considered inverted."), {otherRange.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("...Fixing %s inversion..."), otherRange);
		otherRange.FixRange(); //This should fix inverted ranges
		UNITTESTER_LOG2(testFlags, TXT("Is %s inverted?  %s"), otherRange, BOOL(otherRange.IsInverted()));
		if (otherRange.IsInverted())
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be inverted."), {otherRange.ToString()});
			return false;
		}

		UNITTESTER_LOG2(testFlags, TXT("Does %s contain 25? %s"), testRange, BOOL(testRange.ContainsValue(25)));
		if (!testRange.ContainsValue(25))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should be containing 25"), {testRange.ToString()});
			return false;
		}

		UNITTESTER_LOG2(testFlags, TXT("Does %s contain 125?  %s"), testRange, BOOL(testRange.ContainsValue(125)));
		if (testRange.ContainsValue(125))
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be containing 125"), {testRange.ToString()});
			return false;
		}

		UNITTESTER_LOG2(testFlags, TXT("The difference of %s is:  %s"), testRange, testRange.Difference());
		if (testRange.Difference() != 100)
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  The expected value of the difference of %s is 100."), {testRange.ToString()});
			return false;
		}

		UNITTESTER_LOG2(testFlags, TXT("The center of %s is:  %s"), testRange, testRange.Center());
		if (testRange.Center() != 50)
		{
			UnitTestError(testFlags, TXT("Utility functions failed.  The expected value of the center of %s is 50."), {testRange.ToString()});
			return false;
		}
		
		CompleteTestCategory(testFlags);
		ExecuteSuccessSequence(testFlags, TXT("Range"));

		return true;
	}

	bool DatatypeUnitTester::TestVector2 (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Vector2"));

		Vector2 blankVector;
		Vector2 initializedVector(1, 2);
		Vector2 copiedVector(initializedVector);
		Vector2 typecastedVector(Vector3(10,20,30).ToVector2());

		UNITTESTER_LOG1(testFlags, TXT("Constructor tests:  BlankVector=%s"), blankVector);
		UNITTESTER_LOG1(testFlags, TXT("    initializedVector(1,2)= %s"), initializedVector);
		UNITTESTER_LOG1(testFlags, TXT("    copiedVector from initializedVector= %s"), copiedVector);
		UNITTESTER_LOG1(testFlags, TXT("    typecastedVector from Vector3(10,20,30)= %s"), typecastedVector);

		SetTestCategory(testFlags, TXT("Comparison"));
		UNITTESTER_LOG1(testFlags, TXT("initializedVector != copiedVector ? %s"), BOOL(initializedVector != copiedVector));
		if (initializedVector != copiedVector)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedVector != copiedVector.  Values are:  %s and %s"), {initializedVector.ToString(), copiedVector.ToString()});
			return false;
		}

		Vector2 testVector(5, 10);
		UNITTESTER_LOG1(testFlags, TXT("<5,10> != <1,2> && (<5,10> == <99,99> || <5,10> == <5,10>) ? %s"), BOOL(testVector != copiedVector && (testVector == Vector2(99,99) || testVector == Vector2(5,10))));
		if (!(testVector != copiedVector && (testVector != Vector2(99,99) || testVector == Vector2(5,10))))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testVector is:  %s"), {testVector.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("<5, 10> == <5, 15> ? %s"), BOOL(testVector == Vector2(5, 15)));
		if (testVector == Vector2(5, 15))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have been false.  Instead it returned true."));
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("<5, 10> != <5.025, 10> ? %s"), BOOL(testVector == Vector2(5.025f, 10)));
		if (testVector == Vector2(5.025f, 10))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have been false.  Instead it returned true."));
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Arithmetic"));
		testVector = Vector2(1,2) + Vector2(5,9);
		UNITTESTER_LOG1(testFlags, TXT("<1,2> + <5,9> = %s"), testVector);
		if (testVector != Vector2(6,11))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <6,11>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector = copiedVector - Vector2(4, 10);
		UNITTESTER_LOG1(testFlags, TXT("<1,2> - <4,10> = %s"), testVector);
		if (testVector != Vector2(-3,-8))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <-3,-8>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector += Vector2(4,6);
		UNITTESTER_LOG1(testFlags, TXT("<-3,-8> += <4,6>  ---> %s"), testVector);
		if (testVector != Vector2(1,-2))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1,-2>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector = Vector2(5.f, 8.f) * 2.f;
		UNITTESTER_LOG1(testFlags, TXT("<5,8> * 2 = %s"), testVector);
		if (testVector != Vector2(10,16))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <10,16>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector /= 8.f;
		UNITTESTER_LOG1(testFlags, TXT("<10,16> /=8  ---> %s"), testVector);
		if (testVector != Vector2(1.25, 2))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1.25,2>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector *= 10.f;
		UNITTESTER_LOG1(testFlags, TXT("<1.25,2> *=10  ---> %s"), testVector);
		if (testVector != Vector2(12.5, 20))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <12.5,20>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector += ((Vector2(5.f,5.f) - copiedVector) * 4.f) + Vector2(-10.f,-15.f);
		UNITTESTER_LOG1(testFlags, TXT("<12.5,20> += ((<5,5> - <1,2>) * 4) + <-10,-15>    results in:  %s"), testVector);
		if (testVector != Vector2(18.5, 17))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <18.5,17>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Utility"));
		testVector = Vector2(5,5);
		UNITTESTER_LOG1(testFlags, TXT("Vector Length of <5,5> is:  %s"), testVector.VSize());
		if (abs(testVector.VSize().Value - 7.071067812) > 0.000001)
		{
			UnitTestError(testFlags, TXT("VSize function failed.  Expected length of vector<5,5> is ~7.071067812.  Expected precision is:  10^-6."));
			return false;
		}

		testVector.Normalize();
		UNITTESTER_LOG1(testFlags, TXT("Normalize<5,5> results in:  %s"), testVector);
		if (testVector.X - 0.70710678f < 0.000001f && testVector.Y - 0.70710678f > 0.000001f)
		{
			UnitTestError(testFlags, TXT("Normalize vector function failed.  Expected resulting vector is:  ~<0.707107, 0.707107> with minimum precision of 10^-6."));
			return false;
		}

		testVector = Vector2(0, 5);
		testVector.SetLengthTo(3.5);
		UNITTESTER_LOG1(testFlags, TXT("Setting Length of <0,5> to 3.5 results in:  %s"), testVector);
		if (testVector.VSize() != 3.5f || testVector != Vector2(0, 3.5f))
		{
			UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<0,5> to 3.5 should have resulted in <0,3.5>.  The VSize of %s is %s instead."), {testVector.ToString(), testVector.VSize().ToString()});
			return false;
		}

		testVector = Vector2(-191, 238);
		testVector.SetLengthTo(10.f);
		UNITTESTER_LOG1(testFlags, TXT("Setting length of <-191,238> to 10 results in:  %s"), testVector);
		if (FLOAT::Abs(testVector.VSize() - 10.f) > 0.000001f || (testVector - Vector2(-6.258932f, 7.799088f)).VSize() > 0.000001f)
		{
			UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<-191,238> to 10 should have resulted in ~<-6.258932,7.799088>.  The VSize of %s is %s instead."), {testVector.ToString(), testVector.VSize().ToString()});
			return false;
		}

		testVector = Vector2(5,5);
		Vector2 otherVector = Vector2(-11,-13);
		UNITTESTER_LOG1(testFlags, TXT("<5,5> dot <-11,-13> = %s"), testVector.Dot(otherVector));
		if (testVector.Dot(otherVector) != -120)
		{
			UnitTestError(testFlags, TXT("Dot product of vectors %s and %s failed.  Expected value is -120."), {testVector.ToString(), otherVector.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);
		ExecuteSuccessSequence(testFlags, TXT("Vector2"));

		return true;
	}

	bool DatatypeUnitTester::TestVector3 (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Vector3"));

		Vector3 blankVector;
		Vector3 initializedVector(1, 2, 3);
		Vector3 copiedVector(initializedVector);
		Vector3 typecastedVector(Vector2(10,20).ToVector3());

		UNITTESTER_LOG1(testFlags, TXT("Constructor tests:  BlankVector=%s"), blankVector);
		UNITTESTER_LOG1(testFlags, TXT("    initializedVector(1,2,3)= %s"), initializedVector);
		UNITTESTER_LOG1(testFlags, TXT("    copiedVector from initializedVector= %s"), copiedVector);
		UNITTESTER_LOG1(testFlags, TXT("    typecastedVector from Vector2(10,20)= %s"), typecastedVector);

		SetTestCategory(testFlags, TXT("Comparison"));
		UNITTESTER_LOG1(testFlags, TXT("initializedVector != copiedVector ? %s"), BOOL(initializedVector != copiedVector));
		if (initializedVector != copiedVector)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedVector != copiedVector.  Values are:  %s and %s"), {initializedVector.ToString(), copiedVector.ToString()});
			return false;
		}

		Vector3 testVector(5, 10, 15);
		UNITTESTER_LOG1(testFlags, TXT("<5,10,15> != <1,2,3> && (<5,10,15> == <99,99,15> || <5,10,15> == <5,10,15>) ? %s"), BOOL(testVector != copiedVector && (testVector == Vector3(99,99,15) || testVector == Vector3(5,10,15))));
		if (!(testVector != copiedVector && (testVector != Vector3(99,99,15) || testVector == Vector3(5,10,15))))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testVector is:  %s"), {testVector.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Arithmetic"));
		testVector = copiedVector + Vector3(5,9,12);
		UNITTESTER_LOG1(testFlags, TXT("<1,2,3> + <5,9,12> = %s"), testVector);
		if (testVector != Vector3(6,11,15))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <6,11,15>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector = copiedVector - Vector3(4, 10, 2);
		UNITTESTER_LOG1(testFlags, TXT("<1,2,3> - <4,10,2> = %s"), testVector);
		if (testVector != Vector3(-3,-8,1))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <-3,-8,1>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector += Vector3(4,6,9);
		UNITTESTER_LOG1(testFlags, TXT("<-3,-8,1> += <4,6,9>  ---> %s"), testVector);
		if (testVector != Vector3(1,-2,10))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1,-2,10>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector += Vector2(4, -4).ToVector3();
		UNITTESTER_LOG(testFlags, TXT("<1,-2,10> += <4,-4>"));
		if (testVector != Vector3(5, -6, 10))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <5, -6, 10>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector = Vector3(5.f, 8.f, 6.f) * 2.f;
		UNITTESTER_LOG1(testFlags, TXT("<5,8,6> * 2 = %s"), testVector);
		if (testVector != Vector3(10,16,12))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <10,16,12>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector /= 8.f;
		UNITTESTER_LOG1(testFlags, TXT("<10,16,12> /=8  ---> %s"), testVector);
		if (testVector != Vector3(1.25, 2, 1.5))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1.25,2,1.5>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector *= 10.f;
		UNITTESTER_LOG1(testFlags, TXT("<1.25,2,1.5> *=10  ---> %s"), testVector);
		if (testVector != Vector3(12.5, 20, 15))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <12.5,20,15>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}

		testVector += ((Vector2(5.f,5.f).ToVector3() - copiedVector) * 4.f) + Vector3(-10.f,-15.f,20.f);
		UNITTESTER_LOG1(testFlags, TXT("<12.5,20,15> += ((<5,5,0> - <1,2,3>) * 4) + <-10,-15,20>    results in:  %s"), testVector);
		if (testVector != Vector3(18.5, 17, 23))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <18.5,17,23>.  It's currently:  %s"), {testVector.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Utility"));
		testVector = Vector3(5,5,-10);
		UNITTESTER_LOG1(testFlags, TXT("Vector Length of <5,5,-10> is:  %s"), testVector.VSize());
		if (abs(testVector.VSize().Value - 12.24744871) > 0.000001)
		{
			UnitTestError(testFlags, TXT("VSize function failed.  Expected length of vector<5,5,-10> is ~12.24744871.  Expected precision is:  10^-6."));
			return false;
		}

		testVector.Normalize();
		UNITTESTER_LOG1(testFlags, TXT("Normalize<5,5,-10> results in:  %s"), testVector);
		if (testVector.X - 0.40824829f < 0.000001f && testVector.Y - 0.40824829f > 0.000001f && testVector.Z + 0.816496581f > 0.000001f)
		{
			UnitTestError(testFlags, TXT("Normalize vector function failed.  Expected resulting vector is:  ~<0.408248, 0.408248, -0.816497> with minimum precision of 10^-6."));
			return false;
		}

		testVector = Vector3(0, 0, -15);
		testVector.SetLengthTo(4.25);
		UNITTESTER_LOG1(testFlags, TXT("Setting Length of <0,0,-15> to 4.25 results in:  %s"), testVector);
		if (testVector.VSize() != 4.25f || testVector != Vector3(0, 0, -4.25f))
		{
			UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<0,0,-15> to 4.25 should have resulted in <0,0,4.25>.  The VSize of %s is %s instead."), {testVector.ToString(), testVector.VSize().ToString()});
			return false;
		}

		testVector = Vector3(-84.3f, 148.2f, 76.7f);
		testVector.SetLengthTo(20.4f);
		UNITTESTER_LOG1(testFlags, TXT("Setting length of <-84.3, 148.2, 76.7> to 20.4 results in:  %s"), testVector);
		if (FLOAT::Abs(testVector.VSize() - 20.4f) > 0.00001f || (testVector - Vector3(-9.198518f, 16.171061f, 8.369233f)).VSize() > 0.00001f)
		{
			UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<-84.3, 148.2, 76.7> to 20.4 should have resulted in ~<-9.198518249, 16.17106055, 8.369233093>.  The VSize of %s is %s instead."), {testVector.ToString(), testVector.VSize().ToString()});
			return false;
		}

		testVector = Vector3(5,5,-10);
		Vector3 otherVector = Vector3(-11,-13,17);
		UNITTESTER_LOG1(testFlags, TXT("<5,5,-10> dot <-11,-13,17> = %s"), testVector.Dot(otherVector));
		if (testVector.Dot(otherVector) != -290)
		{
			UnitTestError(testFlags, TXT("Dot product of vectors %s and %s failed.  Expected value is -290."), {testVector.ToString(), otherVector.ToString()});
			return false;
		}

		//Example is based on:  Khan Academy.  Great place to brush up on vector math!
		testVector = Vector3(1, -7, 1);
		otherVector = Vector3(5, 2, 4);
		UNITTESTER_LOG1(testFlags, TXT("<1,-7,1> cross <5,2,4> = %s"), testVector.CrossProduct(otherVector));
		if (testVector.CrossProduct(otherVector) != Vector3(-30, 1, 37))
		{
			UnitTestError(testFlags, TXT("Cross product of vectors %s and %s failed.  Expected value is <-30,1,37>."), {testVector.ToString(), otherVector.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);
		ExecuteSuccessSequence(testFlags, TXT("Vector3"));

		return true;
	}

	bool DatatypeUnitTester::TestRectangle (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Rectangle"));

		Rectangle<FLOAT> blankRectangle;
		Rectangle<FLOAT> initializedRectangle(0, 5, 10, 15);
		Rectangle<FLOAT> copiedRectangle(initializedRectangle);

		UNITTESTER_LOG1(testFlags, TXT("Constructor tests:  BlankRectangle=%s"), blankRectangle);
		UNITTESTER_LOG1(testFlags, TXT("    initializedRectangle(Left(0), Top(5), Right(10), Bottom(15))= %s"), initializedRectangle);
		UNITTESTER_LOG1(testFlags, TXT("    copiedRectangle from initializedRectangle= %s"), copiedRectangle);

		SetTestCategory(testFlags, TXT("Comparison"));
		UNITTESTER_LOG1(testFlags, TXT("blankRectangle != Rect(0,0,0,0)?  %s"), BOOL(blankRectangle != Rectangle<FLOAT>(0,0,0,0)));
		if (blankRectangle != Rectangle<FLOAT>(0,0,0,0))
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  blankRectangle is not zero.  Instead it's:  %s"), {blankRectangle.ToString()});
			return false;
		}

		UNITTESTER_LOG3(testFlags, TXT("initializedRectangle(%s) != copiedRectangle(%s) ? %s"), initializedRectangle, copiedRectangle, BOOL(initializedRectangle != copiedRectangle));
		if (initializedRectangle != copiedRectangle)
		{
			UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRectangle != copiedRectangle.  Values are:  %s and %s"), {initializedRectangle.ToString(), copiedRectangle.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Arithmetic"));
		Rectangle<FLOAT> baseRectangle(0,0,10,10);
		Rectangle<FLOAT> testRectangle = baseRectangle + Rectangle<FLOAT>(5,5,20,20);
		UNITTESTER_LOG1(testFlags, TXT("[0,0,10,10] + [5,5,20,20] = %s"), testRectangle);
		if (testRectangle != Rectangle<FLOAT>(5,5,30,30))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRectangle is [5,5,30,30].  It's currently:  %s"), {testRectangle.ToString()});
			return false;
		}

		testRectangle = baseRectangle;
		testRectangle += Rectangle<FLOAT>(0, 4, 35, 50);
		UNITTESTER_LOG1(testFlags, TXT("[0,0,10,10] += [0,4,35,50]  ---> %s"), testRectangle);
		if (testRectangle != Rectangle<FLOAT>(0,4,45,60))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRectangle is [0,4,45,60].  It's currently:  %s"), {testRectangle.ToString()});
			return false;
		}

		testRectangle = initializedRectangle * 2.f;
		UNITTESTER_LOG1(testFlags, TXT("[0,5,10,15] * 2 = %s"), testRectangle);
		if (testRectangle != Rectangle<FLOAT>(0,10,20,30))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRectangle is [0,10,20,30].  It's currently:  %s"), {testRectangle.ToString()});
			return false;
		}

		testRectangle /= 4.f;
		UNITTESTER_LOG1(testFlags, TXT("[0,10,20,30] /= 4  ---> %s"), testRectangle);
		if (testRectangle != Rectangle<FLOAT>(0,2.5f,5,7.5f))
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRectangle is [0, 2.5, 5, 7.5].  It's currently:  %s"), {testRectangle.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Utility"));
		testRectangle = baseRectangle;
		UNITTESTER_LOG1(testFlags, TXT("Area of [0,0,10,10] is:  %s"), testRectangle.CalculateArea());
		if (testRectangle.CalculateArea() != 100)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Expected value for the area of [0,0,10,10] is 100.  It returned %s instead."), {testRectangle.CalculateArea().ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Perimeter of [0,0,10,10] is:  %s"), testRectangle.CalculatePerimeter());
		if (testRectangle.CalculatePerimeter() != 40)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Expected value for the perimeter of [0,0,10,10] is 40.  It returned %s instead."), {testRectangle.CalculatePerimeter().ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Is Rectangle[0,0,10,10] a square?  %s"), BOOL(testRectangle.IsSquare()));
		if (!testRectangle.IsSquare())
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should have been considered a square."), {testRectangle.ToString()});
			return false;
		}

		Rectangle<FLOAT> tallRectangle = Rectangle<FLOAT>(0,5,5,15);
		UNITTESTER_LOG2(testFlags, TXT("Is Rectangle %s a square?  %s"), tallRectangle, BOOL(tallRectangle.IsSquare()));
		if (tallRectangle.IsSquare())
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should not have been considered a square."), {tallRectangle.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Is Rectangle[0,0,10,10] blank?  %s"), BOOL(testRectangle.IsEmpty()));
		if (testRectangle.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should not have been considered an empty rectangle."), {testRectangle.ToString()});
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Is Rectangle[0,0,0,0] blank?  %s"), BOOL(blankRectangle.IsEmpty()));
		if (!blankRectangle.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should have been considered an empty rectangle."), {blankRectangle.ToString()});
			return false;
		}

		testRectangle = Rectangle<FLOAT>(20, 10, 40, 15);
		UNITTESTER_LOG2(testFlags, TXT("Width of rectangle %s is %s"), testRectangle, testRectangle.GetWidth());
		if (testRectangle.GetWidth() != 20)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  The width of rectangle %s should have returned 20.  Instead it returned %s"), {testRectangle.ToString(), testRectangle.GetWidth().ToString()});
			return false;
		}

		UNITTESTER_LOG2(testFlags, TXT("Height of rectangle %s is %s"), testRectangle, testRectangle.GetHeight());
		if (testRectangle.GetHeight() != 5)
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  The height of rectangle %s should have returned 5.  Instead it returned %s"), {testRectangle.ToString(), testRectangle.GetHeight().ToString()});
			return false;
		}

		Rectangle<FLOAT> otherRectangle(35,14,70,100);
		UNITTESTER_LOG3(testFlags, TXT("Does rectangle %s overlap with rectangle %s ? %s"), testRectangle, otherRectangle, BOOL(testRectangle.Overlaps(otherRectangle)));
		if (!testRectangle.Overlaps(otherRectangle))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Rectangles %s and %s should be overlapping when the function returned false."), {testRectangle.ToString(), otherRectangle.ToString()});
			return false;
		}

		otherRectangle = testRectangle;
		otherRectangle.Top += testRectangle.GetHeight() * 2;
		otherRectangle.Bottom += testRectangle.GetHeight() * 2;
		UNITTESTER_LOG3(testFlags, TXT("Does rectangle %s overlap with rectangle %s ? %s"), testRectangle, otherRectangle, BOOL(testRectangle.Overlaps(otherRectangle)));
		if (testRectangle.Overlaps(otherRectangle))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Rectangles %s and %s should not be overlapping when the function returned true."), {testRectangle.ToString(), otherRectangle.ToString()});
			return false;
		}

		otherRectangle = Rectangle<FLOAT>(5, 12, 30, 35);
		Rectangle<FLOAT> overlappingRectangle = testRectangle.GetOverlappingRectangle(otherRectangle);
		UNITTESTER_LOG3(testFlags, TXT("The resulting overlapping rectangle constructed from rectangles %s and %s is %s"), testRectangle, otherRectangle, overlappingRectangle);
		if (overlappingRectangle != Rectangle<FLOAT>(20, 12, 30, 15))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  The expected resulting overlapping rectangle was [20,12,30,15].  The overlapping rectangle is currently:  %s"), {overlappingRectangle.ToString()});
			return false;
		}
		CompleteTestCategory(testFlags);
		ExecuteSuccessSequence(testFlags, TXT("Rectangle"));

		return true;
	}

	bool DatatypeUnitTester::TestSDFunction (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("SDFunction"));

		SDFunctionTester* functionTester = SDFunctionTester::CreateObject();
		if (!VALID_OBJECT(functionTester))
		{
			UnitTestError(testFlags, TXT("Function unit test failed.  Failed to instantiate SDFunctionTester object."));
			return false;
		}

		//This is handled externally since the function test needs to test instanced objects.
		bool bResult = functionTester->RunTest(this, testFlags);
		functionTester->Destroy();

		return bResult;
	}

	#define DATATYPEUNITTESTER_TESTMATRIX_PRERETURN \
		UNITTESTER_LOG1(testFlags, TXT("The matrix unit test was running for %s milliseconds."), matrixTimer.GetElapsedTime());

	bool DatatypeUnitTester::TestMatrix (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Matrix"));

		Stopwatch matrixTimer(TXT("MatrixTimer"));

		Matrix voidMatrix = Matrix::GetVoidMatrix();
		Matrix matrix1x5(1, 5);
		Matrix matrix5x1(5, 1);
		Matrix matrix2x2(2, 2);
		Matrix matrix6x6(6, 6);
		Matrix matrixTooMuchData(3, 3);
		Matrix invalidMatrix(0, 4);

		vector<FLOAT> matrixData;
		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		matrixData.push_back(2.f);
		matrixData.push_back(3.f);
		matrix2x2.Data = matrixData;
		invalidMatrix.Data = matrixData;

		matrixData.push_back(4.f);
		matrix1x5.Data = matrixData;
		matrix5x1.Data = matrixData;

		matrixData.push_back(5.f);
		matrixData.push_back(6.f);
		matrixTooMuchData.Data = matrixData;

		for (FLOAT curValue = 7.f; curValue < 36.f; curValue++)
		{
			matrixData.push_back(curValue);
		}
		matrix6x6.Data = matrixData;

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("Construct tests.  The following matrices were created:"));
			UNITTESTER_LOG1(testFlags, TXT("    voidMatrix:  %s"), voidMatrix);
			voidMatrix.LogMatrix();
			UNITTESTER_LOG1(testFlags, TXT("    1x5 matrix:  %s"), matrix1x5);
			matrix1x5.LogMatrix();
			UNITTESTER_LOG1(testFlags, TXT("    5x1 matrix:  %s"), matrix5x1);
			matrix5x1.LogMatrix();
			UNITTESTER_LOG1(testFlags, TXT("    2x2 matrix:  %s"), matrix2x2);
			matrix2x2.LogMatrix();
			UNITTESTER_LOG1(testFlags, TXT("    6x6 matrix:  %s"), matrix6x6);
			matrix6x6.LogMatrix();
			UNITTESTER_LOG1(testFlags, TXT("    matrix with too much data:  %s"), matrixTooMuchData);
			matrixTooMuchData.LogMatrix();
			UNITTESTER_LOG1(testFlags, TXT("    0x4 matrix:  %s"), invalidMatrix);
			invalidMatrix.LogMatrix();
		}

		SetTestCategory(testFlags, TXT("Matrix Validation"));
		UNITTESTER_LOG(testFlags, TXT("Testing matrix validation.  Of the matrices listed above, only the 0x4 and matrix with too much data should be invalid."));
		if (!matrix1x5.IsValid())
		{
			UnitTestError(testFlags, TXT("Matrix tests failed.  The 1x5 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), {matrix1x5.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (!matrix5x1.IsValid())
		{
			UnitTestError(testFlags, TXT("Matrix tests failed.  The 5x1 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), {matrix5x1.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (!matrix2x2.IsValid())
		{
			UnitTestError(testFlags, TXT("Matrix tests failed.  The 2x2 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), {matrix2x2.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (!matrix6x6.IsValid())
		{
			UnitTestError(testFlags, TXT("Matrix tests failed.  The 6x6 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), {matrix6x6.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (matrixTooMuchData.IsValid())
		{
			UnitTestError(testFlags, TXT("Matrix tests failed.  The matrix with too much data should be an invalid matrix; instead it returned true for this matrix:  %s"), {matrixTooMuchData.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (invalidMatrix.IsValid())
		{
			UnitTestError(testFlags, TXT("Matrix tests failed.  The 0x4 matrix should be an invalid matrix; instead it returned true for this matrix:  %s"), {invalidMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Matrix Comparison"));
		UNITTESTER_LOG1(testFlags, TXT("Is the 1x5 matrix and 5x1 matrix equal?  %s"), BOOL(matrix1x5 == matrix5x1));
		if (matrix1x5 == matrix5x1)
		{
			UnitTestError(testFlags, TXT("Matrix comparison test failed.  Matrix {%s} should have not been equal to {%s}."), {matrix1x5.ToString(), matrix5x1.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Is the 6x6 matrix equal to itself?  %s"), BOOL(matrix6x6 == matrix6x6));
		if (matrix6x6 != matrix6x6)
		{
			UnitTestError(testFlags, TXT("Matrix comparison test failed.  The 6x6 matrix should have been equal to itself."));
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		Matrix other2x2(matrix2x2);
		UNITTESTER_LOG2(testFlags, TXT("Created a copy of {%s}.  Is that copy equal to the 2x2 matrix?  %s"), matrix2x2, BOOL(matrix2x2 == other2x2));
		if (matrix2x2 != other2x2)
		{
			UnitTestError(testFlags, TXT("Matrix comparison test failed.  The 2x2 matrix {%s} should have been equal to its copy."), {matrix2x2.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		other2x2.Data.at(1) = 100;
		UNITTESTER_LOG3(testFlags, TXT("Changed a value to the 2x2 copy.  Is {%s} equal to {%s} ?  %s"), matrix2x2, other2x2, BOOL(matrix2x2 == other2x2));
		if (matrix2x2 == other2x2)
		{
			UnitTestError(testFlags, TXT("Matrix comparison test failed.  The 2x2 matrix {%s} should have not been equal to {%s}."), {matrix2x2.ToString(), other2x2.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Matrix Accessors"));
		Matrix accessor(3, 4);
		matrixData.clear();
		for (unsigned int i = 0; i < 12; i++)
		{
			matrixData.push_back(FLOAT::MakeFloat(i));
		}
		/*
		[0	3	6	9 ]
		[1	4	7	10]
		[2	5	8	11]
		*/
		accessor.Data = matrixData;
		FLOAT dataElement = accessor.GetDataElement(1, 2);
		UNITTESTER_LOG2(testFlags, TXT("Retrieving data element from 2nd row and 3rd column from matrix {%s} returned %s"), accessor, dataElement);
		if (dataElement != 7.f)
		{
			UnitTestError(testFlags, TXT("Matrix accessor test failed.  Retrieving the data element at the 2nd row, 3rd column from matrix {%s} should have returned 7.  Instead it found %s"), {accessor.ToString(), dataElement.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		matrixData.clear();
		matrixData = accessor.GetColumn(3);
		vector<FLOAT> accessorAnsKey;
		accessorAnsKey.push_back(9.f);
		accessorAnsKey.push_back(10.f);
		accessorAnsKey.push_back(11.f);
		UNITTESTER_LOG2(testFlags, TXT("Retrieving the last column from matrix {%s} returned %s"), accessor, Utils::ListToString(matrixData));
		if (matrixData != accessorAnsKey)
		{
			UnitTestError(testFlags, TXT("Matrix accessor test failed.  Retrieving the last column from matrix {%s} should have resulted in %s.  Instead it found %s"), {accessor.ToString(), Utils::ListToString(accessorAnsKey).ToString(), Utils::ListToString(matrixData).ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		matrixData.clear();
		matrixData = accessor.GetRow(1);
		accessorAnsKey.clear();
		accessorAnsKey.push_back(1.f);
		accessorAnsKey.push_back(4.f);
		accessorAnsKey.push_back(7.f);
		accessorAnsKey.push_back(10.f);
		UNITTESTER_LOG2(testFlags, TXT("Retrieving the second row from matrix {%s} returned %s"), accessor, Utils::ListToString(matrixData));
		if (matrixData != accessorAnsKey)
		{
			UnitTestError(testFlags, TXT("Matrix accessor test failed.  Retrieving the second row from matrix {%s} should have resulted in %s.  Instead it found %s"), {accessor.ToString(), Utils::ListToString(accessorAnsKey).ToString(), Utils::ListToString(matrixData).ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Matrix Operations"));
		Matrix a(2, 4);
		Matrix b(2, 4);
		matrixData.clear();
		matrixData.push_back(5.f);
		matrixData.push_back(12.f);
		matrixData.push_back(0.52f);
		matrixData.push_back(-15.f);
		matrixData.push_back(-4.5f);
		matrixData.push_back(9.2f);
		matrixData.push_back(18.1f);
		matrixData.push_back(-92.68f);
		a.Data = matrixData;

		vector<FLOAT> results = matrixData;

		matrixData.clear();
		matrixData.push_back(3.f);
		matrixData.push_back(16.f);
		matrixData.push_back(0.61f);
		matrixData.push_back(-12.f);
		matrixData.push_back(-8.2f);
		matrixData.push_back(5.5f);
		matrixData.push_back(29.9f);
		matrixData.push_back(92.68f);
		b.Data = matrixData;

		for (unsigned int i = 0; i < matrixData.size(); i++)
		{
			results.at(i) += matrixData.at(i);
		}

		Matrix resultMatrix(2, 4);
		Matrix answerKey(2, 4);
		answerKey.Data = results;
		resultMatrix = a + b;
		UNITTESTER_LOG3(testFlags, TXT("The sum of %s and %s equals %s"), a, b, resultMatrix);
		if (resultMatrix != answerKey)
		{
			UnitTestError(testFlags, TXT("Matrix operator test failed.  The sum of %s and %s should have resulted in %s.  Instead it's %s"), {a.ToString(), b.ToString(), answerKey.ToString(), resultMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		for (unsigned int i = 0; i < a.Data.size(); i++)
		{
			answerKey.Data.at(i) = a.Data.at(i) - b.Data.at(i);
		}
		resultMatrix = a - b;
		UNITTESTER_LOG3(testFlags, TXT("The difference of %s and %s equals %s"), a, b, resultMatrix);
		if (resultMatrix != answerKey)
		{
			UnitTestError(testFlags, TXT("Matrix operator test failed.  The difference of %s and %s should have resulted in %s.  Instead it's %s"), {a.ToString(), b.ToString(), answerKey.ToString(), resultMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		FLOAT scaler = 4.5f;
		for (unsigned int i = 0; i < answerKey.Data.size(); i++)
		{
			answerKey.Data.at(i) *= 4.5f;
		}
		Matrix oldResult(resultMatrix);
		resultMatrix *= scaler;
		UNITTESTER_LOG3(testFlags, TXT("Scaling matrix %s by %s equals %s"), oldResult, scaler, resultMatrix);
		if (resultMatrix != answerKey)
		{
			UnitTestError(testFlags, TXT("Matrix operator test failed.  Scaling matrix %s by %s should have resulted in %s.  Instead it's %s"), {oldResult.ToString(), scaler.ToString(), answerKey.ToString(), resultMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		Matrix aMultiply(2, 2);
		Matrix bMultiply(2, 2);
		Matrix multiplyAnsKey(2, 2);
		Matrix multiplyResult(2, 2);
		matrixData.clear();
		matrixData.push_back(4.f);
		matrixData.push_back(9.f);
		matrixData.push_back(2.5f);
		matrixData.push_back(-2.f);
		aMultiply.Data = matrixData;

		matrixData.clear();
		matrixData.push_back(6.f);
		matrixData.push_back(7.f);
		matrixData.push_back(4.25f);
		matrixData.push_back(-4.f);
		bMultiply.Data = matrixData;

		matrixData.clear();
		matrixData.push_back(41.5f);
		matrixData.push_back(40.f);
		matrixData.push_back(7.f);
		matrixData.push_back(46.25f);
		multiplyAnsKey.Data = matrixData;

		multiplyResult = aMultiply * bMultiply;
		//Round off accumulated float precision errors
		MATRIX_DATA_MACRO(aMultiply, aMultiply.Data.at(i).Roundf(3););
		MATRIX_DATA_MACRO(bMultiply, bMultiply.Data.at(i).Roundf(3););
		MATRIX_DATA_MACRO(multiplyAnsKey, multiplyAnsKey.Data.at(i).Roundf(3););
		MATRIX_DATA_MACRO(multiplyResult, multiplyResult.Data.at(i).Roundf(3););
		UNITTESTER_LOG3(testFlags, TXT("Multiplying %s with %s is equal to %s"), aMultiply, bMultiply, multiplyResult);
		if (multiplyResult != multiplyAnsKey)
		{
			UnitTestError(testFlags, TXT("Matrix operator test failed.  Multiplying %s with %s should have resulted in %s.  Instead it's equal to %s."), {aMultiply.ToString(), bMultiply.ToString(), multiplyAnsKey.ToString(), multiplyResult.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		Matrix aRectMultiply(3, 2);
		Matrix bRectMultiply(2, 3);
		Matrix rectMultAnsKey(3, 3);
		Matrix rectMultResult(3, 3);

		matrixData.clear();
		matrixData.push_back(2.f);
		matrixData.push_back(4.f);
		matrixData.push_back(6.5f);
		matrixData.push_back(8.f);
		matrixData.push_back(-10.f);
		matrixData.push_back(-18.23f);
		aRectMultiply.Data = matrixData;

		matrixData.clear();
		matrixData.push_back(18.2f);
		matrixData.push_back(-3.f);
		matrixData.push_back(-12.7f);
		matrixData.push_back(2.f);
		matrixData.push_back(100.f);
		matrixData.push_back(12.5f);
		bRectMultiply.Data = matrixData;

		matrixData.clear();
		matrixData.push_back(12.4f);
		matrixData.push_back(102.8f);
		matrixData.push_back(172.99f);
		matrixData.push_back(-9.4f);
		matrixData.push_back(-70.8f);
		matrixData.push_back(-119.01f);
		matrixData.push_back(300.f);
		matrixData.push_back(275.f);
		matrixData.push_back(422.125f);
		rectMultAnsKey.Data = matrixData;

		rectMultResult = aRectMultiply * bRectMultiply;
		//Round off accumulated float precision errors
		MATRIX_DATA_MACRO(aRectMultiply, aRectMultiply.Data.at(i).Roundf(3););
		MATRIX_DATA_MACRO(bRectMultiply, bRectMultiply.Data.at(i).Roundf(3););
		MATRIX_DATA_MACRO(rectMultAnsKey, rectMultAnsKey.Data.at(i).Roundf(3););
		MATRIX_DATA_MACRO(rectMultResult, rectMultResult.Data.at(i).Roundf(3););
		UNITTESTER_LOG3(testFlags, TXT("Multiplying %s with %s is equal to %s"), aRectMultiply, bRectMultiply, rectMultResult);

		//comparing operators
		for (unsigned int i = 0; i < rectMultResult.Data.size(); i++)
		{
			if (rectMultResult.Data.at(i) != rectMultAnsKey.Data.at(i))
			{
				UnitTestError(testFlags, TXT("Elements:  %s and %s do not match."), {rectMultResult.Data.at(i).ToString(), rectMultAnsKey.Data.at(i).ToString()});
				DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
				return false;
			}
		}

		if (rectMultResult != rectMultAnsKey)
		{
			UnitTestError(testFlags, TXT("Matrix operator test failed.  Multiplying %s with %s should have resulted in %s.  Instead it's equal to %s."), {aRectMultiply.ToString(), bRectMultiply.ToString(), rectMultAnsKey.ToString(), rectMultResult.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}
		CompleteTestCategory(testFlags);

		SetTestCategory(testFlags, TXT("Matrix Utilities"));
		UNITTESTER_LOG1(testFlags, TXT("Is a 2x2 matrix a square?  %s"), BOOL(matrix2x2.IsSquare()));
		if (!matrix2x2.IsSquare())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  A 2x2 matrix should be considered a square matrix but IsSquare returned false."));
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		UNITTESTER_LOG1(testFlags, TXT("Is 1x5 matrix a square?  %s"), BOOL(matrix1x5.IsSquare()));
		if (matrix1x5.IsSquare())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  A 1x5 matrix should not have been considered a square matrix, but IsSquare returned true."));
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		//Test upper/lower triangular, unitriangular, and unit matrices.
		/*
		[5	5	5]
		[0	5	5]
		[0	0	5]
		*/
		matrixData.clear();
		matrixData.push_back(5.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(5.f);
		matrixData.push_back(5.f);
		matrixData.push_back(0.f);
		matrixData.push_back(5.f);
		matrixData.push_back(5.f);
		matrixData.push_back(5.f);
		Matrix upperTriangle(3, 3, matrixData);

		/*
		[5	0	0]
		[5	5	0]
		[5	5	5]
		*/
		matrixData.clear();
		matrixData.push_back(5.f);
		matrixData.push_back(5.f);
		matrixData.push_back(5.f);
		matrixData.push_back(0.f);
		matrixData.push_back(5.f);
		matrixData.push_back(5.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(5.f);
		Matrix lowerTriangle(3, 3, matrixData);

		/*
		[1	0	0]
		[5	1	0]
		[5	5	1]
		*/
		matrixData.clear();
		matrixData.push_back(1.f);
		matrixData.push_back(5.f);
		matrixData.push_back(5.f);
		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		matrixData.push_back(5.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		Matrix unitriangularMatrix(3, 3, matrixData);

		/*
		[1	0	0]
		[0	1	0]
		[0	0	1]
		*/
		matrixData.at(1) = 0.f;
		matrixData.at(2) = 0.f;
		matrixData.at(5) = 0.f;
		Matrix unitMatrix(3, 3, matrixData);

		UNITTESTER_LOG(testFlags, TXT("Constructed the following matrices:  upperTriangle, lowerTriangle, unitriangularMatrix, unitMatrix respectively. . ."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			lowerTriangle.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("-------------------"));
			upperTriangle.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("-------------------"));
			unitriangularMatrix.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("-------------------"));
			unitMatrix.LogMatrix();
		}

		//Test lower triangular matrix function
		UNITTESTER_LOG4(testFlags, TXT("lowerTriangle actually a lower triangular matrix?  %s.  upperTriangle actually a lower triangular matrix?  %s.  UnitriangularMatrix actually a lower triangle matrix?  %s.  Unit matrix actually a lower triangle matrix?  %s."), BOOL(lowerTriangle.IsLowerTriangularMatrix()), BOOL(upperTriangle.IsLowerTriangularMatrix()), BOOL(unitriangularMatrix.IsLowerTriangularMatrix()), BOOL(unitMatrix.IsLowerTriangularMatrix()));
		if (!lowerTriangle.IsLowerTriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered a lower triangular matrix.  Instead it returned false."), {lowerTriangle.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (upperTriangle.IsLowerTriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered a lower triangular matrix.  Instead it returned true."), {upperTriangle.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		//Skipping lower triangle check for unitriangular matrix since it can be either lower or upper triangular

		if (!unitMatrix.IsLowerTriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered a lower triangular matrix.  Instead it returned false."), {unitMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		//Test upper triangular matrix function
		UNITTESTER_LOG4(testFlags, TXT("lowerTriangle actually an upper triangular matrix?  %s.  upperTriangle actually an upper triangular matrix?  %s.  UnitriangularMatrix actually a lower triangle matrix?  %s.  Unit matrix actually a lower triangle matrix?  %s."), BOOL(lowerTriangle.IsUpperTriangularMatrix()), BOOL(upperTriangle.IsUpperTriangularMatrix()), BOOL(unitriangularMatrix.IsUpperTriangularMatrix()), BOOL(unitMatrix.IsUpperTriangularMatrix()));
		if (lowerTriangle.IsUpperTriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an upper triangular matrix.  Instead it returned true."), {lowerTriangle.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (!upperTriangle.IsUpperTriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an upper triangular matrix.  Instead it returned false."), {upperTriangle.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		//Skipping upper triangle check for unitriangular matrix since it can be either lower or upper triangular

		if (!unitMatrix.IsUpperTriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an upper triangular matrix.  Instead it returned false."), {unitMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		//Test unitriangular matrix function
		UNITTESTER_LOG4(testFlags, TXT("lowerTriangle actually an unitriangular matrix?  %s.  upperTriangle actually an unitriangular matrix?  %s.  UnitriangularMatrix actually an unitriangular matrix?  %s.  Unit matrix actually an unitriangular matrix?  %s."), BOOL(lowerTriangle.IsUnitriangularMatrix()), BOOL(upperTriangle.IsUnitriangularMatrix()), BOOL(unitriangularMatrix.IsUnitriangularMatrix()), BOOL(unitMatrix.IsUnitriangularMatrix()));
		if (lowerTriangle.IsUnitriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unitriangular matrix.  Instead it returned true."), {lowerTriangle.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (upperTriangle.IsUnitriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unitriangular matrix.  Instead it returned true."), {upperTriangle.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (!unitriangularMatrix.IsUnitriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an unitriangular matrix.  Instead it returned false."), {unitriangularMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (!unitMatrix.IsUnitriangularMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an unitriangular matrix.  Instead it returned false."), {unitMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		//Test unit matrix function
		UNITTESTER_LOG4(testFlags, TXT("lowerTriangle actually an unit matrix?  %s.  upperTriangle actually an unit matrix?  %s.  UnitriangularMatrix actually an unit matrix?  %s.  Unit matrix actually an unit matrix?  %s."), BOOL(lowerTriangle.IsUnitMatrix()), BOOL(upperTriangle.IsUnitMatrix()), BOOL(unitriangularMatrix.IsUnitMatrix()), BOOL(unitMatrix.IsUnitMatrix()));
		if (lowerTriangle.IsUnitMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unit matrix.  Instead it returned true."), {lowerTriangle.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (upperTriangle.IsUnitMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unit matrix.  Instead it returned true."), {upperTriangle.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (unitriangularMatrix.IsUnitMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unit matrix.  Instead it returned true."), {unitriangularMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (!unitMatrix.IsUnitMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an unit matrix.  Instead it returned false."), {unitMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		Matrix zeroMatrix = matrix6x6.GetZeroMatrix();
		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("The zero matrix of the 6x6 matrix listed below is listed below."));
			matrix6x6.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("--- Zero Matrix listed below ---"));
			zeroMatrix.LogMatrix();
		}

		for (unsigned int i = 0; i < 36; i++)
		{
			if (zeroMatrix.Data.at(i) != 0.f)
			{
				UnitTestError(testFlags, TXT("Matrix utility test failed.  All elements of a zero matrix should be zero.  Instead element %s is not zero in matrix:  {%s}"), {INT(i).ToString(), zeroMatrix.ToString()});
				DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
				return false;
			}
		}

		Matrix identityMatrix(6, 6);
		Matrix ansKey6x6(6, 6);

		/*
		[1	0	0	0	0	0]
		[0	1	0	0	0	0]
		[0	0	1	0	0	0]
		[0	0	0	1	0	0]
		[0	0	0	0	1	0]
		[0	0	0	0	0	1]
		*/
		//I know you can populate matrixData programmatically within a single loop, but that'll kind of defeat the purpose of this unit test.  If the order of the elements change in some way, this must be manually be updated.  Otherwise, might as well copy/paste the matrix::GetIdentity source.
		matrixData.clear();
		matrixData.push_back(1.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);

		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);

		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);

		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);

		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		matrixData.push_back(0.f);

		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		ansKey6x6.Data = matrixData;

		matrix6x6.GetIdentityMatrix(identityMatrix);
		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("The identity matrix of the 6x6 matrix listed below is listed below."));
			matrix6x6.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("--- Identity Matrix listed below ---"));
			identityMatrix.LogMatrix();
		}

		if (identityMatrix != ansKey6x6)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The identity matrix of the 6x6 matrix should have been {%s}.  Instead it returned {%s}"), {ansKey6x6.ToString(), identityMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		Matrix coMatrix = matrix6x6.GetComatrix();
		/*
		[0	-6	12	-18	24	-30]
		[-1	7	-13	19	-25	31]
		[2	-8	14	-20	26	-32]
		[-3	9	-15	21	-27	33]
		[4	-10	16	-22	28	-34]
		[-5	11	-17	23	-29	35]
		*/
		//I know you can populate matrixData programmatically within a single loop, but that'll kind of defeat the purpose of this unit test.  If the order of the elements change in some way, this must be manually be updated.  Otherwise, might as well copy/paste the matrix::GetIdentity source.
		matrixData.clear();
		matrixData.push_back(0.f);
		matrixData.push_back(-1.f);
		matrixData.push_back(2.f);
		matrixData.push_back(-3.f);
		matrixData.push_back(4.f);
		matrixData.push_back(-5.f);

		matrixData.push_back(-6.f);
		matrixData.push_back(7.f);
		matrixData.push_back(-8.f);
		matrixData.push_back(9.f);
		matrixData.push_back(-10.f);
		matrixData.push_back(11.f);

		matrixData.push_back(12.f);
		matrixData.push_back(-13.f);
		matrixData.push_back(14.f);
		matrixData.push_back(-15.f);
		matrixData.push_back(16.f);
		matrixData.push_back(-17.f);

		matrixData.push_back(-18.f);
		matrixData.push_back(19.f);
		matrixData.push_back(-20.f);
		matrixData.push_back(21.f);
		matrixData.push_back(-22.f);
		matrixData.push_back(23.f);

		matrixData.push_back(24.f);
		matrixData.push_back(-25.f);
		matrixData.push_back(26.f);
		matrixData.push_back(-27.f);
		matrixData.push_back(28.f);
		matrixData.push_back(-29.f);

		matrixData.push_back(-30.f);
		matrixData.push_back(31.f);
		matrixData.push_back(-32.f);
		matrixData.push_back(33.f);
		matrixData.push_back(-34.f);
		matrixData.push_back(35.f);
		ansKey6x6.Data = matrixData;

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("The comatrix of the 6x6 matrix listed below is listed below."));
			matrix6x6.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("--- Comatrix listed below ---"));
			coMatrix.LogMatrix();
		}

		if (coMatrix != ansKey6x6)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The comatrix of the 6x6 matrix should have been {%s}.  Instead it returned {%s}"), {ansKey6x6.ToString(), coMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		/*
		[0	6	12	18	30]
		[1	7	13	19	31]
		[2	8	14	20	32]
		[4	10	16	22	34]
		[5	11	17	23	35]
		*/
		Matrix subMatrix = matrix6x6.GetSubMatrix(3, 4);
		Matrix subMatrixAns(5, 5);

		matrixData.clear();
		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		matrixData.push_back(2.f);
		matrixData.push_back(4.f);
		matrixData.push_back(5.f);

		matrixData.push_back(6.f);
		matrixData.push_back(7.f);
		matrixData.push_back(8.f);
		matrixData.push_back(10.f);
		matrixData.push_back(11.f);

		matrixData.push_back(12.f);
		matrixData.push_back(13.f);
		matrixData.push_back(14.f);
		matrixData.push_back(16.f);
		matrixData.push_back(17.f);

		matrixData.push_back(18.f);
		matrixData.push_back(19.f);
		matrixData.push_back(20.f);
		matrixData.push_back(22.f);
		matrixData.push_back(23.f);

		matrixData.push_back(30.f);
		matrixData.push_back(31.f);
		matrixData.push_back(32.f);
		matrixData.push_back(34.f);
		matrixData.push_back(35.f);
		subMatrixAns.Data = matrixData;
		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("The sub matrix of the 6x6 matrix (listed below) at row idx 3 column idx 4 is listed below."));
			matrix6x6.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("--- Submatrix listed below ---"));
			subMatrix.LogMatrix();
		}

		if (subMatrix != subMatrixAns)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix is not equal to the expected matrix.  See logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				UNITTESTER_LOG(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix at row index 3 and column index 4 should have been the matrix below."));
				subMatrixAns.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT("Instead, the sub matrix is. . ."));
				subMatrix.LogMatrix();
			}

			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		FLOAT deter2x2 = -2.f;
		UNITTESTER_LOG2(testFlags, TXT("The determinant of {%s} is %s"), matrix2x2, matrix2x2.CalculateDeterminant());
		if (matrix2x2.CalculateDeterminant() != deter2x2)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The determinant for a matrix {%s} should have been %s.  Instead it returned %s"), {matrix2x2.ToString(), deter2x2.ToString(), matrix2x2.CalculateDeterminant().ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		//Example given is based from KhanAcademy.org:  https://www.khanacademy.org/math/precalculus/precalc-matrices/determinants-and-inverses-of-large-matrices/v/finding-the-determinant-of-a-3x3-matrix-method-1
		Matrix deterMatrix3x3(3, 3);
		matrixData.clear();
		matrixData.push_back(4.f);
		matrixData.push_back(4.f);
		matrixData.push_back(-2.f);
		matrixData.push_back(-1.f);
		matrixData.push_back(5.f);
		matrixData.push_back(0.f);
		matrixData.push_back(1.f);
		matrixData.push_back(3.f);
		matrixData.push_back(0.f);
		deterMatrix3x3.Data = matrixData;
		FLOAT deter3x3Ans = 16.f;
		FLOAT deter3x3 = deterMatrix3x3.CalculateDeterminant();

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG1(testFlags, TXT("The determinant of the matrix listed below is:  %s"), deter3x3);
			deterMatrix3x3.LogMatrix();
		}

		if (deter3x3 != deter3x3Ans)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The determinant for a matrix {%s} should have been %s.  Instead it returned %s"), {deterMatrix3x3.ToString(), deter3x3Ans.ToString(), deter3x3.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		//Answer to the given matrix was generated from:  http://www.wolframalpha.com/input/?i=determinant
		/*
		[15		-7		1.4		-2.8]
		[-8.2	12		-14		4.6 ]
		[9.1	17.3	5		-22 ]
		[-12	-3.25	-10		7   ]
		*/
		Matrix deterMatrix4x4(4, 4);
		matrixData.clear();
		matrixData.push_back(15.f);
		matrixData.push_back(-8.2f);
		matrixData.push_back(9.1f);
		matrixData.push_back(-12.f);
		matrixData.push_back(-7.f);
		matrixData.push_back(12.f);
		matrixData.push_back(17.3f);
		matrixData.push_back(-3.25f);
		matrixData.push_back(1.4f);
		matrixData.push_back(-14.f);
		matrixData.push_back(5.f);
		matrixData.push_back(-10.f);
		matrixData.push_back(-2.8f);
		matrixData.push_back(4.6f);
		matrixData.push_back(-22.f);
		matrixData.push_back(7.f);
		deterMatrix4x4.Data = matrixData;
		FLOAT deter4x4Ans = -42104.237f;
		FLOAT deter4x4 = deterMatrix4x4.CalculateDeterminant();
		deter4x4.Roundf(3); //Clear any accumulated precision errors

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG1(testFlags, TXT("The determinant of the matrix listed below is:  %s"), deter4x4);
			deterMatrix4x4.LogMatrix();
		}

		if (deter4x4 != deter4x4Ans)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The determinant for a matrix {%s} should have been %s.  Instead it returned %s"), {deterMatrix4x4.ToString(), deter4x4Ans.ToString(), deter4x4.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		//Example given is based from KhanAcademy.org:  https://www.khanacademy.org/math/precalculus/precalc-matrices/determinants-and-inverses-of-large-matrices/v/inverting-3x3-part-1-calculating-matrix-of-minors-and-cofactor-matrix
		/*
		[-1	-2	2]
		[2	1	1]
		[3	4	5]
		*/
		Matrix majorMatrix(3, 3);
		matrixData.clear();
		matrixData.push_back(-1.f);
		matrixData.push_back(2.f);
		matrixData.push_back(3.f);
		matrixData.push_back(-2.f);
		matrixData.push_back(1.f);
		matrixData.push_back(4.f);
		matrixData.push_back(2.f);
		matrixData.push_back(1.f);
		matrixData.push_back(5.f);
		majorMatrix.Data = matrixData;

		/*
		[1		7		5]
		[-18	-11		2]
		[-4		-5		3]
		*/
		Matrix minorMatrixAns(3, 3);
		matrixData.clear();
		matrixData.push_back(1.f);
		matrixData.push_back(-18.f);
		matrixData.push_back(-4.f);
		matrixData.push_back(7.f);
		matrixData.push_back(-11.f);
		matrixData.push_back(-5.f);
		matrixData.push_back(5.f);
		matrixData.push_back(2.f);
		matrixData.push_back(3.f);
		minorMatrixAns.Data = matrixData;

		Matrix minorMatrix(3, 3);
		majorMatrix.GetMatrixOfMinors(minorMatrix);
		MATRIX_DATA_MACRO(minorMatrix, minorMatrix.Data.at(i).Roundf(3);)

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("The matrix of minors for the matrix listed below is listed below."));
			UNITTESTER_LOG(testFlags, TXT("---- Original Matrix ----"));
			majorMatrix.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("---- Matrix of minors ----"));
			minorMatrix.LogMatrix();
		}

		if (minorMatrix != minorMatrixAns)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix of minors for {%s} should have been {%s}.  Instead it returned {%s}"), {majorMatrix.ToString(), minorMatrixAns.ToString(), minorMatrix.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		/*
		[2	-6]
		[8	-4]
		*/
		Matrix adjugate2x2(2, 2);
		matrixData.clear();
		matrixData.push_back(2.f);
		matrixData.push_back(8.f);
		matrixData.push_back(-6.f);
		matrixData.push_back(-4.f);
		adjugate2x2.Data = matrixData;

		/*
		[-4	6]
		[-8	2]
		*/
		Matrix adjugate2x2Ans(2, 2);
		matrixData.clear();
		matrixData.push_back(-4.f);
		matrixData.push_back(-8.f);
		matrixData.push_back(6.f);
		matrixData.push_back(2.f);
		adjugate2x2Ans.Data = matrixData;

		Matrix adjugate2x2Result(2, 2);
		adjugate2x2.CalculateAdjugate(adjugate2x2Result);

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("---- The adjugate of this matrix ----"));
			adjugate2x2.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("---- is ----"));
			adjugate2x2Result.LogMatrix();
		}

		if (adjugate2x2Result != adjugate2x2Ans)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The adjugate of matrix {%s} should have been {%s}.  Instead it returned {%s}"), {adjugate2x2.ToString(), adjugate2x2Ans.ToString(), adjugate2x2Result.ToString()});
			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		/*
		[3	1	2 ]
		[9	-7	-6]
		[4	8	-5]
		*/
		Matrix adjugate3x3(3, 3);
		matrixData.clear();
		matrixData.push_back(3.f);
		matrixData.push_back(9.f);
		matrixData.push_back(4.f);
		matrixData.push_back(1.f);
		matrixData.push_back(-7.f);
		matrixData.push_back(8.f);
		matrixData.push_back(2.f);
		matrixData.push_back(-6.f);
		matrixData.push_back(-5.f);
		adjugate3x3.Data = matrixData;

		/*
		Answer key generated through:  http://www.wolframalpha.com/input/?i=adjugate
		[83		21		8  ]
		[21		-23		36 ]
		[100	-20		-30]
		*/
		Matrix adjugate3x3Ans(3, 3);
		matrixData.clear();
		matrixData.push_back(83.f);
		matrixData.push_back(21.f);
		matrixData.push_back(100.f);
		matrixData.push_back(21.f);
		matrixData.push_back(-23.f);
		matrixData.push_back(-20.f);
		matrixData.push_back(8.f);
		matrixData.push_back(36.f);
		matrixData.push_back(-30.f);
		adjugate3x3Ans.Data = matrixData;

		Matrix adjugate3x3Result(3, 3);
		adjugate3x3.CalculateAdjugate(adjugate3x3Result);

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("---- The adjugate of this matrix ----"));
			adjugate3x3.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("---- is ----"));
			adjugate3x3Result.LogMatrix();
		}
					
		if (adjugate3x3Result != adjugate3x3Ans)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The adjugate of the matrix is not equal to the expected matrix.  See logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				UNITTESTER_LOG(testFlags, TXT("Matrix utility test failed.  The adjugate of original matrix listed below. . ."));
				adjugate3x3.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
				adjugate3x3Ans.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . Instead it generated this matrix. . ."));
				adjugate3x3Result.LogMatrix();
			}

			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		/*
		[12		5		16		7.1 ]
		[-10	-3		4.5		-3  ]
		[11		6.2		8.5		-9.5]
		[4		9		-6		10.1]
		*/
		Matrix adjugate4x4(4, 4);
		matrixData.clear();
		matrixData.push_back(12.f);
		matrixData.push_back(-10.f);
		matrixData.push_back(11.f);
		matrixData.push_back(4.f);
		matrixData.push_back(5.f);
		matrixData.push_back(-3.f);
		matrixData.push_back(6.2f);
		matrixData.push_back(9.f);
		matrixData.push_back(16.f);
		matrixData.push_back(4.5f);
		matrixData.push_back(8.5f);
		matrixData.push_back(-6.f);
		matrixData.push_back(7.1f);
		matrixData.push_back(-3.f);
		matrixData.push_back(-9.5f);
		matrixData.push_back(10.1f);
		adjugate4x4.Data = matrixData;

		/*
		Answer key generated through:  http://www.wolframalpha.com/input/?i=adjugate
		[-411.99	3032.94		30.3		1218.99 ]
		[659.45		-2749.4		-2051.6		-3209.95]
		[-1256.5	-1558.76	-148.4		280.7   ]
		[-1170.9	322.8		1728		-432.3  ]
		*/
		Matrix adjugate4x4Ans(4, 4);
		matrixData.clear();
		matrixData.push_back(-411.99f);
		matrixData.push_back(659.45f);
		matrixData.push_back(-1256.5f);
		matrixData.push_back(-1170.9f);
		matrixData.push_back(3032.94f);
		matrixData.push_back(-2749.4f);
		matrixData.push_back(-1558.76f);
		matrixData.push_back(322.8f);
		matrixData.push_back(30.3f);
		matrixData.push_back(-2051.6f);
		matrixData.push_back(-148.4f);
		matrixData.push_back(1728.f);
		matrixData.push_back(1218.99f);
		matrixData.push_back(-3209.95f);
		matrixData.push_back(280.7f);
		matrixData.push_back(-432.3f);
		adjugate4x4Ans.Data = matrixData;

		Matrix adjugate4x4Result(4, 4);
		adjugate4x4.CalculateAdjugate(adjugate4x4Result);

		//Remove accumulated precision errors
		MATRIX_DATA_MACRO(adjugate4x4Result, adjugate4x4Result.Data.at(i).Roundf(3);)
		MATRIX_DATA_MACRO(adjugate4x4Ans, adjugate4x4Ans.Data.at(i).Roundf(3);)

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("---- The adjugate of this matrix ----"));
			adjugate4x4.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT("---- is ----"));
			adjugate4x4Result.LogMatrix();
		}

		if (adjugate4x4Result != adjugate4x4Ans)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The adjugate of the matrix is not equal to the expected matrix.  See the logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				UNITTESTER_LOG(testFlags, TXT("Matrix utility test failed.  The adjugate of original matrix listed below. . ."));
				adjugate4x4.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
				adjugate4x4Ans.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . Instead it generated this matrix. . ."));
				adjugate4x4Result.LogMatrix();
			}

			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		/*
		[2	8]
		[4	7]
		*/
		Matrix preInverse2x2(2, 2);
		matrixData.clear();
		matrixData.push_back(2.f);
		matrixData.push_back(4.f);
		matrixData.push_back(8.f);
		matrixData.push_back(7.f);
		preInverse2x2.Data = matrixData;

		/*
		[-7/18	4/9]	=>	[-0.388889	0.444444 ]
		[2/9	-1/9]	=>	[0.222222	-0.111111]
		*/
		Matrix inverse2x2Ans(2, 2);
		matrixData.clear();
		matrixData.push_back(-0.388889f);
		matrixData.push_back(0.222222f);
		matrixData.push_back(0.444444f);
		matrixData.push_back(-0.111111f);
		inverse2x2Ans.Data = matrixData;

		Matrix inverse2x2(2, 2);
		preInverse2x2.CalculateInverse(inverse2x2);

		Matrix inverseIdentity2x2(2, 2);
		inverseIdentity2x2 = preInverse2x2 * inverse2x2;

		//Truncate some decimal places due to comparison ahead
		MATRIX_DATA_MACRO(inverse2x2, inverse2x2.Data.at(i).Roundf(3);)
		MATRIX_DATA_MACRO(inverse2x2Ans, inverse2x2Ans.Data.at(i).Roundf(3);)
		MATRIX_DATA_MACRO(inverseIdentity2x2, inverseIdentity2x2.Data.at(i).Roundf(3);)

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("The inverse of the matrix below. . ."));
			preInverse2x2.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT(". . . is approximately (rounded to 3 decimal places) . . ."));
			inverse2x2.LogMatrix();
		}

		if (inverse2x2 != inverse2x2Ans)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The inverse of the matrix is not equal to the expected matrix.  See the logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				UNITTESTER_LOG(testFlags, TXT("Matrix utility test failed.  The inverse of original matrix listed below. . ."));
				preInverse2x2.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
				inverse2x2Ans.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . Instead it generated this matrix. . ."));
				inverse2x2.LogMatrix();
			}

			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("Multiplying the inverse with the original matrix resulted in the matrix listed below. . ."));
			inverseIdentity2x2.LogMatrix();
		}

		if (!inverseIdentity2x2.IsUnitMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				UNITTESTER_LOG(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix.  The matrix listed below is not classified as an unit matrix."));
				inverseIdentity2x2.LogMatrix();
			}

			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		/*
		[20	23	20]
		[16	28	0]
		[26	24	15]
		*/
		Matrix preInverse3x3(3, 3);
		matrixData.clear();
		matrixData.push_back(20.f);
		matrixData.push_back(16.f);
		matrixData.push_back(26.f);
		matrixData.push_back(23.f);
		matrixData.push_back(28.f);
		matrixData.push_back(24.f);
		matrixData.push_back(20.f);
		matrixData.push_back(0.f);
		matrixData.push_back(15.f);
		preInverse3x3.Data = matrixData;

		/*
		Answer key generated through:  http://www.wolframalpha.com/input/?i=inverse+matrix
		1/4000	[-420	-135	560]	=>	[-0.105	-0.03375	0.14  ]
				[240	220		-320]	=>	[0.06	0.055		-0.08 ]
				[344	-118	-192]	=>	[0.086	-0.0295		-0.048]
		*/
		Matrix inverse3x3Ans(3, 3);
		matrixData.clear();
		matrixData.push_back(-0.105f);
		matrixData.push_back(0.06f);
		matrixData.push_back(0.086f);
		matrixData.push_back(-0.03375f);
		matrixData.push_back(0.055f);
		matrixData.push_back(-0.0295f);
		matrixData.push_back(0.14f);
		matrixData.push_back(-0.08f);
		matrixData.push_back(-0.048f);
		inverse3x3Ans.Data = matrixData;

		Matrix inverse3x3(3, 3);
		preInverse3x3.CalculateInverse(inverse3x3);

		Matrix inverseIdentity3x3(3, 3);
		inverseIdentity3x3 = preInverse3x3 * inverse3x3;

		//Truncate some decimal places due to comparison ahead
		MATRIX_DATA_MACRO(inverse3x3, inverse3x3.Data.at(i).Roundf(3);)
		MATRIX_DATA_MACRO(inverse3x3Ans, inverse3x3Ans.Data.at(i).Roundf(3);)
		MATRIX_DATA_MACRO(inverseIdentity3x3, inverseIdentity3x3.Data.at(i).Roundf(3);)

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("The inverse of the matrix below. . ."));
			preInverse3x3.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT(". . . is approximately (rounded to 3 decimal places) . . ."));
			inverse3x3.LogMatrix();
		}

		if (inverse3x3 != inverse3x3Ans)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The inverse of the matrix is not equal to the expected matrix.  See the logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				UNITTESTER_LOG(testFlags, TXT("Matrix utility test failed.  The inverse of original matrix listed below. . ."));
				preInverse3x3.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
				inverse3x3Ans.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . Instead it generated this matrix. . ."));
				inverse3x3.LogMatrix();
			}

			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("Multiplying the inverse with the original matrix resulted in the matrix listed below. . ."));
			inverseIdentity3x3.LogMatrix();
		}

		if (!inverseIdentity3x3.IsUnitMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				UNITTESTER_LOG(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix.  The matrix listed below is not classified as an unit matrix."));
				inverseIdentity3x3.LogMatrix();
			}

			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		/*
		[1.08	0.15	-0.85	-2.07 ]
		[-7.1	1.5		0.68	-11.8 ]
		[0.62	-1.30	5.4		0.75  ]
		[-8.75	-3.15	-4.5	3.7   ]
		*/
		Matrix preInverse4x4(4, 4);
		matrixData.clear();
		matrixData.push_back(1.08f);
		matrixData.push_back(-7.1f);
		matrixData.push_back(0.62f);
		matrixData.push_back(-8.75f);
		matrixData.push_back(0.15f);
		matrixData.push_back(1.5f);
		matrixData.push_back(-1.30f);
		matrixData.push_back(-3.15f);
		matrixData.push_back(-0.85f);
		matrixData.push_back(0.68f);
		matrixData.push_back(5.4f);
		matrixData.push_back(-4.5f);
		matrixData.push_back(-2.07f);
		matrixData.push_back(-11.8f);
		matrixData.push_back(0.75f);
		matrixData.push_back(3.7f);
		preInverse4x4.Data = matrixData;

		/*
		Answer key generated through:  http://www.wolframalpha.com/input/?i=inverse+matrix
		[0.299873	-0.0590573	0.032073	-0.0270793]
		[-0.896168	0.0762782	-0.312901	-0.194678 ]
		[-0.20763	0.0303889	0.113471	-0.0422456]
		[-0.306317	-0.0377637	-0.0525347	-0.0108882]
		*/
		Matrix inverse4x4Ans(4, 4);
		matrixData.clear();
		matrixData.push_back(0.299873f);
		matrixData.push_back(-0.896168f);
		matrixData.push_back(-0.20763f);
		matrixData.push_back(-0.306317f);
		matrixData.push_back(-0.0590573f);
		matrixData.push_back(0.0762782f);
		matrixData.push_back(0.0303889f);
		matrixData.push_back(-0.0377637f);
		matrixData.push_back(0.032073f);
		matrixData.push_back(-0.312901f);
		matrixData.push_back(0.113471f);
		matrixData.push_back(-0.0525347f);
		matrixData.push_back(-0.0270793f);
		matrixData.push_back(-0.194678f);
		matrixData.push_back(-0.0422456f);
		matrixData.push_back(-0.0108882f);
		inverse4x4Ans.Data = matrixData;

		Matrix inverse4x4(4, 4);
		preInverse4x4.CalculateInverse(inverse4x4);

		Matrix inverseIdentity4x4(4, 4);
		inverseIdentity4x4 = preInverse4x4 * inverse4x4;

		//Truncate some decimal places due to comparison ahead
		MATRIX_DATA_MACRO(inverse4x4, inverse4x4.Data.at(i).Roundf(6);)
		MATRIX_DATA_MACRO(inverse4x4Ans, inverse4x4Ans.Data.at(i).Roundf(6);)
		MATRIX_DATA_MACRO(inverseIdentity4x4, inverseIdentity4x4.Data.at(i).Roundf(6);)

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("The inverse of the matrix below. . ."));
			preInverse4x4.LogMatrix();
			UNITTESTER_LOG(testFlags, TXT(". . . is approximately (rounded to 6 decimal places) . . ."));
			inverse4x4.LogMatrix();
		}

		if (inverse4x4 != inverse4x4Ans)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The inverse of the matrix is not equal to the expected matrix.  See the logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				UNITTESTER_LOG(testFlags, TXT("Matrix utility test failed.  The inverse of original matrix listed below. . ."));
				preInverse4x4.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
				inverse4x4Ans.LogMatrix();
				UNITTESTER_LOG(testFlags, TXT(". . . Instead it generated this matrix. . ."));
				inverse4x4.LogMatrix();
			}

			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}

		if (ShouldHaveDebugLogs(testFlags))
		{
			UNITTESTER_LOG(testFlags, TXT("Multiplying the inverse with the original matrix resulted in the matrix listed below. . ."));
			inverseIdentity4x4.LogMatrix();
		}

		if (!inverseIdentity4x4.IsUnitMatrix())
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				UNITTESTER_LOG(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix.  The matrix listed below is not classified as an unit matrix."));
				inverseIdentity4x4.LogMatrix();
			}

			DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
			return false;
		}
		CompleteTestCategory(testFlags);

		DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
		ExecuteSuccessSequence(testFlags, TXT("Matrix"));

		return true;
	}

	#undef DATATYPEUNITTESTER_TESTMATRIX_PRERETURN
}

#endif
#endif