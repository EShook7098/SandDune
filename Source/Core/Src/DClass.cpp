/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DClass.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	vector<DClass*> DClass::RootClasses = GetPreloadedRootClasses();
	vector<DClass*> DClass::PreloadedRootClasses = GetPreloadedRootClasses();
	vector<DClass::SOrphanInfo> DClass::OrphanClasses = GetPreloadedOrphans();
	vector<DClass::SOrphanInfo> DClass::PreloadedOrphans = GetPreloadedOrphans();

	DClass::DClass (const DString& className)
	{
		ClassName = className;
	}

	DClass::~DClass ()
	{
		switch (ClassType)
		{
			case(CT_Object):
				if (CDO.DefaultObject != nullptr)
				{
					delete CDO.DefaultObject;
					CDO.DefaultObject = nullptr;
				}
				break;

			case(CT_EngineComponent):
				if (CDO.DefaultEngineComponent != nullptr)
				{
					delete CDO.DefaultEngineComponent;
					CDO.DefaultEngineComponent = nullptr;
				}
				break;
		}
	}

	DClass* DClass::LoadClass (const DString& className, const DString& parentClass)
	{
		DClass* newClass = nullptr;

		if (parentClass.IsEmpty()) 
		{
			//Check if root class is already loaded
			if (RetrieveRootClass(className) == nullptr)
			{
				newClass = new DClass(className);
				RegisterRootClass(newClass);
				ClaimOrphans(newClass);
			}
		}
		else
		{
			//find parent
			DClass* pClass = FindClassByName(parentClass);

			if (pClass != nullptr)
			{
				//check if class is already loaded
				for (unsigned int i = 0; i < pClass->Children.size(); i++)
				{
					if (pClass->Children.at(i)->Name() == className)
					{
						//class is already in the tree
						return pClass->Children.at(i);
					}
				}
			}
			else
			{
				//make sure the class isn't already listed as an orphan
				for (unsigned int i = 0; i < DClass::OrphanClasses.size(); i++)
				{
					if (DClass::OrphanClasses.at(i).ParentClass == parentClass&& 
							DClass::OrphanClasses.at(i).ClassInstance->Name() == className)
					{
						//DClass is already pending for the parent class to register
						return DClass::OrphanClasses.at(i).ClassInstance;
					}
				}
			}

			newClass = new DClass(className);

			//pickup any orphans that's waiting for this class
			ClaimOrphans(newClass);

			if (pClass != nullptr)
			{
				//notify parent of new child
				pClass->AddChild(newClass);
				newClass->ParentClass = pClass;
			}
			else
			{
				//add this class to the orphan list
				SOrphanInfo newOrphan;
				newOrphan.ClassInstance = newClass;
				newOrphan.ParentClass = parentClass;
				OrphanClasses.push_back(newOrphan);
				PreloadedOrphans.push_back(newOrphan);
			}
		}

		return newClass;
	}

	void DClass::SetCDO (Object* newObject)
	{
		ClassType = CT_Object;
		if (CDO.DefaultObject == nullptr)
		{
			CDO.DefaultObject = newObject;
		}
	}

	void DClass::SetCDO (EngineComponent* newCDO)
	{
		ClassType = CT_EngineComponent;
		if (CDO.DefaultEngineComponent == nullptr)
		{
			CDO.DefaultEngineComponent = newCDO;
		}
	}

	bool DClass::IsChildOf (const DClass* targetClass) const
	{
		if (targetClass == this)
		{
			return true;
		}
		else if (ParentClass == nullptr)
		{
			return false;
		}

		return (ParentClass->IsChildOf(targetClass));
	}

	bool DClass::IsParentOf (const DClass* targetClass) const
	{
		//It's faster to do it this way since we don't have to iterate through all children.
		return (targetClass->IsChildOf(this));
	}

	DClass* DClass::RetrieveRootClass (const DString& className)
	{
		for (unsigned int i = 0; i < DClass::RootClasses.size(); i++)
		{
			if (className == DClass::RootClasses.at(i)->Name())
			{
				return DClass::RootClasses.at(i);
			}
		}

		return nullptr;
	}

	DClass* DClass::FindClassByName (const DString& className)
	{
		DClass* result = nullptr;

		for (unsigned int i = 0; i < RootClasses.size(); i++)
		{
			if (RootClasses.at(i)->Name() == className)
			{
				return RootClasses.at(i);
			}

			result = FindChildByName(className, RootClasses.at(i));

			if (result != nullptr)
			{
				return result;
			}
		}

		//Check orphans
		for (unsigned int i = 0; i < OrphanClasses.size(); i++)
		{
			if (OrphanClasses.at(i).ClassInstance->Name() == className)
			{
				return OrphanClasses.at(i).ClassInstance;
			}

			result = FindChildByName(className, OrphanClasses.at(i).ClassInstance);

			if (result != nullptr)
			{
				return result;
			}
		}

		return nullptr;
	}

	DClass* DClass::FindChildByName (const DString& className, DClass* baseClass)
	{
		DClass* result = nullptr;

		for (unsigned int i = 0; i < baseClass->Children.size(); i++)
		{
			if (baseClass->Children.at(i)->Name() == className)
			{
				return baseClass->Children.at(i);
			}

			//recursively check children
			result = FindChildByName(className, baseClass->Children.at(i));

			if (result != nullptr)
			{
				return result;
			}
		}

		return nullptr;
	}

	const vector<DClass*> DClass::GetRootClasses ()
	{
		return DClass::RootClasses;
	}

	DString DClass::Name () const
	{
		return ClassName;
	}

	DString DClass::ToString () const
	{
		return ClassName;
	}

	const DClass* DClass::GetSuperClass () const
	{
		return ParentClass;
	}

	const Object* DClass::GetDefaultObject () const
	{
		if (ClassType == CT_Object)
		{
			return CDO.DefaultObject;
		}

		return nullptr;
	}

	const EngineComponent* DClass::GetDefaultEngineComponent () const
	{
		if (ClassType == CT_EngineComponent)
		{
			return CDO.DefaultEngineComponent;
		}

		return nullptr;
	}

	vector<DClass::SOrphanInfo> DClass::GetPreloadedOrphans ()
	{
		return (PreloadedOrphans.size() > 0) ? PreloadedOrphans : OrphanClasses;
	}

	vector<DClass*> DClass::GetPreloadedRootClasses ()
	{
		return (PreloadedRootClasses.size() > 0) ? PreloadedRootClasses : RootClasses;
	}

	void DClass::RegisterRootClass (DClass* newRoot)
	{
		unsigned int index;

		//Added alphabetically for organizational purposes
		for (index = 0; index < RootClasses.size(); index++)
		{
			if (RootClasses.at(index)->Name().Compare(newRoot->Name()) > 0)
			{
				break;
			}
		}

		RootClasses.insert(RootClasses.begin() + index, newRoot);
		PreloadedRootClasses.insert(PreloadedRootClasses.begin() + index, newRoot);
	}

	void DClass::ClaimOrphans (DClass* newParentClass)
	{
		for (unsigned int i = 0; i < DClass::OrphanClasses.size(); i++)
		{
			if (OrphanClasses.at(i).ParentClass == newParentClass->Name())
			{
				//claim orphan
				newParentClass->AddChild(OrphanClasses.at(i).ClassInstance);
				OrphanClasses.at(i).ClassInstance->ParentClass = newParentClass;

				OrphanClasses.erase(OrphanClasses.begin() + i);
				i--;
			}
		}
	}

	void DClass::AddChild (DClass* child)
	{
		unsigned int index;

		//Added alphabetically for organizational purposes
		for (index = 0; index < Children.size(); index++)
		{
			if (Children.at(index)->Name().Compare(child->Name()) > 0)
			{
				break;
			}
		}

		Children.insert(Children.begin() + index, child);
	}
}

#endif