/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  UnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_ABSTRACT_CLASS(UnitTester, Object)

	bool UnitTester::MetRequirements (const vector<const UnitTester*>& completedTests) const
	{
		bool bCompletedDatatype = false;
		bool bCompletedEngineInteg = false;

		for (unsigned int i = 0; i < completedTests.size(); i++)
		{
			if (dynamic_cast<const DatatypeUnitTester*>(completedTests.at(i)) != nullptr)
			{
				bCompletedDatatype = true;
				if (bCompletedEngineInteg)
				{
					return true;
				}
			}
			else if (dynamic_cast<const EngineIntegrityUnitTester*>(completedTests.at(i)) != nullptr)
			{
				bCompletedEngineInteg = true;
				if (bCompletedDatatype)
				{
					return true;
				}
			}
		}

		return false;
	}

	bool UnitTester::IsTestEnabled (EUnitTestFlags testFlags) const
	{
		return true;
	}

	bool UnitTester::ShouldHaveDebugLogs (EUnitTestFlags testFlags) const
	{
		return ((testFlags & SD::UnitTester::UTF_ErrorsOnly) == 0 && (testFlags & SD::UnitTester::UTF_Verbose) > 0);
	}

	void UnitTester::UnitTestError (EUnitTestFlags testFlags, const DString& errorMsg, initializer_list<const char*> args) const
	{
		vector<DString> strParams;
		for (auto elem : args)
		{
			strParams.push_back(DString(elem));
		}

		UnitTestError(testFlags, errorMsg, strParams);
	}

	void UnitTester::UnitTestError (EUnitTestFlags testFlags, const DString& errorMsg, const vector<DString>& args) const
	{
		ExecuteFailSequence(testFlags);

		DString fullMsg = DString::FormatString(errorMsg, args);
		if ((testFlags & UTF_Muted) == 0)
		{
			Engine::GetEngine()->RecordLog(fullMsg, LOG_UNIT_TEST_FAILURE);
		}

		if ((testFlags & UTF_CrashOnError) > 0)
		{
			Engine::GetEngine()->FatalError(fullMsg);
		}
	}

	void UnitTester::UnitTestError (EUnitTestFlags testFlags, const DString& errorMsg) const
	{
		vector<DString> strParams;
		UnitTestError(testFlags, errorMsg, strParams);
	}

	void UnitTester::SetTestCategory (EUnitTestFlags testFlags, const DString& newTestCategory) const
	{
		if (newTestCategory.IsEmpty() && !TestCategory.IsEmpty())
		{
			UNITTESTER_LOG1(testFlags, TXT("Finished %s tests."), newTestCategory);
		}

		TestCategory = newTestCategory;
		if (!TestCategory.IsEmpty())
		{
			UNITTESTER_LOG1(testFlags, TXT("Begin %s testing. . ."), TestCategory);
		}
	}

	void UnitTester::CompleteTestCategory (EUnitTestFlags testFlags) const
	{
		if (!TestCategory.IsEmpty())
		{
			UNITTESTER_LOG1(testFlags, TXT("%s tests passed!"), TestCategory);
		}

		TestCategory = TXT("");
	}

	void UnitTester::BeginTestSequence (EUnitTestFlags testFlags, const DString& testName) const
	{
		UNITTESTER_LOG(testFlags, TXT("========================"));
		UNITTESTER_LOG1(testFlags, TXT("-== Begin %s Test ==-"), testName);
		UNITTESTER_LOG(testFlags, TXT("========================"));
		UNITTESTER_LOG(testFlags, TXT(""));
	}

	void UnitTester::ExecuteSuccessSequence (EUnitTestFlags testFlags, const DString& testName) const
	{
		UNITTESTER_LOG(testFlags, TXT("========================"));
		UNITTESTER_LOG1(testFlags, TXT("-== %s Test PASSSED! ==-"), testName);
		UNITTESTER_LOG(testFlags, TXT("========================"));
		UNITTESTER_LOG(testFlags, TXT(""));
		TestCategory = TXT("");
	}

	void UnitTester::ExecuteFailSequence (EUnitTestFlags testFlags) const
	{
		UNITTESTER_LOG(testFlags, TXT("========================"));
		UNITTESTER_LOG1(testFlags, TXT("-== ERROR:  %s Test FAILED! ==-"), GetName());
		UNITTESTER_LOG(testFlags, TXT("========================"));
		UNITTESTER_LOG(testFlags, TXT(""));
		TestCategory = TXT("");
	}

#pragma region "External Operators"
	UnitTester::EUnitTestFlags operator| (UnitTester::EUnitTestFlags a, UnitTester::EUnitTestFlags b)
	{
		return static_cast<UnitTester::EUnitTestFlags>(static_cast<int>(a) | static_cast<int>(b));
	}
#pragma endregion
}

#endif
#endif