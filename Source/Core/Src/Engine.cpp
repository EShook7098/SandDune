/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Engine.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	DString Engine::EngineParameters = TXT("");
	Engine* Engine::EngineInstance = new Engine();

	Engine::Engine ()
	{
		bShuttingDown = false;

		FrameCounter = 0;
		ElapsedTime = 0.f;
		//MinDeltaTime = -1.f; //Disable max frame rate.
		//MinDeltaTime = 0.0333333f; //~30 fps
		MinDeltaTime = 0.015f; //~60 fps
		MaxDeltaTime = 0.2f; //Minimum 5 frames per second.
		PreviousGarbageCollectTime = 0.f;
		GarbageCollectInterval = 30.f;
		PrevTickTime = clock();

		ObjectHashNumber = 0;
		EntityHashNumber = 0;
		ComponentHashNumber = 0;
		TickComponentHashNumber = 0;
	}

	Engine::~Engine ()
	{
		//Everything should have been cleaned up via ShutdownSandDune()
	}

	void Engine::InitializeEngine ()
	{
		static bool bInitialized = false;
		if (bInitialized)
		{
			LOG(LOG_WARNING, TXT("Attempted to initialize an engine that's already initialized."));
			return;
		}

		bInitialized = true;
		
		//Register pending engine components
		for (ClassIterator iter(EngineComponent::SStaticClass()); iter.SelectedClass != nullptr; iter++)
		{
			const EngineComponent* curComp = iter.SelectedClass->GetDefaultEngineComponent();
			if (curComp->GetRegisteredComponent() != nullptr)
			{
				RegisterEngineComponent(curComp->GetRegisteredComponent());
			}
		}

		InitializePlatform(); //Initialize platform-specific functionality

		ObjectHashNumber = RegisterObjectHash(TXT("Object"));
		EntityHashNumber = RegisterObjectHash(TXT("Entity"));
		ComponentHashNumber = RegisterObjectHash(TXT("Entity Component"));
		TickComponentHashNumber = RegisterObjectHash(TXT("Tick Component"));

		//Give engine components an opportunity to reserve partitions of the object hash table for object groupings.
		for (unsigned int i = 0; i < EngineComponents.size() && ObjectHashTable.size() < 31; i++)
		{
			EngineComponents.at(i)->RegisterObjectHash();
		}

		for (unsigned int i = 0; i < EngineComponents.size(); i++)
		{
			EngineComponents.at(i)->PreInitializeComponent();
		}

		for (unsigned int i = 0; i < EngineComponents.size(); i++)
		{
			EngineComponents.at(i)->InitializeComponent();
		}

		for (unsigned int i = 0; i < EngineComponents.size(); i++)
		{
			EngineComponents.at(i)->PostInitializeComponent();
		}

		//Invoke PostEngineInitialize upon all objects
		for (ClassIterator iter(Object::SStaticClass()); iter.SelectedClass; iter++)
		{
			const Object* curObject = static_cast<const Object*>(iter.SelectedClass->GetDefaultObject());

			if (curObject != nullptr)
			{
				curObject->PostEngineInitialize();
			}
		}
	}

	void Engine::SetEngineParameters (int numArgs, TCHAR* args[])
	{
		if (EngineParameters.IsEmpty())
		{
			DString result = TXT("");
			for (int i = 0; i < numArgs; i++)
			{
				result += args[i];
			}

			EngineParameters = result;
		}
	}

	unsigned int Engine::RegisterObjectHash (const DString& friendlyName)
	{
		if (ObjectHashTable.size() >= 31)
		{
			LOG1(LOG_WARNING, TXT("Unable to register a portion of the object hash table for %s since the max size for the table is 31."), friendlyName);
			return UINT_INDEX_NONE;
		}

		SHashTableMeta newEntry;
		newEntry.FriendlyName = friendlyName;
		newEntry.HashNumber = 1 << ObjectHashTable.size();
		ObjectHashTable.push_back(newEntry);

		return newEntry.HashNumber;
	}

	void Engine::FatalError (const DString& errorMsg)
	{
		LOG(LOG_FATAL, errorMsg);

		//Remove clamped mouse position limits
		OS_ClampMousePointer(Rectangle<INT>());

		DString dialogMsg = GenerateFatalErrorMsg(errorMsg);
		PlatformOpenWindow(dialogMsg, TXT("Fatal Error!"));
		bShuttingDown = true;
	}

	void Engine::CollectGarbage ()
	{
		PreviousGarbageCollectTime = ElapsedTime;

		for (ObjectIterator iter; iter.SelectedObject; iter++)
		{
			//Give all objects a chance to set their invalid pointers to null before deleting the objects
			iter.SelectedObject->CleanUpInvalidPointers();
		}

		for (unsigned int i = 0; i < ObjectHashTable.size(); i++)
		{
			Object* prevObject = nullptr;
			Object* curObject = ObjectHashTable.at(i).LeadingObject;

			while (curObject != nullptr)
			{
				Object* nextObject = curObject->NextObject;
				if (curObject->bPendingDelete)
				{
					//Preserve the object linked list
					if (ObjectHashTable.at(i).LeadingObject == curObject) //current object is leading the hash table
					{
						ObjectHashTable.at(i).LeadingObject = nullptr; //failsafe incase no objects are available

						//find the next available object to fill in the object hash table's initial slot
						for (Object* newHashLeader = curObject->NextObject; newHashLeader != nullptr; newHashLeader = newHashLeader->NextObject)
						{
							if (!newHashLeader->bPendingDelete)
							{
								//Found an available object!
								ObjectHashTable.at(i).LeadingObject = newHashLeader;
								break;
							}
						}
					}
					else if (prevObject != nullptr) //Object in middle of linked list
					{
						prevObject->NextObject = nextObject;
					}

					delete curObject;
				}
				else //current object is not PendingDelete
				{
					prevObject = curObject;
				}

				curObject = nextObject;
			}
		}
	}

	void Engine::RecordLog (const DString& message, const DString& type)
	{
		DString fullMsg = message;
		for (unsigned int i = 0; i < EngineComponents.size(); i++)
		{
			EngineComponents.at(i)->RecordLog(fullMsg, type);
		}

#ifdef DEBUG_MODE
#ifndef SAND_DUNE_STANDALONE
		fullMsg = TXT("(Sand Dune) ") + fullMsg;
#endif

		if (fullMsg.At(fullMsg.Length() - 1) != TEXT('\n'))
		{
			fullMsg += TXT("\n");
		}

		//output message to console window
		cout << fullMsg.ToCString();

#ifdef PLATFORM_WINDOWS
		// Instruct Visual Studio to print out message and
		// any external program listening to this message such as DebugView will print this, too.
#if USE_WIDE_STRINGS
		OutputDebugString(StrToLPCWSTR(fullMsg));
#else
		OutputDebugString(StrToLPCSTR(fullMsg));
#endif //wide strings
#endif //windows
#endif //debug
	}

#ifdef DEBUG_MODE
	void Engine::LogObjectHashTable ()
	{
		INT objectCounter = 0;

		LOG(LOG_DEBUG, TXT("-=== Logging Hash Table ===-"));
		for (unsigned int i = 0; i < ObjectHashTable.size(); i++)
		{
			INT hashCounter = 0;
			LOG3(LOG_DEBUG, TXT("Hash Entry[%s]:  %s (%s)"), INT(i), ObjectHashTable.at(i).FriendlyName, DString(ObjectHashTable.at(i).HashNumber));
			for (ObjectIterator iter((1 << i)); iter.SelectedObject; iter++)
			{
				objectCounter++;
				hashCounter++;
				LOG2(LOG_DEBUG, TXT("    %s.  %s"), hashCounter, iter.SelectedObject->GetUniqueName());
			}

			LOG(LOG_DEBUG, TXT(""));
		}

		LOG1(LOG_DEBUG, TXT("Size of ObjectHashTable:  %s"), DString(static_cast<int>(ObjectHashTable.size())));
		LOG1(LOG_DEBUG, TXT("Total number of Objects in hash table:  %s"), objectCounter);
		LOG(LOG_DEBUG, TXT("-=== End Hash Table Logs ===-"));
	}
#endif

	void Engine::Tick ()
	{
		//calculate deltaTime since last tick
		clock_t curTime = clock();
		FLOAT deltaSec = static_cast<float>(curTime - PrevTickTime)/CLOCKS_PER_SEC;
		if (MinDeltaTime > 0 && deltaSec < MinDeltaTime)
		{
			FLOAT sleepTime = MinDeltaTime - deltaSec;
			OS_Sleep(sleepTime * 1000.f);
			deltaSec += sleepTime;
		}

		PrevTickTime = clock();
		if (MaxDeltaTime > 0)
		{
			deltaSec = Utils::Min(deltaSec, MaxDeltaTime);
		}

		ElapsedTime += deltaSec;
		FrameCounter++;

		for (unsigned int i = 0; i < TickingEngineComponents.size(); i++)
		{
			TickingEngineComponents.at(i)->PreTick(deltaSec);
		}

		//Update all TickComponents
		//TODO:  change casting (static_cast) since we know this is a tick component?
		for (ObjectIterator iter(TickComponentHashNumber); iter.SelectedObject; iter++)
		{
			TickComponent* tickingEntity = dynamic_cast<TickComponent*>(iter.SelectedObject);
			if (VALID_OBJECT(tickingEntity) && tickingEntity->IsTicking())
			{
				tickingEntity->Tick(deltaSec);
			}
		}

#ifdef DEBUG_MODE
		for (ObjectIterator iter(EntityHashNumber, true); iter.SelectedObject; iter++)
		{
			if (dynamic_cast<Entity*>(iter.SelectedObject))
			{
				dynamic_cast<Entity*>(iter.SelectedObject)->DebugTick(deltaSec);
			}
		}
#endif

		for (unsigned int i = 0; i < TickingEngineComponents.size(); i++)
		{
			TickingEngineComponents.at(i)->PostTick(deltaSec);
		}

		//Check if the garbage collector needs to run
		if (ElapsedTime - PreviousGarbageCollectTime > GarbageCollectInterval)
		{
			CollectGarbage();
		}

		if (bShuttingDown)
		{
			ShutdownSandDune();
		}
	}

	void Engine::SetMinDeltaTime (FLOAT newMinDeltaTime)
	{
		MinDeltaTime = newMinDeltaTime;
	}

	void Engine::SetMaxDeltaTime (FLOAT newMaxDeltaTime)
	{
		MaxDeltaTime = newMaxDeltaTime;
	}

	void Engine::RemoveEngineComponent (const DClass* engineClass)
	{
		for (unsigned int i = 0; i < EngineComponents.size(); i++)
		{
			if (EngineComponents.at(i)->StaticClass() == engineClass)
			{
				EngineComponent* targetComponent = EngineComponents.at(i);
				if (targetComponent->GetTickingComponent())
				{
					//Find and remove this component from the TickingComponents vector
					for (unsigned int j = 0; j < TickingEngineComponents.size(); j++)
					{
						if (TickingEngineComponents.at(j) == targetComponent)
						{
							TickingEngineComponents.erase(TickingEngineComponents.begin() + j);
							break;
						}
					}
				}

				EngineComponents.at(i)->ShutdownComponent();
				delete EngineComponents.at(i);
				EngineComponents.erase(EngineComponents.begin() + i);

				return;
			}
		}
	}

	Engine* Engine::GetEngine ()
	{
		return EngineInstance;
	}

	FLOAT Engine::GetElapsedTime () const
	{
		return ElapsedTime;
	}

	unsigned int Engine::GetFrameCounter () const
	{
		return FrameCounter;
	}

	FLOAT Engine::GetMinDeltaTime () const
	{
		return MinDeltaTime;
	}

	FLOAT Engine::GetMaxDeltaTime () const
	{
		return MaxDeltaTime;
	}

	DString Engine::GetEngineParameters () const
	{
		return EngineParameters;
	}

	unsigned int Engine::GetObjectHashNumber () const
	{
		return ObjectHashNumber;
	}

	unsigned int Engine::GetEntityHashNumber () const
	{
		return EntityHashNumber;
	}

	unsigned int Engine::GetComponentHashNumber () const
	{
		return ComponentHashNumber;
	}

	unsigned int Engine::GetTickComponentHashNumber () const
	{
		return TickComponentHashNumber;
	}

	void Engine::RegisterEngineComponent (EngineComponent* newComponent)
	{
		EngineComponents.push_back(newComponent);

		if (newComponent->GetTickingComponent())
		{
			TickingEngineComponents.push_back(newComponent);
		}
	}

	void Engine::RegisterObject (Object* newObject)
	{
		unsigned int objHash = newObject->GetObjectHash();

		if (objHash != 0 && !Utils::IsPowerOf2(objHash))
		{
			LOG2(LOG_WARNING, TXT("Unable to register %s to hash table.  %s must either be 0 or a number in powers of 2."), newObject->GetName(), DString(objHash));
			return;
		}

		unsigned int tableIdx = static_cast<unsigned int>(trunc(log2(objHash)));
		if (tableIdx >= ObjectHashTable.size())
		{
			LOG3(LOG_WARNING, TXT("Unable to register %s to hash table.  The table index (%s) is larger than the object hash table size (%s)."), newObject->GetName(), DString(tableIdx), DString(static_cast<int>(ObjectHashTable.size())));
			return;
		}

		if (ObjectHashTable.at(tableIdx).LeadingObject != nullptr)
		{
			//Maintain linked list
			newObject->NextObject = ObjectHashTable.at(tableIdx).LeadingObject;
		}

		//Push new object to the front of the link list
		ObjectHashTable.at(tableIdx).LeadingObject = newObject;
	}

	DString Engine::GenerateFatalErrorMsg (const DString& specificError)
	{
		return (TXT("FATAL ERROR!\n") + specificError);
	}

	void Engine::ShutdownSandDune ()
	{
		//Notify components that the engine is about to shutdown
		for (unsigned int i = 0; i < EngineComponents.size(); i++)
		{
			EngineComponents.at(i)->ShutdownComponent();
		}

		//Destroy all objects
		for (ObjectIterator iter(ObjectHashNumber, true); iter.SelectedObject; iter++)
		{
			if (VALID_OBJECT(iter.SelectedObject))
			{
				iter.SelectedObject->Destroy();
			}
		}

		for (unsigned int i = 0; i < EngineComponents.size(); i++)
		{
			delete EngineComponents.at(i);
		}

		CollectGarbage();
		EngineComponents.clear();

		//Clean up DClasses and their DefaultObjects (DefaultObjects are cleared when the DClasses are deleted).
		//We don't call destroy on DefaultObjects since they are not registered to the hash table.
		vector<DClass*> rootClasses = DClass::GetRootClasses();
		vector<const DClass*> classesToDelete;
		for (unsigned int i = 0; i < rootClasses.size(); i++)
		{
			for (ClassIterator iter(rootClasses.at(i)); iter.SelectedClass; iter++)
			{
				classesToDelete.push_back(iter.SelectedClass);
			}
		}

		for (unsigned int i = 0; i < classesToDelete.size(); i++)
		{
			delete classesToDelete.at(i);
		}

		Engine::EngineInstance = nullptr;
	}
}

#endif