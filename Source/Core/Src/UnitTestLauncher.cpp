/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  UnitTestLauncher.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/
 
 #include "CoreClasses.h"

 #ifdef DEBUG_MODE

 using namespace std;

 namespace SD
 {
	bool UnitTestLauncher::RunAllTests (UnitTester::EUnitTestFlags testFlags)
	{
		//Generate a list of unit tests that needs to run
		vector<const UnitTester*> incompleteTests;
		for (ClassIterator iter(UnitTester::SStaticClass()); iter.SelectedClass; iter++)
		{
			const UnitTester* unitTester = dynamic_cast<const UnitTester*>(iter.SelectedClass->GetDefaultObject());
			if (VALID_OBJECT(unitTester))
			{
				incompleteTests.push_back(unitTester);
			}
		}

		vector<const UnitTester*> completedTests;
		while(incompleteTests.size() > 0)
		{
			INT oldIncompleteSize = static_cast<int>(incompleteTests.size());

			for (unsigned int i = 0; i < incompleteTests.size(); i++)
			{
				if (!incompleteTests.at(i)->MetRequirements(completedTests))
				{
					//Not all requirements are met.  Try next test.
					continue;
				}

				if (!incompleteTests.at(i)->RunTests(testFlags))
				{
					return false;
				}

				completedTests.push_back(incompleteTests.at(i));
				incompleteTests.erase(incompleteTests.begin() + i);
				i--;
			}

			if (oldIncompleteSize == static_cast<int>(incompleteTests.size()))
			{
				UNITTESTER_LOG(testFlags, TXT("Unable to complete all unit tests since there's a circular dependency.  The following unit tests depend on each other:"));
				for (INT i = 0; i < static_cast<int>(incompleteTests.size()); i++)
				{
					UNITTESTER_LOG2(testFlags, TXT("    [%s]:  %s"), i, incompleteTests.at(i.ToUnsignedInt())->GetName());
				}

				return false;
			}
		}

		UNITTESTER_LOG(testFlags, TXT("========================"));
		UNITTESTER_LOG(testFlags, TXT("All unit tests PASSED!"));
		UNITTESTER_LOG(testFlags, TXT("========================"));
		UNITTESTER_LOG(testFlags, TXT(""));

		return true;
	}
 }

 #endif