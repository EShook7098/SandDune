/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TickComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TickComponent, EntityComponent)

	void TickComponent::InitProps ()
	{
		Super::InitProps();

		bTicking = true;
		OnTick.ClearFunction();
	}

	unsigned int TickComponent::CalculateHashID () const
	{
		return (Engine::GetEngine()->GetTickComponentHashNumber());
	}

	void TickComponent::SetTickHandler (SDFunction<void, FLOAT> newHandler)
	{
		OnTick = newHandler;
	}

	void TickComponent::Tick (FLOAT deltaTime)
	{
		if (OnTick.IsBounded())
		{
			OnTick(deltaTime);
		}
	}

	void TickComponent::SetTicking (bool bNewTicking)
	{
		bTicking = bNewTicking;
	}

	bool TickComponent::IsTicking () const
	{
		return bTicking;
	}
}

#endif