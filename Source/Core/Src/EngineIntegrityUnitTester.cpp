/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EngineIntegrityUnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(EngineIntegrityUnitTester, UnitTester)

	bool EngineIntegrityUnitTester::RunTests (EUnitTestFlags testFlags) const
	{
		if ((testFlags & UTF_FeatureTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
		{
			EngineIntegrityTester* tester = EngineIntegrityTester::CreateObject();
			if (tester == nullptr)
			{
				UnitTestError(testFlags, TXT("Failed to launch Engine Integrity Tests.  Could not instantiate test object."));
				return false;
			}

			bool bResult = (tester->TestClassIterator(this, testFlags) && tester->TestObjectInstantiation(this, testFlags) &&
					tester->TestObjectIterator(this, testFlags) && tester->TestPropertyIterator(this, testFlags) &&
					tester->TestCleanUp(this, testFlags) && tester->TestComponents(this, testFlags));

			tester->Destroy();
			return bResult;
		}

		return true;
	}

	bool EngineIntegrityUnitTester::MetRequirements (const vector<const UnitTester*>& completedTests) const
	{
		for (unsigned int i = 0; i < completedTests.size(); i++)
		{
			if (dynamic_cast<const DatatypeUnitTester*>(completedTests.at(i)) != nullptr)
			{
				return true;
			}
		}

		return false;
	}
}

#endif
#endif