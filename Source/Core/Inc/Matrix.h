/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Matrix.h
  A class that represents a 2D array of numbers and contains utility functions
  revolving matrices.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef MATRIX_H
#define MATRIX_H

#include "Configuration.h"
#if INCLUDE_CORE

#include "SDFloat.h"

/**
  Macro to execute on each data element for the give matrix (eg: round all floats).
 */
#define MATRIX_DATA_MACRO(matrix, operation) \
	for (unsigned int i = 0; i < matrix##.Data.size(); i++) \
	{ \
		operation \
	} \

namespace SD
{
	class Matrix : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Series of numbers within the matrix.  The data appears in this order (for a 4x4 matrix)
		0	4	8	12
		1	5	9	13
		2	6	10	14
		3	7	11	15
		*/
		std::vector<FLOAT> Data;

	protected:
		INT Rows;
		INT Columns;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		Matrix (INT numRows, INT numColumns);
		Matrix (INT numRows, INT numColumns, const std::vector<FLOAT>& inData);
		Matrix (const Matrix& copyMatrix);
		virtual ~Matrix ();


		/*
		=====================
		  Operators
		=====================
		*/

	public:
		virtual void operator= (const Matrix& copyMatrix);
		
		
		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual DString ToString () const override; //Displaying the contents of this matrix within a single line.

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Returns a 1x1 matrix with the element equal to 0.
		 */
		static Matrix GetVoidMatrix ();

		/**
		  Returns the dot product of the vectors assuming the length of both vectors are equal.
		 */
		static FLOAT DotProduct (const std::vector<FLOAT>& left, const std::vector<FLOAT>& right);

#ifdef DEBUG_MODE
		/**
		  Contrast to the ToString method, this logs out this matrix in a multi-line format.
		 */
		virtual void LogMatrix () const;
#endif

		/**
		  Returns true if the length of the Data vector fits within the num columns and rows.
		 */
		bool IsValid () const;

		/**
		  Returns true this matrix is a square matrix (number of rows is equal to the number of columns).
		 */
		bool IsSquare () const;

		/**
		  Returns true if all elements below the main diagonal is 0.
		 */
		bool IsUpperTriangularMatrix () const;

		/**
		  Returns true if all elements above the main diagonal is 0.
		 */
		bool IsLowerTriangularMatrix () const;

		/**
		  Returns if the matrix is a Unitriangular matrix (must either be upper or lower triangular matrix, and
		  and all elements within the main diagonal is equal to 1).
		 */
		bool IsUnitriangularMatrix () const;

		/**
		  Returns true if the matrix is an identity matrix (must be both upper and lower triangular matrix and a unitriangular matrix.
		 */
		bool IsUnitMatrix () const;

		/**
		  Returns the identity matrix for this squared matrix.
		  Returns false if this matrix does not have an identity matrix or is not squared.
		  This function does not handle nonsquared matrices.
		 */
		bool GetIdentityMatrix (Matrix& outIdentity) const;

		/**
		  Computes the determinant of this matrix.  Only matrices up to 4x4 are supported.
		 */
		virtual FLOAT CalculateDeterminant () const;

		/**
		  Computes the adjugate of this matrix.  Only matrices up to 4x4 are supported.
		 */
		virtual bool CalculateAdjugate (Matrix& outAdjugate) const;

		/**
		  Computes the inverse of this matrix.  Only matrices up to 4x4 are supported.
		  Returns false if the matrix is not invertible.
		 */
		virtual bool CalculateInverse (Matrix& outInverse) const;

		/**
		  Returns a matrix of equal dimensions with all elements being 0.
		 */
		Matrix GetZeroMatrix () const;

		/**
		  Returns the Matrix of Cofactors of this matrix, where each element of this matrix alternates signage.
		 */
		Matrix GetComatrix () const;

		/**
		  Returns a sub matrix of this matrix that is composed of all elements excluding the specified row and column.
		 */
		Matrix GetSubMatrix (INT rowIdx, INT columnIdx) const;

		/**
		  Caculates and returns the matrix of minors of this matrix.  A matrix of minors is a matrix of determinants of sub matrices.
		  Returns false if this cannot generate a matrix of minors from this matrix.  Supports up to 4x4 matrices.
		  Warning:  This function is expensive (increases in complexity exponentially based on number of dimensions).
		 */
		bool GetMatrixOfMinors (Matrix& outMatrixOfMinors) const;


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual INT GetNumRows () const;
		virtual INT GetNumColumns () const;
		virtual std::vector<FLOAT> GetRow (INT rowIdx) const;
		virtual void GetRow (INT rowIdx, std::vector<FLOAT>& outRowData) const;
		virtual std::vector<FLOAT> GetColumn (INT columnIdx) const;
		virtual void GetColumn (INT columnIdx, std::vector<FLOAT>& outColumnData) const;
		virtual FLOAT GetDataElement (INT rowIdx, INT columnIdx) const;
		virtual INT GetDataIndex (INT rowIdx, INT columnIdx) const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual FLOAT CalcDeterminant2x2 () const;
		virtual FLOAT CalcDeterminant3x3 () const;
		virtual FLOAT CalcDeterminant4x4 () const;

		virtual void CalcAdjugate2x2 (Matrix& outAdjugate) const;
		virtual void CalcAdjugate3x3 (Matrix& outAdjugate) const;
		virtual void CalcAdjugate4x4 (Matrix& outAdjugate) const;

		virtual bool CalcInverse2x2 (Matrix& outInverse) const;
		virtual bool CalcInverse3x3 (Matrix& outInverse) const;
		virtual bool CalcInverse4x4 (Matrix& outInverse) const;
	};

#pragma region "External Operators"
	bool operator== (const Matrix& left, const Matrix& right);
	bool operator!= (const Matrix& left, const Matrix& right);

	Matrix operator+ (const Matrix& left, const Matrix& right);
	Matrix operator+= (Matrix& left, const Matrix& right);
	Matrix operator- (const Matrix& left, const Matrix& right);
	Matrix operator-= (Matrix& left, const Matrix& right);

	Matrix operator* (const Matrix& left, const FLOAT& right);
	Matrix operator* (const FLOAT& left, const Matrix& right);
	Matrix operator* (const Matrix& left, const Matrix& right);
	Matrix& operator*= (Matrix& left, const FLOAT& right);
	Matrix& operator*= (Matrix& left, const Matrix& right);
#pragma endregion
}

#endif
#endif