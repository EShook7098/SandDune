/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Vector2.h
  Self-contained 2D vector class with some utilities.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef VECTOR2_H
#define VECTOR2_H

#include "Configuration.h"
#if INCLUDE_CORE

#include "SDFLOAT.h"

namespace SD
{
	class Vector3;

	class Vector2 : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		FLOAT X;
		FLOAT Y;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		Vector2 ();

		Vector2 (const float x, const float y);

		Vector2 (const FLOAT x, const FLOAT y);

		Vector2 (const Vector2& copyVector);

		
		/*
		=====================
		  Operators
		=====================
		*/

	public:
		void operator= (const Vector2& copyVector);


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		DString ToString () const override;

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		static Vector2 SFMLtoSD (const sf::Vector2f sfVector);
		static sf::Vector2f SDtoSFML (const Vector2 sdVector);

		/**
		  Returns the length of this vector.
		 */
		FLOAT VSize () const;

		/**
		  Adjusts this vector's size so the length of the vector is equal to the specified length.
		 */
		void SetLengthTo (FLOAT targetLength);

		FLOAT Dot (const Vector2& otherVector) const;

		void Normalize ();

		/**
		  Returns true of all axis of this vector are the equal.
		 */
		bool IsUniformed () const;

		/**
		  Returns true if all axis is 0.
		 */
		bool IsEmpty () const;

		/**
		  Returns a Vector3 from this vector's components.  Z axis is 0.
		 */
		Vector3 ToVector3 () const;
	};

#pragma region "External Operators"
	bool operator== (const Vector2& left, const Vector2& right);
	bool operator!= (const Vector2& left, const Vector2& right);
	Vector2 operator+ (const Vector2& left, const Vector2& right);
	Vector2& operator+= (Vector2& left, const Vector2& right);
	Vector2 operator- (const Vector2& left, const Vector2& right);
	Vector2& operator-= (Vector2& left, const Vector2& right);
	Vector2 operator* (const Vector2& left, const Vector2& right);
	Vector2& operator*= (Vector2& left, const Vector2& right);
	Vector2 operator/ (const Vector2& left, const Vector2& right);
	Vector2& operator/= (Vector2& left, const Vector2& right);

	template<class T> Vector2 operator* (const Vector2& left, const T& right)
	{
		return Vector2(left.X * right, left.Y * right);
	}

	template<class T> Vector2 operator* (const T& left, const Vector2& right)
	{
		return Vector2(left * right.X, left * right.Y);
	}

	template<class T> Vector2& operator*= (Vector2& left, const T& right)
	{
		left.X *= right;
		left.Y *= right;

		return left;
	}

	template<class T> Vector2 operator/ (const Vector2& left, const T& right)
	{
		if (right != 0)
		{
			return Vector2(left.X / right, left.Y / right);
		}

		return left;
	}

	template<class T> Vector2& operator/= (Vector2& left, const T& right)
	{
		if (right != 0)
		{
			left.X /= right;
			left.Y /= right;
		}

		return left;
	}
#pragma endregion
}

#endif
#endif