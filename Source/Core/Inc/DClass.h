/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DClass.h
  At startup time, the DClass will generate a class tree of all classes
  (with DECLARE_CLASS macro).  The DClasses may possess a reference to the
  parent and children DClasses.

  NOTE:  You can still have multi-inherited classes.  This class is mostly
  used to help iterate through classes, and obtain default objects from each class.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef DCLASS_H
#define DCLASS_H

#include "Core.h"

#if INCLUDE_CORE

#include "DString.h"

namespace SD
{
	class Object;
	class EngineComponent;

	class DClass
	{


		/*
		=====================
		  Data types
		=====================
		*/

	public:
		enum EClassType
		{
			CT_Object,
			CT_EngineComponent
		};

	protected:
		struct SOrphanInfo
		{
			DClass* ClassInstance;
			DString ParentClass;
		};


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Type of class this DClass refers to, and helps determine which default object pointer in DefaultInstance union is used. */
		enum EClassType ClassType;

	protected:
		/* A list of all classes without a parent class */
		static std::vector<DClass*> RootClasses;

		/* A list of classes with undefined parent classes (due to run-time ordering) */
		static std::vector<SOrphanInfo> OrphanClasses;

		/* Vectors to track classes that attempted initialization before DClass was initialized.  See GetPreloadedOrphans(). */
		static std::vector<SOrphanInfo> PreloadedOrphans;
		static std::vector<DClass*> PreloadedRootClasses;

		DString ClassName;
		DClass* ParentClass = nullptr;

		std::vector<DClass*> Children;

		union DefaultInstance
		{
			/* Reference to an instantiated object that simply contains a reference to its default properties,
			   and is not meant for any modifications at run time.*/
			Object* DefaultObject;
			EngineComponent* DefaultEngineComponent;
		} CDO;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		DClass (const DString& className);

		virtual ~DClass ();


		/*
		=====================
		  Methods
		=====================
		*/

	public:

		/**
		  Registers class to the class tree.
		 */
		static DClass* LoadClass (const DString& className, const DString& parentClass = TXT(""));

		/**
		  Updates the DefaultObject's value if it's not set already.
		  This DefaultObject will automatically be deleted when this DClass instance is destroyed.
		 */
		virtual void SetCDO (Object* newCDO);
		virtual void SetCDO (EngineComponent* newCDO);

		/**
		  Returns true if the parameter is the parent class of the current class.
		 */
		virtual bool IsChildOf (const DClass* targetClass) const;

		/**
		  Returns true if the parameter is a child of the current class.
		 */
		virtual bool IsParentOf (const DClass* targetClass) const;


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		/**
		  Obtains a root class based on name
		 */
		static DClass* RetrieveRootClass (const DString& className);

		static const std::vector<DClass*> GetRootClasses ();

		DString Name () const;

		virtual DString ToString () const;

		const DClass* GetSuperClass () const;

		const Object* GetDefaultObject () const;
		const EngineComponent* GetDefaultEngineComponent () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  In C++, static classes are initialized in alphabetical order.  This means that some classes such
		  as Camera, ConfigWriter, and Console may initialize before the DClass initializes their vectors.
		  The Orphan vector for example, will reset its size when DClass initializes (clearing references such as Camera).
		  The DClasses are stored in two vectors, where one copies over the other vector during init.
		  These two functions returns the vector that is not empty since only one vector may be cleared at a time.
		 */
		static std::vector<SOrphanInfo> GetPreloadedOrphans ();
		static std::vector<DClass*> GetPreloadedRootClasses ();

		static void RegisterRootClass (DClass* newRoot);

		/**
		  After creating a new class, the new class has an oportunity to claim any orphan that's waiting for that class to register.
		 */
		static void ClaimOrphans (DClass* newParentClass);

		/**
		  Iterates through all registered classes until the class with matching name is found.
		 */
		static DClass* FindClassByName (const DString& className);

		/**
		  Searches through all registered children of baseClass to find the class with the matching name.
		 */
		static DClass* FindChildByName (const DString& className, DClass* baseClass);

		void AddChild (DClass* child);

		friend class ClassIterator;
	};
}

#endif
#endif