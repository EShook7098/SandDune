/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformWindows.h
  Includes any Windows-specific libraries and definitions.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef PLATFORMWINDOWS_H
#define PLATFORMWINDOWS_H

#include "Configuration.h"

#if INCLUDE_CORE

#ifdef PLATFORM_WINDOWS

//HACK:  Need to redirect INT so the typedef INT in minwindef.h doesn't override SD INT when working with external namespaces
#define INT MS_INT

//Do the same for other datatypes
#define FLOAT MS_FLOAT

//Platform-specific includes
#include <windows.h>
#include <shellapi.h>

#undef FLOAT
#undef INT

#if _WIN32
#include "..\Windows\resource.h"
#endif


/*
=====================
  Defines
=====================
*/

// Get 32/64 bit defines
#if _WIN64
#define PLATFORM_64BIT
#elif _WIN32
#define PLATFORM_32BIT
#else
#error "Please define your platform."
#endif

#include "Core.h"
#include "Rectangle.h"
#include "INT.h"

namespace SD
{
	class DString;


	/*
	=====================
	  Methods
	=====================
	*/

	/**
	  Setup any platform-specific initialization.
	 */
	void InitializePlatform ();

	void PlatformOpenWindow (const DString& windowMsg, const DString& windowTitle);

	/**
	  Informs the operating system to freeze this application for the specified amount.
	  This function does not return until enough time elapsed.
	 */
	void OS_Sleep (FLOAT milliseconds);

	/**
	  Copies the copyContent to the OS clipboard.  Returns true if successful.
	 */
	bool OS_CopyToClipboard (const DString& copyContent);

	/**
	  Retrieves the text buffer from the OS clipboard, and returns as result as string.
	  Returns an empty string if clipboard is empty, or if application was unable to paste from clipboard.
	 */
	DString OS_PasteFromClipboard ();

	/**
	  Clamps the operating system's mouse pointer to be within a rectangle.
	  If everything is 0, then no clamps are applied.
	 */
	void OS_ClampMousePointer (const Rectangle<INT>& region);
}

#endif //PLATFORM_WINDOWS
#endif //INCLUDE_CORE
#endif //Include guard