/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Object.h
  Base class that gets registered to the engine.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef OBJECT_H
#define OBJECT_H

#include "Core.h"
#include "Engine.h"
#include "DClass.h"

#if INCLUDE_CORE

// Macro to quickly check if a pointer is a valid pointer, and if the object is not going to be removed soon
#define VALID_OBJECT(objectPtr) (objectPtr != nullptr && !objectPtr->GetPendingDelete())

// Same as VALID_OBJECT but this also casts the object to a particular type.
#define VALID_OBJECT_CAST(objectPtr, castTo) (dynamic_cast<castTo>(objectPtr) != nullptr && !objectPtr->GetPendingDelete())

/* Macro to quickly remove invalid pointers for the CleanUpInvalidPointers function. */
#define CLEANUP_INVALID_POINTER(memberVariable) \
	if (memberVariable != nullptr && memberVariable->GetPendingDelete()) \
	{ \
		memberVariable = nullptr; \
	} \
	else {}

/* Same as CLEANUP_INVALID_POINTER but specific to clearing empty or invalid elements of a vector. */
#define CLEANUP_INVALID_POINTER_ARRAY(memberArray) \
for (unsigned int i = 0; i < memberArray.size(); i++) \
{ \
	if (memberArray.at(i) == nullptr || memberArray.at(i)->GetPendingDelete()) \
	{ \
		memberArray.erase(memberArray.begin() + i); \
		i--; \
	} \
}

namespace SD
{
	class DPropertyExtension;

	class Object
	{
		DECLARE_CLASS(Object)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
#ifdef DEBUG_MODE
		/* Quick string reference used to quickly identify a particular object instance.  This is not used
		anywhere, and is expected to be set manually whenever a developer wants to track particular object(s). */
		DString DebugName;
#endif

		/* First PropertyExtension found in this object's property linked list. */
		DPropertyExtension* FirstProperty;

	protected:
		/* True if this object is going to be deleted next time the Engine's Garbage Collector runs. */
		bool bPendingDelete;

		unsigned int ObjectHash;

		/* Unique identifier for this object.  The identifier is only unique compared to other objects of matching class. */
		INT ID;

	private:
		/* Reference to the next object with the same hash table ID. */
		Object* NextObject;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		Object ();
		Object (const Object& copyConstructor);
		virtual ~Object ();


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Displays a string representation for this object.  This is primarily used for logging.
		 */
		DString ToString () const;

		/**
		  Initializes all properties for this object when this object is created.
		 */
		virtual void InitProps ();

		/**
		  Notifies the object (and subclasses) to initialize itself.
		 */
		virtual void BeginObject ();

		/**
		  Removes this object from the game.
		*/
		virtual void Destroy ();

		/**
		  Returns true if this particular object is the default object its DClass references.
		 */
		bool IsDefaultObject () const;


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		unsigned int GetObjectHash () const;
		DString GetUniqueName () const;

		/**
		  Returns this object's generic name.
		 */
		DString GetName () const;

		/**
		  Returns this object's descriptive name.  By default, it'll return the unique name, but subclasses
		  may provide a better name.  For example, a texture object may return the texture name it references.
		 */
		virtual DString GetFriendlyName () const;

		/**
		  Returns the object instance registered to this object's DClass.
		 */
		const Object* GetDefaultObject () const;

		bool GetPendingDelete () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Calculates the object's hash number for the Engine::ObjectHashTable.  The result should yield in powers of 2.
		 */
		virtual unsigned int CalculateHashID () const;

		/**
		  Invoked whenever the engine finished initializing.
		  Only the DClass::DefaultObject of this class gets invoked.  
		 */
		virtual void PostEngineInitialize () const;

		/**
		  Event broadcasted from the Engine when the Engine is about to collect garbage.
		  Use this function if your class contains member variables to memory addresses to check
		  if the object the address is pointing to is pending deletion or not.
		  If they are pending deletion, then set the memory address to null to avoid crashes.
		  See the CLEANUP_INVALID_POINTER macro.
		 */
		virtual void CleanUpInvalidPointers ();

		/**
		  Invoked via IMPLEMENT_CLASS macro's CreateObject function, this simply
		  initializes the object's properties, and registers this object to the engine's hash table.
		 */
		virtual void InitializeObject ();

	private:
		/**
		  Finds unique name and adds object to Engine's object linked list.
		 */
		void RegisterObject ();

		friend class ObjectIterator;
		friend class Engine;
	};
}

#endif
#endif