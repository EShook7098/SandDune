/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Utils.h
  Contains multiple commonly used static utility functions.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef UTILS_H
#define UTILS_H

#include "BaseUtils.h"

#if INCLUDE_CORE

#define PI 3.14159265358
#define PI_FLOAT 3.141593f

namespace SD
{
	class Utils : public BaseUtils
	{
		DECLARE_CLASS(Utils)


		/*
		=====================
		  Templates
		=====================
		*/

	public:
#pragma region "Templates"
		template <class Type> static const Type Max (const Type a, const Type b)
		{
			return (a > b) ? a : b;
		}

		template <class Type> static const Type Min (const Type a, const Type b)
		{
			return (a < b) ? a : b;
		}

		template <class Type> static const Type Clamp (const Type input, const Type min, const Type max)
		{
			return Max(Min(input, max), min);
		}

		/**
		  Linear interpolation  between minBounds and maxBounds based on the given ratio.
		  Ratio ranges from [0,1] to return a value between min and max bounds.  0 being equal to min bounds, and 1 being equal to max bounds.
		 */
		template <class Type> static const Type Lerp (const Type ratio, const Type minBounds, const Type maxBounds)
		{
			return ((ratio * (maxBounds - minBounds)) + minBounds);
		}

		template <class Type> static const Type Round (const Type a)
		{
			return floor(a + 0.5f);
		}

		/**
		  Returns true if the given is in powers of 2.
		 */
		template <class Type> static bool IsPowerOf2 (const Type num)
		{
			return (num > 0 && !(num & (num - 1)));
		}

		/**
		  Logs out the contents of the specified list within a single line.
		 */
		template <class Type> static DString ListToString (const std::vector<Type>& list)
		{
			DString result = TXT("(");
			for (unsigned int i = 0; i < list.size(); i++)
			{
				result += list.at(i).ToString() + TXT(", ");
			}

			//Replace the trailing ", " with ")"
			result = result.SubString(0, result.Length() - 3);
			return result + TXT(")");
		}
#pragma endregion Templates
	};
}

#endif
#endif