/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CoreMacros.h
  Contains essential macros to be used for many objects.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef COREMACROS_H
#define COREMACROS_H

#define STRINGIFY_IMPL(x) #x
#define STRINGIFY(x) STRINGIFY_IMPL(x)

/**
  Macro used to cause an assertion if the condition fails, and displays helpful debugging information.
 */
#ifdef DEBUG_MODE
#define CHECK(condition) \
	CHECK_INFO(##condition##, "Assertion failed (" STRINGIFY(condition) ")  Line:" STRINGIFY(__LINE__) ", File:" STRINGIFY(__FILE__));

/**
  Same as CHECK but provides an additional parameter to display a custom message.
 */
#define CHECK_INFO(condition, message) \
	if (!(##condition##)) \
	{ \
		SD::Engine::GetEngine()->FatalError(##message##); \
		assert(false); \
	} \
	else {}
#else
//Log it instead of forcing a crash.
#define CHECK(condition) \
	CHECK_INFO(##condition##, "Check condition failed (" STRINGIFY(condition) ")  Line:" STRINGIFY(__LINE__) ", File:" STRINGIFY(__FILE__));

#define CHECK_INFO(condition, message) \
	if (!(##condition##)) \
	{ \
		SD::Engine::GetEngine()->RecordLog(##message##, LOG_CRITICAL); \
	} \
	else {}
#endif

/**
  Short-cut to declare public methods for a class (this goes in header files).
 */
#define DECLARE_CLASS(className) \
	private: \
		/* Counter of how many objects were instantiated of this class. */ \
		static SD::INT className##InstanceCounter; \
		\
		/** \
		  Instantiates and registers a DClass object that'll represent this class. \
		 */ \
		static const SD::DClass* InitializeStaticClass (); \
		/** \
		  Increments a counter to ensure that this object contains a unique name compared to other objects. \
		 */ \
		virtual void CalculateUniqueName (); \
		\
	public: \
		static const SD::DClass* className##Class; \
		\
		/** \
		  Obtains the static reference of the class \
		 */ \
		virtual inline const SD::DClass* StaticClass () const \
		{ \
			return className##Class; \
		} \
		\
		/** \
		  Static method to obtain the DClass \
		 */ \
		static inline const SD::DClass* SStaticClass () \
		{ \
			return className##Class; \
		} \
		\
		/** \
		  Static version:  Returns the object instance registered to this class's DClass. \
		  Abstract classes will nullptr. \
		 */ \
		static inline const SD::Object* SGetDefaultObject () \
		{ \
			return className##Class->GetDefaultObject(); \
		} \
		\
		/** \
		  Instantiates an object of this class. \
		 */ \
		static className##* CreateObject (); \
		\
		/** \
		  Instantiates a class that matches this object's class.  This is useful if you have a pointer \
		  to an object, but you do not know its class at design time. \
		 */ \
		virtual inline className##* CreateObjectOfMatchingClass () const \
		{ \
			return className##::CreateObject(); \
		}


/**
  Inserts general properties and functions for an Engine Component.
  All subclasses of EngineComponents are expected to be singleton objects.
 */
#define DECLARE_ENGINE_COMPONENT(className) \
	private: \
		/* Static class reference of this Engine Component. */ \
		static SD::DClass* className##Class; \
		\
		/* Unique name that distinguishes this component from other components without having a reference to an instance. */ \
		static SD::DString className##Name; \
	\
		/* Reference to the instantiated singleton Engine Component that's registered to Engine. */ \
		static className##* RegisteredComponent; \
	\
		static SD::DClass* InitializeStaticClass (); \
	\
	public: \
		static SD::DString GetComponentName (); \
		\
		/** \
		  Returns the singleton registered Engine Component. \
		 */ \
		static className##* Get (); \
		virtual className##* GetRegisteredComponent () const; \
		\
		virtual SD::DString GetName () const; \
		\
		/** \
		  Obtains the static reference of the class \
		 */ \
		virtual inline const SD::DClass* StaticClass () const \
		{ \
			return className##Class; \
		} \
		\
		/** \
		  Static method to obtain the DClass \
		 */ \
		static inline const SD::DClass* SStaticClass () \
		{ \
			return className##Class; \
		} \
		\
		virtual inline className##* CreateObjectOfMatchingClass () const \
		{ \
			return new className##(); \
		}



/**
  Short-cut to associate a DClass with this class (this goes in the cpp file)
  These macros expect that your class contains a function called InitProps for the DClass::DefaultObject pointer
  These macros also expect that your parent class to contain a function called InitializeObject.
  Use IMPLEMENT_CLASS to define a standard class with a parent.
  Use IMPLEMENT_ABSTRACT_CLASS to define an abstract class with a parent.
  Use IMPLEMENT_CLASS_NO_PARENT to define a standard class without a parent.
  Use IMPLEMENT_ABSTRACT_CLASS_NO_PARENT to define an abstract class without a parent.
 */
#define IMPLEMENT_CLASS(className, parentClass) \
	/* Ensure this class is actually a subclass of parentClass. */ \
	static_assert(std::is_base_of<##parentClass, className##>::value, #className " does not derive from " #parentClass ".  Please advise the parentClass parameter for the IMPLEMENT_CLASS macro."); \
	const SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	typedef parentClass Super; \
	\
	IMPLEMENT_ANY_CLASS(className) \
	\
	IMPLEMENT_STATIC_CLASS(##className##, parentClass##)


#define IMPLEMENT_ABSTRACT_CLASS(className, parentClass) \
	/* Ensure this class is actually a subclass of parentClass. */ \
	static_assert(std::is_base_of<##parentClass, className##>::value, #className " does not derive from " #parentClass ".  Please advise the parentClass parameter for the IMPLEMENT_ABSTRACT_CLASS macro."); \
	const SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	typedef parentClass Super; \
	\
	IMPLEMENT_ANY_CLASS(className) \
	\
	IMPLEMENT_STATIC_ABSTRACT_CLASS(##className##, parentClass##)
	

//short-cut to create an instance of DClass without a parent class
#define IMPLEMENT_CLASS_NO_PARENT(className) \
	const SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	IMPLEMENT_ANY_CLASS(className) \
	\
	IMPLEMENT_STATIC_CLASS(##className##, TXT(""))


#define IMPLEMENT_ABSTRACT_CLASS_NO_PARENT(className) \
	const SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	IMPLEMENT_ANY_CLASS(className) \
	\
	IMPLEMENT_STATIC_ABSTRACT_CLASS(##className##, TXT(""))

/**
  Macro used in all IMPLEMENT_CLASS macros.
  Do NOT call this directly.  Use IMPLEMENT_CLASS... macros instead.
 */
#define IMPLEMENT_ANY_CLASS(className) \
	/* Initialize to 0 since 0 is reserved for the DefaultObject. */ \
	SD::INT className##::##className##InstanceCounter = 1; \
	\
	void className##::CalculateUniqueName () \
	{ \
		ID = className##InstanceCounter; \
		/* Protect against number overflow */ \
		if (className##InstanceCounter == MAXINT) \
		{ \
			className##InstanceCounter = 1; \
		} \
		else \
		{ \
			className##InstanceCounter++; \
		} \
	}


/**
  These macros implement the InitializeStaticClass function.  There are two versions.
  The standard one registers an instanced DClass to the class tree, and instantiates a default object.
  The other one is for abstract classes where it registers a DClass to the tree, but does not create a default object.
  Do NOT call this directly.  Use IMPLEMENT_CLASS... macros instead.
 */
#define IMPLEMENT_STATIC_CLASS(className, parentClass) \
	const SD::DClass* className##::InitializeStaticClass () \
	{ \
		SD::DClass* result = DClass::LoadClass(TXT(#className), TXT(#parentClass)); \
		\
		if (result != nullptr) \
		{ \
			/* Instantiate a defaultObject.  It's intentional to exclude this object from the Engine's hash table so that the */ \
			/* object iterators will not include these objects since we don't want these objects to tick, render, or directly impact the game. */ \
			/* This will generate a compiler error if an abstract class uses this macro.  Use IMPLEMENT_ABSTRACT_CLASS instead. */ \
			className##* defaultObject = new className##(); \
			\
			if (defaultObject != nullptr) \
			{ \
				defaultObject->InitProps(); \
				defaultObject->ID = 0; \
				result->SetCDO(defaultObject); \
			} \
		} \
		\
		return result; \
	} \
	\
	className##* className##::CreateObject () \
	{ \
		className##* newObject = new className##(); \
		if (newObject == nullptr) \
		{ \
			LOG(LOG_FATAL, TXT("Failed to allocate memory to instantiate a " #className "!")); \
			return nullptr; \
		} \
		\
		newObject->InitializeObject(); \
		\
		return newObject; \
	}


/**
  Since abstract classes cannot be instantiated, we must have a separate macro to skip the defaultObject implementation.
  Do NOT call this directly.  Use IMPLEMENT_CLASS... macros instead.
 */
#define IMPLEMENT_STATIC_ABSTRACT_CLASS(className, parentClass) \
	const SD::DClass* className##::InitializeStaticClass () \
	{ \
		/* Insert a compile error if the nonabstract class is using a macro for abstract classes. */ \
		/* If you receive a compile error stating "is_abstract undeclared identifier", then you'll need to place the macro within std blocks. */ \
		static_assert(is_abstract<##className##>::value, #className " is not an abstract class but IMPLMENT_ABSTRACT_CLASS macro was used."); \
		return SD::DClass::LoadClass(TXT(#className), TXT(#parentClass)); \
	} \
	\
	className##* className##::CreateObject () \
	{ \
		LOG(LOG_WARNING, TXT("Cannot instantiate an abstract class:  " #className)); \
		/* Cannot instantiate an abstract class. */ \
		return nullptr; \
	}


/**
  This macro helps implement an Engine component.  This must be separate from IMPLEMENT_CLASS macros
  since EngineComponents cannot contain DClass properties.  DClasses depends on the engine.
 */
#define IMPLEMENT_ENGINE_COMPONENT(className) \
	typedef SD::EngineComponent Super; \
	IMPLEMENT_ENGINE_COMPONENT_PARENT(className, EngineComponent)

#define IMPLEMENT_ENGINE_COMPONENT_PARENT(className, parentClass) \
	SD::DString className##::##className##Name = TXT(#className); \
	SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	/* Create an instance early so the Engine will know which engine component instance to register. */ \
	className##* className##::RegisteredComponent = new className##(); \
	\
	SD::DClass* className##::InitializeStaticClass () \
	{ \
		SD::DClass* result = SD::DClass::LoadClass(TXT(#className), TXT(#parentClass)); \
		if (result != nullptr) \
		{ \
			className##* cdo = new className##(); \
			result->SetCDO(cdo); \
		} \
	\
		return result; \
	} \
	\
	SD::DString className##::GetComponentName () \
	{ \
		return className##Name; \
	} \
	\
	className##* className##::Get () \
	{ \
		return RegisteredComponent; \
	} \
	\
	className##* className##::GetRegisteredComponent () const \
	{ \
		return RegisteredComponent; \
	} \
	\
	SD::DString className##::GetName () const \
	{ \
		return className##Name; \
	}

#endif