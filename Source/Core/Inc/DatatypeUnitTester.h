/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DatatypeUnitTester.h
  This class is responsible for conducting unit tests for the custom
  data types such as Vectors and INTs.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef DATATYPEUNITTESTER_H
#define DATATYPEUNITTESTER_H

#include "Object.h"

#if INCLUDE_CORE

#ifdef DEBUG_MODE

namespace SD
{
	class DatatypeUnitTester : public UnitTester
	{
		DECLARE_CLASS(DatatypeUnitTester)


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual bool RunTests (EUnitTestFlags testFlags) const override;
		virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual bool TestDString (EUnitTestFlags testFlags) const;
		virtual bool TestBOOL (EUnitTestFlags testFlags) const;
		virtual bool TestINT (EUnitTestFlags testFlags) const;
		virtual bool TestFLOAT (EUnitTestFlags testFlags) const;
		virtual bool TestRange (EUnitTestFlags testFlags) const;
		virtual bool TestVector2 (EUnitTestFlags testFlags) const;
		virtual bool TestVector3 (EUnitTestFlags testFlags) const;
		virtual bool TestRectangle (EUnitTestFlags testFlags) const;
		virtual bool TestSDFunction (EUnitTestFlags testFlags) const;
		virtual bool TestMatrix (EUnitTestFlags testFlags) const;
	};
}

#endif //debug_mode

#endif //INCLUDE_CORE
#endif //include guard