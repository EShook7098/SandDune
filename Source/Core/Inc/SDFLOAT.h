/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FLOAT.h
  A class that represents a 32 bit floating point number.  Contains numeric
  utility functions, and supports typecasting to various standard C++ numbers.

  Without the "SD" prefix in the file name, the compiler will generate errors
  such as error C2065: 'FLT_RADIX' : undeclared identifier
  The file name "float.h" is already taken.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef SDFLOAT_H
#define SDFLOAT_H

#include "Configuration.h"
#if INCLUDE_CORE

#include "DProperty.h"

#ifndef FLOAT_TOLERANCE
/* Define how close floats can be to each other to be considered equal. */
#define FLOAT_TOLERANCE 0.000001f
#endif

#ifndef MAXFLOAT
#define MAXFLOAT std::numeric_limits<float>::max()
#endif

#ifndef MINFLOAT
#define MINFLOAT std::numeric_limits<float>::lowest()
#endif

namespace SD
{
	class INT;

	class FLOAT : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		float Value = 0;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		FLOAT ();
		FLOAT (const float newValue);
		FLOAT (const FLOAT& copyFloat);
		explicit FLOAT (const INT& newValue);
		explicit FLOAT (const DString& text);

		
		/*
		=====================
		  Operators
		=====================
		*/

	public:
		virtual void operator= (const FLOAT& copyFloat);
		virtual void operator= (float otherFloat);

		virtual FLOAT operator++ ();
		virtual FLOAT operator++ (int);
		virtual FLOAT operator- () const;
		virtual FLOAT operator-- ();
		virtual FLOAT operator-- (int);

#if 0
		virtual bool operator== (const FLOAT& otherFloat) const;
		virtual bool operator!= (const FLOAT& otherFloat) const;
		virtual bool operator< (const FLOAT& otherFloat) const;
		virtual bool operator<= (const FLOAT& otherFloat) const;
		virtual bool operator> (const FLOAT& otherFloat) const;
		virtual bool operator>= (const FLOAT& otherFloat) const;

		virtual FLOAT operator+ (const FLOAT& otherFloat) const;
		virtual void operator+= (const FLOAT& otherFloat);
		virtual FLOAT operator++ (); //++iter
		virtual FLOAT operator++ (int); //iter++

		virtual FLOAT operator- (const FLOAT& otherFloat) const;
		virtual FLOAT operator- () const; //-Value
		virtual void operator-= (const FLOAT& otherFloat);
		virtual FLOAT operator-- ();
		virtual FLOAT operator-- (int);

		virtual FLOAT operator* (const FLOAT& otherFloat) const;
		virtual void operator*= (const FLOAT& otherFloat);

		virtual FLOAT operator/ (const FLOAT& otherFloat) const;
		virtual void operator/= (const FLOAT& otherFloat);
		virtual void operator%= (const FLOAT& otherFloat);
		virtual FLOAT operator% (const FLOAT& otherFloat) const;
#endif


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		DString ToString () const override;

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		static FLOAT MakeFloat (int value);
		static FLOAT MakeFloat (unsigned int value);
#ifdef PLATFORM_64BIT
		static FLOAT MakeFloat (const size_t& value);
#endif

		/**
		  Returns an INT from this FLOAT.  This conversion truncates all decimal values.
		 */
		virtual INT ToINT () const;

		/**
		  Returns the absolute value of this FLOAT.
		 */
		static FLOAT Abs (const FLOAT value);
		virtual void Abs ();

		static FLOAT Round (const FLOAT value);
		virtual void Round ();
		virtual void Roundf (INT numDecimals);

		static FLOAT RoundUp (const FLOAT value);
		virtual void RoundUp ();

		static FLOAT RoundDown (const FLOAT value);
		virtual void RoundDown ();

		/**
		  Converts the value to an float.  This function may cause precision errors if double is large enough.
		 */
		virtual float GetFloat (double target);

		/**
		  Returns a double from this value
		 */
		virtual double ToDouble () const;
	};

#pragma region "External Operators"
	bool operator== (const FLOAT& left, const FLOAT& right);
	bool operator== (const FLOAT& left, const float& right);
	bool operator== (const float& left, const FLOAT& right);

	bool operator!= (const FLOAT& left, const FLOAT& right);
	bool operator!= (const FLOAT& left, const float& right);
	bool operator!= (const float& left, const FLOAT& right);

	bool operator< (const FLOAT& left, const FLOAT& right);
	bool operator< (const FLOAT& left, const float& right);
	bool operator< (const float& left, const FLOAT& right);
	bool operator<= (const FLOAT& left, const FLOAT& right);
	bool operator<= (const FLOAT& left, const float& right);
	bool operator<= (const float& left, const FLOAT& right);
	bool operator> (const FLOAT& left, const FLOAT& right);
	bool operator> (const FLOAT& left, const float& right);
	bool operator> (const float& left, const FLOAT& right);
	bool operator>= (const FLOAT& left, const FLOAT& right);
	bool operator>= (const FLOAT& left, const float& right);
	bool operator>= (const float& left, const FLOAT& right);

	FLOAT operator+ (const FLOAT& left, const FLOAT& right);
	FLOAT operator+ (const FLOAT& left, const float& right);
	FLOAT operator+ (const float& left, const FLOAT& right);
	FLOAT& operator+= (FLOAT& left, const FLOAT& right);
	FLOAT& operator+= (FLOAT& left, const float& right);
	float& operator+= (float& left, const FLOAT& right);

	FLOAT operator- (const FLOAT& left, const FLOAT& right);
	FLOAT operator- (const FLOAT& left, const float& right);
	FLOAT operator- (const float& left, const FLOAT& right);
	FLOAT& operator-= (FLOAT& left, const FLOAT& right);
	FLOAT& operator-= (FLOAT& left, const float& right);
	float& operator-= (float& left, const FLOAT& right);

	FLOAT operator* (const FLOAT& left, const FLOAT& right);
	FLOAT operator* (const FLOAT& left, const float& right);
	FLOAT operator* (const float& left, const FLOAT& right);
	FLOAT& operator*= (FLOAT& left, const FLOAT& right);
	FLOAT& operator*= (FLOAT& left, const float& right);
	float& operator*= (float& left, const FLOAT& right);

	FLOAT operator/ (const FLOAT& left, const FLOAT& right);
	FLOAT operator/ (const FLOAT& left, const float& right);
	FLOAT operator/ (const float& left, const FLOAT& right);
	FLOAT& operator/= (FLOAT& left, const FLOAT& right);
	FLOAT& operator/= (FLOAT& left, const float& right);
	float& operator/= (float& left, const FLOAT& right);

	FLOAT operator% (const FLOAT& left, const FLOAT& right);
	FLOAT operator% (const FLOAT& left, const float& right);
	FLOAT operator% (const float& left, const FLOAT& right);
	FLOAT& operator%= (FLOAT& left, const FLOAT& right);
	FLOAT& operator%= (FLOAT& left, const float& right);
	float& operator%= (float& left, const FLOAT& right);

#if 0
	bool operator== (float a, const FLOAT& b);
	bool operator!= (float a, const FLOAT& b);
	bool operator< (float a, const FLOAT& b);
	bool operator<= (float a, const FLOAT& b);
	bool operator> (float a, const FLOAT& b);
	bool operator>= (float a, const FLOAT& b);

	FLOAT operator+ (float a, const FLOAT& b);
	FLOAT operator- (float a, const FLOAT& b);
	FLOAT operator* (float a, const FLOAT& b);
	FLOAT operator/ (float a, const FLOAT& b);
	FLOAT operator% (float a, const FLOAT& b);
#endif
#pragma endregion
}

#endif
#endif