/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TickComponent.h
  TickComponent contains a delegate that is continuously called from the Engine every frame.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TICKCOMPONENT_H
#define TICKCOMPONENT_H

#include "EntityComponent.h"

#if INCLUDE_CORE

namespace SD
{
	class TickComponent : public EntityComponent
	{
		DECLARE_CLASS(TickComponent)


		/*
		=====================
		  Properties
		=====================
		*/
		
	protected:
		/* Function to callback on every frame update. */
		SDFunction<void, FLOAT /*deltaSec*/> OnTick;

		/* If true, then the Tick function is active and is being called every frame. */
		bool bTicking;

		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;

	protected:
		unsigned int CalculateHashID () const override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Assigns the OnTick delegate to the given parameter.
		 */
		virtual void SetTickHandler (SDFunction<void, FLOAT> newHandler);

	public:
		/**
		  This function runs every update call where deltaTime is the time since the last update call.
		 */
		virtual void Tick (FLOAT deltaTime);

		virtual void SetTicking (bool bNewTicking);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual bool IsTicking () const;
	};
}

#endif
#endif