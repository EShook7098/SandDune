/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CoreDatatypesClasses.h
  Includes all Core datatypes.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef COREDATATYPES_H
#define COREDATATYPES_H

#include "Configuration.h"
#if INCLUDE_CORE

#define MODULE_CORE 1

#include "DProperty.h"
#include "DPropertyExtension.h"
#include "INT.h"
#include "SDFLOAT.h"
#include "BOOL.h"
#include "DString.h"
#include "Range.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Rectangle.h"
#include "Matrix.h"
#include "SDFunction.h"

#endif
#endif