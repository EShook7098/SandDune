/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Range.h
  A templated class responsible for tracking min/max numbers.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef RANGE_H
#define RANGE_H

#include "Configuration.h"
#if INCLUDE_CORE

namespace SD
{
	template<class T>
	class Range : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		typename T Min;
		typename T Max;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		Range ()
		{
		}

		Range (const Range& copyConstructor)
		{
			Min = copyConstructor.Min;
			Max = copyConstructor.Max;
		}

		Range (const T& inMin, const T& inMax)
		{
			Min = inMin;
			Max = inMax;
		}

		
		/*
		=====================
		  Operators
		=====================
		*/

	public:
		void operator= (const Range& copyValue)
		{
			Min = copyValue.Min;
			Max = copyValue.Max;
		}

		bool operator== (const Range& otherRange) const
		{
			return (Min == otherRange.Min && Max == otherRange.Max);
		}

		bool operator!= (const Range& otherRange) const
		{
			return !(operator==(otherRange));
		}
		

		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		DString ToString () const override
		{
			return DString::Format(TXT("[%s, %s]"), {Min.ToString().ToCString(), Max.ToString().ToCString()});
		}

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Returns true if the min/max values are equal.
		 */
		virtual bool IsConverged () const
		{
			return (Min == Max);
		}

		/**
		  Returns true if any of the values between min/max resides within the other range.
		 */
		virtual bool Intersects (const Range& otherRange) const
		{
			return (Min < otherRange.Max && Max > otherRange.Min);
		}

		/**
		  Returns the intersection of this range and the other range.
		 */
		virtual Range GetIntersectionFrom (const Range& otherRange) const
		{
			if (!Intersects(otherRange))
			{
				return Range();
			}

			return Range(Utils::Max(Min, otherRange.Min), Utils::Min(Max, otherRange.Max));
		}

		/**
		  Returns true this range entirely fits within the otherRange.
		  (Evaluates:  otherRange.Min <= Min && otherRange.Max >= Max).
		 */
		virtual bool IsWithin (const Range& otherRange) const
		{
			return (otherRange.Min <= Min && otherRange.Max >= Max);
		}

		/**
		  Returns true if this range completely encapsulates the otherRange (Opposite of IsWithin).
		 */
		virtual bool Surrounds (const Range& otherRange) const
		{
			return otherRange.IsWithin(*this);
		}

		/**
		  Returns true if the Max is less than Min
		 */
		virtual bool IsInverted () const
		{
			return (Max < Min);
		}

		/**
		  Swaps Min/Max values.
		 */
		virtual void Invert ()
		{
			const T oldMin = Min;
			Min = Max;
			Max = oldMin;
		}

		/**
		  Ensures the Min is at most the Max.
		 */
		virtual void FixRange ()
		{
			if (IsInverted())
			{
				Invert();
			}
		}

		/**
		  Returns true if this range encapsulates the given value. (Evaluates:  Min <= targetValue <= Max)
		 */
		virtual bool ContainsValue (const T targetValue) const
		{
			return (Min <= targetValue && Max >= targetValue);
		}

		/**
		  Returns the difference between the min and max values.
		 */
		virtual T Difference () const
		{
			return (Max - Min);
		}

		/**
		  Returns the average of Min and Max.
		 */
		virtual T Center () const
		{
			return (Max - Min)/2;
		}

		/**
		  Clamps the value to be within this range's bounds.
		 */
		virtual T GetClampedValue (const T input) const
		{
			return Utils::Clamp<T>(input, Min, Max);
		}
	};
}

#endif
#endif