/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DPropertyExtension.h
  Class that allows external classes to extend functionality to DProperties that may be beyond scope
  of primitive data types.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef DPROPERTYEXTENSION_H
#define DPROPERTYEXTENSION_H

#include "Core.h"

#if INCLUDE_CORE

namespace SD
{
	class DProperty;
	class Object;

	class DPropertyExtension
	{


		/*
		=====================
		  Properties
		=====================
		*/

		/* Next property extension found in the object's property linked list.  The property extension must be bound to the same object. */
		DPropertyExtension* NextProperty = nullptr;

		/* DProperty this object is extending functionality for.  There's a many-to-one relationship from PropertyExtensions to DProperties.
		A DProperty could have multiple extensions such as:  supporting editor functionality and network replication. */
		DProperty* OwningProperty = nullptr;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		DPropertyExtension ();
		DPropertyExtension (const DPropertyExtension& copyProperty);
		virtual ~DPropertyExtension ();


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Binds this property to a specified object that owns this variable.  This shouldn't be essential
		  unless you plan on using the PropertyIterator to find this property.
		 */
		virtual void BindPropertyToObject (Object* propertyOwner);

		/**
		  Binds this object to extend functionality for the specified DProperty.
		 */
		virtual void SetOwningProperty (DProperty* newOwningProperty);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual DPropertyExtension* GetNextProperty () const;
		virtual DProperty* GetOwningProperty () const;
	};
}

#endif
#endif