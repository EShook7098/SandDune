/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BOOL.h
  A class that represents a simple bool containing various utility
  functions and conversions.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef BOOL_H
#define BOOL_H

#include "Configuration.h"
#if INCLUDE_CORE

#include "DProperty.h"

namespace SD
{
	class INT;
	class FLOAT;

	class BOOL : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		bool Value = false;

	protected:
		/* Cached language this boolean was translated for (no need to compute BOOL to DString every time). */
		static int LanguageID; //id equivalent to the ELanguages enum
		static DString ToTrueText;
		static DString ToFalseText;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		BOOL ();
		BOOL (const bool newValue);
		BOOL (const BOOL& copyBool);
		explicit BOOL (const INT& intValue);
		explicit BOOL (const FLOAT& floatValue);
		explicit BOOL (const DString& stringValue);

		
		/*
		=====================
		  Operators
		=====================
		*/

	public:
		virtual void operator= (const BOOL& copyBool);
		virtual void operator= (const bool otherBool);
		virtual operator bool () const;
		virtual bool operator ! () const;
		

		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		//Note:  This is implemented in the localization module to handle language translations.
		DString ToString () const override;

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual FLOAT ToFLOAT () const;
		virtual INT ToINT () const;
	};

#pragma region "External Operators"
	bool operator== (const BOOL& left, const BOOL& right);
	bool operator== (const BOOL& left, const bool& right);
	bool operator== (const bool& left, const BOOL& right);

	bool operator!= (const BOOL& left, const BOOL& right);
	bool operator!= (const BOOL& left, const bool& right);
	bool operator!= (const bool& left, const BOOL& right);
#pragma endregion
}

#endif
#endif