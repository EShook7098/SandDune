/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Engine.h
  The Engine class is a singletone object responsible for managing
  instanced Objects.  Management includes the following roles:
  handling object table, updating objects (Tick), and cleaning up.

  The Engine can be treated as a general application manager that launches
  at startup time, and is responsible when the application updates and cleans up.

  Functionality of the Engine could be extended using an EngineComponent, where
  module-specific engine implementations are handled throughout various events.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef ENGINE_H
#define ENGINE_H

#include "Configuration.h"

#if INCLUDE_CORE

#include "Core.h"
#include "DString.h"

/* Quick macros to format a string and write messages to the log. */
#define LOG(category, msg) SD::Engine::GetEngine()->RecordLog(msg, category)
#define LOG1(category, msg, prop1) SD::Engine::GetEngine()->RecordLog(SD::DString::Format(msg, {prop1.ToString().ToCString()}), category)
#define LOG2(category, msg, prop1, prop2) SD::Engine::GetEngine()->RecordLog(SD::DString::Format(msg, {prop1.ToString().ToCString(), prop2.ToString().ToCString()}), category)
#define LOG3(category, msg, prop1, prop2, prop3) SD::Engine::GetEngine()->RecordLog(SD::DString::Format(msg, {prop1.ToString().ToCString(), prop2.ToString().ToCString(), prop3.ToString().ToCString()}), category)
#define LOG4(category, msg, prop1, prop2, prop3, prop4) SD::Engine::GetEngine()->RecordLog(SD::DString::Format(msg, {prop1.ToString().ToCString(), prop2.ToString().ToCString(), prop3.ToString().ToCString(), prop4.ToString().ToCString()}), category)
#define LOG5(category, msg, prop1, prop2, prop3, prop4, prop5) SD::Engine::GetEngine()->RecordLog(SD::DString::Format(msg, {prop1.ToString().ToCString(), prop2.ToString().ToCString(), prop3.ToString().ToCString(), prop4.ToString().ToCString(), prop5.ToString().ToCString()}), category)
#define LOG6(category, msg, prop1, prop2, prop3, prop4, prop5, prop6) SD::Engine::GetEngine()->RecordLog(SD::DString::Format(msg, {prop1.ToString().ToCString(), prop2.ToString().ToCString(), prop3.ToString().ToCString(), prop4.ToString().ToCString(), prop5.ToString().ToCString(), prop6.ToString().ToCString()}), category)
#define LOG7(category, msg, prop1, prop2, prop3, prop4, prop5, prop6, prop7) SD::Engine::GetEngine()->RecordLog(SD::DString::Format(msg, {prop1.ToString().ToCString(), prop2.ToString().ToCString(), prop3.ToString().ToCString(), prop4.ToString().ToCString(), prop5.ToString().ToCString(), prop6.ToString().ToCString(), prop7.ToString().ToCString()}), category)
#define LOG8(category, msg, prop1, prop2, prop3, prop4, prop5, prop6, prop7, prop8) SD::Engine::GetEngine()->RecordLog(SD::DString::Format(msg, {prop1.ToString().ToCString(), prop2.ToString().ToCString(), prop3.ToString().ToCString(), prop4.ToString().ToCString(), prop5.ToString().ToCString(), prop6.ToString().ToCString(), prop7.ToString().ToCString(), prop8.ToString().ToCString()}), category)

namespace SD
{
	class EngineComponent;
	class DClass;
	class Object;

	class Engine
	{


		/*
		=====================
		  Datatypes
		=====================
		*/

	public:
		/**
		  Collection of properties related to a registered object hash identifier.
		 */
		struct SHashTableMeta
		{
			unsigned int HashNumber;

			/* Brief descriptive name for the group of objects using this hash identifier. */
			DString FriendlyName;

			/* First object in linked list with matching HashNumber is referenced here. */
			Object* LeadingObject;

			SHashTableMeta ()
			{
				LeadingObject = nullptr;
				HashNumber = 0;
				FriendlyName = TXT("Unknown Hash Meta");
			}
		};


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* If true, then the engine is cleaning up resources before it closes the application.
		  Setting this flag directly to true will force the application to close at the end of current tick.*/
		bool bShuttingDown;

	protected:
		/* List of all engine components that's influencing the engine. */
		std::vector<EngineComponent*> EngineComponents;

		/* List of engine components that are registered to the Tick cycle. */
		std::vector<EngineComponent*> TickingEngineComponents;

		/* current instance of the engine that's running the game. */
		static Engine* EngineInstance;

		/* String representation of the user specified parameters passed into the executable. */
		static DString EngineParameters;

		/* Time reference variable from previous frame.  Used to generate deltaTime for tick. */
		clock_t PrevTickTime;

		/* If greater than zero, this is the maximum delta time this engine will report.  Not to be confused with Max Frame Rate!
		Essentially increasing this value will reduce the liklihood of causing drastic update rates (ie:  physics causing character
		moving 30 meters within a tick if the application is returning from a break point).  But raising this value too high will
		actually change the result of the physics if the machines are running below minimum frame rate (ie:  physics causes character
		to travel slower than normal).  This value is in delta time (seconds) instead of frames per second. */
		FLOAT MaxDeltaTime;

		/* If greater than zero, this ensures that the engine doesn't invoke objects' Tick Rate faster than this specified amount.
		Instead, it'll cause the application to sleep until enough time elapsed.  This value is in delta time (seconds) instead of frames per second. */
		FLOAT MinDeltaTime;

		/* Object hash IDs that are used to determine where an object can be found within the ObjectHashTable. */
		unsigned int ObjectHashNumber;
		unsigned int EntityHashNumber;
		unsigned int ComponentHashNumber;
		unsigned int TickComponentHashNumber;

		/* Time how long this engine instance was running for (in seconds). */
		FLOAT ElapsedTime;

		/* Counter how many times the engine ticked. */
		unsigned int FrameCounter;

		/* Determines how often will the engine remove objects that are pending deletion from the object hash table (in seconds). */
		FLOAT GarbageCollectInterval;

		/* List of all objects that may be found through the object iterator (generally includes everything except for default objects).
		   Each entry within the vector represents a group with matching HashIDs.  It's sorted in increasing order.  Max size:  31.*/
		std::vector<SHashTableMeta> ObjectHashTable;

	private:
		/* Time stamp when the garbage collector last ran. */
		FLOAT PreviousGarbageCollectTime;


		/*
		=====================
		  Constructors
		=====================
		*/

	protected:
		Engine ();

	public:
		/**
		  Unless you're the main application loop, you should never call this destructor!
		 */
		virtual ~Engine ();


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Instructs the Engine to initialize itself and its engine components.
		 */
		virtual void InitializeEngine ();

		/**
		  Updates the EngineParameters if the EngineParameters were not set before.
		 */
		static void SetEngineParameters (int numArgs, TCHAR* args[]);

		/**
		  Reserves a part in the ObjectHashTable and returns the hash number for the newly created entry.
		  Friendly name is the brief description what this entry is for.
		  This function will return UINT_INDEX_NONE if the hash table ran out of space (max size:  31).
		 */
		virtual unsigned int RegisterObjectHash (const DString& friendlyName);

		/**
		  Causes the engine to crash out with a dialog window to appear.
		 */
		virtual void FatalError (const DString& errorMsg);

		/**
		  Deletes all objects with PendingDelete to true.
		 */
		virtual void CollectGarbage ();

		virtual void RecordLog (const DString& msg, const DString& type = LOG_DEFAULT);

#ifdef DEBUG_MODE
		virtual void LogObjectHashTable ();
#endif

		/**
		  Invokes Tick upon all objects
		 */
		virtual void Tick ();

		virtual void SetMinDeltaTime (FLOAT newMinDeltaTime);
		virtual void SetMaxDeltaTime (FLOAT newMaxDeltaTime);

		/**
		  Removes a registered engine component with matching DClass.
		 */
		virtual void RemoveEngineComponent (const DClass* engineClass);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		/**
		  Returns the instantiated engine.
		 */
		static Engine* GetEngine ();

		/**
		  Obtains the engine's current run time.
		 */
		virtual FLOAT GetElapsedTime () const;

		virtual unsigned int GetFrameCounter () const;
		virtual FLOAT GetMinDeltaTime () const;
		virtual FLOAT GetMaxDeltaTime () const;

		DString GetEngineParameters () const;

		virtual unsigned int GetObjectHashNumber () const;
		virtual unsigned int GetEntityHashNumber () const;
		virtual unsigned int GetComponentHashNumber () const;
		virtual unsigned int GetTickComponentHashNumber () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Function to register a new engine component (this should be called at startup time).
		 */
		void RegisterEngineComponent (EngineComponent* newComponent);

		/**
		  Adds an object to the linked list
		 */
		virtual void RegisterObject (Object* newObject);

		/**
		  Formats fatal errors to a uniformed template that appears in the crash dialogue window.
		 */
		virtual DString GenerateFatalErrorMsg (const DString& specificError);

		virtual void ShutdownSandDune ();

		friend class Object; //to register objects
		friend class ObjectIterator; //to iterate through linked list
		friend class EngineComponent; //to register components
	};
}

#endif
#endif