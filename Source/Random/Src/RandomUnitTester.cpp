/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RandomUnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "RandomClasses.h"

#if INCLUDE_RANDOM

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(RandomUnitTester, UnitTester)

	bool RandomUnitTester::RunTests (EUnitTestFlags testFlags) const
	{
		if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
		{
			return TestUtilityFunctions(testFlags);
		}

		return true;
	}

	bool RandomUnitTester::TestUtilityFunctions (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Random Utilities"));

		UNITTESTER_LOG(testFlags, TXT("Testing fRand.  fRand should be returning a random number from 0-1.  Running 20 iterations. . ."));
		for (unsigned int i = 0; i < 20; i++)
		{
			FLOAT randResult = RandomUtils::fRand();
			UNITTESTER_LOG1(testFlags, TXT("    fRand returned %s"), randResult);

			if (randResult < 0 || randResult > 1)
			{
				UnitTestError(testFlags, TXT("fRand test failed.  The expected result for fRand should range between 0-1.  Instead it returned %s"), {randResult.ToString()});
				return false;
			}
		}
		UNITTESTER_LOG(testFlags, TXT("fRand test passed!"));

		UNITTESTER_LOG(testFlags, TXT("Testing RandRange for range from 50 - 1000.  Running 20 iterations. . ."));
		for (unsigned int i = 0; i < 20; i++)
		{
			FLOAT randResult = RandomUtils::RandRange(50, 1000);
			UNITTESTER_LOG1(testFlags, TXT("    RandRange from 50 - 1000 returned:  %s"), randResult);

			if (randResult < 50 || randResult > 1000)
			{
				UnitTestError(testFlags, TXT("RandRange test failed.  The expected result for RandRange(50,1000) should be between 50-1000.  Instead it returned %s"), {randResult.ToString()});
				return false;
			}
		}
		UNITTESTER_LOG(testFlags, TXT("RandRange test passed!"));

		UNITTESTER_LOG(testFlags, TXT("Testing Rand for range between 0-9.  Running 20 iterations. . ."));
		for (unsigned int i = 0; i < 20; i++)
		{
			INT randResult = RandomUtils::Rand(10);
			UNITTESTER_LOG1(testFlags, TXT("    Rand from 0-9 returned:  %s"), randResult);
			if (randResult < 0 || randResult > 9)
			{
				UnitTestError(testFlags, TXT("Rand test failed.  The expected result for Rand(10) should be between 0-9.  Instead it returned %s"), {randResult.ToString()});
				return false;
			}
		}
		UNITTESTER_LOG(testFlags, TXT("Rand test passed!"));

		UNITTESTER_LOG(testFlags, TXT("Testing shuffle vector of size 100.  Vector was created with sorted elements."));
		vector<INT> shuffledVector;
		for (INT i = 0; i < 100; i++)
		{
			shuffledVector.push_back(i);
		}
		RandomUtils::ShuffleVector(shuffledVector);
		for (unsigned int i = 0; i < shuffledVector.size(); i++)
		{
			UNITTESTER_LOG2(testFlags, TXT("    shuffledVector[%s] = %s"), INT(i), shuffledVector.at(i));
		}

		UNITTESTER_LOG(testFlags, TXT("Testing random points within a circle with max radius 50.  Running 20 iterations. . ."));
		for (unsigned int i = 0; i < 20; i++)
		{
			Vector2 randPoint = RandomUtils::RandPointWithinCircle(50);
			UNITTESTER_LOG1(testFlags, TXT("    RandPoint within circle returned:  %s"), randPoint);

			if (randPoint.VSize() > 50)
			{
				UnitTestError(testFlags, TXT("RandPointWithinCircle test failed.  The distance from the given point and circle's center should be no greater than the circle's max radius (50).  Instead the distance from center and the given point is %s for point %s"), {randPoint.VSize().ToString(), randPoint.ToString()});
				return false;
			}
		}

		UNITTESTER_LOG(testFlags, TXT("Testing random points within a hollow circle (min radius 100, max radius 1000).  Running 20 iterations. . ."));
		for (unsigned int i = 0; i < 20; i++)
		{
			Vector2 randPoint = RandomUtils::RandPointWithinCircle(100, 1000);
			UNITTESTER_LOG1(testFlags, TXT("    RandPoint within hollow circle returned:  %s"), randPoint);

			if (randPoint.VSize() < 100 || randPoint.VSize() > 1000)
			{
				UnitTestError(testFlags, TXT("RandPointWithinCircle test failed.  The distance from the given point and the circle's center should be between 100-1000.  Instead the distance from center and the given point is %s for point %s"), {randPoint.VSize().ToString(), randPoint.ToString()});
				return false;
			}
		}
		UNITTESTER_LOG(testFlags, TXT("RandPointWithinCircle test passed!"));

		ExecuteSuccessSequence(testFlags, TXT("Random Utilities"));
		return true;
	}
}

#endif
#endif