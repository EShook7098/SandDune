/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ResourcePoolMacros.h
  Defines various macros to make it easier to implement various ResourcePools.
  Using Macros instead of templated classes primarily to utilize Object macros
  such as DECLARE_CLASS() and IMPLEMENT_CLASS().  This will also make it easier
  to implement any additional functions to a particular resource pool should
  there ever be a need to handle unique cases.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef RESOURCEPOOLMACROS_H
#define RESOURCEPOOLMACROS_H

#include "Graphics.h"

#if INCLUDE_GRAPHICS


/**
  Declares various functions and variables for a ResourcePool.  The ResourcePool must be named [ResourceType]Pool.
 */
#define DECLARE_RESOURCE_POOL(ResourceType) \
\
/* Data Types */ \
\
protected: \
	struct S##ResourceType##Mapping \
	{ \
		DString ResourceType##Name; \
		ResourceType##* ResourceType##; \
	}; \
\
\
/* Properties */ \
\
protected: \
	static ResourceType##Pool* ResourceType##PoolInstance; \
\
	std::vector<S##ResourceType##Mapping> ImportedResources; \
\
\
/* Methods */ \
\
public: \
	/** \
	  Adds new resource to the ImportedResources vector. \
	  Returns false if a texture with matching name is already imported. \
	*/ \
	virtual bool Import##ResourceType (##ResourceType##* new##ResourceType##, const DString& ##ResourceType##Name); \
	\
	/** \
	  Returns true if the given name is valid and not clashing with another imported resource. \
	*/ \
	virtual bool IsValidName (DString ResourceType##Name) const; \
\
	virtual ResourceType##* Find##ResourceType (const DString& ResourceType##Name); \
\
	/** \
	  Removes a particular texture from the ImportedTextures vector. \
	  Returns true if found and removed. \
	*/ \
	virtual bool Remove##ResourceType (ResourceType##* target); \
\
	/** \
	  Clears any empty or invalid entries in the resource pool. \
	  This should be called after removing many resources. \
	 */ \
	virtual void FlushInvalidEntries (); \
\
	/** \
	  Lists all imported resources to the logs. \
	 */ \
	virtual void LogImportedResources () const; \
\
\
/* Accessors */ \
public: \
	static ResourceType##Pool* Get##ResourceType##Pool (); 

/**
  Implements the functions and variables declared from DECLARE_RESOURCE_POOL
 */
#define IMPLEMENT_RESOURCE_POOL(ResourceType) \
ResourceType##Pool* ResourceType##Pool::ResourceType##PoolInstance = nullptr; \
\
bool ResourceType##Pool::Import##ResourceType (##ResourceType##* new##ResourceType##, const DString& ResourceType##Name) \
{ \
	if (VALID_OBJECT(Find##ResourceType##(##ResourceType##Name))) \
	{ \
		LOG1(LOG_GRAPHICS, TXT("Unable to import " #ResourceType " named %s since another " #ResourceType " is already imported with that name."), ResourceType##Name); \
		return false; \
	} \
\
	if (##ResourceType##Name.IsEmpty()) \
	{ \
		LOG(LOG_GRAPHICS, TXT("Unable to import " #ResourceType " without specifying a name.")); \
		return false; \
	} \
\
	S##ResourceType##Mapping newResource; \
	newResource.##ResourceType = new##ResourceType##; \
	newResource.##ResourceType##Name = ResourceType##Name; \
\
	ImportedResources.push_back(newResource); \
\
	return true; \
} \
\
bool ResourceType##Pool::IsValidName (DString ResourceType##Name) const \
{ \
	if (ResourceType##Name.IsEmpty()) \
	{ \
		return false; \
	} \
\
	/* Ensure the name is not already taken */ \
	for (unsigned int i = 0; i < ImportedResources.size(); i++) \
	{ \
		if (ImportedResources.at(i).##ResourceType##Name.Compare(##ResourceType##Name, false) == 0) \
		{ \
			return false; \
		} \
	} \
\
	return true; \
} \
\
ResourceType##* ResourceType##Pool::Find##ResourceType (const DString& ResourceType##Name) \
{ \
	for (unsigned int i = 0; i < ImportedResources.size(); i++) \
	{ \
		if (ImportedResources.at(i).##ResourceType##Name == ResourceType##Name) \
		{ \
			if (!VALID_OBJECT(ImportedResources.at(i).##ResourceType##)) \
			{ \
				ImportedResources.erase(ImportedResources.begin() + i); \
				return nullptr; /* Resource is about to be deleted.  The name is no longer mapped to anything */ \
			}  \
\
			return ImportedResources.at(i).##ResourceType##; \
		} \
	} \
\
	return nullptr; \
} \
\
bool ResourceType##Pool::Remove##ResourceType (##ResourceType##* target) \
{ \
	for (unsigned int i = 0; i < ImportedResources.size(); i++) \
	{ \
		if (ImportedResources.at(i).##ResourceType == target) \
		{ \
			if (!VALID_OBJECT(ImportedResources.at(i).##ResourceType##)) \
			{ \
				ImportedResources.erase(ImportedResources.begin() + i); \
				return true; /* Already destroyed */ \
			} \
\
			ImportedResources.at(i).##ResourceType##->Destroy(); \
			ImportedResources.erase(ImportedResources.begin() + i); \
			return true; \
		} \
	} \
\
	return false; \
} \
\
void ResourceType##Pool::FlushInvalidEntries () \
{ \
	for (unsigned int i = 0; i < ImportedResources.size(); i++) \
	{ \
		if (!VALID_OBJECT(ImportedResources.at(i).##ResourceType##)) \
		{ \
			ImportedResources.erase(ImportedResources.begin() + i); \
			i--; \
		} \
	} \
} \
\
void ResourceType##Pool::LogImportedResources () const \
{ \
	LOG(LOG_GRAPHICS, TXT("-== Begin " #ResourceType " listing ==-")); \
	for (unsigned int i = 0; i < ImportedResources.size(); i++) \
	{ \
		if (VALID_OBJECT(ImportedResources.at(i).##ResourceType##)) \
		{ \
			LOG2(LOG_GRAPHICS, TXT("    [%s] Imported " #ResourceType ":  %s"), INT(i), ImportedResources.at(i).ResourceType##Name); \
		} \
		else \
		{ \
			LOG1(LOG_GRAPHICS, TXT("    [%s] <Deleted texture>"), INT(i)); \
		} \
	} \
\
	LOG(LOG_GRAPHICS, TXT("-== End " #ResourceType " listing ==-")); \
} \
\
ResourceType##Pool* ResourceType##Pool::Get##ResourceType##Pool () \
{ \
	return ResourceType##PoolInstance; \
}

#endif
#endif