/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  AbsTransformComponent.h
  A transformation component that determines where an object resides within the screen.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef ABSTRANSFORMCOMPONENT_H
#define ABSTRANSFORMCOMPONENT_H

#include "Graphics.h"
#include "TransformationRenderInterface.h"

#if INCLUDE_GRAPHICS

namespace SD
{
	class AbsTransformComponent : public TransformationComponent, public TransformationRenderInterface
	{
		DECLARE_CLASS(AbsTransformComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Callback to invoke whenever the size or scale properties changed. */
		SDFunction<void> OnSizeChanged;

		/* Callback to invoke whenever this transform's position changed. */
		SDFunction<void, const Vector2&> OnPositionChanged;

	protected:
		/* Transform component this component is relative to.  If null, then this component is relative to the screen's upper left corner. */
		AbsTransformComponent* RelativeTo;

		/* Location offset where the pivot point resides.  Default (0,0) the pivot point is on the upper left corner of render object. */
		Vector2 PivotPoint;

		/* Screen coordinates this entity resides (relative to the upper left corner of application window). */
		Vector2 AbsCoordinates;

		/* Absolute size.  If Scale multipliers are 1.f, this 1 unit cooresponds to 1 pixel. */
		Vector2 BaseSize;

		/* Multipliers for BaseSize in individual axis. */
		Vector2 Scale2D;

		/* Scale Multiplier of BaseSize in all axis. */
		FLOAT Scale;

		Rotator Rotation;

		/* Min/Max positions this component could achieve.  If negative, then the range is unbounded. */
		Range<FLOAT> AbsCoordinatesXClamps;
		Range<FLOAT> AbsCoordinatesYClamps;

		/* Min/Max PivotPoints this compomonent could achieve. If negative, then the range is unbounded. */
		Range<FLOAT> PivotPointXClamps;
		Range<FLOAT> PivotPointYClamps;

		/* Min/Max dimensions this transformation's calculated size may achieve.  If negative, then the range is unbounded. */
		Range<FLOAT> SizeXClamps;
		Range<FLOAT> SizeYClamps;


		/* Cached value of clamped size after multiplying BaseSize * Scale2D * Scale. */
		Vector2 CurrentSize;

	private:
		/* Cached position to measure how much this component moved. */
		Vector2 OldAbsCoordinates;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;

		virtual void ApplyPivotPoint (sf::Transformable* renderObject) override;
		virtual void ApplyDrawPosition (sf::Transformable* renderObject, const Camera* camera) override;
		virtual void ApplyDrawSize (sf::Transformable* renderObject, const Camera* camera) override;
		virtual void ApplyDrawRotation (sf::Transformable* renderObject, const Camera* camera) override;

	protected:
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual FLOAT GetRightBounds () const;
		virtual FLOAT GetBottomBounds () const;
		virtual FLOAT CalcFinalRightBounds () const;
		virtual FLOAT CalcFinalBottomBounds () const;

		virtual void SetRelativeTo (AbsTransformComponent* newRelativeTo);
		virtual void SetPivotPoint (const Vector2& newPivotPoint);
		virtual void SetAbsCoordinates (const Vector2& newAbsCoordinates);
		virtual void SetBaseSize (const Vector2& newBaseSize);
		virtual void SetScale2D (const Vector2& newScale2D);
		virtual void SetScale (FLOAT newScale);
		virtual void SetRotation (Rotator newRotation);

		virtual void SetAbsCoordinatesXClamps (const Range<FLOAT>& newAbsCoordinatesXClamps);
		virtual void SetAbsCoordinatesYClamps (const Range<FLOAT>& newAbsCoordinatesYClamps);
		virtual void SetPivotPointXClamps (const Range<FLOAT>& newPivotPointXClamps);
		virtual void SetPivotPointYClamps (const Range<FLOAT>& newPivotPointYClamps);
		virtual void SetSizeXClamps (const Range<FLOAT>& newSizeXClamps);
		virtual void SetSizeYClamps (const Range<FLOAT>& newSizeYClamps);

		/**
		  Returns the resulting coordinates while considering the transform this component is relative to.
		 */
		virtual Vector2 CalcFinalCoordinates () const;
		virtual FLOAT CalcFinalCoordinatesX () const;
		virtual FLOAT CalcFinalCoordinatesY () const;

		/**
		  Returns a region that's within bounds.
		 */
		virtual Rectangle<FLOAT> GetRectangleRegion () const;

		/**
		  Returns true if the given absolute coordinates are within this components's borders.
		 */
		virtual bool WithinBounds (FLOAT x, FLOAT y) const;

		/**
		  Sets the abs coordinates to snap left of owning gui component.
		 */
		virtual void SnapLeft ();

		/**
		  Sets the abs coordinates to snap center horizontally of owning gui component.
		 */
		virtual void SnapCenterHorizontally ();

		/**
		  Sets the abs coordinates to snap right of owning gui component.
		 */
		virtual void SnapRight ();

		/**
		  Sets the abs coordinates to snap top of owning gui component.
		 */
		virtual void SnapTop ();

		/**
		  Sets the abs coordinates to snap center vertically of owning gui component.
		 */
		virtual void SnapCenterVertically ();

		/**
		  Sets the abs coordinates to snap bottom of owning gui component.
		 */
		virtual void SnapBottom ();


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual AbsTransformComponent* GetRelativeTo () const;
		virtual Vector2 GetPivotPoint () const;
		virtual Vector2 GetAbsCoordinates () const;
		virtual FLOAT GetAbsCoordinatesX () const;
		virtual FLOAT GetAbsCoordinatesY () const;
		virtual Vector2 GetBaseSize () const;
		virtual FLOAT GetBaseSizeX () const;
		virtual FLOAT GetBaseSizeY () const;
		virtual Vector2 GetScale2D () const;
		virtual FLOAT GetScale2DX () const;
		virtual FLOAT GetScale2DY () const;
		virtual FLOAT GetScale () const;
		virtual Rotator GetRotation () const;

		/**
		  Returns the clamped resulting size after multiplying the base size with scale multipliers.
		 */
		virtual Vector2 GetCurrentSize () const;
		virtual FLOAT GetCurrentSizeX () const;
		virtual FLOAT GetCurrentSizeY () const;

		virtual Range<FLOAT> GetAbsCoordinatesXClamps () const;
		virtual Range<FLOAT> GetAbsCoordinatesYClamps () const;
		virtual Range<FLOAT> GetPivotPointXClamps () const;
		virtual Range<FLOAT> GetPivotPointYClamps () const;
		virtual Range<FLOAT> GetSizeXClamps () const;
		virtual Range<FLOAT> GetSizeYClamps () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void ClampAbsCoordinates ();
		virtual void ClampPivotPoint ();

		/**
		  Calculates the resulting clamped size after scaling base size.
		 */
		virtual void CalculateCurrentSize ();
	};
}

#endif
#endif