/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsUnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(GraphicsUnitTester, UnitTester)

	bool GraphicsUnitTester::RunTests (EUnitTestFlags testFlags) const
	{
		bool bResult = true;

		if ((testFlags & UTF_Manual) > 0 && (testFlags & UTF_Asynchronous) > 0 && (testFlags & UTF_NeverFails) > 0)
		{
			bResult = TestSolidColor(testFlags) && TestSprites(testFlags);
		}

		if (bResult && (testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_Synchronous) > 0 && (testFlags & UTF_CanDetectErrors) > 0)
		{
			bResult = TestAbsTransform(testFlags);
		}
		
		return bResult;
	}

	bool GraphicsUnitTester::TestSolidColor (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Solid Color Component"));

		Entity* colorTester = Entity::CreateObject();

		AbsTransformComponent* transformation = AbsTransformComponent::CreateObject();
		transformation->SetBaseSize(Vector2(256.f, 256.f));
		if (!colorTester->AddComponent(transformation))
		{
			UNITTESTER_LOG(testFlags, TXT("Solid Color test failed.  Unable to attach AbsTransformComponent to an Entity."));
			transformation->Destroy();
			colorTester->Destroy();
			return false;
		}

		SolidColorRenderComponent* colorComponent = SolidColorRenderComponent::CreateObject();
		colorComponent->SolidColor = sf::Color::Cyan;
		colorComponent->TransformationInterface = transformation;
		if (!colorTester->AddComponent(colorComponent))
		{
			UNITTESTER_LOG(testFlags, TXT("Solid Color test failed.  Unable to attach SolidColorRenderComponent to an Entity."));
			colorComponent->Destroy();
			colorTester->Destroy();
			return false;
		}

		LifeSpanComponent* lifeSpan = LifeSpanComponent::CreateObject();
		lifeSpan->LifeSpan = 3.f;
		if (!colorTester->AddComponent(lifeSpan))
		{
			UNITTESTER_LOG(testFlags, TXT("Solid Color test failed.  Unable to attach a life span component to test entity.  Test cancelled to avoid everlasting entities."));
			colorTester->Destroy();
			lifeSpan->Destroy();
			return false;
		}

		UNITTESTER_LOG(testFlags, TXT("Cyan entity created, and it will automatically perish in 3 seconds."));

		return true;
	}

	bool GraphicsUnitTester::TestSprites (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Sprite"));
		SpriteTester* tester = SpriteTester::CreateObject();
		tester->TestFlags = testFlags;
		UNITTESTER_LOG(testFlags, TXT("SpriteTester created.  Entity will automatically perish after animation."));

		return true;
	}

	bool GraphicsUnitTester::TestAbsTransform (EUnitTestFlags testFlags) const
	{
		BeginTestSequence(testFlags, TXT("Absolute Transformation Component"));
		
		Entity* transformOwner = Entity::CreateObject();
		AbsTransformComponent* baseTransform = AbsTransformComponent::CreateObject();
		if (!transformOwner->AddComponent(baseTransform))
		{
			UnitTestError(testFlags, TXT("Absolute transformation component test failed.  Failed to attach absolute transformation component to an entity."));
			transformOwner->Destroy();
			return false;
		}

		baseTransform->SetAbsCoordinates(Vector2(64, 64));
		baseTransform->SetBaseSize(Vector2(32, 32));
		baseTransform->SetScale(2.f);
		baseTransform->SetScale2D(Vector2(2.f, 4.f));
		UNITTESTER_LOG5(testFlags, TXT("Base transformation component attributes:   Coordinates=%s    BaseSize=%s    Scale=%s    Scale2D=%s    ResultingSize=%s"), baseTransform->GetAbsCoordinates(), baseTransform->GetBaseSize(), baseTransform->GetScale(), baseTransform->GetScale2D(), baseTransform->GetCurrentSize());
		if (baseTransform->GetCurrentSizeX() != 128.f)
		{
			UnitTestError(testFlags, TXT("Absolute transformation component test failed.  The resulting size of base 32, scale 2, and scale2D of 2 should have resulted in 128.  Instead, the transform's scale is %s"), {baseTransform->GetCurrentSizeX().ToString()});
			transformOwner->Destroy();
			return false;
		}

		if (baseTransform->GetCurrentSizeY() != 256.f)
		{
			UnitTestError(testFlags, TXT("Absolute transformation component test failed.  The resulting size of base 32, scale 2, and scale2D of 4 should have resulted in 256.  Instead, the transform's scale is %s"), {baseTransform->GetCurrentSizeY().ToString()});
			transformOwner->Destroy();
			return false;
		}

		AbsTransformComponent* childTransform = AbsTransformComponent::CreateObject();
		if (!transformOwner->AddComponent(childTransform))
		{
			UnitTestError(testFlags, TXT("Absolute transformation component test failed.  Failed to attach multiple transform components to entity."));
			transformOwner->Destroy();
			return false;
		}

		childTransform->SetAbsCoordinates(Vector2(100, 200));
		childTransform->SetRelativeTo(baseTransform);
		UNITTESTER_LOG2(testFlags, TXT("Child transformation component that is relative to base transformation:   Coordinates=%s    ResultingCoordinates=%s"), childTransform->GetAbsCoordinates(), childTransform->CalcFinalCoordinates());
		if (childTransform->CalcFinalCoordinates() != Vector2(164, 264))
		{
			UnitTestError(testFlags, TXT("Absolute transformation component test failed.  The child transform with coordinates %s that is relative to another transform with coordinates %s should resulted in (164, 264).  Instead, the resulting coordinates is %s"), {childTransform->GetAbsCoordinates().ToString(), baseTransform->GetAbsCoordinates().ToString(), childTransform->CalcFinalCoordinates().ToString()});
			transformOwner->Destroy();
			return false;
		}

		ExecuteSuccessSequence(testFlags, TXT("Absolute Transformation Component"));
		transformOwner->Destroy();

		return true;
	}
}

#endif
#endif