/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolidColorRenderComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(SolidColorRenderComponent, RenderComponent)

	void SolidColorRenderComponent::InitProps ()
	{
		Super::InitProps();

		SolidColor = sf::Color(0, 0, 0, 255);
	}

	void SolidColorRenderComponent::Render (const Camera* targetCamera, const Window* targetWindow)
	{
		sf::RectangleShape renderedShape;
		renderedShape.setSize(sf::Vector2f(1.f, 1.f));
		if (TransformationInterface != nullptr)
		{
			TransformationInterface->ApplyPivotPoint(&renderedShape);
			TransformationInterface->ApplyDrawPosition(&renderedShape, targetCamera);
			TransformationInterface->ApplyDrawSize(&renderedShape, targetCamera);
			TransformationInterface->ApplyDrawRotation(&renderedShape, targetCamera);
		}

		renderedShape.setFillColor(SolidColor);
		targetWindow->Resource->draw(renderedShape);
	}
}

#endif