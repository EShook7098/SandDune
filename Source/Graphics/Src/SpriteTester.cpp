/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsUnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(SpriteTester, Entity)

	void SpriteTester::InitProps ()
	{
		Super::InitProps();

		SwellMultiplier = 1.f;

		LivingTime = 0.f;
		TestingStage = -1;
		Tick = nullptr;
		Sprite = nullptr;
		Transformation = nullptr;
	}

	void SpriteTester::BeginObject ()
	{
		Super::BeginObject();

		if (!VALID_OBJECT(Tick))
		{
			Tick = TickComponent::CreateObject();
			if (!AddComponent(Tick))
			{
				Tick->Destroy();
				Tick = nullptr;
				UNITTESTER_LOG(TestFlags, TXT("Sprite Test failed.  Unable to attach a Tick Component to SpriteTester."));
				Destroy();
				return;
			}
		}
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, SpriteTester, HandleTick, void, FLOAT));

		if (!VALID_OBJECT(Transformation))
		{
			Transformation = AbsTransformComponent::CreateObject();
			if (!AddComponent(Transformation))
			{
				UNITTESTER_LOG(TestFlags, TXT("Sprite Test failed.  Unable to attach an Absolute Transformation Component to SpriteTester."));
				Transformation->Destroy();
				Transformation = nullptr;
				Destroy();
				return;
			}
		}
		Transformation->SetAbsCoordinates(Vector2(128.f, 128.f));
		Transformation->SetBaseSize(Vector2(256.f, 256.f));

		if (!VALID_OBJECT(Sprite))
		{
			Sprite = SpriteComponent::CreateObject();
			if (!AddComponent(Sprite))
			{
				UNITTESTER_LOG(TestFlags, TXT("Sprite Test failed.  Unable to attach a Sprite Component to SpriteTester."));
				Sprite->Destroy();
				Sprite = nullptr;
				Destroy();
				return;
			}
		}
		Sprite->TransformationInterface = Transformation;
	}

	FLOAT SpriteTester::CalculateSpriteScale () const
	{
		//Formula:  (cos((x*speed) + pi)/2) + 0.6     cos: x is time, the +pi ensures the wave starts from 0.  Divide 2 keeps the wave height at 1.  +0.6 increases high so the valley of wave is positive (min value 0.1).
		return (cosf(((LivingTime * SwellMultiplier) + PI_FLOAT).Value)/2.f) + 0.6f;
	}

	bool SpriteTester::TickTestStage (FLOAT deltaSec)
	{
		FLOAT stageIncrementInterval = SwellMultiplier / (2 * PI_FLOAT); //Multiplier for the frequency in how often the stage increments.  This should increment every time the wave is at a valley.
		FLOAT oldTime = LivingTime;
		LivingTime += deltaSec;
		bool bNewStage = ((LivingTime * stageIncrementInterval).ToINT()) != ((oldTime * stageIncrementInterval).ToINT());
		
		return bNewStage;
	}

	void SpriteTester::HandleTick (FLOAT deltaSec)
	{
		if (TestingStage < 4)
		{
			Transformation->SetScale(CalculateSpriteScale());
		}

		if (!TickTestStage(deltaSec))
		{
			return;
		}

		TestingStage++;
		switch(TestingStage.Value)
		{
			case(0):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Displaying scaled texture, no sub divisions."));
				Sprite->SetSpriteTexture(TexturePool::GetTexturePool()->FindTexture(TXT("DebuggingTexture")));
				Sprite->SetSubDivision(1, 1, 0, 0);
				break;

			case(1):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Displaying scaled texture, sub divisions (1x2).  Displaying lower half."));
				Sprite->SetSubDivision(1, 2, 0, 1);
				break;

			case(2):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Displaying scaled texture, sub divisions (2x2).  Displaying upper right corner"));
				Sprite->SetSubDivision(2, 2, 1, 0);
				break;

			case(3):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Displaying scaled texture, sub divisions (4x4)"));
				Sprite->SetSubDivision(4, 4, 0, 0);
				break;

			case(4):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Drawing top left half of sprite.  Clearing scale multipliers."));
				Sprite->SetDrawCoordinatesMultipliers(0.5f, 0.5f, 0.f, 0.f);
				Transformation->SetScale(1.f);
				break;

			case(5):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Drawing center of half of sprite."));
				Sprite->SetDrawCoordinatesMultipliers(0.5f, 0.5f, 0.5f, 0.5f);
				break;

			case(6):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Drawing 4x of sprite (should be 2x2 tiles)."));
				Sprite->SetDrawCoordinatesMultipliers(2.f, 2.f, 0.f, 0.f);
				break;

			case(7):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester: Drawing 4x of sprite (should be 2x2 tiles).  Doubled scale."));
				Sprite->SetDrawCoordinatesMultipliers(2.f, 2.f, 0.f, 0.f);
				Transformation->SetScale(2.f);
				break;

			case(8):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Drawing 12.5% of sprite.  Clearing scale multipliers."));
				Sprite->SetDrawCoordinatesMultipliers(0.125f, 0.125f, 0.f, 0.f);
				Transformation->SetScale(1.f);
				break;

			case(9):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Drawing top left 64 pixels of sprite."));
				Sprite->SetDrawCoordinates(0, 0, 64, 64);
				break;

			case(10):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Drawing 256 pixels of sprite starting from coordinate 128."));
				Sprite->SetDrawCoordinates(128, 128, 256, 256);
				break;

			case(11):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Drawing 1024 pixels of sprite.  Halved scaling."));
				Sprite->SetDrawCoordinates(0, 0, 1024, 1024);
				Transformation->SetScale(0.5f);
				break;

			case(12):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Drawing 1024 pixels of sprite.  Starting coordinates at 32 pixels.  Cleared scaling."));
				Sprite->SetDrawCoordinates(32, 32, 1024, 1024);
				Transformation->SetScale(1.f);
				break;

			case(13):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Clearing texture coordinates.  Drawing sprite plainly."));
				Sprite->SetDrawCoordinatesMultipliers(1.f, 1.f, 0.f, 0.f);
				break;

			case(14):
				UNITTESTER_LOG(TestFlags, TXT("SpriteTester:  Finished Sprite Component test.  Destroying self."));
				Destroy();
				break;
		}
	}
}

#endif
#endif