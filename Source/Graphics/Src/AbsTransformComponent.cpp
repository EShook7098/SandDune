/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  AbsTransformComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(AbsTransformComponent, TransformationComponent)

	void AbsTransformComponent::InitProps ()
	{
		Super::InitProps();

		RelativeTo = nullptr;
		AbsCoordinates = Vector2();
		BaseSize = Vector2(1.f, 1.f);
		Scale = 1.f;
		Scale2D = Vector2(1.f, 1.f);
		
		//Unbounded clamps
		AbsCoordinatesXClamps = Range<FLOAT>(-1.f, -1.f);
		AbsCoordinatesYClamps = Range<FLOAT>(-1.f, -1.f);
		PivotPointXClamps = Range<FLOAT>(-1.f, -1.f);
		PivotPointYClamps = Range<FLOAT>(-1.f, -1.f);
		SizeXClamps = Range<FLOAT>(-1.f, -1.f);
		SizeYClamps = Range<FLOAT>(-1.f, -1.f);

		CalculateCurrentSize();	
	}

	void AbsTransformComponent::ApplyPivotPoint (sf::Transformable* renderObject)
	{
		renderObject->setOrigin(Vector2::SDtoSFML(PivotPoint));
	}

	void AbsTransformComponent::ApplyDrawPosition (sf::Transformable* renderObject, const Camera* camera)
	{
		renderObject->setPosition(Vector2::SDtoSFML(CalcFinalCoordinates()));
	}

	void AbsTransformComponent::ApplyDrawSize (sf::Transformable* renderObject, const Camera* camera)
	{
		renderObject->setScale(Vector2::SDtoSFML(CurrentSize));
	}

	void AbsTransformComponent::ApplyDrawRotation (sf::Transformable* renderObject, const Camera* camera)
	{
		renderObject->setRotation(Rotation.GetDegrees().Value);
	}

	void AbsTransformComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(RelativeTo)
	}

	FLOAT AbsTransformComponent::GetRightBounds () const
	{
		return AbsCoordinates.X + CurrentSize.X;
	}

	FLOAT AbsTransformComponent::GetBottomBounds () const
	{
		return AbsCoordinates.Y + CurrentSize.Y;
	}

	FLOAT AbsTransformComponent::CalcFinalRightBounds () const
	{
		return CalcFinalCoordinatesX() + CurrentSize.X;
	}

	FLOAT AbsTransformComponent::CalcFinalBottomBounds () const
	{
		return CalcFinalCoordinatesY() + CurrentSize.Y;
	}

	void AbsTransformComponent::SetRelativeTo (AbsTransformComponent* newRelativeTo)
	{
		//Check for circular references
		for (AbsTransformComponent* transform = newRelativeTo; transform != nullptr; transform = transform->RelativeTo)
		{
			if (transform == this)
			{
				LOG4(LOG_WARNING, TXT("Failed to set %s relative to %s since %s is relative to %s."), GetUniqueName(), newRelativeTo->GetUniqueName(), newRelativeTo->GetUniqueName(), GetUniqueName());
				return;
			}
		}

		RelativeTo = newRelativeTo;
	}

	void AbsTransformComponent::SetPivotPoint (const Vector2& newPivotPoint)
	{
		PivotPoint = newPivotPoint;
	}

	void AbsTransformComponent::SetAbsCoordinates (const Vector2& newAbsCoordinates)
	{
		OldAbsCoordinates = AbsCoordinates;
		AbsCoordinates = newAbsCoordinates;
		ClampAbsCoordinates();
	}

	void AbsTransformComponent::SetBaseSize (const Vector2& newBaseSize)
	{
		BaseSize = newBaseSize;
		CalculateCurrentSize();
	}

	void AbsTransformComponent::SetScale2D (const Vector2& newScale2D)
	{
		Scale2D = newScale2D;
		CalculateCurrentSize();
	}

	void AbsTransformComponent::SetScale (FLOAT newScale)
	{
		Scale = newScale;
		CalculateCurrentSize();
	}

	void AbsTransformComponent::SetRotation (Rotator newRotation)
	{
		Rotation = newRotation;
	}

	void AbsTransformComponent::SetAbsCoordinatesXClamps (const Range<FLOAT>& newAbsCoordinatesXClamps)
	{
		OldAbsCoordinates = AbsCoordinates;
		AbsCoordinatesXClamps = newAbsCoordinatesXClamps;
		ClampAbsCoordinates();
	}

	void AbsTransformComponent::SetAbsCoordinatesYClamps (const Range<FLOAT>& newAbsCoordinatesYClamps)
	{
		OldAbsCoordinates = AbsCoordinates;
		AbsCoordinatesYClamps = newAbsCoordinatesYClamps;
		ClampAbsCoordinates();
	}

	void AbsTransformComponent::SetPivotPointXClamps (const Range<FLOAT>& newPivotPointXClamps)
	{
		PivotPointXClamps = newPivotPointXClamps;
		ClampPivotPoint();
	}

	void AbsTransformComponent::SetPivotPointYClamps (const Range<FLOAT>& newPivotPointYClamps)
	{
		PivotPointYClamps = newPivotPointYClamps;
		ClampPivotPoint();
	}

	void AbsTransformComponent::SetSizeXClamps (const Range<FLOAT>& newSizeXClamps)
	{
		SizeXClamps = newSizeXClamps;
		CalculateCurrentSize();
	}

	void AbsTransformComponent::SetSizeYClamps (const Range<FLOAT>& newSizeYClamps)
	{
		SizeYClamps = newSizeYClamps;
		CalculateCurrentSize();
	}

	Vector2 AbsTransformComponent::CalcFinalCoordinates () const
	{
		AbsTransformComponent* parentTransform = GetRelativeTo();
		Vector2 result = AbsCoordinates;
		while (parentTransform != nullptr)
		{
			result += parentTransform->AbsCoordinates;
			parentTransform = parentTransform->GetRelativeTo();
		}

		return result;
	}

	FLOAT AbsTransformComponent::CalcFinalCoordinatesX () const
	{
		AbsTransformComponent* parentTransform = GetRelativeTo();
		FLOAT result = AbsCoordinates.X;
		while (parentTransform != nullptr)
		{
			result += parentTransform->AbsCoordinates.X;
			parentTransform = parentTransform->GetRelativeTo();
		}

		return result;
	}

	FLOAT AbsTransformComponent::CalcFinalCoordinatesY () const
	{
		AbsTransformComponent* parentTransform = GetRelativeTo();
		FLOAT result = AbsCoordinates.Y;
		while (parentTransform != nullptr)
		{
			result += parentTransform->AbsCoordinates.Y;
			parentTransform = parentTransform->GetRelativeTo();
		}

		return result;
	}

	Rectangle<FLOAT> AbsTransformComponent::GetRectangleRegion () const
	{
		const Vector2 finalCoordinates = CalcFinalCoordinates();
		return Rectangle<FLOAT>(finalCoordinates.X, finalCoordinates.Y, finalCoordinates.X + GetCurrentSizeX(), finalCoordinates.Y + GetCurrentSizeY());
	}

	bool AbsTransformComponent::WithinBounds (FLOAT x, FLOAT y) const
	{
		const Vector2 finalCoordinates = CalcFinalCoordinates();

		return (x >= finalCoordinates.X && y >= finalCoordinates.Y &&
				x <= finalCoordinates.X + GetCurrentSizeX() && y <= finalCoordinates.Y + GetCurrentSizeY());
	}

	void AbsTransformComponent::SnapLeft ()
	{
		if (VALID_OBJECT(RelativeTo))
		{
			SetAbsCoordinates(Vector2(0.f, GetAbsCoordinatesY()));
		}
	}

	void AbsTransformComponent::SnapCenterHorizontally ()
	{
		if (VALID_OBJECT(RelativeTo))
		{
			SetAbsCoordinates(Vector2((RelativeTo->GetCurrentSizeX() * 0.5f) - (GetCurrentSizeX() * 0.5f), GetAbsCoordinatesY()));
		}
	}

	void AbsTransformComponent::SnapRight ()
	{
		if (VALID_OBJECT(RelativeTo))
		{
			SetAbsCoordinates(Vector2(RelativeTo->GetCurrentSizeX() - GetCurrentSizeX(), GetAbsCoordinatesY()));
		}
	}

	void AbsTransformComponent::SnapTop ()
	{
		if (VALID_OBJECT(RelativeTo))
		{
			SetAbsCoordinates(Vector2(GetAbsCoordinatesX(), 0.f));
		}
	}

	void AbsTransformComponent::SnapCenterVertically ()
	{
		if (VALID_OBJECT(RelativeTo))
		{
			SetAbsCoordinates(Vector2(GetAbsCoordinatesX(), (RelativeTo->GetCurrentSizeY() * 0.5f) - (GetCurrentSizeY() * 0.5f)));
		}
	}

	void AbsTransformComponent::SnapBottom ()
	{
		if (VALID_OBJECT(RelativeTo))
		{
			SetAbsCoordinates(Vector2(GetAbsCoordinatesX(), RelativeTo->GetCurrentSizeY() - GetCurrentSizeY()));
		}
	}

	AbsTransformComponent* AbsTransformComponent::GetRelativeTo () const
	{
		return RelativeTo;
	}

	Vector2 AbsTransformComponent::GetPivotPoint () const
	{
		return PivotPoint;
	}

	Vector2 AbsTransformComponent::GetAbsCoordinates () const
	{
		return AbsCoordinates;
	}

	FLOAT AbsTransformComponent::GetAbsCoordinatesX () const
	{
		return AbsCoordinates.X;
	}

	FLOAT AbsTransformComponent::GetAbsCoordinatesY () const
	{
		return AbsCoordinates.Y;
	}

	Vector2 AbsTransformComponent::GetBaseSize () const
	{
		return BaseSize;
	}

	FLOAT AbsTransformComponent::GetBaseSizeX () const
	{
		return BaseSize.X;
	}

	FLOAT AbsTransformComponent::GetBaseSizeY () const
	{
		return BaseSize.Y;
	}

	Vector2 AbsTransformComponent::GetScale2D () const
	{
		return Scale2D;
	}

	FLOAT AbsTransformComponent::GetScale2DX () const
	{
		return Scale2D.X;
	}

	FLOAT AbsTransformComponent::GetScale2DY () const
	{
		return Scale2D.Y;
	}

	FLOAT AbsTransformComponent::GetScale () const
	{
		return Scale;
	}

	Rotator AbsTransformComponent::GetRotation () const
	{
		return Rotation;
	}

	Vector2 AbsTransformComponent::GetCurrentSize () const
	{
		return CurrentSize;
	}

	FLOAT AbsTransformComponent::GetCurrentSizeX () const
	{
		return CurrentSize.X;
	}

	FLOAT AbsTransformComponent::GetCurrentSizeY () const
	{
		return CurrentSize.Y;
	}

	Range<FLOAT> AbsTransformComponent::GetAbsCoordinatesXClamps () const
	{
		return AbsCoordinatesXClamps;
	}

	Range<FLOAT> AbsTransformComponent::GetAbsCoordinatesYClamps () const
	{
		return AbsCoordinatesYClamps;
	}

	Range<FLOAT> AbsTransformComponent::GetPivotPointXClamps () const
	{
		return PivotPointXClamps;
	}

	Range<FLOAT> AbsTransformComponent::GetPivotPointYClamps () const
	{
		return PivotPointYClamps;
	}

	Range<FLOAT> AbsTransformComponent::GetSizeXClamps () const
	{
		return SizeXClamps;
	}

	Range<FLOAT> AbsTransformComponent::GetSizeYClamps () const
	{
		return SizeYClamps;
	}

	void AbsTransformComponent::ClampAbsCoordinates ()
	{
		Range<FLOAT> actualXClamps = AbsCoordinatesXClamps;
		Range<FLOAT> actualYClamps = AbsCoordinatesYClamps;

		if (actualXClamps.Min < 0.f)
		{
			actualXClamps.Min = MINFLOAT;
		}

		if (actualXClamps.Max < 0.f)
		{
			actualXClamps.Max = MAXFLOAT;
		}

		if (actualYClamps.Min < 0.f)
		{
			actualYClamps.Min = MINFLOAT;
		}

		if (actualYClamps.Max < 0.f)
		{
			actualYClamps.Max = MAXFLOAT;
		}

		AbsCoordinates.X = Utils::Clamp(AbsCoordinates.X, actualXClamps.Min, actualXClamps.Max);
		AbsCoordinates.Y = Utils::Clamp(AbsCoordinates.Y, actualYClamps.Min, actualYClamps.Max);

		Vector2 deltaMove = AbsCoordinates - OldAbsCoordinates;
		if (OnPositionChanged.IsBounded() && !deltaMove.IsEmpty())
		{
			OnPositionChanged.Execute(deltaMove);
		}
	}

	void AbsTransformComponent::ClampPivotPoint ()
	{
		Range<FLOAT> actualXClamps = PivotPointXClamps;
		Range<FLOAT> actualYClamps = PivotPointYClamps;

		if (actualXClamps.Min < 0.f)
		{
			actualXClamps.Min = MINFLOAT;
		}

		if (actualXClamps.Max < 0.f)
		{
			actualXClamps.Max = MAXFLOAT;
		}

		if (actualYClamps.Min < 0.f)
		{
			actualYClamps.Min = MINFLOAT;
		}

		if (actualYClamps.Max < 0.f)
		{
			actualYClamps.Max = MAXFLOAT;
		}

		PivotPoint.X = Utils::Clamp(PivotPoint.X, actualXClamps.Min, actualXClamps.Max);
		PivotPoint.Y = Utils::Clamp(PivotPoint.Y, actualYClamps.Min, actualYClamps.Max);
	}

	void AbsTransformComponent::CalculateCurrentSize ()
	{
		Vector2 oldSize = CurrentSize;
		CurrentSize = BaseSize * Scale2D * Scale;

		Range<FLOAT> actualXClamps = SizeXClamps;
		Range<FLOAT> actualYClamps = SizeYClamps;

		if (actualXClamps.Min < 0.f)
		{
			actualXClamps.Min =  MINFLOAT;
		}

		if (actualXClamps.Max < 0.f)
		{
			actualXClamps.Max = MAXFLOAT;
		}

		if (actualYClamps.Min < 0.f)
		{
			actualYClamps.Min = MINFLOAT;
		}

		if (actualYClamps.Max < 0.f)
		{
			actualYClamps.Max = MAXFLOAT;
		}

		CurrentSize.X = Utils::Clamp(CurrentSize.X, actualXClamps.Min, actualXClamps.Max);
		CurrentSize.Y = Utils::Clamp(CurrentSize.Y, actualYClamps.Min, actualYClamps.Max);

		if (OnSizeChanged.IsBounded() && oldSize != CurrentSize)
		{
			OnSizeChanged.Execute();
		}
	}
}

#endif